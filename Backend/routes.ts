import express from 'express'
import { upload } from './main'
import { authController } from './main'
import { userController } from './main'
import { challengeController } from './main'
import { lobbyController } from './main'
import { roomController } from './main'
import { forumController } from './main'
import { isLoggedIn } from './guards'
import { verifyController } from './main'

export const routes = express.Router()

//user routes
routes.get('/user', isLoggedIn, userController.getCurrentUser)
routes.post('/login', authController.post)
routes.post(
    '/user/register',
    upload.single('profilePicture'),
    userController.register,
)
routes.delete('/user/deleteAccount', isLoggedIn, userController.deleteAccount)

//forum routes
routes.get('/forum/posts', isLoggedIn, forumController.getPosts)
routes.post('/forum/posts', isLoggedIn, upload.single('postPhoto'), forumController.postPost)
routes.get('/forum/comments/:id', isLoggedIn, forumController.getComments)
routes.post('/forum/comments', isLoggedIn, forumController.postComment)
routes.get('/forum/posts/like', isLoggedIn, forumController.getLike)
routes.post('/forum/posts/like/:id', isLoggedIn, forumController.postLike)
routes.delete('/forum/posts/like/:id', isLoggedIn, forumController.deleteLike)

//challenge route
routes.get('/categories', challengeController.getCategories)
routes.get('/verifications', challengeController.getVerifications)
routes.get('/like/:id', isLoggedIn, challengeController.getUserLike)
routes.post('/like/:id', isLoggedIn, challengeController.postUserLike)
routes.delete('/like/:id', isLoggedIn, challengeController.deleteUserLike)
routes.get('/likes/:id', challengeController.getTotalLikes)
routes.get('/challenge/:id', challengeController.getChallengeDetails)
routes.post('/challenge', isLoggedIn, challengeController.createChallenge)
routes.post('/event', isLoggedIn, challengeController.createChallengeEvent)
routes.post('/challenges', isLoggedIn, lobbyController.getChallenges)
routes.post('/events', isLoggedIn, lobbyController.getEvents)
routes.get('/event/:id', isLoggedIn, roomController.getEventDetails)
routes.get('/relation/:id', isLoggedIn, roomController.getUserEventRelation)
routes.post('/event/:id', isLoggedIn, roomController.joinEvent)
routes.get('/challenges/ongoing', isLoggedIn, challengeController.getUserOngoingChallengeEvents)
routes.get('/challenges/ongoing/dots/:id', isLoggedIn, challengeController.getUserOngoingChallengeDots)
routes.get('/challenges/ongoing/dots/:id/:date', isLoggedIn, challengeController.getUserOngoingChallengeByDate)
routes.get('/challenges/past', isLoggedIn, challengeController.getUserPastChallengeEvents)
// routes.get('/events/participated', isLoggedIn, personalController.getUserParticipatedEvents)
// routes.get('challenges/saved', isLoggedIn, personalController.getUserSavedChallenge)

routes.get('/messages/:eventId', roomController.getMessages)
routes.post('/message/:eventId', isLoggedIn, roomController.postMessage)

//Edward Challenge route
routes.post(
    '/verify/livePhoto',
    upload.single('livePhoto'),
    verifyController.uploadVerificationRecord,
)
routes.get('/details/:id', roomController.detailInfo)
routes.get('/verify/check/:eventId/:userId', verifyController.uploadRecord)
routes.get('/details/allRecords/:id', roomController.allRecords)
routes.get('/details/startEnd/:id', roomController.startEnd)
//Edward Story route
routes.get('/story/users/:id', roomController.getEventParti)
routes.get('/story/allContent/:id', roomController.allStories)

//Justin attempt
routes.get('/verification/allRecords/:eventId', roomController.getAllVerificationRecords)
routes.get('/verification/reports/:eventId', roomController.getAllVerificationReports)
routes.post('/verification/report/:verificationId', isLoggedIn, roomController.postVerificationRecord)
routes.delete('/verification/report/reject/:reportId', roomController.rejectVerificationReport)
routes.delete('/verification/report/accept/:reportId', roomController.acceptVerificationReport)
routes

setInterval(()=>{
    challengeController.checkChallengeEventTimeout()
},1000)


