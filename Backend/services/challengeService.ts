import { Knex } from 'knex'
import { IPersonalEvent } from '../models'
import { Challenge, Event, CycleDate } from './models'

export class ChallengeService {
    public constructor(private knex: Knex) {}

    public async getCategories() {
        return await this.knex.raw(`SELECT * from categories`)
    }

    public async getVerifications() {
        return await this.knex.raw(`SELECT * from verifications`)
    }

    public async getTotalLikes(challengeId: number) {
        const totalLikesCount = (
            await this.knex.raw(
                `SELECT COUNT(user_id) FROM likes WHERE challenge_id = ?`,
                [challengeId],
            )
        ).rows[0].count
        await this.knex.raw(`UPDATE challenges SET likes = (?) WHERE id = ?`, [
            totalLikesCount,
            challengeId,
        ])
        return totalLikesCount
    }

    public async getUserLike(userId: number, challengeId: number) {
        return await this.knex.raw(
            `SELECT COUNT(user_id) FROM likes WHERE user_id = ? AND challenge_id = ?`,
            [userId, challengeId],
        )
    }

    public async postUserLike(userId: number, challengeId: number) {
        // await this.knex.raw(`INSERT INTO likes (user_id, challenge_id) VALUES (?,?)`,[userId, challengeId])
        await this.knex.raw(
            `INSERT INTO likes (user_id, challenge_id)
                            SELECT ?, ? 
                            WHERE NOT EXISTS (SELECT id FROM likes 
                            WHERE user_id = ? AND challenge_id = ?)`,
            [userId, challengeId, userId, challengeId],
        )
        return true
    }

    public async deleteUserLike(userId: number, challengeId: number) {
        await this.knex.raw(
            `DELETE FROM likes WHERE user_id = ? AND challenge_id = ?`,
            [userId, challengeId],
        )
        return false
    }

    public async postChallenge(userId: number, challenge: Challenge) {
        const challengeId = (
            await this.knex.raw(
                `INSERT INTO challenges (creator_id, title, rule, task, description, persistence, consecutive, times, days, cycles, renew, with_section, public, sections ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id`,
                [
                    userId,
                    challenge.title,
                    challenge.rule,
                    challenge.task,
                    challenge.description,
                    challenge.persistence,
                    challenge.consecutive,
                    challenge.times,
                    challenge.days,
                    challenge.cycles,
                    challenge.renewTime,
                    challenge.withSection,
                    challenge.publicity,
                    challenge.sections,
                ],
            )
        ).rows[0].id
        challenge.categories.map(async (categoriesId) => {
            await this.knex.raw(
                `INSERT INTO challenges_categories (challenge_id, category_id) VALUES (?,?)`,
                [challengeId, categoriesId],
            )
        })
        challenge.verifyMethods.map(async (verificationId) => {
            await this.knex.raw(
                `INSERT INTO challenges_verifications (challenge_id, verification_id) VALUES (?,?)`,
                [challengeId, verificationId],
            )
        })
        return challengeId
    }

    public async getChallengeDetails(challengeId: number) {
        let categories: number[] = []
        let verifications: number[] = []
        let challengeDetails: Challenge = (
            await this.knex.raw(
                `SELECT challenges.*, users.nickname as creator_name FROM challenges 
            INNER JOIN users ON challenges.creator_id = users.id WHERE challenges.id = ?`,
                [challengeId],
            )
        ).rows[0]
        let selectedCategories = (
            await this.knex.raw(
                `SELECT * from challenges_categories WHERE challenge_id = ?`,
                [challengeId],
            )
        ).rows
        selectedCategories.map((category: any) => {
            categories.push(category.category_id)
        })
        let verificationsDetails = (
            await this.knex.raw(
                `SELECT * from challenges_verifications WHERE challenge_id = ?`,
                [challengeId],
            )
        ).rows
        verificationsDetails.map((verification: any) => {
            verifications.push(verification.verification_id)
        })
        let pastParticipants = (
            await this.knex.raw(
                `SELECT COUNT(challengers.user_id) FROM challengers 
        INNER JOIN challenge_events ON challengers.event_id = challenge_events.id
        WHERE challenge_events.challenge_id = ?`,
                [challengeId.toString()],
            )
        ).rows[0].count
        // let creatorName = (await this.knex.raw(`SELECT nickname FROM users where id = ${challengeDetails.creator_id}`)).rows
        challengeDetails.categories = categories
        challengeDetails.verifyMethods = verifications
        challengeDetails.past_participants = pastParticipants
        return challengeDetails
    }

    public async postChallengeEvent(userId: number, event: Event) {
        const eventId = (
            await this.knex.raw(
                `INSERT INTO challenge_events (challenge_id, host_id, accommodation, official, official_reward, comment, start_date, end_date, created_at) VALUES (?,?,?,?,?,?,?,?,NOW()) RETURNING id`,
                [
                    event.challengeId,
                    userId,
                    event.accommodation,
                    event.official,
                    event.officialReward,
                    event.comment,
                    event.start.toString().split('T')[0],
                    event.end.toString().split('T')[0],
                ],
            )
        ).rows[0].id
        await this.knex.raw(
            `INSERT INTO challengers (user_id, event_id, role) VALUES (?,?,?)`,
            [userId, eventId, event.official ? 'official_host' : 'host'],
        )
        return eventId
    }

    //jus
    public async getUserChallengeEvents(ongoing: boolean, userId: number) {
        let eventList = (
            await this.knex.raw(
                `SELECT challenges.*, challenges.id as challenge_id, challenge_events.*, challenge_events.id as event_id, challengers.id as challenger_id FROM challenges 
                                                INNER JOIN challenge_events ON challenges.id = challenge_events.challenge_id
                                                INNER JOIN challengers ON challenge_events.id = challengers.event_id
                                                WHERE challenge_events.status ${
                                                    ongoing
                                                        ? "!= 'ended'"
                                                        : "= 'ended'"
                                                } AND challengers.user_id = ?`,
                [userId],
            )
        ).rows
        interface additionalDetailsObject {
            challenge_id: number
            categories: number[]
            verifications: number[]
            verificationSubmittedTimes: number
            currentCycleVerificationTimes: number
            cycleDates: CycleDate[]
        }
        // get categories and verifications of challenge of event from table challenges_categories and challenges_verifications respectively
        let arrayOfAdditionalDetails = [] as additionalDetailsObject[]
        await Promise.all(
            eventList.map(async (item: any) => {
                try{
                const selectedCategories = (
                    await this.knex.raw(
                        `SELECT category_id FROM challenges_categories WHERE challenge_id = ${item.challenge_id}`,
                    )
                ).rows
                let categories = [] as number[]
                selectedCategories.map((category: any) => {
                    categories.push(category.category_id)
                })
                const selectedVerifications = (
                    await this.knex.raw(
                        `SELECT verification_id FROM challenges_verifications WHERE challenge_id = ${item.challenge_id}`,
                    )
                ).rows
                let verifications = [] as number[]
                selectedVerifications.map((verification: any) => {
                    verifications.push(verification.verification_id)
                })
                const verificationSubmittedTimes = (
                    await this.knex.raw(
                        `SELECT COUNT(verification_records.id) FROM verification_records 
                                                                        INNER JOIN challengers ON verification_records.challenger_id = challengers.id
                                                                        WHERE verification_records.challenger_id = ? AND event_id = ?`,
                        [item.challenger_id, item.event_id],
                    )
                ).rows[0].count
                const currentTime = new Date()
                let startDate = new Date(item.start_date)
                startDate.setTime(startDate.getTime() - 8 * 60 * 60 * 1000) // convert hk time to javascript international
                const nthDaySinceStart = Math.ceil(
                    (currentTime.getTime() - startDate.getTime()) /
                        (1000 * 3600 * 24),
                )
                const nthCycles = Math.ceil(nthDaySinceStart / item.days)
                let cycleDates = [] as CycleDate[]
                for (let i = 0; i < nthCycles; i++) {
                    let cycleStartDate = new Date()
                    cycleStartDate.setTime(
                        startDate.getTime() +
                            item.days * (i + 1) * 24 * 60 * 60 * 1000,
                    )
                    const startTimestampSQL = cycleStartDate
                        .toISOString()
                        .split('T')[0]
                    let cycleEndDate = new Date()
                    cycleEndDate.setTime(
                        startDate.getTime() +
                            item.days * (i + 1 + 1) * 24 * 60 * 60 * 1000,
                    )
                    const endTimestampSQL = cycleEndDate
                        .toISOString()
                        .split('T')[0]
                    cycleDates.push({
                        current_cycle: i + 1,
                        start_date: startTimestampSQL,
                        end_date: endTimestampSQL,
                    })
                }
                const currentCycleVerificationTimes = (
                    await this.knex.raw(
                        `SELECT COUNT(challenger_id) FROM verification_records
                                                    WHERE challenger_id = ? AND submitted_at > ?`,
                        [
                            item.challenger_id,
                            cycleDates[cycleDates.length - 1].start_date,
                        ],
                    )
                ).rows[0].count

                arrayOfAdditionalDetails.push({
                    challenge_id: item.challenge_id,
                    categories: categories,
                    verifications: verifications,
                    verificationSubmittedTimes: verificationSubmittedTimes,
                    currentCycleVerificationTimes:
                        currentCycleVerificationTimes,
                    cycleDates: cycleDates,
                })
            }catch(e){console.log(e)}
            }),
        )
        for (let i = 0; i < eventList.length; i++) {
            arrayOfAdditionalDetails.map((item) => {
                if (item.challenge_id === eventList[i].challenge_id) {
                    eventList[i] = {
                        ...eventList[i],
                        categories: item.categories,
                        verifications: item.verifications,
                        verificationSubmittedTimes:
                            item.verificationSubmittedTimes,
                        currentCycleVerificationTimes:
                            item.currentCycleVerificationTimes,
                        cycleDates: item.cycleDates,
                    }
                }
            })
        }
        return eventList
    }

    async personalOngoingChallengeByAllDate(userId: number) {
        const result = await this.knex.raw(
            `SELECT challenge_events.id,  challenge_events.status, challenge_events.start_date, challenge_events.end_date,
            challengers.user_id, challengers.role,
            challenges.title, challenges.rule, challenges.persistence, challenges.consecutive,challenges.sections, challenges.times, challenges.days, challenges.cycles,
            challenges_categories.category_id, 
            categories.color, categories.selected_color
            FROM challenge_events
            INNER JOIN challengers ON challenge_events.id = challengers.event_id 
            INNER JOIN challenges ON challenge_events.challenge_id = challenges.id
            INNER JOIN challenges_categories ON challenge_events.challenge_id = challenges_categories.challenge_id
            INNER JOIN categories ON challenges_categories.category_id = categories.id
            WHERE user_id = ? AND challenge_events.status != 'ended'`,
            [userId],
        )
        return result.rows
    }

    async calculateEventDots(challenges: IPersonalEvent[]) {
        function addDays(date: string, days: number) {
            const result = new Date(date)
            result.setDate(result.getDate() + days)
            return result
        }

        let calculatedData: { [key: string]: any } = {}

        for (const challenge of challenges) {
            for (let i = 0; i <= challenge.cycles * challenge.days; i++) {
                const addedDate = addDays(challenge.start_date, i)
                    .toISOString()
                    .slice(0, 10)
                const addedFields = {
                    key: challenge.id.toString(),
                    color: challenge.color,
                    selectedDotColor: challenge.selected_color,
                }
                if (!calculatedData[addedDate]) {
                    const dotData = { dots: [addedFields] }
                    calculatedData[addedDate] = dotData
                } else {
                    calculatedData[addedDate]['dots'].push(addedFields)
                }
            }
        }
        return calculatedData
    }

    async calculateRequiredDateEvents(
        challenges: IPersonalEvent[],
        requestDate: string,
    ) {
        function addDays(date: string, days: number) {
            const result = new Date(date)
            result.setDate(result.getDate() + days)
            return result
        }

        let calculatedData: { [key: string]: any[] } = {}

        for (const challenge of challenges) {
            for (let i = 0; i <= challenge.cycles * challenge.days; i++) {
                const addedDate = addDays(challenge.start_date, i)
                    .toISOString()
                    .slice(0, 10)
                const addedFields = {
                    time: '00:00',
                    title: challenge.title,
                    description: challenge.rule,
                }
                if (!calculatedData[addedDate]) {
                    calculatedData[addedDate] = [addedFields]
                } else {
                    calculatedData[addedDate].push(addedFields)
                }
            }
        }
        return calculatedData[requestDate]
    }

    async checkChallengeEventTimeout() {
        let today = new Date()
        today.setTime(today.getTime() + 8 * 60 * 60 * 1000)
        const todaySQL = new Date().toISOString().split('T')[0]
        await this.knex.raw(
            `UPDATE challenge_events SET status = 'pending' WHERE start_date >= ?`,
            [todaySQL],
        )
        await this.knex.raw(
            `UPDATE challenge_events SET status = 'doing' WHERE start_date < ?`,
            [todaySQL],
        )
        await this.knex.raw(
            `UPDATE challenge_events SET status = 'ended' WHERE end_date <= ?`,
            [todaySQL],
        )

    }
}
