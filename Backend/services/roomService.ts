import { Knex } from 'knex'
import {
    CycleDate,
    ChallengeTimeDetails,
    ChallengerInfo,
    VerificationRecord,
    ChallengerVerificationRecords,
    CycleVerificationRecords,
    // MultipleCyclesStoriesFlatList
} from './models'

export class RoomService {
    public constructor(private knex: Knex) {}

    public async getEventDetails(userId: number, eventId: number) {
        let event = (
            await this.knex
                .raw(`SELECT challenges.*, challenges.id as challenge_id, challenge_events.*, challenge_events.id as event_id FROM challenges 
        INNER JOIN challenge_events ON challenges.id = challenge_events.challenge_id 
        WHERE challenge_events.id = ${eventId}`)
        ).rows[0]

        let categories = [] as number[]
        let verifications = [] as number[]
        const selectedCategories = (
            await this.knex.raw(
                `SELECT category_id FROM challenges_categories WHERE challenge_id = ${event.challenge_id}`,
            )
        ).rows
        selectedCategories.map((category: any) => {
            categories.push(category.category_id)
        })
        const selectedVerifications = (
            await this.knex.raw(
                `SELECT verification_id FROM challenges_verifications WHERE challenge_id = ${event.challenge_id}`,
            )
        ).rows
        selectedVerifications.map((verification: any) => {
            verifications.push(verification.verification_id)
        })

        const challengeCreator = (
            await this.knex.raw(
                `SELECT nickname FROM users WHERE id = ${event.creator_id}`,
            )
        ).rows[0].nickname
        const eventHost = (
            await this.knex.raw(
                `SELECT nickname FROM users WHERE id = ${event.host_id}`,
            )
        ).rows[0].nickname

        const participants = parseInt(
            (
                await this.knex.raw(
                    `SELECT COUNT(user_id) FROM challengers WHERE event_id = ?`,
                    [event.id],
                )
            ).rows[0].count,
        )

        ///
        let cycleDates = [] as CycleDate[]
        let currentCycleVerificationTimes = 0
        let verificationSubmittedTimes = 0
        if (event.status !== 'pending') {
            const currentTime = new Date()
            let startDate = new Date(event.start_date)
            startDate.setTime(startDate.getTime() - 8 * 60 * 60 * 1000) // convert hk time to javascript international
            const nthDaySinceStart = Math.ceil(
                (currentTime.getTime() - startDate.getTime()) /
                    (1000 * 3600 * 24),
            )
            const nthCycles = Math.ceil(nthDaySinceStart / event.days)
            for (let i = 0; i < nthCycles; i++) {
                let cycleStartDate = new Date()
                cycleStartDate.setTime(
                    startDate.getTime() +
                        event.days * (i + 1) * 24 * 60 * 60 * 1000,
                )
                const startTimestampSQL = cycleStartDate
                    .toISOString()
                    .split('T')[0]
                let cycleEndDate = new Date()
                cycleEndDate.setTime(
                    startDate.getTime() +
                        event.days * (i + 1 + 1) * 24 * 60 * 60 * 1000,
                )
                const endTimestampSQL = cycleEndDate.toISOString().split('T')[0]
                cycleDates.push({
                    current_cycle: i + 1,
                    start_date: startTimestampSQL,
                    end_date: endTimestampSQL,
                })
            }

            currentCycleVerificationTimes = (
                await this.knex.raw(
                    `SELECT COUNT(verification_records.id) FROM verification_records
                                                                    INNER JOIN challengers ON verification_records.challenger_id = challengers.id
                                                                    INNER JOIN users ON challengers.user_id = users.id
                                                                    WHERE users.id = ? AND challengers.event_id = ? AND submitted_at > ?`,
                    [
                        userId,
                        eventId,
                        cycleDates[cycleDates.length - 1].start_date,
                    ],
                )
            ).rows[0].count

            /////
            verificationSubmittedTimes = (
                await this.knex.raw(
                    `SELECT COUNT(verification_records.id) FROM verification_records
        INNER JOIN challengers ON verification_records.challenger_id = challengers.id
        INNER JOIN users ON challengers.user_id = users.id
        WHERE users.id = ? AND challengers.event_id = ?`,
                    [userId, eventId],
                )
            ).rows[0].count

        }
        event = {
            ...event,
            categories: categories,
            verifications: verifications,
            creator_name: challengeCreator,
            host_name: eventHost,
            participants: participants,
            currentCycleVerificationTimes: currentCycleVerificationTimes,
            verificationSubmittedTimes: verificationSubmittedTimes,
            cycleDates: cycleDates,
        }

        return event
    }

    public async getUserEventRelation(userId: number, eventId: number) {
        let relation = (
            await this.knex.raw(
                `SELECT * FROM challengers WHERE user_id = ${userId} AND event_id = ${eventId}`,
            )
        ).rows[0]
        return relation
    }

    public async postEventChallenger(userId: number, eventId: number) {
        const challengerId = await this.knex.raw(
            `INSERT INTO challengers (event_id, user_id, role, status, created_at) VALUES (?,?,?,?,NOW()) RETURNING id`,
            [eventId, userId, 'participant', 'waiting'],
        )
        return challengerId
    }

    public async getEventParticipants(eventId: number) {
        const usersStoryInfo = await this.knex.raw(
            `select users.id, users.nickname,users.pro_pic from challengers  JOIN users on challengers.user_id = users.id where challengers.event_id = (?) `,
            [eventId],
        )
        return usersStoryInfo
    }

    public async getAllStories(eventId: number, today: string, tmr: string) {
        const allStoriesOfTheDate = await this.knex.raw(
            `Select users.id,users.nickname,users.pro_pic, verification_records.status, verification_records.content, challengers.role, verification_records.updated_at  from challengers  JOIN verification_records on challengers.id = verification_records.challenger_id RIGHT JOIN users on challengers.user_id = users.id  where challengers.event_id = (?) and   verification_records.updated_at > (?) and verification_records.updated_at < (?)`,
            [eventId, `${today}`, `${tmr}`],
        )
        return allStoriesOfTheDate
    }

    public async detailInfoToday(eventId: number, today: string, tmr: string) {
        const totalUsers = await this.knex.raw(
            `select COUNT(user_id) from challengers where event_id = (?)`,
            [eventId],
        )
        const failed = await this.knex.raw(
            `select COUNT(verification_records.challenger_id) from challengers  JOIN verification_records on challengers.id = verification_records.challenger_id where challengers.event_id = (?) and verification_records.status = 'failed'  and verification_records.updated_at > (?) and verification_records.updated_at < (?)`,
            [eventId, `${today}`, `${tmr}`],
        )

        const verified = await this.knex.raw(
            `select COUNT(verification_records.challenger_id) from challengers  JOIN verification_records on challengers.id = verification_records.challenger_id where challengers.event_id = (?) and verification_records.status = 'verified'  and verification_records.updated_at > (?) and verification_records.updated_at < (?)`,
            [eventId, `${today}`, `${tmr}`],
        )

        const pendingForVerify = await this.knex.raw(
            `select COUNT(verification_records.challenger_id) from challengers  JOIN verification_records on challengers.id = verification_records.challenger_id where challengers.event_id = (?) and verification_records.status = 'pending_for_verify'  and verification_records.updated_at > (?) and verification_records.updated_at < (?)`,
            [eventId, `${today}`, `${tmr}`],
        )

        const pendingUpload =
            totalUsers.rows[0].count -
            failed.rows[0].count -
            verified.rows[0].count -
            pendingForVerify.rows[0].count

        const detailJson = {
            total: totalUsers.rows[0].count,
            failed: failed.rows[0].count,
            verified: verified.rows[0].count,
            pendingForVerify: pendingForVerify.rows[0].count,
            pendingUpload: pendingUpload,
        }

        return detailJson
    }

    public async getAllRecordList(eventId: number) {
        const allRecordList = await this.knex.raw(
            `Select users.id,users.nickname,users.pro_pic, verification_records.status, verification_records.content, challengers.role, verification_records.updated_at  from challengers  JOIN verification_records on challengers.id = verification_records.challenger_id RIGHT JOIN users on challengers.user_id = users.id  where challengers.event_id = (?) `,
            [eventId],
        )
        return allRecordList
    }

    public async getStartEndDate(eventId: number) {
        const startEnd = await this.knex.raw(
            `select start_date, end_date from challenge_events where id = (?) `,
            [eventId],
        )
        return startEnd
    }
    public async getMessages(eventId: number) {
        const messages = (
            await this.knex.raw(
                `SELECT messages.*, users.id, users.nickname, users.pro_pic FROM messages
                                                INNER JOIN users ON messages.user_id = users.id
                                                WHERE messages.event_id = ?`,
                [eventId],
            )
        ).rows
        return messages
    }

    public async getMessage(messageId: number) {
        const message = (
            await this.knex.raw(
                `SELECT messages.*, users.id, users.nickname, users.pro_pic FROM messages
                                                INNER JOIN users ON messages.user_id = users.id
                                                WHERE messages.id = ?`,
                [messageId],
            )
        ).rows[0]
        return message
    }

    public async postMessage(userId: number, eventId: number, content: String) {
        const messageId = (
            await this.knex.raw(
                `INSERT INTO messages (user_id, event_id, content) VALUES (?,?,?) RETURNING id`,
                [userId, eventId, `${content}`],
            )
        ).rows[0].id
        const messageDetails = await this.getMessage(messageId)
        return { success: true, messageDetails: messageDetails }
    }

    /////////
    public async getAllVerificationRecords(eventId: number) {
        const challengers: ChallengerInfo[] = (
            await this.knex.raw(
                `SELECT challengers.id as challenger_id, users.id as user_id, users.nickname, users.pro_pic FROM challengers
                                                    INNER JOIN challenge_events ON challengers.event_id = challenge_events.id
                                                    INNER JOIN users ON challengers.user_id = users.id
                                                    WHERE challenge_events.id = ?`,
                [eventId],
            )
        ).rows
        const challengeTimeDetails: ChallengeTimeDetails = (
            await this.knex.raw(
                `SELECT challenges.times, challenges.days, challenges.cycles, challenge_events.start_date FROM challenges
                                                    INNER JOIN challenge_events ON challenges.id = challenge_events.challenge_id
                                                    WHERE challenge_events.id = ?`,
                [eventId],
            )
        ).rows[0]
        const verificationDetailsSQL = `SELECT verification_records.*, users.nickname, users.pro_pic FROM verification_records
                                        INNER JOIN challengers ON verification_records.challenger_id = challengers.id
                                        INNER JOIN challenge_events ON challengers.event_id = challenge_events.id
                                        INNER JOIN users ON challengers.user_id = users.id`
        const currentTime = new Date()
        let startDate = new Date(challengeTimeDetails.start_date)
        let endDate = new Date()
        endDate.setDate(
            startDate.getDate() +
                challengeTimeDetails.days *
                    challengeTimeDetails.cycles *
                    24 *
                    60 *
                    60 *
                    1000,
        )
        startDate.setTime(startDate.getTime() - 8 * 60 * 60 * 1000) // convert hk time to javascript international
        const nthDaySinceStart = Math.floor(
            (currentTime.getTime() - startDate.getTime()) / (1000 * 3600 * 24),
        )
        let nthCycles
        if (currentTime.getDate() > endDate.getDate()) {
            nthCycles = challengeTimeDetails.cycles
        } else {
            nthCycles = Math.floor(nthDaySinceStart / challengeTimeDetails.days)
        }
        let allCyclesVerifications: CycleVerificationRecords[] = []
        let cycleDates = [] as CycleDate[]
        for (let i = 0; i <= nthCycles; i++) {
            let oneCycleVerifications = {} as CycleVerificationRecords
            oneCycleVerifications.nth_cycle = i + 1
            oneCycleVerifications.challengersVerificationRecords =
                [] as ChallengerVerificationRecords[]
            let cycleStartDate = new Date()
            cycleStartDate.setTime(
                startDate.getTime() +
                    challengeTimeDetails.days * (i + 1) * 24 * 60 * 60 * 1000,
            )
            const startTimestampSQL = cycleStartDate.toISOString().split('T')[0]
            let cycleEndDate = new Date()
            cycleEndDate.setTime(
                startDate.getTime() +
                    challengeTimeDetails.days *
                        (i + 1 + 1) *
                        24 *
                        60 *
                        60 *
                        1000,
            )
            const endTimestampSQL = cycleEndDate.toISOString().split('T')[0]
            cycleDates.push({
                current_cycle: i,
                start_date: startTimestampSQL,
                end_date: endTimestampSQL,
            })
            const verificationsWithinCycles = (
                await this.knex.raw(
                    `${verificationDetailsSQL} WHERE challenge_events.id = ?
                                                                    AND verification_records.submitted_at BETWEEN 
                                                                    '${startTimestampSQL}'::date AND '${endTimestampSQL}'::date`,
                    [eventId],
                )
            ).rows
            challengers.map((challenger: ChallengerInfo) => {
                // create array of challengers' verification records with challenger details
                let challengerVerificationRecords =
                    {} as ChallengerVerificationRecords
                challengerVerificationRecords.challenger = challenger
                challengerVerificationRecords.verificationRecords = []
                oneCycleVerifications.challengersVerificationRecords.push(
                    challengerVerificationRecords,
                )
            })
            verificationsWithinCycles.map(
                (verificationRecord: VerificationRecord) => {
                    // mapping each verify records within cycle
                    oneCycleVerifications.challengersVerificationRecords.map(
                        (challengerRecords: ChallengerVerificationRecords) => {
                            // mapping to find the corresponding challenger and insert to his/her verificationRecords Array
                            if (
                                challengerRecords.challenger.challenger_id ===
                                verificationRecord.challenger_id
                            ) {
                                challengerRecords.verificationRecords.push({
                                    ...verificationRecord,
                                    nth_cycle: oneCycleVerifications.nth_cycle,
                                })
                            }
                        },
                    )
                },
            )
            oneCycleVerifications.challengersVerificationRecords.sort(
                (a, b) => {
                    if (
                        a.verificationRecords.length <
                        b.verificationRecords.length
                    ) {
                        return 1
                    }
                    if (
                        a.verificationRecords.length >
                        b.verificationRecords.length
                    ) {
                        return -1
                    }
                    return 0
                },
            )
            let storyFlatListIndex = 0
            oneCycleVerifications.challengersVerificationRecords.map(
                (challenger: ChallengerVerificationRecords) => {
                    challenger.verificationRecords.map(
                        (record: VerificationRecord) => {
                            record.storyFlatListIndex = storyFlatListIndex
                            storyFlatListIndex += 1
                        },
                    )
                },
            )
            allCyclesVerifications.push(oneCycleVerifications)
        }

        return {
            success: true,
            pastVerifications: allCyclesVerifications.reverse(),
            cycleDates: cycleDates,
            challengeTimeDetails: challengeTimeDetails,
        }
    }

    public async getAllVerificationReports(eventId: number) {
        const reports = (
            await this.knex.raw(
                `SELECT verification_reports.*, users.id as challenger_id, users.nickname as challenger_name, verification_records.id as verification_id, verification_records.content as verification_content, verification_records.submitted_at as verification_submitted_at FROM verification_reports
                                            INNER JOIN verification_records ON verification_reports.verification_id = verification_records.id
                                            INNER JOIN challengers ON verification_records.challenger_id = challengers.id
                                            INNER JOIN users ON challengers.user_id = users.id
                                            WHERE challengers.event_id = ?`,
                [eventId],
            )
        ).rows
        return reports
    }

    public async getVerificationReport(verificationReportId: number) {
        const report = (
            await this.knex.raw(
                `SELECT verification_reports.*, users.id as challenger_id, users.nickname as challenger_name, users.pro_pic as challenger_pro_pic, verification_records.id as verification_id, verification_records.content as verification_content, verification_records.submitted_at as verification_submitted_at FROM verification_reports
                                            INNER JOIN verification_records ON verification_reports.verification_id = verification_records.id
                                            INNER JOIN challengers ON verification_records.challenger_id = challengers.id
                                            INNER JOIN users ON challengers.user_id = users.id
                                            WHERE verification_reports.id = ?`,
                [verificationReportId],
            )
        ).rows[0]
        return report
    }

    public async postVerificationReport(
        userId: number,
        verificationId: number,
        content: string,
    ) {
        const reportId = (
            await this.knex.raw(
                `INSERT INTO verification_reports(reporter_id, verification_id, content)
                                                SELECT ?,?,? WHERE NOT EXISTS
                                                (SELECT id FROM verification_reports
                                                WHERE reporter_id = ? AND verification_id = ? AND content = ?)                                        
                                                RETURNING id`,
                [
                    userId,
                    verificationId,
                    content,
                    userId,
                    verificationId,
                    content,
                ],
            )
        ).rows[0].id

        return this.getVerificationReport(reportId)
    }

    public async rejectVerificationReport(verificationReportId: number) {
        await this.knex.raw(`DELETE FROM verification_reports WHERE id = ?`, [
            verificationReportId,
        ])
        return { success: true }
    }

    public async acceptVerificationReport(verificationReportId: number) {
        const verificationId = (
            await this.knex.raw(
                `SELECT verification_records.id FROM verification_records
                                                    INNER JOIN verification_reports ON verification_reports.verification_id = verification_records.id
                                                    WHERE verification_reports.id = ?`,
                verificationReportId,
            )
        ).rows[0].id

        await this.knex.raw(`DELETE FROM verification_reports WHERE id = ?`, [
            verificationReportId,
        ])
        await this.knex.raw(`DELETE FROM verification_records WHERE id = ?`, [
            verificationId,
        ])
        // await this.knex.raw(`DELETE FROM verification_records WHERE id IN
        //                     (SELECT verification_records.id FROM verification_records
        //                         INNER JOIN verification_reports ON verification_reports.verification_id = verification_records.id
        //                         WHERE verification_reports.id = ?)`,[verificationReportId])
        return { success: true }
    }
}
