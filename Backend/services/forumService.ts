// import { UserLogin, User } from '../models';
import { Knex } from 'knex'
import { IPostInfo , ICommentInfo} from './models'

export class ForumService {
    constructor(private knex: Knex) {}

    async addPost(userId: number, body: IPostInfo, file: any) {
        if (file) {
            const result = (
                await this.knex
                    .insert({
                        category_id: body.categoryId,
                        content: body.content,
                        user_id: userId,
                        photo: file.key,
                    })
                    .into('posts')
                    .returning('id')
            )[0]
            return result
        } else {
            const result = (
                await this.knex
                    .insert({
                        category_id: body.categoryId,
                        content: body.content,
                        user_id: userId,
                        photo: null,
                    })
                    .into('posts')
                    .returning('id')
            )[0]
            return result
        }
    }

    async getAllPosts() {
        const result = await this.knex.raw(`
        SELECT postswithuser.*, withlikecount.likes, withcommentcount.comments
        FROM
        (SELECT posts.id, posts.category_id, posts.user_id, posts.content, posts.photo, posts.created_at,
        users.nickname, users.pro_pic
        FROM posts
        LEFT JOIN users ON posts.user_id = users.id) AS postswithuser
        LEFT JOIN
        (SELECT posts.id, COUNT(posts_likes.id) AS likes
        FROM posts
        LEFT JOIN posts_likes ON posts.id = posts_likes.post_id
        GROUP BY posts.id) AS withlikecount
        ON postswithuser.id = withlikecount.id
        LEFT JOIN
        (SELECT posts.id, COUNT(comments.id) AS comments
        FROM posts
        LEFT JOIN comments ON posts.id = comments.post_id
        GROUP BY posts.id) AS withcommentcount
        ON postswithuser.id = withcommentcount.id
        ORDER BY postswithuser.created_at DESC
        `)
        return result.rows
    }

    async addComment(userId: number, body: ICommentInfo) {
        const result = (
            await this.knex
                .insert({
                    post_id: body.postId,
                    user_id: userId,
                    comment: body.comment,
                })
                .into('comments')
                .returning('id')
        )[0]
        return result
    }

    async getCommentsById(postId:number) {
        const result = await this.knex.raw(`
        SELECT comments.id, comments.post_id, comments.user_id, comments.comment, comments.created_at AS comment_created_at,
        users.nickname, users.pro_pic
        FROM comments
        LEFT JOIN users ON comments.user_id = users.id
        WHERE post_id = ?
        `,[postId])
        return result.rows
    }

    async postLikeById(userId:number, postId:number) {
        const result = (
            await this.knex
                .insert({
                    post_id: postId,
                    user_id: userId
                })
                .into('posts_likes')
                .returning('id')
        )[0]
        return result
    }
    async deleteLikeById(userId:number, postId:number) {
        const result = (
            await this.knex('posts_likes')
            .where({
                post_id: postId,
                user_id: userId
            })
            .del()
            .returning('id')
        )
        return result
    }

    async getLikesByUser(userId:number) {
        const result = await this.knex.raw(`
        SELECT post_id FROM posts_likes WHERE user_id = ?
        `,[userId])
        return result.rows
    }
}
