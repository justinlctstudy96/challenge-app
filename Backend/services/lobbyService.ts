import { Knex } from 'knex'
import { FilterOrder } from './models'

export class LobbyService {
    public constructor(private knex: Knex) {}

    public async getChallenges(userId: number, filterOrder: FilterOrder) {
        let whereFilterSQL = ''
        let orderedBySQL = ''
        let daysCalculatedColumn = ''
        let userSavedChallengeIdArray: number[] = []
        let inCategoriesChallengeIdArray: number[] = []
        let whereInArray: number[] = []
        if (filterOrder.savedFilter) {
            const userSavedChallengeIds = (
                await this.knex.raw(
                    `SELECT challenge_id FROM likes WHERE user_id = ?`,
                    [userId],
                )
            ).rows
            userSavedChallengeIds.map((select: any) => {
                userSavedChallengeIdArray.push(select.challenge_id)
            })
        }
        if (filterOrder.filter.length > 0) {
            const inCategoryIdSQL = `(${filterOrder.filter.toString()})`
            const challengesInCategories = (
                await this.knex.raw(
                    `SELECT DISTINCT challenge_id FROM challenges_categories WHERE category_id IN ${inCategoryIdSQL}`,
                )
            ).rows
            challengesInCategories.map((item: { challenge_id: number }) => {
                inCategoriesChallengeIdArray.push(item.challenge_id)
            })
        }
        if (
            userSavedChallengeIdArray.length !== 0 &&
            inCategoriesChallengeIdArray.length !== 0
        ) {
            whereInArray = userSavedChallengeIdArray.filter(function (
                challengeId,
            ) {
                return inCategoriesChallengeIdArray.indexOf(challengeId) > -1
            })
            whereFilterSQL = ` WHERE challenges.id IN (${whereInArray.toString()})`
        } else if (userSavedChallengeIdArray.length !== 0) {
            whereInArray = userSavedChallengeIdArray
            whereFilterSQL = ` WHERE challenges.id IN (${whereInArray.toString()})`
        } else if (filterOrder.filter.length > 0) {
            whereInArray = inCategoriesChallengeIdArray
            whereFilterSQL = ` WHERE challenges.id IN (${whereInArray.toString()})`
        } else {
            whereFilterSQL = ''
        }
        if (filterOrder.orderedBy === 'Likes') {
            orderedBySQL = ` ORDER BY challenges.likes DESC`
        }
        if (filterOrder.orderedBy === 'Days') {
            daysCalculatedColumn = `, (challenges.days*challenges.cycles) as total_days`
            orderedBySQL = ` ORDER BY total_days DESC`
        }
        // if (filterOrder.orderedBy==='Challengers'){
        //     countChallengersColumn = `, COUNT(challengers.user_id) as challengers`
        //     innerJoinSQL = ` INNER JOIN challengers ON challenges.id = challengers.challenge_id`
        //     orderedBySQL = ` ORDER BY challengers DESC`
        // }
        let challengeList = (
            await this.knex
                .raw(`SELECT challenges.*, challenges.id as challenge_id, users.nickname as creator_name ${daysCalculatedColumn} FROM challenges   
                                                INNER JOIN users ON challenges.creator_id = users.id
                                                ${whereFilterSQL} ${orderedBySQL}`)
        ).rows
        // if (filterOrder.orderedBy === 'Challengers') {
        //     challengeList
        // }

        interface categoriesObject {
            challenge_id: number
            categories: number[]
            verifications: number[]
            pastParticipants: number
        }
        // get categories and verifications of challenge of event from table challenges_categories and challenges_verifications respectively
        let arrayOfCategoriesVerifications = [] as categoriesObject[]
        await Promise.all(
            challengeList.map(async (item: any) => {
                const selectedCategories = (
                    await this.knex.raw(
                        `SELECT category_id FROM challenges_categories WHERE challenge_id = ${item.challenge_id}`,
                    )
                ).rows
                let categories = [] as number[]
                selectedCategories.map((category: any) => {
                    categories.push(category.category_id)
                })
                const selectedVerifications = (
                    await this.knex.raw(
                        `SELECT verification_id FROM challenges_verifications WHERE challenge_id = ${item.challenge_id}`,
                    )
                ).rows
                let verifications = [] as number[]
                selectedVerifications.map((verification: any) => {
                    verifications.push(verification.verification_id)
                })
                const pastParticipants = (
                    await this.knex
                        .raw(`SELECT COUNT(challengers.user_id) FROM challengers 
                                                                INNER JOIN challenge_events ON challengers.event_id = challenge_events.id
                                                                WHERE challenge_events.challenge_id = ${item.challenge_id.toString()}`)
                ).rows[0].count
                arrayOfCategoriesVerifications.push({
                    challenge_id: item.challenge_id,
                    categories: categories,
                    verifications: verifications,
                    pastParticipants: pastParticipants,
                })
                // item = {...item, categories:categories}
            }),
        )
        for (let i = 0; i < challengeList.length; i++) {
            arrayOfCategoriesVerifications.map((item) => {
                if (item.challenge_id === challengeList[i].challenge_id) {
                    challengeList[i] = {
                        ...challengeList[i],
                        categories: item.categories,
                        verifications: item.verifications,
                        past_participants: item.pastParticipants,
                    }
                }
            })
        }
        if (filterOrder.orderedBy === 'Challengers') {
            const  compare = (a:any, b:any) => {
                if (a.past_participants < b.past_participants) {
                    return 1
                }
                if (a.past_participants > b.past_participants) {
                    return -1
                }
                return 0
            }
            challengeList.sort(compare)
        }
        return challengeList
    }

    public async getEvents(userId: number, filterOrder: FilterOrder) {
        let whereFilterSQL = "WHERE challenge_events.status != 'ended'"
        let orderedBySQL = ''
        // if (filterOrder.filter.length>0){
        //     const inCategoryIdSQL = `(${filterOrder.filter.toString()})`
        //     const challengesInCategories = (await this.knex.raw(`SELECT DISTINCT challenge_id FROM challenges_categories WHERE category_id IN ${inCategoryIdSQL}`)).rows
        //     let inCategoriesChallengeIdArray: number[] = []
        //     challengesInCategories.map((item:{challenge_id: number}) => {inCategoriesChallengeIdArray.push(item.challenge_id)})
        //     whereFilterSQL = ` AND challenges.id IN (${inCategoriesChallengeIdArray.toString()})`
        // }
        let daysCalculatedColumn = ''
        let userSavedChallengeIdArray: number[] = []
        let inCategoriesChallengeIdArray: number[] = []
        let whereInArray: number[] = []
        if (filterOrder.savedFilter) {
            const userSavedChallengeIds = (
                await this.knex.raw(
                    `SELECT challenge_id FROM likes WHERE user_id = ?`,
                    [userId],
                )
            ).rows
            userSavedChallengeIds.map((select: any) => {
                userSavedChallengeIdArray.push(select.challenge_id)
            })
        }
        if (filterOrder.filter.length > 0) {
            const inCategoryIdSQL = `(${filterOrder.filter.toString()})`
            const challengesInCategories = (
                await this.knex.raw(
                    `SELECT DISTINCT challenge_id FROM challenges_categories WHERE category_id IN ${inCategoryIdSQL}`,
                )
            ).rows
            challengesInCategories.map((item: { challenge_id: number }) => {
                inCategoriesChallengeIdArray.push(item.challenge_id)
            })
        }
        if (
            userSavedChallengeIdArray.length !== 0 &&
            inCategoriesChallengeIdArray.length !== 0
        ) {
            whereInArray = userSavedChallengeIdArray.filter(function (
                challengeId,
            ) {
                return inCategoriesChallengeIdArray.indexOf(challengeId) > -1
            })
            whereFilterSQL += ` AND challenges.id IN (${whereInArray.toString()})`
        } else if (userSavedChallengeIdArray.length !== 0) {
            whereInArray = userSavedChallengeIdArray
            whereFilterSQL += ` AND challenges.id IN (${whereInArray.toString()})`
        } else if (filterOrder.filter.length > 0) {
            whereInArray = inCategoriesChallengeIdArray
            if (whereInArray.length === 0) {
                whereInArray = [100]
            }
            whereFilterSQL += ` AND challenges.id IN (${whereInArray.toString()})`
        } else {
            whereFilterSQL += ''
        }
        if (filterOrder.orderedBy === 'Likes') {
            orderedBySQL = ` ORDER BY challenges.likes DESC`
        }
        if (filterOrder.orderedBy === 'Days') {
            daysCalculatedColumn = `, (challenges.days*challenges.cycles) as total_days`
            orderedBySQL = ` ORDER BY total_days DESC`
        }
        let eventList = (
            await this.knex
                .raw(`SELECT challenges.*, challenges.id as challenge_id, challenge_events.*, challenge_events.id as event_id, users.nickname as host_name ${daysCalculatedColumn} FROM challenges 
                                                INNER JOIN challenge_events ON challenges.id = challenge_events.challenge_id
                                                INNER JOIN users ON challenge_events.host_id = users.id
                                                ${whereFilterSQL} ${orderedBySQL}`)
        ).rows

        interface categoriesObject {
            challenge_id: number
            categories: number[]
            verifications: number[]
        }
        // get categories and verifications of challenge of event from table challenges_categories and challenges_verifications respectively
        let arrayOfCategories = [] as categoriesObject[]
        await Promise.all(
            eventList.map(async (item: any) => {
                const selectedCategories = (
                    await this.knex.raw(
                        `SELECT category_id FROM challenges_categories WHERE challenge_id = ${item.challenge_id}`,
                    )
                ).rows
                let categories = [] as number[]
                selectedCategories.map((category: any) => {
                    categories.push(category.category_id)
                })
                const selectedVerifications = (
                    await this.knex.raw(
                        `SELECT verification_id FROM challenges_verifications WHERE challenge_id = ${item.challenge_id}`,
                    )
                ).rows
                let verifications = [] as number[]
                selectedVerifications.map((verification: any) => {
                    verifications.push(verification.verification_id)
                })
                arrayOfCategories.push({
                    challenge_id: item.challenge_id,
                    categories: categories,
                    verifications: verifications,
                })
                // item = {...item, categories:categories}
            }),
        )
        for (let i = 0; i < eventList.length; i++) {
            arrayOfCategories.map((item) => {
                if (item.challenge_id === eventList[i].challenge_id) {
                    eventList[i] = {
                        ...eventList[i],
                        categories: item.categories,
                        verifications: item.verifications,
                    }
                }
            })
        }

        interface participantsObject {
            event_id: number
            participants: number
        }
        // get participants of event from table challengers
        let arrayOfParticipants = [] as participantsObject[]
        await Promise.all(
            eventList.map(async (item: any) => {
                arrayOfParticipants.push({
                    event_id: item.event_id,
                    participants: parseInt(
                        (
                            await this.knex.raw(
                                `SELECT COUNT(user_id) FROM challengers WHERE event_id = ${item.event_id.toString()}`,
                            )
                        ).rows[0].count,
                    ),
                })
            }),
        )
        for (let i = 0; i < eventList.length; i++) {
            arrayOfParticipants.map((item) => {
                if (item.event_id === eventList[i].event_id) {
                    eventList[i] = {
                        ...eventList[i],
                        participants: item.participants,
                    }
                }
            })
        }
        return eventList
    }
}
