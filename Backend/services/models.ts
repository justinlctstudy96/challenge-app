export interface Challenge {
    id: number;
    title: string;
    rule: string;
    task: string;
    description: string;
    persistence: boolean;
    times: number;
    days: number;
    cycles: number;
    renewTime: string;
    withSection: boolean;
    sections: string[];
    consecutive: boolean;
    publicity: boolean;
    verifyMethods: number[];
    categories: number[];
    creator_id: number;
    creator_name: string;
    past_participants: number;
}

export interface Event {
    challengeId: number;
    accommodation: number;
    official: boolean;
    officialReward: string;
    start: Date;
    end: Date;
    comment: string;
}

export interface FilterOrder {
    filter: number[];
    savedFilter: boolean;
    orderedBy: string
}

export interface Message {
    user_id: number;
    nickname: string
    pro_pic: string;
    content: string;
    likes: number;
    created_at: string;
}

export interface VerificationRecord {
    nth_cycle: number;
    storyFlatListIndex: number;
    id: number;
    challenger_id: number;
    method_id: number;
    content: string;
    role: string;
    status: string;
    created_at: string;
    updated_at: string;
    submitted_at: string;
    nickname: string;
    pro_pic: string;
}

export interface ChallengerVerificationRecords { // records of one challenger
    challenger: ChallengerInfo;
    verificationRecords: VerificationRecord[]
}

export interface CycleVerificationRecords { // records of may challengers within a cycle
    nth_cycle: number;
    challengersVerificationRecords: ChallengerVerificationRecords[]
}

export interface ChallengerInfo {
    challenger_id: number;
    user_id: number;
    nickname: string;
    pro_pic: string;
}

export interface ChallengeTimeDetails {
    times: number;
    days: number;
    cycles: number;
    start_date: string
}

export interface CycleDate{
    current_cycle: number
    start_date: string;
    end_date: string;
}

export interface IPostInfo {
    categoryId: number,
    content: string
}

export interface ICommentInfo {
    comment: string,
    postId?: number|null
}
