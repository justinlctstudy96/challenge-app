// import { UserLogin, User } from '../models';
import { Knex } from 'knex'
import { hashPassword } from '../hash';
import { logger } from '../logger';
import { IAddedUserInfo, ISignUpInfo } from '../models';


export class UserService {
    constructor(private knex: Knex) {}

    async getUserByEmail(email: string) {
        const gotUser= await this.knex
        .select('*')
        .from('users')
        .where('email', email)
        return gotUser[0]
    }

    async getUserById(id: string) {
        return (await this.knex.raw(`SELECT * FROM users WHERE id = ?`, [id]))
            .rows
    }

    async checkUserEmail(email: string) {
        const userExist = await this.knex
            .select('*')
            .from('users')
            .where('email', email)
        return userExist[0] ? userExist[0] : false
    }

    async checkUserNickname(nickname: string) {
        const userExist = await this.knex
            .select('*')
            .from('users')
            .where('nickname', nickname)
        return userExist[0] ? userExist[0] : false
    }

    async addUser(body: ISignUpInfo, file: any) {
        const hashedPassword = await hashPassword(body.password)
        const result:Partial<IAddedUserInfo> = (await this.knex
                             .insert({
                                 email: body.email,
                                 password: hashedPassword,
                                 nickname: body.nickname,
                                 pro_pic: file.key
                             })
                             .into("users")
                             .returning("*"))[0]
        return result
    }

    async removeUser(userId: number) {
        const result = await this.knex('users')
        .where('id', userId)
        .del()
        logger.debug('result')
        return result
    }

    async getUser(userId: string) {
        const result = (
            await this.knex.select('*').from('users').where('id', userId)
        )[0]
        return result
    }
    
}
