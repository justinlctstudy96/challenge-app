import { Knex } from 'knex'

export class VerifyService {
    public constructor(private knex: Knex) {}

    async addVerifyRecord(body: any, file: any) {
        const times = (await this.knex.raw(`SELECT challenges.times FROM challenges
                                            INNER JOIN challenge_events ON challenges.id = challenge_events.challenge_id
                                            INNER JOIN challengers ON challenge_events.id = challengers.event_id
                                            WHERE challengers.id = ?`,[body.challengerId])).rows[0]
        const verifiedTimes = (await this.knex.raw(`SELECT COUNT(challenger_id) FROM verification_records
                                                    WHERE challenger_id = ? AND submitted_at > ?`,[body.challengerId, body.cycleStartDate])).rows[0].count
        if (verifiedTimes<times){
            await this.knex
                .insert({
                    challenger_id: body.challengerId,
                    method_id: body.methodId,
                    status: 'pending_for_verify',
                    content: file.key,
                    submitted_at: 'NOW()'
                })
                .into('verification_records')
            return true
        }
        return false
    }

    public async seeIfUploaded(
        userId: number,
        eventId: number,
        today: string,
        tmr: string,
    ) {
        // const haveUploaded = await this.knex.raw(
        //     `Select verification_records.challenger_id from verification_records  JOIN challengers on challengers.id = verification_records.challenger_id  where verification_records.challenger_id  = (?) and challengers.event_id = (?) and   verification_records.updated_at > (?) and verification_records.updated_at < (?)`,
        //     [userId, eventId, `${today}`, `${tmr}`],
        // )
        const haveUploaded = await this.knex.raw(
            `Select verification_records.challenger_id from verification_records  JOIN challengers on challengers.id = verification_records.challenger_id  where verification_records.challenger_id  = (?) and challengers.event_id = (?) and verification_records.updated_at > NOW() - INTERVAL 15 MINUTE`,
            [userId, eventId],
        )
        return haveUploaded
    }
}
