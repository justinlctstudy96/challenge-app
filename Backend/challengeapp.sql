select*from users;
select*from challenges;

SELECT challenges.*, challenges_categories.category_id FROM challenges INNER JOIN challenges_categories ON challenges.id = challenges_categories.challenge_id 

SELECT challenges.*, challenge_events.* FROM challenges INNER JOIN challenge_events ON challenges.id = challenge_events.challenge_id WHERE challenges.id IN (5,12)

SELECT challenges.*, challenges.id as challenge_id, challenge_events.*, challenge_events.id as event_id FROM challenges 
                                                INNER JOIN challenge_events ON challenges.id = challenge_events.challenge_id

SELECT challenges.*, challenges.id as challenge_id, users.nickname as creator_name, COUNT(challengers.id) as challengers FROM challenges  
INNER JOIN users ON challenges.creator_id = users.id
INNER JOIN challenge_events ON challenges.id = challenge_events.challenge_id
INNER JOIN challengers ON challenge_events.id = challengers.event_id
GROUP BY users.nickname
ORDER BY challengers DESC

SELECT challenges.*, challenges.id as challenge_id, users.nickname as creator_name FROM challenges  
INNER JOIN users ON challenges.creator_id = users.id
INNER JOIN challenge_events ON challenges.id = challenge_events.challenge_id
INNER JOIN challengers ON challenge_events.id = challengers.event_id
GROUP BY challenges.id

SELECT COUNT(challenge_events.id) AS events FROM challenge_events WHERE challenges_events.id = 1
SELECT COUNT(challengers.user_id) AS partis FROM challengers WHERE challengers.event_id = 1

SELECT challenges.*, challenges.id as challenge_id, users.nickname as creator_name, ((SELECT COUNT(challenge_events.id) AS events FROM challenge_events)*(SELECT COUNT(challengers.user_id) AS partis FROM challengers)) as challengers FROM challenges  
INNER JOIN users ON challenges.creator_id = users.id
INNER JOIN challenge_events ON challenges.id = challenge_events.challenge_id
INNER JOIN challengers ON challenge_events.id = challengers.event_id
GROUP BY challenges.id
ORDER BY challengers DESC




SELECT verification_records.*, users.nickname, users.pro_pic FROM verification_records
INNER JOIN challengers ON verification_records.challenger_id = challengers.id
INNER JOIN challenge_events ON challengers.event_id = challenge_events.id
INNER JOIN users ON challengers.user_id = users.id
WHERE challenge_events.id = 1
ORDER BY verification_records.submitted_at DESC

DELETE FROM verification_records WHERE id IN (11);

UPDATE challenge_events SET start_date = '2021-06-19', end_date = '2021-07-19' WHERE id = 1
UPDATE challenge_events SET start_date = '2021-05-28', end_date = '2021-06-28' WHERE id = 5


INSERT INTO verification_records (challenger_id, method_id, status, content,submitted_at) VALUES
(5,2,'pending_for_verify','livePhoto-1624457852533.jpeg','20210622 10:34:09 AM')
INSERT INTO verification_records (challenger_id, method_id, status, content,submitted_at) VALUES
(7,2,'pending_for_verify','livePhoto-1624456096789.jpeg','20210622 10:34:09 AM')
INSERT INTO verification_records (challenger_id, method_id, status, content,submitted_at) VALUES
(6,2,'pending_for_verify','livePhoto-1624456096789.jpeg','20210621 10:34:09 AM')
INSERT INTO verification_records (challenger_id, method_id, status, content,submitted_at) VALUES
(7,2,'pending_for_verify','livePhoto-1624457852533.jpeg','20210621 10:34:09 AM')
INSERT INTO verification_records (challenger_id, method_id, status, content,submitted_at) VALUES
(5,2,'pending_for_verify','livePhoto-1624457852533.jpeg','20210620 11:35:09 AM')
INSERT INTO verification_records (challenger_id, method_id, status, content,submitted_at) VALUES
(6,2,'pending_for_verify','livePhoto-1624457852533.jpeg','20210620 11:35:09 AM')
INSERT INTO verification_records (challenger_id, method_id, status, content,submitted_at) VALUES
(7,2,'pending_for_verify','livePhoto-1624457852533.jpeg','20210620 11:35:09 AM')

INSERT INTO verification_records (challenger_id, method_id, status, content,submitted_at) VALUES
(3,2,'pending_for_verify','livePhoto-1624457852533.jpeg','2021-06-24T16:00:00.000Z')

SELECT verification_records.*, users.nickname, users.pro_pic FROM verification_records
INNER JOIN challengers ON verification_records.challenger_id = challengers.id
INNER JOIN challenge_events ON challengers.event_id = challenge_events.id
INNER JOIN users ON challengers.user_id = users.id
WHERE challenge_events.id = 1 AND verification_records.submitted_at
BETWEEN '021-06-20T16:00:00.000Z'::timestamp AND '2021-06-21T16:00:00.000Z'::timestamp

SELECT verification_records.*, users.nickname, users.pro_pic FROM verification_records
INNER JOIN challengers ON verification_records.challenger_id = challengers.id
INNER JOIN challenge_events ON challengers.event_id = challenge_events.id
INNER JOIN users ON challengers.user_id = users.id
WHERE challenge_events.id = 1 AND verification_records.submitted_at
BETWEEN '2021-06-20'::date AND '2021-06-21'::date


SELECT verification_records.*, users.nickname, users.pro_pic FROM verification_records
INNER JOIN challengers ON verification_records.challenger_id = challengers.id
INNER JOIN challenge_events ON challengers.event_id = challenge_events.id
INNER JOIN users ON challengers.user_id = users.id
WHERE challenge_events.id = 4 AND verification_records.submitted_at
BETWEEN '2021-06-26'::date AND '2021-06-27'::date

INSERT INTO verification_reports(reporter_id, verification_id, content)
SELECT 1,29,'untrustable' WHERE NOT EXISTS
(SELECT id FROM verification_reports
WHERE reporter_id = 1 AND verification_id = 29 AND content = 'untrustable')                                        
RETURNING id

SELECT COUNT(verification_records.id) FROM verification_records                                                                                                                                                                                      
INNER JOIN challengers ON verification_records.challenger_id = challengers.id                                                                                                                    
INNER JOIN users ON challengers.user_id = users.id                                                                                                                                               
WHERE users.id = 1 and challengers.event_id = 4 and submitted_at > '2021-06-27';