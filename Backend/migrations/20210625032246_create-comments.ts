import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('comments')
    if (!hasTable) {
        return knex.schema.createTable('comments', (table) => {
            table.increments()
            table.integer('post_id').references('id').inTable('posts')
            table.integer('user_id').references('id').inTable('users')
            table.string('comment').notNullable()
            table.timestamps(false, true)
        })
    } else {
        return Promise.resolve()
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('comments')
}


