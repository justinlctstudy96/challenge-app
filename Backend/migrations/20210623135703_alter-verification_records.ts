import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('verification_records')){
        await knex.schema.alterTable('verification_records',(table)=>{
            table.timestamp('submitted_at').defaultTo('NOW()')
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("verification_records")){
        await knex.schema.alterTable('verification_records',(table)=>{
            table.dropColumn('submitted_at')
        });
    }
}

