import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('likes')
    if(!hasTable){
        return knex.schema.createTable('likes', table => {
            table.increments();
            table.integer('user_id').references('id').inTable('users')
            table.integer('challenge_id').references('id').inTable('challenges')
            table.timestamps(false,true);
        })
    }else{
        return Promise.resolve()
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('likes')
}

