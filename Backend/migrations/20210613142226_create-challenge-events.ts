import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('challenge_events')
    if(!hasTable){
        return knex.schema.createTable('challenge_events', table => {
            table.increments();
            table.integer('challenge_id').references('id').inTable('challenges')
            table.integer('host_id').references('id').inTable('users')
            table.string('status').defaultTo('pending')
            table.integer('accommodation').defaultTo(1)
            table.boolean('official').defaultTo(false)
            table.string('official_reward').defaultTo('')
            table.string('start_date')
            table.string('end_date')
            table.string('comment').defaultTo('')
            table.timestamps(false,true);
        })
    }else{
        return Promise.resolve()
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('challenge_events')
}

