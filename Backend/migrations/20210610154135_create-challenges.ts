import { Knex } from 'knex'

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('challenges')

    if (hasTable) {
        await knex.schema.dropTable('challenges')
    }
    return knex.schema.createTable('challenges', (table) => {
        table.increments()
        table.integer('creator_id').references('id').inTable('users')
        table.string('title', 255).notNullable()
        table.string('rule', 255).notNullable()
        table.string('task', 255).notNullable()
        table.string('description', 255).defaultTo('')
        table.boolean('persistence').defaultTo(false)
        table.boolean('consecutive').defaultTo(false)
        table.integer('times').defaultTo(1)
        table.integer('days').defaultTo(1)
        table.integer('cycles').defaultTo(1)
        table.string('renew', 255).defaultTo('00:00')
        table.boolean('with_section').defaultTo(false)
        table.boolean('public').defaultTo(false)
        table.specificType('sections', 'TEXT[]').defaultTo("{''foo'',''bar''}")
        // table.specificType('sections', 'text ARRAY').defaultTo('{\'\'foo\'\',\'\'bar\'\'}')
        table.integer('likes').defaultTo(0)
        table.integer('wishes').defaultTo(0)
        table.timestamps(false, true)
    })
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('challenges')
}
