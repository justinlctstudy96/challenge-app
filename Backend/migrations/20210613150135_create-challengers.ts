import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('challengers')
    if(!hasTable){
        return knex.schema.createTable('challengers', table => {
            table.increments();
            table.integer('user_id').references('id').inTable('users')
            table.integer('event_id').references('id').inTable('challenge_events')
            table.string('status').defaultTo('waiting')
            table.string('role').defaultTo('player')
            table.timestamps(false,true);
        })
    }else{
        return Promise.resolve()
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('challengers')
}

