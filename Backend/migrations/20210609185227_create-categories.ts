import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('categories')
    if(!hasTable){
        return knex.schema.createTable('categories', table => {
            table.increments();
            table.string('category',255).notNullable();
            table.boolean('official').defaultTo(false);
            table.string('color',255).notNullable();
            table.string('selected_color',255).notNullable();
            table.timestamps(false,true)
        })
    }else{
        return Promise.resolve()
    }

}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('categories')
}

