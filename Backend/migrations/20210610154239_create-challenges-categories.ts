import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('challenges_categories')
    if(!hasTable){
        return knex.schema.createTable('challenges_categories', table => {
            table.increments();
            table.integer('challenge_id').references('id').inTable('challenges')
            table.integer('category_id').references('id').inTable('categories')
        })
    }else{
        return Promise.resolve()
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('challenges_categories')
}