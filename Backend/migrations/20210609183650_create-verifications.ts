import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('verifications')
    if(!hasTable){
        return knex.schema.createTable('verifications', table => {
            table.increments();
            table.string('method',255).notNullable();
            table.timestamps(false,true)
        })
    }else{
        return Promise.resolve()
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('verifications')
}

