import { Knex } from 'knex'

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('posts')
    if (!hasTable) {
        return knex.schema.createTable('posts', (table) => {
            table.increments()
            table.integer('category_id').references('id').inTable('categories')
            table.integer('user_id').references('id').inTable('users')
            table.string('content').notNullable()
            table.string('photo')
            table.timestamps(false, true)
        })
    } else {
        return Promise.resolve()
    }
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('posts')
}
