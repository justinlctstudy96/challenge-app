import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('verification_records')
    if(!hasTable){
        return knex.schema.createTable('verification_records', table => {
            table.increments();
            table.integer('challenger_id').references('id').inTable('challengers')
            table.integer('method_id').references('id').inTable('verifications')
            table.string('content').notNullable()
            table.string('role').defaultTo('player')
            table.string('status').defaultTo('pending_upload')
            table.timestamps(false,true);
        })
    }else{
        return Promise.resolve()
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('verification_records')
}

