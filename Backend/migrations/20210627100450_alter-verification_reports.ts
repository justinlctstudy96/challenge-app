import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('verification_reports')){
        await knex.schema.alterTable('verification_reports',(table)=>{
            table.string('content').defaultTo('')
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("verification_reports")){
        await knex.schema.alterTable('verification_reports',(table)=>{
            table.dropColumn('content')
        });
    }
}

