import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('posts_likes')
    if (!hasTable) {
        return knex.schema.createTable('posts_likes', (table) => {
            table.increments()
            table.integer('post_id').references('id').inTable('posts')
            table.integer('user_id').references('id').inTable('users')
            table.timestamps(false, true)
        })
    } else {
        return Promise.resolve()
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('posts_likes')
}

