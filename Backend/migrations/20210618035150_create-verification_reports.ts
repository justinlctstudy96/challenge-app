import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('verification_reports')
    if(!hasTable){
        return knex.schema.createTable('verification_reports', table => {
            table.increments();
            table.integer('verification_id').references('id').inTable('verification_records')
            table.integer('reporter_id').references('id').inTable('users')
            table.timestamps(false,true);
        })
    }else{
        return Promise.resolve()
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('verification_reports')
}

