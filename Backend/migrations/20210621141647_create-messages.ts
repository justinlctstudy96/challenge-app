import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('messages')
    if(!hasTable){
        return knex.schema.createTable('messages', table => {
            table.increments();
            table.integer('event_id').references('id').inTable('challenge_events')
            table.integer('user_id').references('id').inTable('users')
            table.string('content').defaultTo('')
            table.integer('likes').defaultTo(0)
            table.timestamps(false,true);
        })
    }else{
        return Promise.resolve()
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('messages')
}

