import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('users')
    if(!hasTable){
        return knex.schema.createTable('users', table => {
            table.increments();
            table.string('email',255).notNullable();
            table.string('password',255).notNullable();
            table.string('nickname',255).notNullable();
            table.string('pro_pic',255).defaultTo('');
            table.decimal('experiences',5,2).defaultTo(0);
            table.integer('clover').defaultTo(0);
            table.timestamps(false,true);
        })
    }else{
        return Promise.resolve()
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('users')
}

