import { UserService } from '../services/userService'
import { Request, Response } from 'express'
import jwtSimple from 'jwt-simple'
import jwt from '../jwt'
import { logger } from '../logger'
import { Bearer } from 'permit'

const permit = new Bearer({
    query: 'access_token',
})

export class UserController {
    constructor(private userService: UserService) {}

    getCurrentUser = (req: Request, res: Response) => {
        logger.log('debug', JSON.stringify(req.user))
        return res.json(req.user)
    }

    register = async (req: Request, res: Response) => {
        if (await this.userService.checkUserEmail(req.body['email'])) {
            return res
                .status(400)
                .json({ message: 'Email already been registered' })
        } else if (
            await this.userService.checkUserNickname(
                req.body['nickname'].trim(),
            )
        ) {
            return res
                .status(400)
                .json({ message: 'Nickname already been taken' })
        } else {
            const addedUser = await this.userService.addUser(req.body, req.file)
            logger.log('debug', typeof addedUser)
            logger.log('debug', JSON.stringify(addedUser))
            if (addedUser) {
                const payload = {
                    id: addedUser.id,
                    email: addedUser.email,
                    // nick name not included since it may be changed not along with the token
                }
                const token = jwtSimple.encode(payload, jwt.jwtSecret)
                const userInfo = {
                    id: addedUser.id,
                    email: addedUser.email,
                    nickname: addedUser.nickname,
                    pro_pic: addedUser.pro_pic
                }
                logger.log(
                    'info',
                    'User Registered: ' + JSON.stringify(addedUser),
                )
                return res.status(200).json({
                    message: 'Successfully registered',
                    token: token,
                    user: userInfo,
                })
            }
            return res.status(500).json({ message: 'Internal Server Error' })
        }
    }
    deleteAccount = async (req: Request, res: Response) => {
        const token = permit.check(req);
        const payload = jwtSimple.decode(token,jwt.jwtSecret);

        const removedUser = await this.userService.removeUser(payload.id)
        if(removedUser){
            return res.status(200).json({ message: 'Successfully deleted account' })
        }
        return res.status(500).json({ message: 'Internal Server Error' }) 
    }
}
