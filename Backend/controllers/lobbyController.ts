import { Request, Response } from 'express'
import { LobbyService } from '../services/lobbyService'

export class LobbyController {
    constructor(private lobbyService: LobbyService) {}

    getChallenges = async (req: Request, res: Response) => {
        try {
            const userId = req.user.id
            const filterOrder = req.body
            const challengeList = await this.lobbyService.getChallenges(userId, filterOrder)
            return res.json(challengeList)
        } catch (e) {
            return res.status(500).json([{ msg: e.toString() }])
        }
    }

    getEvents = async (req: Request, res: Response) => {
        try {
            const userId = req.user.id
            const filterOrder = req.body
            const eventList = await this.lobbyService.getEvents(userId, filterOrder)
            return res.json(eventList)
        } catch (e) {
            return res.status(500).json([{ msg: e.toString() }])
        }
    }
}
