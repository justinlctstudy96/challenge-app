import  { Request, Response } from 'express'
import { ChallengeService } from '../services/challengeService'

export class ChallengeController {
    constructor(private challengeService: ChallengeService) {}

    getCategories = async (req: Request, res: Response) => {
        try {
            const categories = await this.challengeService.getCategories()
            return res.json(categories)
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    getVerifications = async (req: Request, res: Response) => {
        try {
            const verifications = await this.challengeService.getVerifications()
            return res.json(verifications)
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    getTotalLikes = async (req: Request, res: Response) => {
        const challengeId = parseInt(req.params.id)
        try {
            const totalLikesCount = await this.challengeService.getTotalLikes(
                challengeId,
            )
            return res.json({ success: true, totalLikes: totalLikesCount })
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    getUserLike = async (req: Request, res: Response) => {
        const userId = req.user!.id!
        const challengeId = parseInt(req.params.id)
        try {
            const userLikeCount = (
                await this.challengeService.getUserLike(userId, challengeId)
            ).rows[0].count
            let liked = false
            if (parseInt(userLikeCount) !== 0) {
                liked = true
            }
            return res.json({ success: true, userLiked: liked })
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    postUserLike = async (req: Request, res: Response) => {
        const userId = req.user!.id!
        const challengeId = parseInt(req.params.id)
        try {
            return res.json({
                success: true,
                userLiked: await this.challengeService.postUserLike(
                    userId,
                    challengeId,
                ),
            })
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    deleteUserLike = async (req: Request, res: Response) => {
        const userId = req.user!.id!
        const challengeId = parseInt(req.params.id)
        try {
            return res.json({
                success: true,
                userLiked: await this.challengeService.deleteUserLike(
                    userId,
                    challengeId,
                ),
            })
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    createChallenge = async (req: Request, res: Response) => {
        try {
            const userId = req.user!.id!
            const challenge = req.body
            const challengeId = await this.challengeService.postChallenge(
                userId,
                challenge,
            )
            return res.json({ success: true, challengeId: challengeId })
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    getChallengeDetails = async (req: Request, res: Response) => {
        try {
            const challengeId = parseInt(req.params.id)
            const challengeDetails =
                await this.challengeService.getChallengeDetails(challengeId)
            return res.json(challengeDetails)
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    createChallengeEvent = async (req: Request, res: Response) => {
        try {
            const userId = req.user!.id!
            const event = req.body
            const eventId = await this.challengeService.postChallengeEvent(
                userId,
                event,
            )
            return res.json({ success: true, eventId: eventId })
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    getUserOngoingChallengeEvents = async (req: Request, res: Response) => {
        try {
            const userId = req.user!.id!
            const ongoingChallenges =
                await this.challengeService.getUserChallengeEvents(true, userId)
            return res.json(ongoingChallenges)
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    getUserOngoingChallengeDots = async (req: Request, res: Response) => {
        const userId = parseInt(req.params.id)
        const challenges = await this.challengeService.personalOngoingChallengeByAllDate(userId)
        if(challenges){
            const eventDots = await this.challengeService.calculateEventDots(challenges)
            if(eventDots){
                return res.status(200).json(eventDots)
            }
        }
        return res.status(500).json({ message:'Internal Server Error}'})
    }

    getUserOngoingChallengeByDate = async (req: Request, res: Response) => {
        const userId = parseInt(req.params.id)
        const requestDate= req.params.date
        const challenges = await this.challengeService.personalOngoingChallengeByAllDate(userId)
        if(challenges){
            const dateEvents = await this.challengeService.calculateRequiredDateEvents(challenges, requestDate)
            if(dateEvents){
                return res.status(200).json(dateEvents)
            }
        }
        return res.status(200).json({ message:'Internal Server Error'})
    }

    getUserPastChallengeEvents = async (req: Request, res: Response) => {
        try {
            const userId = req.user!.id!
            const pastChallenges =
                await this.challengeService.getUserChallengeEvents(false, userId)
            return res.json(pastChallenges)
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    checkChallengeEventTimeout = async () => {
        await this.challengeService.checkChallengeEventTimeout()
    }
}
