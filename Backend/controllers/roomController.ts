import { Request, Response } from 'express'
import { RoomService } from '../services/roomService'

export class RoomController {
    constructor(private roomService: RoomService) {}

    getEventDetails = async (req: Request, res: Response) => {
        try {
            const userId = req.user.id
            const eventId = parseInt(req.params.id)
            const eventDetails = await this.roomService.getEventDetails(userId, eventId)
            return res.json(eventDetails)
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    getUserEventRelation = async (req: Request, res: Response) => {
        try {
            const eventId = parseInt(req.params.id)
            const userId = req.user.id
            const relation = await this.roomService.getUserEventRelation(
                userId,
                eventId,
            )
            if (relation) {
                return res.json(relation)
            } else {
                return res.json({ challengerId: 0, role: '', status: '' })
            }
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    joinEvent = async (req: Request, res: Response) => {
        try {
            const userId = req.user.id
            const eventId = parseInt(req.params.id)
            const challengerId = await this.roomService.postEventChallenger(
                userId,
                eventId,
            )
            return res.json({ success: true, challengerId: challengerId })
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    getEventParti = async (req: Request, res: Response) => {
        try {
            const eventId = parseInt(req.params.id)
            const getParti = await this.roomService.getEventParticipants(
                eventId,
            )
            return res.json(getParti.rows)
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    allStories = async (req: Request, res: Response) => {
        try {
            const eventId = parseInt(req.params.id)
            const todayFull = new Date()
            const today = todayFull.toISOString().slice(0, 10)
            const yesterdayFull = new Date(todayFull)
            yesterdayFull.setDate(yesterdayFull.getDate() - 1)
            // const yesterday = yesterdayFull.toISOString().slice(0, 10)
            const tomorrowFull = new Date(todayFull)
            tomorrowFull.setDate(tomorrowFull.getDate() + 1)
            const tomorrow = tomorrowFull.toISOString().slice(0, 10)

            const getStories = await this.roomService.getAllStories(
                eventId,
                today,
                tomorrow,
            )
            return res.json(getStories.rows)
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    detailInfo = async (req: Request, res: Response) => {
        try {
            const eventId = parseInt(req.params.id)
            const todayFull = new Date()
            const today = todayFull.toISOString().slice(0, 10)
            const yesterdayFull = new Date(todayFull)
            yesterdayFull.setDate(yesterdayFull.getDate() - 1)
            // const yesterday = yesterdayFull.toISOString().slice(0, 10)
            const tomorrowFull = new Date(todayFull)
            tomorrowFull.setDate(tomorrowFull.getDate() + 1)
            const tomorrow = tomorrowFull.toISOString().slice(0, 10)

            const getInfo = await this.roomService.detailInfoToday(
                eventId,
                today,
                tomorrow,
            )
            return res.json(getInfo)
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    allRecords = async (req: Request, res: Response) => {
        try {
            const eventId = parseInt(req.params.id)

            // const today = req.params.today
            // const tmr = req.params.tmr

            const getAllRecords = await this.roomService.getAllRecordList(
                eventId,
            )
            return res.json(getAllRecords.rows)
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    getMessages = async (req: Request, res: Response) => {
        try {
            const eventId = parseInt(req.params.eventId)
            const messages = await this.roomService.getMessages(eventId)
            return res.json(messages)
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    startEnd = async (req: Request, res: Response) => {
        try {
            const eventId = parseInt(req.params.id)

            const getStartEnd = await this.roomService.getStartEndDate(eventId)

            const getDaysArray = function (s: Date, e: Date) {
                for (
                    var a = [], d = new Date(s);
                    d <= e;
                    d.setDate(d.getDate() + 1)
                ) {
                    a.push(new Date(d))
                }
                return a
            }
            let daylist = getDaysArray(
                new Date(getStartEnd.rows[0].start_date),
                new Date(getStartEnd.rows[0].end_date),
            )


            return res.json(daylist.map((v) => v))
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }
    postMessage = async (req: Request, res: Response) => {
        try {
            const userId = req.user.id
            const eventId = parseInt(req.params.eventId)
            const message = req.body.content
            return res.json(
                await this.roomService.postMessage(userId, eventId, message),
            )
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }
    getAllVerificationRecords = async (req: Request, res: Response) => {
        try {
            const eventId = parseInt(req.params.eventId)
            return res.json(
                await this.roomService.getAllVerificationRecords(eventId),
            )
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    getAllVerificationReports = async (req: Request, res: Response) => {
        try {
            const eventId = parseInt(req.params.eventId)
            return res.json(
                await this.roomService.getAllVerificationReports(eventId),
            )
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    postVerificationRecord = async (req: Request, res: Response) => {
        try {
            const userId = req.user!.id
            const eventId = parseInt(req.params.verificationId)
            const content = req.body.content
            return res.json(
                await this.roomService.postVerificationReport(
                    userId,
                    eventId,
                    content,
                ),
            )
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    rejectVerificationReport = async (req: Request, res: Response) => {
        try {
            const reportId = parseInt(req.params.reportId)
            return res.json(
                await this.roomService.rejectVerificationReport(reportId),
            )
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }

    acceptVerificationReport = async (req: Request, res: Response) => {
        try {
            const reportId = parseInt(req.params.reportId)
            return res.json(
                await this.roomService.acceptVerificationReport(reportId),
            )
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }
}
