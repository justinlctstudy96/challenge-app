import { ForumService } from '../services/forumService'
import { Request, Response } from 'express'
import jwtSimple from 'jwt-simple'
import jwt from '../jwt'
import { Bearer } from 'permit'
import { ILikeId } from '../models'

const permit = new Bearer({
    query: 'access_token',
})

export class ForumController {
    constructor(private forumService: ForumService) {}

    getPosts = async (req: Request, res: Response) => {
        const allPosts = await this.forumService.getAllPosts()
        if (allPosts) {
            return res.status(200).json(allPosts)
        }
        return res.status(500).json({ message: 'Internal Server Error' })
    }

    postPost = async (req: Request, res: Response) => {
        const token = permit.check(req)
        const payload = jwtSimple.decode(token, jwt.jwtSecret)
        const addedPost = await this.forumService.addPost(
            payload.id,
            req.body,
            req.file ? req.file : null,
        )
        if (addedPost) {
            return res.status(200).json({ message: 'Success' })
        }
        return res.status(500).json({ message: 'Internal Server Error' })
    }

    getComments = async (req: Request, res: Response) => {
        const postId = parseInt(req.params.id)
        const comments = await this.forumService.getCommentsById(postId)
        if (comments) {
            return res.status(200).json(comments)
        }
        return res.status(500).json({ message: 'Internal Server Error' })
    }

    postComment = async (req: Request, res: Response) => {
        const token = permit.check(req)
        const payload = jwtSimple.decode(token, jwt.jwtSecret)
        const addedCommentId = await this.forumService.addComment(
            payload.id,
            req.body,
        )
        if (addedCommentId) {
            return res.status(200).json({ message: 'Success' })
        }
        return res.status(500).json({ message: 'Internal Server Error' })
    }

    postLike = async (req: Request, res: Response) => {
        const token = permit.check(req)
        const payload = jwtSimple.decode(token, jwt.jwtSecret)
        const postId = parseInt(req.params.id)
        const likedPost = await this.forumService.postLikeById(
            payload.id,
            postId,
        )
        if (likedPost) {
            return res.status(200).json({ message: 'like success' })
        }
        return res.status(500).json({ message: 'Internal Server Error' })
    }

    deleteLike = async (req: Request, res: Response) => {
        const token = permit.check(req)
        const payload = jwtSimple.decode(token, jwt.jwtSecret)
        const postId = parseInt(req.params.id)
        const deletedLike = await this.forumService.deleteLikeById(
            payload.id,
            postId,
        )
        if (deletedLike) {
            return res.status(200).json({ message: 'unlike success' })
        }
        return res.status(500).json({ message: 'Internal Server Error' })
    }

    getLike = async (req: Request, res: Response) => {
        const token = permit.check(req)
        const payload = jwtSimple.decode(token, jwt.jwtSecret)
        const likesByUser: ILikeId[] = await this.forumService.getLikesByUser(payload.id)
        const likeIds = likesByUser.map(item => item.post_id)

        if(likesByUser){
            return res.status(200).json(likeIds)
        }
        return res.status(500).json({ message: 'Internal Server Error'})
        
    }
}
