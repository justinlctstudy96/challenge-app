import { Request, Response } from 'express'
import { UserService } from '../services/userService'
import jwtSimple from 'jwt-simple'
import jwt from '../jwt'
import { checkPassword } from '../hash'
import { logger } from '../logger'

export class AuthController {
    constructor(private userService: UserService) {}

    post = async (req: Request, res: Response) => {
        try {
            logger.log('debug', `${req.body.email} is trying to login`)
            const { email, password } = req.body
            const user = await this.userService.getUserByEmail(email)
            logger.log('debug', user)
            if (!user || !(await checkPassword(password, user.password))) {
                res.status(401).json({ message: 'Wrong Email/Password' })
                return
            }
            const payload = {
                id: user.id,
                email: user.email,
                // nick name not included since it may be changed not along with the token
            }
            const token = jwtSimple.encode(payload, jwt.jwtSecret)
            logger.log(
                'debug',
                `User: ${user.id} has logged in with token: ${token}`,
            )
            return res.json({
                token: token,
                user: {
                    id: user.id,
                    email: user.email,
                    nickname: user.nickname,
                    pro_pic: user.pro_pic
                },
            })
        } catch (e) {
            logger.log('error', e)
            return res.status(500).json({ msg: e.toString() })
        }
    }
}
