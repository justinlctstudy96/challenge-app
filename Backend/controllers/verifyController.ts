import { VerifyService } from '../services/verifyService'
import { Request, Response } from 'express'

export class VerifyController {
    constructor(private verifyService: VerifyService) {}

    uploadVerificationRecord = async (req: Request, res: Response) => {
        const uploadSuccess = await this.verifyService.addVerifyRecord(req.body, req.file)
        if(uploadSuccess){
            return res.status(200).json({ message: 'Successfully send' })
        }else{
            return res.status(500).json({ message: 'Already Verified'})
        }
    }

    uploadRecord = async (req: Request, res: Response) => {
        try {
            const eventId = parseInt(req.params.eventId)
            const userId = parseInt(req.params.userId)
            const todayFull = new Date()
            const today = todayFull.toISOString().slice(0, 10)
            const tomorrowFull = new Date(todayFull)
            tomorrowFull.setDate(tomorrowFull.getDate() + 1)
            const tomorrow = tomorrowFull.toISOString().slice(0, 10)

            const getHaveUpload = await this.verifyService.seeIfUploaded(
                userId,
                eventId,
                today,
                tomorrow,
            )
            return res.json(getHaveUpload.rows[0])
        } catch (e) {
            return res.status(500).json({ msg: e.toString() })
        }
    }
}
