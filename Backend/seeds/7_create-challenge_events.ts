import { Knex } from 'knex'

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('challenge_events').del()

    // Inserts seed entries
    await knex('challenge_events').insert([
        {
            challenge_id: 1,
            host_id: 1,
            status: 'doing',
            accommodation: 20,
            official: false,
            official_reward: '',
            start_date: '2021-06-19',
            end_date: '2021-07-19',
            comment: '',
            created_at: '2021-06-23 12:46:37.567749+08',
            updated_at: '2021-06-23 12:46:37.567749+08',
        },
        {
            challenge_id: 2,
            host_id: 2,
            status: 'doing',
            accommodation: 10,
            official: false,
            official_reward: '',
            start_date: '2021-06-25',
            end_date: '2021-07-23',
            comment: '',
            created_at: '2021-06-24 13:25:38.290713+08',
            updated_at: '2021-06-24 13:25:38.290713+08',
        },
        {
            challenge_id: 3,
            host_id: 3,
            status: 'doing',
            accommodation: 5,
            official: false,
            official_reward: '',
            start_date: '2021-06-20',
            end_date: '2021-07-10',
            comment: '',
            created_at: '2021-06-28 16:24:04.299864+08',
            updated_at: '2021-06-28 16:24:04.299864+08',
        },
        {
            challenge_id: 3,
            host_id: 1,
            status: 'ended',
            accommodation: 3,
            official: false,
            official_reward: '',
            start_date: '2021-06-08',
            end_date: '2021-06-28',
            comment: '',
            created_at: '2021-06-28 16:24:04.299864+08',
            updated_at: '2021-06-28 16:24:04.299864+08',
        },
        {
            challenge_id: 4,
            host_id: 1,
            status: 'doing',
            accommodation: 3,
            official: false,
            official_reward: '',
            start_date: '2021-06-28',
            end_date: '2021-07-05',
            comment: '',
            created_at: '2021-06-28 16:24:04.299864+08',
            updated_at: '2021-06-28 16:24:04.299864+08',
        },
    ])
}
