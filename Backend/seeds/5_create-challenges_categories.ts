import { Knex } from 'knex'

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('challenges_categories').del()

    // Inserts seed entries
    await knex('challenges_categories').insert([
        { challenge_id: 1, category_id: 5 },
        { challenge_id: 2, category_id: 4 },
        { challenge_id: 3, category_id: 3 },
        { challenge_id: 4, category_id: 1 },
    ])
}
