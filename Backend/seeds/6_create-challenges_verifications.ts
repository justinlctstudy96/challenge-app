import { Knex } from 'knex'

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('challenges_verifications').del()

    // Inserts seed entries
    await knex('challenges_verifications').insert([
        { challenge_id: 1, verification_id: 2 },
        { challenge_id: 2, verification_id: 2 },
        { challenge_id: 3, verification_id: 2 },
        { challenge_id: 4, verification_id: 2 },
    ])
}
