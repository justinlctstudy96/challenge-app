import { Knex } from 'knex'

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('challengers').del()

    // Inserts seed entries
    await knex('challengers').insert([
        {
            user_id: 1,
            event_id: 1,
            status: 'waiting',
            role: 'host',
            created_at: '2021-06-23 12:46:37.571233+08',
            updated_at: '2021-06-23 12:46:37.571233+08',
        },
        {
            user_id: 2,
            event_id: 1,
            status: 'waiting',
            role: 'participant',
            created_at: '2021-06-23 16:57:53.583973+08',
            updated_at: '2021-06-23 16:57:53.583973+08',
        },
        {
            user_id: 3,
            event_id: 1,
            status: 'waiting',
            role: 'participant',
            created_at: '2021-06-23 16:57:53.583973+08',
            updated_at: '2021-06-23 16:57:53.583973+08',
        },
        {
            user_id: 2,
            event_id: 2,
            status: 'waiting',
            role: 'host',
            created_at: '2021-06-23 12:46:37.571233+08',
            updated_at: '2021-06-23 12:46:37.571233+08',
        },
        {
            user_id: 1,
            event_id: 2,
            status: 'waiting',
            role: 'participant',
            created_at: '2021-06-23 16:57:53.583973+08',
            updated_at: '2021-06-23 16:57:53.583973+08',
        },
        {
            user_id: 3,
            event_id: 2,
            status: 'waiting',
            role: 'participant',
            created_at: '2021-06-23 16:57:53.583973+08',
            updated_at: '2021-06-23 16:57:53.583973+08',
        },
        {
            user_id: 3,
            event_id: 3,
            status: 'waiting',
            role: 'host',
            created_at: '2021-06-23 12:46:37.571233+08',
            updated_at: '2021-06-23 12:46:37.571233+08',
        },
        {
            user_id: 1,
            event_id: 4,
            status: 'waiting',
            role: 'host',
            created_at: '2021-06-23 12:46:37.571233+08',
            updated_at: '2021-06-23 12:46:37.571233+08',
        },
        {
            user_id: 2,
            event_id: 4,
            status: 'waiting',
            role: 'participant',
            created_at: '2021-06-23 16:57:53.583973+08',
            updated_at: '2021-06-23 16:57:53.583973+08',
        },
        {
            user_id: 3,
            event_id: 4,
            status: 'waiting',
            role: 'participant',
            created_at: '2021-06-23 16:57:53.583973+08',
            updated_at: '2021-06-23 16:57:53.583973+08',
        },
        {
            user_id: 1,
            event_id: 5,
            status: 'waiting',
            role: 'host',
            created_at: '2021-06-23 12:46:37.571233+08',
            updated_at: '2021-06-23 12:46:37.571233+08',
        },
        {
            user_id: 2,
            event_id: 5,
            status: 'waiting',
            role: 'participant',
            created_at: '2021-06-23 16:57:53.583973+08',
            updated_at: '2021-06-23 16:57:53.583973+08',
        },
        {
            user_id: 3,
            event_id: 5,
            status: 'waiting',
            role: 'participant',
            created_at: '2021-06-23 16:57:53.583973+08',
            updated_at: '2021-06-23 16:57:53.583973+08',
        },
    ])
}
