import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("categories").del();

    // Inserts seed entries
    await knex("categories").insert([
        { category: 'Health', official: true, color: '#F0CE1D', selected_color: '#251DF0'},
        { category: 'Study', official: true, color: '#597EDE', selected_color: '#DEB759'},
        { category: 'Lifestyle', official: true, color: '#1F9D3B', selected_color: '#9E202E'},
        { category: 'Sport', official: true, color: '#FFA800', selected_color: '#0065FF'},
        { category: 'Music', official: true, color: '#9D1F1F', selected_color: '#209E43'},
        { category: 'Others', official: true, color: '#CB59DE', selected_color: '#A1DE59'},
    ]);
};