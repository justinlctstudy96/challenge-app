import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("verifications").del();

    // Inserts seed entries
    await knex("verifications").insert([
        { method: "Button" },
        { method: "Camera" },
        { method: "Photo"},
        // { method: "Video" },
        // { method: "Location" },
        // { method: "Text" },
        // { method: "Dialog" },
    ]);
};
