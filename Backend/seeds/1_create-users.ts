import { Knex } from 'knex'
import { hashPassword } from '../hash'

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('users').del()

    // Inserts seed entries
    await knex('users').insert([
        {
            email: 'justin@gmail.com',
            password: await hashPassword('EJP@123'),
            nickname: 'Justin Lo',
            pro_pic: 'AdminJustin.jpeg',
        },
        {
            email: 'edward@gmail.com',
            password: await hashPassword('EJP@123'),
            nickname: 'Edward Wong',
            pro_pic: 'AdminEdward.jpeg',
        },
        {
            email: 'peter@gmail.com',
            password: await hashPassword('EJP@123'),
            nickname: 'Peter Chan',
            pro_pic: 'AdminPeter.jpeg',
        },
    ])
}
