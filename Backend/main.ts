import { Express } from 'express'
const express = require('express')

//import required modules
import dotenv from 'dotenv'
// import cors from 'cors'
import KnexFunction from 'knex'
import multer from 'multer'
import multerS3 from 'multer-s3'
import aws from 'aws-sdk'

import { UserService } from './services/userService'
import { AuthController } from './controllers/authController'
import { UserController } from './controllers/userController'
import { ChallengeService } from './services/challengeService'
import { ChallengeController } from './controllers/challengeController'
import { LobbyService } from './services/lobbyService'
import { LobbyController } from './controllers/lobbyController'
import { RoomService } from './services/roomService'
import { RoomController } from './controllers/roomController'
import { VerifyController } from './controllers/verifyController'
import { VerifyService } from './services/verifyService'
import { ForumService } from './services/forumService'
import { ForumController } from './controllers/forumController'
import { logger } from './logger'
import { Message } from './services/models'

dotenv.config()

//Configure multer
const s3 = new aws.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: 'ap-southeast-1',
})

export const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'challengeyou',
        metadata: (req, file, cb) => {
            cb(null, { fieldName: file.fieldname })
        },
        key: (req, file, cb) => {
            cb(
                null,
                `${file.fieldname}-${Date.now()}.${
                    file.mimetype.split('/')[1]
                }`,
            )
        },
    }),
})

const knexfile = require('./knexfile')
const configEnv = process.env.NODE_ENV || 'development'
// const configEnv = process.env.NODE_ENV || 'production'
export const knex = KnexFunction(knexfile[configEnv])

const app: Express = express()

export const userService = new UserService(knex)
export const authController = new AuthController(userService)
export const userController = new UserController(userService)
export const challengeService = new ChallengeService(knex)
export const challengeController = new ChallengeController(challengeService)
export const lobbyService = new LobbyService(knex)
export const lobbyController = new LobbyController(lobbyService)
export const roomService = new RoomService(knex)
export const roomController = new RoomController(roomService)
export const verifyService = new VerifyService(knex)
export const verifyController = new VerifyController(verifyService)
export const forumService = new ForumService(knex)
export const forumController = new ForumController(forumService)

import http from 'http'
import SocketIO from 'socket.io'

const server = http.createServer(app)
export const io = new SocketIO.Server(server)

io.on('connection', function (socket) {
    socket.on('joinRoom', (eventId) => {
        socket.join(eventId)
        socket.on('chatMessage', (message: Message) => {
            io.to(eventId).emit('chatMessage', message)
        })
    })
})

app.use(express.urlencoded({ extended: false }))
app.use(express.json())

import { routes } from './routes'

app.use('/uploads', express.static('uploads'))
app.use('/', routes)

const PORT = 8080

server.listen(PORT, () => {
    logger.log('info', `Listening at http://localhost:${PORT}/`)
    if (process.env.CI) {
        process.exit(0)
    }
})
