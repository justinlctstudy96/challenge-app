export interface User {
    id: number;
    email: string;
    password: string;
}

export interface ISignUpInfo {
    email: string,
    password: string,
    nickname: string
}

export interface IAddedUserInfo {
    id: number,
    email: string,
    nickname: string,
    pro_pic: string,
}

export interface IPersonalEvent {
    id: number,
    status: string,
    start_date: string,
    end_date: string,
    user_id: number,
    role: string,
    title: string,
    rule: string
    persistence: boolean,
    consecutive: boolean,
    sections:string[],
    times: number,
    days: number,
    cycles: number,
    category_id: number,
    color:string,
    selected_color:string
}

export interface ILikeId {
    post_id: number
}

declare global{
    namespace Express{
        interface Request{
            user: User
        }
    }
}