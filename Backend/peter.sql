SELECT * FROM challenge_events;
SELECT * FROM challenges;
SELECT * FROM challengers;
SELECT * FROM challenges_categories;
SELECT * FROM challenges_verifications;
SELECT * FROM users;
SELECT * FROM categories;
SELECT * FROM posts;
SELECT * FROM posts_likes;
SELECT * FROM comments;


SELECT id, challenge_id, status, start_date, end_date FROM challenge_events;

SELECT user_id, event_id, role FROM challengers WHERE user_id = 18;

SELECT id, title, rule, persistence, consecutive, times, days, cycles FROM challenges;


-- select all participated events
SELECT challenge_events.id,  challenge_events.status, challenge_events.start_date, challenge_events.end_date,
challengers.user_id, challengers.role,
challenges.title, challenges.rule, challenges.persistence, challenges.consecutive,challenges.sections, challenges.times, challenges.days, challenges.cycles,
challenges_categories.category_id, 
categories.color, categories.selected_color
FROM challenge_events
INNER JOIN challengers ON challenge_events.id = challengers.event_id 
INNER JOIN challenges ON challenge_events.challenge_id = challenges.id
INNER JOIN challenges_categories ON challenge_events.challenge_id = challenges_categories.challenge_id
INNER JOIN categories ON challenges_categories.category_id = categories.id
WHERE user_id = 29 AND challenge_events.status = 'pending';


-- select all posts with likes and comments
SELECT postswithuser.*, withlikecount.likes, withcommentcount.comments
FROM
(SELECT posts.id, posts.category_id, posts.user_id, posts.content, posts.photo, posts.created_at,
users.nickname, users.pro_pic
FROM posts
LEFT JOIN users ON posts.user_id = users.id) AS postswithuser
LEFT JOIN
(SELECT posts.id, COUNT(posts_likes.id) AS likes
FROM posts
LEFT JOIN posts_likes ON posts.id = posts_likes.post_id
GROUP BY posts.id) AS withlikecount
ON postswithuser.id = withlikecount.id
LEFT JOIN
(SELECT posts.id, COUNT(comments.id) AS comments
FROM posts
LEFT JOIN comments ON posts.id = comments.post_id
GROUP BY posts.id) AS withcommentcount
ON postswithuser.id = withcommentcount.id
ORDER BY postswithuser.created_at DESC;




-- select comments by postId
SELECT comments.post_id, comments.user_id, comments.comment, comments.created_at AS comment_created_at,
users.nickname, users.pro_pic
FROM comments
LEFT JOIN users ON comments.user_id = users.id
WHERE post_id = 2;


SELECT post_id FROM posts_likes WHERE user_id = 42