import {createStore, combineReducers, compose, applyMiddleware} from 'redux';
import {LoadingState, loadingReducer} from './redux/loading/reducer';
import {LoadingActions} from './redux/loading/actions';
// import {logger} from 'redux-logger';  Can ignore 

//createRoom Form
import {
    CreateChallengeScreenState,
    createChallengeReducer,
} from './redux/challengeForm/reducers';

//Journal
import {journalReducer} from './redux/challengeJournal/reducers';
import {IJournalScreenState} from './redux/challengeJournal/state';
import {IJournalScreenActions} from './redux/challengeJournal/actions';

//room Verification
import {
    ICamImg,
    camImgReducer,
    btnStatusReducer,
    IbtnStatus,
} from './redux/roomVerification/reducers';
import {camImgAction, pressBtnAction} from './redux/roomVerification/actions';

//roomStory related
import {
    allInfoReducer,
    allRecordsReducer,
    allStoriesReducer,
    allUserReducer,
    IStoryNum,
    IUploadStatus,
    preventUploadReducer,
    storyNumReducer,
} from './redux/roomStory/reducer';
import {PreventUploadAction, StoryNumAction} from './redux/roomStory/actions';

//challenge related
import {
    allDateReducer,
    challengeRoomReducer,
    ChallengeRoomScreenState,
} from './redux/challengeRoom/reducers';

//user related
import {IUserState} from './redux/user/state';
import {userReducers} from './redux/user/reducers';
import {IUserActions} from './redux/user/actions';

//forum related
import {IForumState} from './redux/forum/state';
import {forumReducers} from './redux/forum/reducers';
import {IForumActions} from './redux/forum/actions';

//chat room related
import {chatRoomReducer, ChatRoomState} from './redux/chatRoom/reducers';
import {ChatRoomActions} from './redux/chatRoom/actions';

//all verification records details
import {verificationRecordsReducer, RoomVerificationRecordsState} from './redux/roomStory/reducer'
import {RoomVerificationRecordsActions} from './redux/roomStory/actions'

//challenge lobby related

import thunk, {ThunkDispatch as OldThunkDispatch} from 'redux-thunk';

export interface RootState {
    loading: LoadingState;
    storyId: IStoryNum;
    camImg: ICamImg;
    createChallengeScreen: CreateChallengeScreenState;
    user: IUserState;
    challengeRoomScreen: ChallengeRoomScreenState;
    btnStatus: IbtnStatus;
    journalScreen: IJournalScreenState;
    forum: IForumState;
    userList: any;
    allStories: any;
    allInfo: any;
    allDate: any;
    allRecords: any;

    preventUpload: IUploadStatus;
    chatRoom: ChatRoomState;

    roomVerificationRecordsState: RoomVerificationRecordsState;
}

export type RootActions =
    | LoadingActions
    | StoryNumAction
    | IUserActions
    | IForumActions
    | camImgAction
    | pressBtnAction
    | PreventUploadAction
    | ChatRoomActions
    | IJournalScreenActions
    | RoomVerificationRecordsActions;

const reducer = combineReducers<RootState>({
    loading: loadingReducer,
    storyId: storyNumReducer,
    preventUpload: preventUploadReducer,
    camImg: camImgReducer,
    createChallengeScreen: createChallengeReducer,
    user: userReducers,
    forum: forumReducers,
    challengeRoomScreen: challengeRoomReducer,
    btnStatus: btnStatusReducer,
    userList: allUserReducer,
    allStories: allStoriesReducer,
    allInfo: allInfoReducer,
    journalScreen: journalReducer,
    allDate: allDateReducer,
    allRecords: allRecordsReducer,
    chatRoom: chatRoomReducer,
    roomVerificationRecordsState: verificationRecordsReducer
});

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
    }
}

// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export type ThunkDispatch = OldThunkDispatch<RootState, null, RootActions>;

export const store = createStore<RootState, RootActions, {}, {}>(
    reducer,
    compose(applyMiddleware(thunk), 
    // applyMiddleware(logger)
    ),
);
