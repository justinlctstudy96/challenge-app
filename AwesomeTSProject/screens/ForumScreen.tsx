import React, {useEffect, useState} from 'react';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';

import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../store';
import {useNavigation} from '@react-navigation/native';
import ForumCategories from './components/ForumCategories';
import Icon from 'react-native-vector-icons/Ionicons';
import Posts from './components/ForumPosts';
import {fetchAllPosts, fetchLikes} from '../redux/forum/thunks';
import LinearGradient from 'react-native-linear-gradient';

function ForumScreen() {
    const userId = useSelector((state: RootState) => state.user.user?.id);
    const dispatch = useDispatch();
    const navigation = useNavigation();

    const isAuthenticated = useSelector(
        (state: RootState) => state.user.isAuthenticated,
    );

    useEffect(() => {
        if (!isAuthenticated) {
            navigation.navigate('Login');
        }
        dispatch(fetchAllPosts());
        dispatch(fetchLikes());
    }, [isAuthenticated, navigation]);

    const createPostBtn = () => {
        navigation.navigate('CreatePost');
    };

    return (
        <SafeAreaView style={styles.background}>
            <LinearGradient
                colors={['rgba(204, 255, 102, 0.38)', '#313131']}
                style={styles.linear}
            />
            {/* <LoadingSpinner /> */}
            <View style={styles.forumContainer}>
                <View style={styles.forumTitleContainer}>
                    <Text style={styles.forumTitle}>Forum</Text>
                </View>
                <View style={styles.catcat}>
                    <ForumCategories />
                </View>
            </View>
            <ScrollView
                style={styles.container}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}>
                <View style={styles.postsBlockContainer}>
                    <View style={styles.postsHeader}>
                        <View style={styles.postsTitleContainer}>
                            <Text style={styles.postsTitle}>Posts</Text>
                        </View>
                    </View>
                    <Posts />
                </View>
            </ScrollView>
            <TouchableOpacity
                style={styles.createPostButtonContainer}
                onPress={createPostBtn}>
                <LinearGradient
                    colors={['#1F9D3B', '#CCFF66']}
                    style={styles.linearInsideBtn}
                />
                <Icon name="pencil-outline" size={30} color="#FFFFFF" />
            </TouchableOpacity>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
    },

    background: {
        backgroundColor: '#313131',
        height: '100%',
    },
    linear: {
        width: '100%',
        height: '20%',
        position: 'absolute',
        top: 0,
    },
    linearInsideBtn: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        borderRadius: 30,
        top: 0,
    },
    //styles for posts
    forumContainer: {
        display: 'flex',
        flexDirection: 'column',

        width: '100%',
    },
    forumTitleContainer: {
        padding: 15,
    },
    catcat: {},
    forumTitle: {
        fontSize: 25,
        fontWeight: '500',
        textAlign: 'left',
        color: '#FFFFFF',
    },

    //Styles for posts
    postsBlockContainer: {
        width: '100%',
    },
    postsHeader: {
        padding: 10,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },
    postsTitleContainer: {
        justifyContent: 'center',
    },
    postsTitle: {
        fontSize: 25,
        fontWeight: '500',
        textAlign: 'left',
        color: '#FFFFFF',
    },
    //Styles for create post button
    createPostButtonContainer: {
        position: 'absolute',
        bottom: 10,
        right: 10,
        alignSelf: 'flex-end',
        height: 50,
        width: 50,
        borderRadius: 40,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#ccff66',
    },
});

export default ForumScreen;
