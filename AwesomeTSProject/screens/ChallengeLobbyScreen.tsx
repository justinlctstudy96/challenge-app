import React, {useEffect, useState} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';
import {RootState} from '../store';
import {useDispatch, useSelector} from 'react-redux';
import LobbyEventsList from './components/LobbyEventsList';
import LobbyChallengesList from './components/LobbyChallengesList';

import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    TouchableWithoutFeedback,
    useColorScheme,
    View,
    Keyboard,
} from 'react-native';
import {withSafeAreaInsets} from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';
import {Category} from '../redux/challengeForm/reducers';
import {
    fetchCategories,
    fetchAllCategories,
    resetCreateChallengeScreen,
} from '../redux/challengeForm/actions';
import LoadingSpinner from './components/LoadingSpinner';

export default function ChallengeLobbyScreen({route}) {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const [useSavedFilter, setSavedFilter] = useState<boolean>(false);
    const [useFilter, setUseFilter] = useState<boolean>(true);
    const [useOrder, setUseOrder] = useState<boolean>(false);
    const [filterCategories, setFilterCategories] = useState<number[]>([]);
    const [orderedBy, setOrderedBy] = useState('');
    // const [allCategories, setAllCategories] = useState<Category[]>([]);
    const [officialCategories, setOfficialCategories] = useState<Category[]>(
        [],
    );
    const [searchInput, setSearchInput] = useState('');
    const [showChallenges, setShowChallenges] = useState(
        route.params?.showChallenges ? true : false,
    );

    const allCategories = useSelector(
        (state: RootState) => state.createChallengeScreen.allCategories,
    );
    
    const eventRoomNavigate = (eventId: number) => {
        navigation.navigate('ChallengeRoom', {eventId: eventId});
    };

    const getCategories = async () => {
        // dispatch({type: 'LOADING_SPINNER_START'});
        // setAllCategories((await fetchCategories()).rows);
        dispatch(fetchAllCategories());
        let officialCategoriesArray: Category[] = [];
        allCategories.map(category => {
            if (category.official === true) {
                officialCategoriesArray.push(category);
            }
        });
        setOfficialCategories(officialCategoriesArray);
        // dispatch({type: 'LOADING_SPINNER_STOP'});
    };
    useEffect(() => {
        // setUseFilter(true)
        // getCategories();
        dispatch(fetchAllCategories());
        setShowChallenges(route.params?.showChallenges);
    }, [route.params?.showChallenges]);

    // useEffect(() => {
    //     getCategories();

    // }, [allCategories])

    const CategorySelection = () => {
        let categoryButtons = [];
        allCategories.map(category => {
            if (!filterCategories.includes(category.id)) {
                categoryButtons.push(
                    <View style={styles.optionalCategory}>
                        <TouchableOpacity
                            onPress={() => {
                                setFilterCategories([
                                    ...filterCategories,
                                    category.id,
                                ]);
                                // setCategorySelect(false)
                            }}>
                            <Text>{category.category}</Text>
                        </TouchableOpacity>
                    </View>,
                );
            }
        });
        // categoryButtons.push(
        //     <View style={styles.optionalCategory}>
        //         <TouchableOpacity onPress={() => {}}>
        //             <Text>Other</Text>
        //         </TouchableOpacity>
        //     </View>,
        // );

        return (
            <View style={{flexDirection: 'row'}}>
                <ScrollView horizontal={true}>
                    <View
                        style={styles.categorySelection}
                        onStartShouldSetResponder={() => true}>
                        {categoryButtons}
                    </View>
                </ScrollView>
            </View>
        );
    };

    const DismissKeyboard = ({children}: any) => {
        return (
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                {children}
            </TouchableWithoutFeedback>
        );
    };

    const SelectedCategories = () => {
        let selectedCategoriesBlocks: any = [];
        filterCategories.map(categoryId => {
            let categoryName = '';
            allCategories.map(category => {
                if (category.id === categoryId) {
                    categoryName = category.category;
                }
            });
            selectedCategoriesBlocks.push(
                <View style={styles.category}>
                    <TouchableOpacity
                        onPress={() => {
                            setFilterCategories(
                                filterCategories.filter(
                                    id => id !== categoryId,
                                ),
                            );
                        }}>
                        <Text>{categoryName}</Text>
                    </TouchableOpacity>
                </View>,
            );
        });
        return (
            <View style={{flexDirection: 'row'}}>
                {selectedCategoriesBlocks}
            </View>
        );
    };

    return (
        // <DismissKeyboard>
        <View style={styles.background}>
            <LinearGradient
                colors={['rgba(204, 255, 102, 0.38)', '#313131']}
                style={styles.gradient}
            />
            <SafeAreaView>
                <LoadingSpinner />
                <View
                    style={{
                        height: '7.0%',
                        width: '100%',
                        alignItems: 'center',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                    }}>
                    <View style={{width: '25%'}}></View>
                    <View style={{width: '50%'}}>
                        <Text
                            style={{
                                color: 'white',
                                fontSize: 20,
                                alignSelf: 'center',
                                fontWeight: 'bold',
                            }}>
                            {showChallenges ? 'Challenges' : 'Challenge Rooms'}
                        </Text>
                    </View>
                    <View style={{width: '25%'}}>
                        <View>
                            <TouchableOpacity
                                onPress={() => {
                                    setShowChallenges(!showChallenges);
                                }}>
                                <Text
                                    style={{
                                        alignSelf: 'flex-end',
                                        color: 'white',
                                        textAlign: 'center',
                                    }}>
                                    {showChallenges ? 'Rooms' : 'Challenges'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <View>
                    <View style={styles.searchContainer}>
                        <TouchableOpacity
                            onPress={() => {
                                setUseFilter(true);
                                getCategories();
                            }}>
                            <Icon name="apps" color={'white'} size={30} />
                        </TouchableOpacity>
                        <View style={styles.searchInputContainer}>
                            <TextInput
                                style={styles.searchInput}
                                value={searchInput}
                                onChangeText={setSearchInput}
                            />
                            <TouchableOpacity
                                style={{
                                    width: 100,
                                    alignItems: 'flex-start',
                                }}>
                                <Icon
                                    name="search"
                                    color={'white'}
                                    size={25}
                                    style={{marginRight: 20}}
                                />
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity
                            onPress={() => {
                                // setUseOrder(!useOrder);
                                setUseFilter(false);
                            }}>
                            <Icon name="list" color={'white'} size={30} />
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            justifyContent: 'space-between',
                            flexDirection: 'row',
                        }}>
                        <View
                            style={[
                                styles.savedButtonContainer,
                                {width: '10%'},
                            ]}>
                            <View
                                style={
                                    useSavedFilter
                                        ? styles.saved
                                        : styles.unSaved
                                }>
                                <TouchableOpacity
                                    onPress={() => {
                                        setSavedFilter(!useSavedFilter);
                                    }}>
                                    <Icon
                                        name="heart"
                                        color={'white'}
                                        size={30}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View
                            style={{
                                flexDirection: 'row',
                                alignSelf: 'flex-end',
                                alignContent: 'center',
                                width: '80%',
                            }}>
                            {useFilter ? (
                                <View
                                    style={{
                                        flex: 1,
                                        flexDirection: 'column',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                    <Text style={{color: '#ccff66'}}>
                                        Categories:{' '}
                                    </Text>
                                    <View style={{flexDirection: 'row'}}>
                                        <ScrollView
                                            style={{
                                                borderRadius: 30,
                                            }}
                                            horizontal={true}
                                            showsVerticalScrollIndicator={false}
                                            showsHorizontalScrollIndicator={
                                                false
                                            }>
                                            <View
                                                style={{flexDirection: 'row'}}>
                                                <SelectedCategories />
                                                <CategorySelection />
                                            </View>
                                        </ScrollView>
                                    </View>
                                </View>
                            ) : (
                                <View
                                    style={{
                                        flex: 1,
                                        justifyContent: 'space-between',

                                        alignItems: 'center',
                                    }}>
                                    <Text style={{color: '#ccff66'}}>
                                        Ordered by:{' '}
                                    </Text>

                                    <ScrollView
                                        horizontal={true}
                                        showsVerticalScrollIndicator={false}
                                        showsHorizontalScrollIndicator={false}>
                                        <TouchableOpacity
                                            style={orderedBy==='Likes'?styles.selected:styles.filter}
                                            onPress={() => {
                                                orderedBy === 'Likes'
                                                    ? setOrderedBy('')
                                                    : setOrderedBy('Likes');
                                            }}>
                                            <Text>Likes</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            style={orderedBy==='Days'?styles.selected:styles.filter}
                                            onPress={() => {
                                                orderedBy === 'Days'
                                                    ? setOrderedBy('')
                                                    : setOrderedBy('Days');
                                            }}>
                                            <Text>Days</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            style={orderedBy==='Challengers'?styles.selected:styles.filter}
                                            onPress={() => {
                                                orderedBy === 'Challengers'
                                                    ? setOrderedBy('')
                                                    : setOrderedBy(
                                                          'Challengers',
                                                      );
                                            }}>
                                            <Text>Challengers</Text>
                                        </TouchableOpacity>
                                    </ScrollView>
                                </View>
                            )}
                        </View>
                        <View style={styles.createChallenge}>
                            <TouchableOpacity
                                style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                                onPress={() => {
                                    navigation.navigate('CreateChallenge');
                                    dispatch(resetCreateChallengeScreen())
                                }}>
                                <Icon
                                    name="add-circle"
                                    color={'#D1EF0F'}
                                    size={40}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{height: '80%'}}>
                    {/* <LoadingSpinner /> */}
                    {showChallenges ? (
                        <LobbyChallengesList
                            searchText={searchInput}
                            filter={filterCategories}
                            savedFilter={useSavedFilter}
                            orderedBy={orderedBy}
                        />
                    ) : (
                        <LobbyEventsList
                            searchText={searchInput}
                            filter={filterCategories}
                            savedFilter={useSavedFilter}
                            orderedBy={orderedBy}
                        />
                    )}
                </View>
            </SafeAreaView>
        </View>
        // </DismissKeyboard>
    );
}


// Can move to different file
const styles = StyleSheet.create({
    background: {
        // justifyContent: 'center',
        // alignItems: 'stretch',
        backgroundColor: '#313131',
        height: '100%',
        padding: 20,
    },
    searchContainer: {
        // flex:1,
        // height: '4%',
        alignItems: 'center',
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    searchInputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#6d6d6d71',
        height: 40,
        borderRadius: 30,
        width: '80%',
        justifyContent: 'space-between',
        borderWidth: 2,
        padding: 1,
        borderColor: '#ccff66',
    },
    searchInput: {
        marginLeft: 10,
        width: '86%',
    },
    login: {
        backgroundColor: 'yellow',
    },
    gradient: {
        width: '120%',
        height: '20%',
        position: 'absolute',
        top: 0,
        left: 0,
    },
    createChallenge: {
        width: '10%',
        height: 40,
        borderRadius: 40,
        // backgroundColor: 'black',
        // borderWidth: 2,
        borderColor: '#D1EF0F',
        alignItems: 'center',
        justifyContent: 'center',
    },
    shortCut: {
        color: 'white',
    },
    category: {
        padding: 5,
        borderRadius: 30,

        backgroundColor: '#ccff66',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 0,
        marginLeft: 2,
        // width: '80%',
    },
    'category:nth-child(3n+1)': {
        backgroundColor: 'black',
    },
    optionalCategory: {
        borderColor: '#D1EF0F',
        borderRadius: 30,
        padding: 5,

        backgroundColor: '#e7e7e799',
        justifyContent: 'center',
        alignItems: 'center',

        marginRight: 0,
        marginLeft: 2,
    },
    categorySelection: {
        flexDirection: 'row',
    },
    savedButtonContainer: {
        width: '10%',
        height: 40,
        // borderRadius: 40,
        // backgroundColor: 'red',

        justifyContent: 'center',
        alignItems: 'center',
    },
    unSaved: {
        borderColor: 'white',
        borderWidth: 1,
        width: 35,
        height: 35,
        borderRadius: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(49,49,49,0.85)',
    },
    saved: {
        width: 35,
        height: 35,
        borderRadius: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'red',
    },
    filter: {
        padding: 5,
        backgroundColor: '#e7e7e799',
        borderRadius: 20,
        marginRight: 3,
    },
    selected: {
        padding: 5,
        backgroundColor: 'cyan',
        borderRadius: 20,
        marginRight: 3,
    },
});
