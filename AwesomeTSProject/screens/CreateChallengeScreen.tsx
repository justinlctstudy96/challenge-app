import React, {useEffect, useState} from 'react';
import {
    Text,
    View,
    StyleSheet,
    SafeAreaView,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    TimePickerAndroidDismissedAction,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';
import CreateChallengeForm from './components/CreateChallengeForm';
import ChallengeSummaryBlock from './components/ChallengeSummaryBlock';
import CreateChallengeEventForm from './components/CreateChallengeEventForm';
import {useSelector, useDispatch} from 'react-redux';
import {RootState} from '../store';
import {
    fetchChallengeDetails,
    resetCreateChallengeScreen,
} from '../redux/challengeForm/actions';
import {baseProps} from 'react-native-gesture-handler/lib/typescript/handlers/gestureHandlers';
import LoadingSpinner from './components/LoadingSpinner';
import Icon from 'react-native-vector-icons/Ionicons';
// import {challengeFormDetailsStates} from './components/CreateChallengeForm'

export const DismissKeyboard = ({children}: any) => {
    return (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            {children}
        </TouchableWithoutFeedback>
    );
};

function CreateChallengeScreen() {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const openChallengeForm = useSelector(
        (state: RootState) => state.createChallengeScreen.openChallengeForm,
    );
    const openChallengeEventForm = useSelector(
        (state: RootState) =>
            state.createChallengeScreen.openChallengeEventForm,
    );
    const challengeId = useSelector(
        (state: RootState) => state.createChallengeScreen.challengeId,
    );
    const challengeDetails = useSelector(
        (state: RootState) => state.createChallengeScreen.challengeDetails,
    );

    useEffect(() => {
        dispatch(fetchChallengeDetails(challengeId));
    }, [openChallengeForm,challengeId,dispatch]);

    return (
        // <DismissKeyboard>
        <SafeAreaView style={styles.background}>
            <LoadingSpinner />
            <LinearGradient
                colors={['rgba(204, 255, 102, 0.38)', '#313131']}
                style={styles.gradient}
            />
            <ScrollView
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                style={styles.container}
                onStartShouldSetResponder={() => true}>
                <View
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'space-between',
                        alignItems: 'baseline',
                        flexWrap: 'wrap',
                    }}>
                    <TouchableOpacity
                        style={{width: 50}}
                        // style={styles.back}
                        onPress={() => { // Write in function 
                            dispatch(resetCreateChallengeScreen());
                            navigation.navigate('Challenges');
                        }}>
                        <Icon
                            name="chevron-back-outline"
                            size={30}
                            color="#FFFFFF"
                        />
                    </TouchableOpacity>
                    <View
                        style={{
                            width: '100%',
                            borderColor: 'transparent',
                            borderBottomColor: '#ccff66',
                            borderWidth: 1,
                            marginBottom: 10,
                        }}>
                        <Text style={styles.title}>
                            Challenge Template
                            {/* {route.params.eventId} */}
                        </Text>
                    </View>
                </View>
                {openChallengeForm ? (
                    <CreateChallengeForm challengeId={challengeId} />
                ) : (
                    <View>
                        <ChallengeSummaryBlock
                            challengeDetails={challengeDetails}
                        />
                        {openChallengeEventForm ? (
                            <CreateChallengeEventForm
                                challengeId={challengeId}
                                persistence={challengeDetails.persistence}
                                days={challengeDetails.days}
                                cycles={challengeDetails.cycles}
                            />
                        ) : null}
                    </View>
                )}
            </ScrollView>
        </SafeAreaView>
        // </DismissKeyboard>
    );
}

const styles = StyleSheet.create({
    background: {backgroundColor: '#313131'},
    container: {
        // backgroundColor: '#313131',
        height: '100%',

        // padding: 20,
    },
    back: {
        // marginTop: 30,
        color: 'white',
    },
    gradient: {
        width: '120%',
        height: '20%',
        position: 'absolute',
        top: 0,
        left: 0,
    },
    title: {
        marginBottom: 10,
        fontStyle: 'normal',
        fontWeight: '500',
        color: 'white',
        fontSize: 23,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },
});

export default CreateChallengeScreen;
