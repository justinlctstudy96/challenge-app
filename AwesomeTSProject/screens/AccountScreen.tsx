import React, {Profiler, useState, useEffect} from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    FlatList,
    useColorScheme,
    View,
    Image,
    TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import {useNavigation} from '@react-navigation/native';

import * as Progress from 'react-native-progress';

import PopularChallenges from './components/HomePopularChallenges';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../store';
import AccountButtons from './components/AccountButtons';
import LoadingSpinner from './components/LoadingSpinner';
import {config} from '../redux/config';
// import PersonalChallenges from './components/JournalPersonalChallenges';
import PersonalPastChallenges from './components/PersonalPastChallenges';
import ChallengeCalendar from './components/JournalChallengeCalendar';
import ChallengeTimeline from './components/JournalTimeline';
// const { BarChart } = require('react-native-ios-charts');

// import { Radar } from 'react-chartjs-2';
// const pathJs = require('react-native-pathjs-charts')

// import { Radar } from 'react-native-pathjs-charts'

function AccountScreen() {
    const dispatch = useDispatch();
    const [openHistory, setOpenHistory] = useState(false);
    const userDetails = useSelector((state: RootState) => state.user.user);
    const navigation = useNavigation();
    const isAuthenticated = useSelector(
        (state: RootState) => state.user.isAuthenticated,
    );

    // let data = [{
    //   "speed": 74,
    //   "balance": 29,
    //   "explosives": 40,
    //   "energy": 40,
    //   "flexibility": 30,
    //   "agility": 25,
    //   "endurance": 44
    // }]

    // let options = {
    //   width: 290,
    //   height: 290,
    //   margin: {
    //     top: 20,
    //     left: 20,
    //     right: 30,
    //     bottom: 20
    //   },
    //   r: 150,
    //   max: 100,
    //   fill: "#2980B9",
    //   stroke: "#2980B9",
    //   animate: {
    //     type: 'oneByOne',
    //     duration: 200
    //   },
    //   label: {
    //     fontFamily: 'Arial',
    //     fontSize: 14,
    //     fontWeight: true,
    //     fill: '#34495E'
    //   }
    // }

    useEffect(() => {
        if (!isAuthenticated) {
            navigation.navigate('Login');
        }
    }, [isAuthenticated, navigation]);

    return (
        <SafeAreaView style={styles.background}>
            {/* <BarChart
          config={{
            dataSets: [{
              values: [1, 2, 3, 10],
              colors: ['green'],
              label: '2015',
            }, {
              values: [3, 2, 1, 5],
              colors: ['red'],
              label: '2014',
            }],
            labels: ['a', 'b', 'c', 'd'],
          }}
          style={styles.chart}
        /> */}
            <ScrollView
                style={styles.container}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}>
                {/* <LoadingSpinner /> */}
                {/* <Radar type={'radar'} data={data} options={options} /> */}
                <View style={styles.profileContainer}>
                    <View style={styles.profileRowContainer}>
                        <Image
                            style={styles.profilePicture}
                            source={{
                                uri: `${config.S3_BUCKET_URL}/${userDetails?.pro_pic}`,
                            }}
                        />
                        <View style={styles.profileNicknameContainer}>
                            <Text style={styles.profileNickname}>
                                {userDetails?.nickname
                                    ? userDetails?.nickname
                                    : 'null'}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.horizontalLine}></View>
                    {/* <View style={styles.profileRowContainer}>
                        <View style={styles.statusTitleContainer}>
                            <Text style={styles.statusTitle}>Status</Text>
                        </View>
                        <View style={styles.statusContentContainer}>
                            <Text style={styles.statusContent}>
                                I am single.
                            </Text>
                        </View>
                    </View> */}
                </View>
                <View style={styles.calendarHeader}>
                    <View style={styles.calendarTitleContainer}>
                        <Text style={styles.calendarTitle}>Calendar</Text>
                    </View>
                </View>
                <ChallengeCalendar />
                <ChallengeTimeline />
                <View style={styles.horizontalLine}></View>
                <TouchableOpacity
                    onPress={() => {
                        setOpenHistory(!openHistory);
                    }}>
                    <View style={styles.item}>
                        <View
                            style={[
                                styles.iconContainer /*  {backgroundColor:`${item.backgroundColor}`} */,
                            ]}>
                            <Icon name={'book'} size={30} color="#FFFFFF" />
                        </View>
                        <View style={styles.itemTextContainer}>
                            <Text style={styles.itemText}>Past Challenges</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                {openHistory ? <PersonalPastChallenges /> : null}
                <AccountButtons />
            </ScrollView>
        </SafeAreaView>
    );
}

// Can be in different file
const styles = StyleSheet.create({
    calendarContainer: {
        width: '100%',
    },
    calendarHeader: {
        padding: 10,
        paddingTop: 0,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },
    calendarTitleContainer: {
        justifyContent: 'center',
    },
    calendarTitle: {
        fontSize: 25,
        fontWeight: '500',
        textAlign: 'left',
        color: '#FFFFFF',
    },
    horizontalLine: {
        borderBottomWidth: 2,
        borderColor: '#ccff66',
        marginBottom: 15,
        marginTop: 10,
    },
    background: {
        backgroundColor: '#313131',
    },
    container: {
        backgroundColor: 'transparent',
        height: '100%',
        marginRight: 10,
        marginLeft: 10,
    },
    rowContainer: {
        flexDirection: 'row',
        width: '100%',
    },
    //styles for profile
    profileContainer: {},
    profileRowContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: '100%',
        padding: 5,
        flexWrap: 'wrap',
    },
    profilePicture: {
        width: 75,
        height: 75,
        borderRadius: 50,
        borderColor: '#ccff66',
        borderWidth: 1,
    },
    profileNicknameContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
    },
    profileNickname: {
        fontSize: 20,
        color: '#FFFFFF',
        fontWeight: '500',
    },

    progressbarContainer: {
        justifyContent: 'center',
        width: '70%',
    },
    badgeContainer: {
        width: '30%',
        paddingLeft: 5,
    },
    level: {
        color: '#FFFFFF',
    },
    //styles for statusbox
    statusTitleContainer: {
        width: '100%',
        padding: 5,
    },
    statusTitle: {
        color: '#FFFFFF',
        fontSize: 20,
        fontWeight: '500',
    },
    statusContentContainer: {
        borderRadius: 15,
        borderWidth: 1,
        borderColor: '#CCFF66',
        height: 70,
        width: '100%',
        padding: 5,
    },
    statusContent: {
        color: '#FFFFFF',
        fontSize: 15,
    },
    //styles for challenges
    challengesContainer: {
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
    },
    challengeTitleContainer: {
        padding: 10,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },
    challengeTitle: {
        fontSize: 25,
        fontWeight: '500',
        textAlign: 'left',
        color: '#FFFFFF',
    },

    //Styles for calendar
    settingButtonsContainer: {
        width: '100%',
    },
    item: {
        padding: 10,
        borderRadius: 10,
        backgroundColor: 'rgba(196,196,196,0.2)',
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginBottom: 5,
        /* add drop shadow */
        shadowColor: '#CCFF66',
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    },
    iconContainer: {
        borderRadius: 50,
        width: '20%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemTextContainer: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: 10,
        width: '80%',
    },
    itemText: {
        color: '#FFFFFF',
        textAlign: 'left',
        fontSize: 20,
    },
});

export default AccountScreen;
