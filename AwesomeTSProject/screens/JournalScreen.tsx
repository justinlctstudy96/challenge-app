import React, {useEffect, useState} from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    FlatList,
    useColorScheme,
    View,
    Image,
    TouchableOpacity,
} from 'react-native';

import ChallengeTimeline from './components/JournalTimeline';
import ChallengeCalendar from './components/JournalChallengeCalendar';

import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../store';
import {useNavigation} from '@react-navigation/native';
import PersonalChallenges from './components/JournalPersonalChallenges';
import LoadingSpinner from './components/LoadingSpinner';
import {fetchEventByDate, fetchEventDots} from '../redux/challengeJournal/thunks';
import LinearGradient from 'react-native-linear-gradient';
import { Calendar, DateObject } from 'react-native-calendars';

function JournalScreen() {
    const calendarMode = useSelector(
        (state: RootState) => state.journalScreen.calendarMode,
    );
    const ongoingChallenges = useSelector(
        (state: RootState) => state.journalScreen.userOngoingChallenges,
    );
    const userId = useSelector((state: RootState) => state.user.user?.id);
    const dispatch = useDispatch();
    const navigation = useNavigation();

    // No need to check here, check in loading screen
    const isAuthenticated = useSelector(
        (state: RootState) => state.user.isAuthenticated,
    );

    useEffect(() => {
        if (!isAuthenticated) {
            navigation.navigate('Login');
        }
        dispatch(fetchEventDots(userId));
        dispatch(fetchEventByDate(userId, (new Date()).toISOString().split('T')[0]))
    }, [isAuthenticated, navigation]);

    const updateDisplayMode = () => {
        dispatch({type: 'SWITCH_DISPLAY_MODE'});
    };

    return (
        <SafeAreaView style={styles.background}>
            <LinearGradient
                colors={['rgba(204, 255, 102, 0.38)', '#313131']}
                style={styles.linear}></LinearGradient>
            <ScrollView
                style={styles.container}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}>
                    
                {/* <LoadingSpinner /> */}
                {/* <View style={styles.friendContainer}>
                    <View style={styles.friendTitleContainer}>
                        <Text style={styles.friendTitle}>
                            Chat with friends
                        </Text>
                    </View>
                </View>  */}
                <View style={styles.challengesContainer}>
                    <View style={styles.challengeTitleContainer}>
                        <Text style={styles.challengeTitle}>
                            On going challenges
                        </Text>
                    </View>
                    <PersonalChallenges />
                </View>
                <View style={styles.calendarContainer}>
                    <View style={styles.calendarHeader}>
                        <View style={styles.calendarTitleContainer}>
                            <Text style={styles.calendarTitle}>
                                {/* {calendarMode ? 'Calendar' : 'Timeline'} */}
                                Calendar
                            </Text>
                        </View>
                        <View style={styles.calendarToggleContainer}>
                            {/* <TouchableOpacity
                                onPress={() => updateDisplayMode()}>
                                <Text style={styles.calendarToggle}>
                                    {calendarMode
                                        ? 'Timeline Mode'
                                        : 'Calendar Mode'}
                                </Text>
                            </TouchableOpacity> */}
                        </View>
                    </View>
                    {/* {calendarMode ? (
                        <ChallengeCalendar />
                    ) : (
                        <ChallengeTimeline />
                    )} */}
                    <ChallengeCalendar />
                    <ChallengeTimeline />
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    linear: {
        width: '100%',
        height: '20%',
        position: 'absolute',
        top: 0,
    },
    background: {
        backgroundColor: '#313131',
    },
    container: {
        backgroundColor: 'transparent',
        height: '100%',
        marginRight: 10,
        marginLeft: 10,
    },

    rowContainer: {
        flexDirection: 'row',
        width: '100%',
    },
    //styles for friend
    friendContainer: {
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
    },
    friendTitleContainer: {
        padding: 10,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },
    friendTitle: {
        fontSize: 25,
        fontWeight: '500',
        textAlign: 'left',
        color: '#FFFFFF',
    },
    //styles for challenges
    challengesContainer: {
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
    },
    challengeTitleContainer: {
        padding: 10,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },
    challengeTitle: {
        fontSize: 25,
        fontWeight: '500',
        textAlign: 'left',
        color: '#FFFFFF',
    },

    //Styles for calendar
    calendarContainer: {
        width: '100%',
    },
    calendarHeader: {
        padding: 10,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },
    calendarTitleContainer: {
        justifyContent: 'center',
    },
    calendarTitle: {
        fontSize: 25,
        fontWeight: '500',
        textAlign: 'left',
        color: '#FFFFFF',
    },
    calendarToggleContainer: {
        justifyContent: 'center',
    },
    calendarToggle: {
        fontSize: 15,
        fontWeight: '300',
        color: '#FFFFFF',
    },
});

export default JournalScreen;
