import React, {useState, useEffect, useRef} from 'react';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';
import {useTime} from 'react-timer-hook';
import StoryIconFlat from './components/StoryIconFlat';
import {getCurrentDate} from '../utils/getCurrentDate';
import {useSelector, useDispatch} from 'react-redux';
import {RootState} from '../store';
import CameraVerify from './components/CameraVerify';
import BtnVerify from './components/BtnVerify';
import LinearGradient from 'react-native-linear-gradient';
// import PercentageCircle from 'react-native-percentage-circle';
// const PercentageCircle = require('react-native-percentage-circle').default

import {
    Animated,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Button,
    Pressable,
    KeyboardAvoidingView,
    ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import {
    fetchAllDate,
    fetchEventDetails,
    fetchUserEventRelation,
    joinEvent,
} from '../redux/challengeRoom/actions';
import {ChallengeEventSummary} from '../redux/challengeLobby/reducers';
import {Category, Verification} from '../redux/challengeForm/reducers';
import {UserEventRelation} from '../redux/challengeRoom/reducers';
import SelectedCategoryBlocks from './components/SelectedCategoryBlocks';
// import ChatRoom from './ChatRoomScreen';
import {
    createChallengeEventSuccess,
    fetchCategories,
    fetchVerifications,
} from '../redux/challengeForm/actions';
import {
    fetchAllInfo,
    fetchAllRecordsAccordingToDate,
    fetchAllVerificationRecords,
    fetchReports,
    fetchRoomUser,
} from '../redux/roomStory/actions';
import {
    CycleVerificationRecords,
    IdetailInfo,
    VerificationReport,
} from '../redux/roomStory/reducer';
import LoadingSpinner from './components/LoadingSpinner';
import CircularProgress from './CircularProgress';
import VerificationReports from './components/VerificationReports';
import {config} from '../redux/config';
import {dateString} from './components/JournalPersonalChallenges';

const getRemainingDays = (endDate: Date) => {
    let remainingDays = Math.floor(
        (endDate.getTime() - new Date().getTime()) / (1000 * 3600 * 24),
    );
    if (remainingDays < 0) {
        return 0;
    } else {
        return remainingDays;
    }
};

function ChallengeRoomScreen({route}) {
    const dispatch = useDispatch();
    const [allCategories, setAllCategories] = useState<Category[]>([]);
    const [allVerifications, setAllVerifications] = useState<Verification[]>(
        [],
    );
    const [selectedVerification, setSelectedVerification] = useState('');
    const {seconds, minutes, hours} = useTime({});
    const navigation = useNavigation();
    const [todayDate, setTodayDate] = useState('');

    const [expand, setExpand] = useState(false);
    const [remainingDates, setRemainingDates] = useState(0);
    const img = useSelector((state: RootState) => state.camImg.camImg);

    const userDetails = useSelector((state: RootState) => state.user);

    // const allUsers = useSelector((state: RootState) => state.userList.allUser);

    const eventDetails: ChallengeEventSummary = useSelector(
        (state: RootState) => state.challengeRoomScreen.eventDetails,
    );

    const userRole: UserEventRelation = useSelector(
        (state: RootState) => state.challengeRoomScreen.userRole,
    );
    const detailInfo = useSelector(
        (state: RootState) => state.allInfo.detailInfo,
    );

    const getAllCategoriesVerifications = async () => {
        setAllCategories((await fetchCategories()).rows);
        setAllVerifications((await fetchVerifications()).rows);
    };

    const cycleDates = useSelector(
        (state: RootState) => state.roomVerificationRecordsState.cycleDates,
    );

    useEffect(() => {
        dispatch(fetchAllInfo(route.params.eventId));
        setTodayDate(getCurrentDate());
        getAllCategoriesVerifications();
        dispatch(fetchEventDetails(route.params.eventId));
        dispatch(fetchUserEventRelation(route.params.eventId));
        // dispatch(fetchAllDate(route.params.eventId));
        // dispatch(fetchAllRecordsAccordingToDate(route.params.eventId));
        // dispatch(fetchRoomUser(route.params.eventId));
        dispatch(fetchAllVerificationRecords(route.params.eventId));
        dispatch(fetchReports(route.params.eventId));
        allVerifications.map(verification => {
            if (verification.id === eventDetails.verifications[0]) {
                setSelectedVerification(verification.method);
            }
        });
        const endDate = new Date(eventDetails.end_date);
        setRemainingDates(Math.floor(getRemainingDays(endDate)));
        // alert(eventDetails.status+'status')
    }, [route.params.eventId]);


    const eventId = useSelector((state: RootState) => state.createChallengeScreen.eventId)

    useEffect(() => {
        dispatch(fetchAllInfo(eventId));
        setTodayDate(getCurrentDate());
        getAllCategoriesVerifications();
        dispatch(fetchEventDetails(eventId));
        dispatch(fetchUserEventRelation(eventId));
        // dispatch(fetchAllDate(route.params.eventId));
        // dispatch(fetchAllRecordsAccordingToDate(route.params.eventId));
        // dispatch(fetchRoomUser(route.params.eventId));
        dispatch(fetchAllVerificationRecords(eventId));
        dispatch(fetchReports(eventId));
        allVerifications.map(verification => {
            if (verification.id === eventDetails.verifications[0]) {
                setSelectedVerification(verification.method);
            }
        });
        const endDate = new Date(eventDetails.end_date);
        setRemainingDates(Math.floor(getRemainingDays(endDate)));
        // alert(eventDetails.status+'status')
    }, [eventId]);

    useEffect(() => {
        dispatch(createChallengeEventSuccess(route.params.eventId))
    },[route.params.eventId])

    const chatRoomNavigate = (eventId: number) => {
        navigation.navigate('ChatRoom', {eventId: eventId});
    };

    const allVerificationRecords: CycleVerificationRecords[] = useSelector(
        (state: RootState) =>
            state.roomVerificationRecordsState.allVerificationRecords,
    );

    // const scrollViewRef = useRef<ScrollView | null>(null);

    return (
        // <KeyboardAvoidingView
        //     behavior={Platform.OS === 'ios' ? 'padding' : null}
        //     keyboardVerticalOffset={Platform.OS === 'ios' ? 0 : 0}>
        <ImageBackground
            source={{
                uri: `${config.S3_BUCKET_URL}/background/${eventDetails.categories[0]}.jpg`,
            }}
            style={{width: '100%'}}
            // borderRadius={100}
            // style={{flex: 1, backgroundColor: 'rgba(0,0,0,0)'}}
        >
            <SafeAreaView style={styles.background}>
                {/* <LoadingSpinner /> */}
                <LinearGradient
                    colors={['rgba(204, 255, 102, 1)', 'rgba(49,49,49,0)']}
                    style={styles.linear}
                />
                <ScrollView
                    style={styles.mainContainer}
                    showsVerticalScrollIndicator={false}>
                    <View
                        style={{
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'baseline',
                            flexWrap: 'wrap',
                        }}>
                        <TouchableOpacity
                            onPress={() => {
                                navigation.navigate('Challenges');
                            }}>
                            <Icon
                                name="chevron-back-outline"
                                size={30}
                                color="#FFFFFF"
                            />
                        </TouchableOpacity>
                        <Text style={styles.title}>{eventDetails.title}</Text>
                        {/* <Text>{eventDetails.event_id}</Text> */}
                    </View>
                    {/* <View style={styles.category}>
                    <View style={styles.lifeStyle}>
                        <Text style={styles.text}>lifestyle</Text>
                    </View>
                    <View style={styles.successRate}>
                        <Text style={styles.text}>success rate</Text>
                    </View>
                    <View style={styles.additionCat}>
                        <Text style={styles.text}>addition category</Text>
                    </View>
                    </View> */}
                    <View style={styles.rowContainer1}>
                        <View style={{width: '50%'}}>
                            <ScrollView
                                showsVerticalScrollIndicator={false}
                                showsHorizontalScrollIndicator={false}>
                                <View>
                                    <SelectedCategoryBlocks
                                        selectedCategories={
                                            eventDetails.categories
                                        }
                                        allCategories={allCategories}
                                    />
                                </View>
                            </ScrollView>
                        </View>
                        <View style={styles.totalDays}>
                            <Text
                                style={{
                                    color: 'white',
                                    justifyContent: 'center',
                                    textAlign: 'center',
                                    fontSize: 17,
                                }}>
                                {eventDetails.persistence
                                    ? eventDetails.days * eventDetails.cycles
                                    : eventDetails.days}{' '}
                                {(eventDetails.persistence
                                    ? eventDetails.days * eventDetails.cycles
                                    : eventDetails.days) == 1
                                    ? 'Day Challenge'
                                    : 'Days Challenge'}
                            </Text>
                        </View>
                    </View>
                    <View
                        style={{
                            marginLeft: 15,
                            marginRight: 15,
                        }}>
                        <Text style={styles.ruleText}>{eventDetails.rule}</Text>
                        <Text style={[styles.text, styles.descriptionHeading]}>
                            Description:
                        </Text>
                        <Text style={styles.text}>
                            {eventDetails.description}
                        </Text>
                    </View>
                    <View style={styles.horizontalLine}></View>
                    {/* User info / host management */}
                    <View style={styles.hostContainer}>
                        <View style={styles.userInfo}>
                            <Text style={styles.text}>
                                {eventDetails.host_name}
                            </Text>
                            <Text style={styles.hosthost}>HOST</Text>
                        </View>
                        <View style={{marginRight:10,flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                        <Icon
                                        name="md-people"
                                        size={20}
                                        color="#FFFFFF"
                                    />
                            <Text style={{color:'white', }}> {eventDetails.participants} /{' '}
                                        {eventDetails.accommodation}</Text>
                        </View>
                        {userRole.role === 'participant' ||
                        userRole.role === 'host' ? (
                            <TouchableOpacity
                                onPress={() => {
                                    chatRoomNavigate(route.params.eventId);
                                }}
                                style={styles.manageBtn}>
                                <Icon
                                    style={{marginRight: 5}}
                                    name="chatbubble-outline"
                                    size={20}
                                    color="#FFFFFF"
                                />
                                <Text style={styles.text}>Group Chat</Text>
                            </TouchableOpacity>
                        ) : null}
                    </View>
                    <View style={styles.horizontalLine}></View>
                    {/*  */}

                    {/* <Text style={styles.title}>{eventDetails.title}</Text>
                        <Text
                            style={{
                                color: 'orange',
                                height: 20,
                            }}>
                            {eventDetails.rule}
                        </Text> */}
                    {eventDetails.status !== 'pending' ? (
                        <>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-around',
                                }}>
                                <View style={styles.percentContainer}>
                                    <Text style={styles.timeTitle}>
                                        Cycle{' '}
                                        {
                                            eventDetails.cycleDates[
                                                eventDetails.cycleDates.length -
                                                    1
                                            ]?.current_cycle
                                        }
                                    </Text>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                        }}>
                                        <CircularProgress
                                            percent={
                                                (eventDetails.currentCycleVerificationTimes /
                                                    eventDetails.times) *
                                                100
                                            }
                                            radius={40}
                                            bgRingWidth={5}
                                            progressRingWidth={5}
                                            showPercent={true}
                                            textFontSize={15}
                                            textFontColor={
                                                'rgba(204, 255, 102, 1)'
                                            }
                                            ringColor={'rgba(204, 255, 102, 1)'}
                                        />
                                        <View
                                            style={{
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                margin: 5,
                                            }}>
                                            <Text style={styles.timeText}>
                                                {dateString(
                                                    eventDetails.cycleDates[
                                                        eventDetails.cycleDates
                                                            .length - 1
                                                    ]?.start_date,
                                                )}
                                            </Text>
                                            {/* <Icon
                                            name="arrow-down"
                                            color={'white'}
                                            size={20}
                                        /> */}
                                            <Text style={styles.timeText}>
                                                to
                                            </Text>
                                            <Text style={styles.timeText}>
                                                {dateString(
                                                    eventDetails.cycleDates[
                                                        eventDetails.cycleDates
                                                            .length - 1
                                                    ]?.end_date,
                                                )}
                                            </Text>
                                        </View>
                                    </View>
                                    <Text
                                        style={[
                                            styles.timeText,
                                            {
                                                margin: 5,
                                                alignSelf: 'center',
                                                fontWeight: 'normal',
                                            },
                                        ]}>
                                        Times:{' '}
                                        {
                                            eventDetails.currentCycleVerificationTimes
                                        }
                                        /{eventDetails.times}
                                    </Text>
                                </View>
                                <View style={styles.percentContainer}>
                                    <Text style={styles.timeTitle}>
                                        Total {eventDetails.cycles} Cycles
                                    </Text>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                        }}>
                                        <CircularProgress
                                            percent={
                                                (eventDetails.verificationSubmittedTimes /
                                                    (eventDetails.times *
                                                        eventDetails.cycles)) *
                                                100
                                            }
                                            radius={40}
                                            bgRingWidth={5}
                                            progressRingWidth={5}
                                            showPercent={true}
                                            textFontSize={15}
                                            textFontColor={
                                                'rgba(204, 255, 102, 1)'
                                            }
                                            ringColor={'rgba(204, 255, 102, 1)'}
                                        />
                                        <View
                                            style={{
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                margin: 5,
                                            }}>
                                            <Text style={styles.timeText}>
                                                {dateString(
                                                    eventDetails.start_date,
                                                )}
                                            </Text>
                                            {/* <Icon
                                            name="arrow-down"
                                            color={'white'}
                                            size={20}
                                        /> */}
                                            <Text style={styles.timeText}>
                                                to
                                            </Text>
                                            <Text style={styles.timeText}>
                                                {dateString(
                                                    eventDetails.end_date,
                                                )}
                                            </Text>
                                        </View>
                                    </View>
                                    <Text
                                        style={[
                                            styles.timeText,
                                            {
                                                margin: 5,
                                                alignSelf: 'center',
                                                fontWeight: 'normal',
                                            },
                                        ]}>
                                        Times:{' '}
                                        {
                                            eventDetails.verificationSubmittedTimes
                                        }
                                        /
                                        {eventDetails.times *
                                            eventDetails.cycles}
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.subCat}>
                                <VerificationReports />
                            </View>
                        </>
                    ) : (
                        <>
                            <Text
                                style={{
                                    marginTop: 15,
                                    alignSelf: 'center',
                                    color: 'orange',
                                    fontSize: 30,
                                }}>
                                Pending
                            </Text>
                            <View
                                style={{
                                    alignItems: 'center',
                                    height: 60,
                                    flexDirection: 'row',
                                    justifyContent: 'space-around',
                                }}>
                                <Text style={{color: 'white'}}>
                                    {eventDetails.start_date}
                                </Text>
                                <Text style={{color: 'white'}}>To</Text>
                                <Text style={{color: 'white'}}>
                                    {eventDetails.end_date}
                                </Text>
                            </View>
                        </>
                    )}
                    {/* <Text style={styles.itemText}>{eventDetails.verificationSubmittedTimes}</Text> */}

                    {/*  */}

                    <View style={styles.horizontalLine}></View>

                    {/* timer part */}

                    <View style={styles.verificationContainer}>
                        {/* <View style={styles.timer}>
                                <View>
                                    <Text style={styles.text}>
                                        Current time:
                                    </Text>
                                    <Text style={styles.currentTime}>
                                        {hours}:
                                        {minutes > 9 ? minutes : '0' + minutes}:
                                        {seconds > 9 ? seconds : '0' + seconds}
                                    </Text>
                                </View>
                            </View> */}

                        {/* verify part */}
                        {/* {img === '' ? null : (
                        <Image
                            style={{
                                width: 100,
                                height: 100,
                                borderWidth: 1,
                                borderColor: 'red',
                            }}
                            //@ts-ignore
                            source={{uri: img}}
                        />
                        )} */}
                        
                        {/* {edwardpeter} */}
                        {((eventDetails.currentCycleVerificationTimes /
                            eventDetails.times !==
                            1) && (eventDetails.status !== 'pending')) ? (
                            <CameraVerify
                                challengerID={userRole.challengerId}
                                methodID={eventDetails.verifications[0]}
                                eventId={route.params.eventId}
                                cycleStartDate={cycleDates?
                                    cycleDates[cycleDates?.length - 1]
                                        ?.start_date:''
                                }
                            />
                        ) : null}

                        <Animated.View
                            style={[
                                styles.story,
                                expand ? null : styles.beforeExpand,
                            ]}>
                            <StoryIconFlat
                                eventId={route.params.eventId}
                                methodId={eventDetails?.verifications[0]}
                            />
                        </Animated.View>

                        {allVerificationRecords?.length > 1 ? (
                            <LinearGradient
                                colors={[
                                    'rgba(196, 196, 196, 0)',
                                    'rgba(196, 196, 196, 0.384)',
                                ]}
                                style={styles.expand}>
                                <TouchableOpacity
                                    onPress={() => setExpand(!expand)}>
                                    <Text style={styles.text}>
                                        {expand
                                            ? "Tap to hide cycles' info"
                                            : "Tap to show all cycles' info"}
                                    </Text>
                                </TouchableOpacity>
                            </LinearGradient>
                        ) : null}
                        {userRole.role === '' ? (
                            <View style={styles.notParticipant}>
                                <View style={styles.participantsInfo}>
                                    <Icon
                                        name="md-people"
                                        size={35}
                                        color="#FFFFFF"
                                    />
                                    <Text
                                        style={{
                                            fontSize: 30,
                                            color: '#FFFFFF',
                                        }}>
                                        {eventDetails.participants} /{' '}
                                        {eventDetails.accommodation}
                                    </Text>
                                    <Text
                                        style={{
                                            marginTop: 15,
                                            fontSize: 30,
                                            color: '#FFFFFF',
                                        }}>
                                        {remainingDates} Days
                                    </Text>
                                    <Text
                                        style={{
                                            fontSize: 20,
                                            color: '#FFFFFF',
                                        }}>
                                        Remaining
                                    </Text>
                                </View>
                                <View style={styles.participateButtons}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            dispatch(
                                                joinEvent(route.params.eventId),
                                            );
                                        }}>
                                        <Text
                                            style={{
                                                width: '100%',
                                                color: 'black',
                                                fontSize: 30,
                                            }}>
                                            Join
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        ) : null}
                        {userRole.role === '' ? (
                            <View style={styles.notParticipant}>
                                <View style={styles.participantsInfo}>
                                    <Icon
                                        name="md-people"
                                        size={35}
                                        color="#FFFFFF"
                                    />
                                    <Text
                                        style={{
                                            fontSize: 30,
                                            color: '#FFFFFF',
                                        }}>
                                        {eventDetails.participants} /{' '}
                                        {eventDetails.accommodation}
                                    </Text>
                                    <Text
                                        style={{
                                            marginTop: 15,
                                            fontSize: 30,
                                            color: '#FFFFFF',
                                        }}>
                                        {remainingDates} Days
                                    </Text>
                                    <Text
                                        style={{
                                            fontSize: 20,
                                            color: '#FFFFFF',
                                        }}>
                                        Remaining
                                    </Text>
                                </View>
                                <View style={styles.participateButtons}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            dispatch(
                                                joinEvent(route.params.eventId),
                                            );
                                        }}>
                                        <Text
                                            style={{
                                                width: '100%',
                                                color: 'black',
                                                fontSize: 30,
                                            }}>
                                            Join
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        ) : null}
                    </View>
                    <LinearGradient
                        colors={[
                            'rgba(196, 196, 196, 0)',
                            'rgba(196, 196, 196, 0.1)',
                        ]}
                        style={styles.expand}>
                        <TouchableOpacity onPress={() => setExpand(!expand)}>
                            <Text style={styles.text}>
                                {expand ? 'shrink' : 'expand'}
                            </Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </ScrollView>
            </SafeAreaView>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    background: {
        backgroundColor: 'rgba(49,49,49,0.8)',
    },
    linear: {
        width: '100%',
        height: '20%',
        position: 'absolute',
        top: 0,
    },
    mainContainer: {
        backgroundColor: 'transparent',
        height: '100%',
        // marginRight: 10,
        // marginLeft: 10,
    },
    text: {
        color: 'white',
        fontSize: 16,
    },

    title: {
        marginBottom: 10,
        fontStyle: 'normal',
        fontWeight: '500',
        color: 'white',
        fontSize: 23,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },
    category: {
        display: 'flex',
        flexDirection: 'row',
    },
    lifeStyle: {
        backgroundColor: '#1F9D3B',
        padding: 7,
        borderRadius: 15,
    },
    successRate: {
        padding: 7,
        borderRadius: 15,
        backgroundColor: '#2DC5A0',
    },
    additionCat: {
        padding: 7,
        borderRadius: 15,
        backgroundColor: '#CB59DE',
    },
    descriptionHeading: {
        marginTop: 15,
        marginBottom: 5,
    },
    exp: {
        display: 'flex',
        alignItems: 'flex-end',
        borderWidth: 1,
        borderColor: 'transparent',
        borderBottomColor: '#CCFF66',
    },
    expText: {
        color: '#CCFF66',
    },
    hostContainer: {
        marginLeft: 15,
        marginTop: 10,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    userInfo: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    hostImg: {
        width: 30,
        height: 30,
        marginRight: 7,
    },
    hosthost: {
        marginLeft: 7,
        color: '#FFA800',
    },
    manageBtn: {
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#37D0E4',
        padding: 5,
        borderRadius: 6,
        marginRight: 10,
    },
    timer: {
        marginTop: 5,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'baseline',
        justifyContent: 'space-between',
    },
    currentTime: {
        fontSize: 30,
        color: '#CCFF66',
    },
    deadTime: {
        fontSize: 30,
        color: '#FE0303',
    },
    story: {
        paddingTop: 0,
        marginTop: 10,
        marginBottom: 10,
        display: 'flex',
        flexDirection: 'row',
    },
    indiStory: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginRight: 10,
    },
    todayDate: {
        color: '#A0D03E',
        fontSize: 18,
        marginTop: 10,
    },
    detailsText: {
        fontSize: 16,
        color: 'white',
    },
    blue: {
        color: '#37D0E4',
    },
    green: {
        color: '#CCFF66',
    },
    grey: {
        color: '#C4C4C4',
    },
    red: {
        color: '#FE0303',
    },
    detailSection: {
        borderWidth: 1,
        borderColor: 'transparent',
        borderTopColor: '#CCFF66',
    },
    firstRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    secondRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    verifyContainer: {
        marginTop: 20,
        padding: 15,
        borderWidth: 2,
        width: '100%',
        borderColor: '#CCFF66',
        borderRadius: 10,
        paddingTop: 0,
        marginBottom: 10,
    },
    verifyHeading: {
        marginTop: 15,
    },

    expand: {
        borderWidth: 2,
        borderColor: 'transparent',
        borderBottomColor: '#CCFF66',
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    beforeExpand: {
        height: 160,
    },
    rowContainer1: {
        flexDirection: 'row',
    },
    ruleText: {
        color: 'orange',
    },
    totalDays: {
        justifyContent: 'center',
        textAlign: 'center',
        width: '50%',
    },
    notParticipant: {
        flex: 1,
        position: 'absolute',
        // height:'100%',
        // width:'100%',
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
        backgroundColor: 'rgba(0,0,0,0.7)',
        // justifyContent: 'center',
        alignItems: 'center',
    },
    participantsInfo: {
        marginTop: 80,
        justifyContent: 'center',
        alignItems: 'center',
    },
    participateButtons: {
        width: 200,
        height: 100,
        marginTop: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#rgba(255,255,0,0.9)',
        borderRadius: 20,
    },
    verificationContainer: {
        // flex:1,
        // alignSelf:'stretch',
        height: '100%',
        // height: '90%',
        width: '100%',
        padding: 15,
    },
    //
    ongoingEventBlock: {
        display: 'flex',
        overflow: 'hidden',
        alignItems: 'center',
        // padding: 10,
        borderRadius: 15,
        marginLeft: 5,
        marginRight: 5,
        backgroundColor: 'rgba(196,196,196,0.2)',
        height: 200,
        // width: 250,
        /* add drop shadow */
        shadowColor: '#CCFF66',
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    },
    // title: {
    //     fontSize: 20,
    //     color: '#FFFFFF',
    //     fontWeight: 'bold',
    // },
    image: {
        flex: 1,
        borderRadius: 100,
        height: 200,
        width: 360,
    },
    overlay: {
        backgroundColor: 'rgba(49,49,49,0.8)',
        flex: 1,
        padding: 10,
    },
    timeTitle: {
        color: 'white',
        fontSize: 15,
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 5,
    },
    timeText: {
        color: 'white',
        letterSpacing: 1,
        fontWeight: 'bold',
    },
    percentContainer: {
        margin: 5,
    },
    subCat: {
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#ccff66',

        borderRadius: 10,
        borderWidth: 2,
        marginLeft: '5%',
        marginRight: '5%',
    },
    horizontalLine: {
        marginTop: 15,
        borderWidth: 1,
        borderColor: 'transparent',
        borderBottomColor: '#CCFF66',
    },
});

export default ChallengeRoomScreen;
