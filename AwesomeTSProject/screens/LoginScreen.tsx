import React, {useEffect, useState} from 'react';
import {
    SafeAreaView,
    ScrollView,
    Text,
    View,
    Image,
    TextInput,
} from 'react-native';
import {useForm, Controller, useController} from 'react-hook-form';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {RootState} from '../store';
import {login} from '../redux/user/thunks';
import {ErrorMessage} from '@hookform/error-message';
import {ISignUpData} from './SignUpScreen';
import { styles } from './LoginScreen.styles';
// import Logo from '../uploads/challengelogo.png';

export interface ILoginData {
    email: string;
    password: string;
}

interface IErrorProps {
    name: keyof ISignUpData;
}

const LoginScreen = () => {
    const {
        control,
        handleSubmit,
        formState: {errors},
    } = useForm();
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const isAuthenticated = useSelector(
        (state: RootState) => state.user.isAuthenticated,
    );

    const onLogin = (data: ILoginData) => {
        dispatch(login(data.email, data.password));
        // Cannot use here
        // if (isAuthenticated) {
        //     navigation.navigate('Home');
        // }
    };

    useEffect(() => {
        // This is correct
        if (isAuthenticated) {
            navigation.navigate('Home');
        }
    }, [isAuthenticated, navigation]);

    const gotoSignUp = () => {
        navigation.navigate('SignUp');
    };

    const ErrorBlock = (props: IErrorProps) => {
        return (
            <ErrorMessage
                errors={errors}
                name={props.name}
                render={({message}) => (
                    <View style={styles.errorContainer}>
                        <Text style={styles.errorMessage}>{message}</Text>
                    </View>
                )}
            />
        );
    };

    return (
        <SafeAreaView style={styles.background}>
            <ScrollView
                style={styles.container}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}>
                {/* <LoadingSpinner /> */}
                <View style={styles.challengeHeaderContainer}>
                    <View style={styles.challengeLogoContainer}>
                        <Image
                            style={styles.challengeLogo}
                            source={require('../uploads/challengelogo.png')}
                        />
                    </View>
                    {/* <View style={styles.challengeTitleContainer}>
                        <Text style={styles.challengeTitle}>Challenge</Text>
                    </View> */}
                </View>
                <View style={styles.form}>
                    <Controller
                        control={control}
                        render={({field: {onChange, onBlur, value}}) => (
                            <TextInput
                                style={styles.input}
                                onBlur={onBlur}
                                placeholder="User Email"
                                placeholderTextColor="#ccff6683"
                                onChangeText={value => onChange(value)}
                                value={value}
                            />
                        )}
                        name="email"
                        rules={{
                            required: 'Email/Password cannot be empty',
                            pattern: {
                                value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                message: 'Please enter a valid email',
                            },
                        }}
                        defaultValue=""
                    />
                    <ErrorBlock name="email" />
                    <Controller
                        control={control}
                        render={({field: {onChange, onBlur, value}}) => (
                            <TextInput
                                secureTextEntry={true}
                                style={styles.input}
                                onBlur={onBlur}
                                placeholder="Password"
                                placeholderTextColor="#ccff6683"
                                onChangeText={value => onChange(value)}
                                value={value}
                            />
                        )}
                        name="password"
                        rules={{required: 'Email/Password cannot be empty'}}
                        defaultValue=""
                    />
                    <ErrorBlock name="password" />
                    <TouchableOpacity
                        style={styles.login}
                        onPress={handleSubmit(onLogin)}>
                        <Text style={styles.loginText}>Login</Text>
                    </TouchableOpacity>
                    <View style={styles.signUpContainer}>
                        <Text style={styles.signUpText}>New here?</Text>
                        <TouchableOpacity
                            style={styles.signUpLinkContainer}
                            onPress={gotoSignUp}>
                            <Text style={styles.signUpLink}>Sign up</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};



export default LoginScreen;
