import React, {useEffect} from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    FlatList,
    useColorScheme,
    View,
    Image,
    TouchableOpacity,
} from 'react-native';
import * as Progress from 'react-native-progress';

import RoutingButtons from './components/HomeRoutingButtons';
import SearchBar from './components/SearchBar';
import PopularChallenges from './components/HomePopularChallenges';

import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../store';
import LoadingSpinner from './components/LoadingSpinner';
import {config} from '../redux/config';
import PersonalChallenges from './components/JournalPersonalChallenges';
import ChallengeTimeline from './components/JournalTimeline';
import ChallengeCalendar from './components/JournalChallengeCalendar';
import {
    fetchEventByDate,
    fetchEventDots,
} from '../redux/challengeJournal/thunks';

import ForumCategoriesHome from './components/ForumCategoriesHome';

function HomeScreen() {
    const navigation = useNavigation();
    const userDetails = useSelector((state: RootState) => state.user.user);
    const dispatch = useDispatch();
    const isAuthenticated = useSelector(
        (state: RootState) => state.user.isAuthenticated,
    );

    useEffect(() => {
        if (!isAuthenticated) {
            navigation.navigate('Login');
        }
    }, [isAuthenticated, navigation]);

    useEffect(() => {
        if (!isAuthenticated) {
            navigation.navigate('Login');
        }
        dispatch(fetchEventDots(userDetails?.id));
        dispatch(
            fetchEventByDate(
                userDetails?.id,
                new Date().toISOString().split('T')[0],
            ),
        );
    }, [isAuthenticated, navigation]);
    // Loading Spinner can be at App Level

    return (
        <SafeAreaView style={styles.background}>
            <ScrollView
                style={styles.container}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}>
                <LoadingSpinner />
                <View style={styles.profileContainer}>
                    <Image
                        style={styles.profilePicture}
                        source={{
                            uri: `${config.S3_BUCKET_URL}/${userDetails?.pro_pic}`,
                        }}
                    />
                    <View style={styles.profileNicknameContainer}>
                        <Text style={styles.profileNickname}>
                            {userDetails?.nickname
                                ? userDetails?.nickname
                                : 'null'}
                        </Text>
                    </View>
                </View>
                <View style={styles.horizontalLine}></View>
                {/* <View style={styles.profileContainer}>
          <View style={styles.progressbarContainer}>
            <Progress.Bar progress={0.7} width={240} color={'rgba(204,255,102,1)'} />
          </View>
          <View style={styles.badgeContainer}>
            <Text style={styles.level} >Lv18</Text>
          </View>
        </View> */}
                <RoutingButtons />
                {/* <SearchBar /> */}
                <View style={styles.popularContainer}>
                    <View style={styles.popularHeaderContainer}>
                        <View style={styles.popularTitleContainer}>
                            <Text style={styles.popularTitle}>Popular Templates</Text>
                        </View>
                        {/* <View style={styles.popularFilterContainer}>
                            <Text style={styles.popularFilter}>ShowAll</Text>
                        </View> */}
                    </View>
                    <PopularChallenges />
                </View>
                <View style={styles.homeForum}>
                    <View style={styles.popularHeaderContainer}>
                        <Text style={styles.categoryTitle}>Forum</Text>
                    </View>
                    <ForumCategoriesHome />
                </View>
                <View style={styles.categoryContainer}>
                    <View style={styles.categoryTitleContainer}>
                        <Text style={styles.categoryTitle}>
                            Ongoing Challenges
                        </Text>
                    </View>
                    <PersonalChallenges />

                    <View style={styles.calendarContainer}>
                        <View style={styles.calendarHeader}>
                            <View style={styles.calendarTitleContainer}>
                                <Text style={styles.calendarTitle}>
                                    Calendar
                                </Text>
                            </View>
                            {/* <View style={styles.calendarToggleContainer}></View> */}
                        </View>
                        {/* {calendarMode ? (
                        <ChallengeCalendar />
                    ) : (
                        <ChallengeTimeline />
                    )} */}
                        <ChallengeCalendar />
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    horizontalLine: {
        borderBottomWidth: 2,
        borderColor: '#ccff66',
        marginBottom: 10,
        marginTop: 10,
    },
    background: {
        backgroundColor: '#313131',
    },
    container: {
        backgroundColor: 'transparent',
        height: '100%',
        marginRight: 10,
        marginLeft: 10,
    },
    rowContainer: {
        flexDirection: 'row',
        width: '100%',
    },
    //styles for profile
    profileContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: '100%',
        padding: 5,
    },
    profilePicture: {
        width: 75,
        height: 75,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: '#ccff66',
    },
    profileNicknameContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
    },
    profileNickname: {
        fontSize: 20,
        color: '#FFFFFF',
        fontWeight: 'bold',
    },

    progressbarContainer: {
        justifyContent: 'center',
        width: '70%',
    },
    badgeContainer: {
        width: '30%',
        paddingLeft: 5,
    },
    level: {
        color: '#FFFFFF',
    },
    //styles for popular challenges
    popularContainer: {
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
    },
    popularHeaderContainer: {
        padding: 10,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },
    popularTitleContainer: {
        justifyContent: 'center',
    },
    popularTitle: {
        fontSize: 25,
        fontWeight: '500',
        textAlign: 'left',
        color: '#FFFFFF',
    },
    popularFilterContainer: {
        justifyContent: 'center',
    },
    popularFilter: {
        fontSize: 15,
        fontWeight: '300',
        color: '#FFFFFF',
    },
    //Styles for category
    categoryContainer: {
        width: '100%',
        flexWrap: 'wrap',
    },
    categoryTitleContainer: {
        padding: 10,
        flexDirection: 'row',
        width: '100%',
    },
    categoryTitle: {
        fontSize: 25,
        fontWeight: '500',
        textAlign: 'left',
        width: '100%',
        color: '#FFFFFF',
    },
    calendarContainer: {
        width: '100%',
    },
    calendarHeader: {
        padding: 10,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },
    calendarTitleContainer: {
        justifyContent: 'center',
    },
    calendarTitle: {
        fontSize: 25,
        fontWeight: '500',
        textAlign: 'left',
        color: '#FFFFFF',
    },
    calendarToggleContainer: {
        justifyContent: 'center',
    },
    calendarToggle: {
        fontSize: 15,
        fontWeight: '300',
        color: '#FFFFFF',
    },
    homeForum: {},
});

export default HomeScreen;
