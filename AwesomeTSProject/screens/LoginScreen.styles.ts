import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    background: {
        backgroundColor: '#313131',
    },
    container: {
        backgroundColor: 'transparent',
        height: '100%',
        marginRight: 10,
        marginLeft: 10,
    },
    challengeHeaderContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
    },
    challengeLogoContainer: {
        width: 100,
        height: 100,
        borderRadius: 70,
        marginBottom: 80,
        marginTop: 80,

        justifyContent: 'center',
        alignItems: 'center',
        /* add drop shadow */
        // shadowColor: '#D4FF80',
        // shadowOffset: {
        //     width: 1,
        //     height: 1,
        // },
        // shadowOpacity: 0.25,
        // shadowRadius: 10,
    },
    challengeLogo: {},
    challengeTitleContainer: {
        marginTop: 10,
    },
    challengeTitle: {
        color: '#FFFFFF',
        fontSize: 35,
        fontWeight: '700',
    },
    form: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#313131',
    },
    input: {
        height: 60,
        borderColor: '#ccff66',
        borderWidth: 1,
        borderRadius: 20,
        marginBottom: 10,
        paddingLeft: 10,
        color: '#FFFFFF',
        fontSize: 20,
    },
    //error message styles
    errorContainer: {
        paddingBottom: 5,
    },
    errorMessage: {
        color: '#FFFFFF',
    },
    login: {
        backgroundColor: '#ccff66',
        height: 50,
        width: '100%',
        borderRadius: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    loginText: {
        fontSize: 20,
        fontWeight: '500',
    },
    signUpContainer: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
    },
    signUpText: {
        fontSize: 20,
        color: '#FFFFFF',
    },
    signUpLinkContainer: {
        marginLeft: 5,
    },
    signUpLink: {
        fontSize: 20,
        color: '#89B336',
    },
});