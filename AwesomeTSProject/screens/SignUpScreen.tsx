import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    FlatList,
    useColorScheme,
    View,
    Image,
    TextInput,
    Platform,
    Alert,
} from 'react-native';
import {
    launchCamera,
    launchImageLibrary,
    Asset,
    ImageLibraryOptions,
} from 'react-native-image-picker';
import { useForm, Controller } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { RootState } from '../store';
import { uploadProfileImage } from '../redux/user/actions';
import { fetchAccountSignUp } from '../redux/user/thunks';
import LoadingSpinner from './components/LoadingSpinner';

export interface ISignUpData {
    email: string;
    password: string;
    nickname: string;
}

const SignUpScreen = () => {
    const {
        control,
        handleSubmit,
        formState: { errors },
    } = useForm<ISignUpData>();
    const dispatch = useDispatch();
    const navigation = useNavigation();

    const isAuthenticated = useSelector(
        (state: RootState) => state.user.isAuthenticated,
    );

    useEffect(() => {
        if (isAuthenticated) {
            navigation.navigate('Home');
        }
    }, [isAuthenticated, navigation]);

    interface IErrorProps {
        name: keyof ISignUpData;
    }

    const ErrorBlock = (props: IErrorProps) => {
        return (
            <ErrorMessage
                errors={errors}
                name={props.name}
                render={({ message }) => (
                    <View style={styles.errorContainer}>
                        <Text style={styles.errorMessage}>{message}</Text>
                    </View>
                )}
            />
        );
    };

    //setting image upload logic
    const photo = useSelector((state: RootState) => state.user.uploadPhoto);

    const options: ImageLibraryOptions = {
        mediaType: 'photo',
        quality: 0.1,
    };

    const handleChoosePhoto = () => {
        launchImageLibrary(options, response => {
            if (response.assets) {
                dispatch(uploadProfileImage(response.assets[0]));
            } else {
                console.log(response);
            }
        });
    };

    const handleTakePhoto = () => {
        launchCamera(options, response => {
            if (response.assets) {
                dispatch(uploadProfileImage(response.assets[0]));
            } else {
                console.log(response);
            }
        });
    };

    const submitSignUp = (data: ISignUpData) => {
        if (!photo) {
            Alert.alert('Photo is missing!');
        } else {
            const signUpInfo = new FormData();
            signUpInfo.append('email', data.email);
            signUpInfo.append('password', data.password);
            signUpInfo.append('nickname', data.nickname);
            signUpInfo.append('profilePicture', {
                name: photo.fileName,
                type: photo.type,
                uri:
                    Platform.OS === 'ios'
                        ? photo.uri.replace('file://', '')
                        : photo.uri,
            });
            dispatch(fetchAccountSignUp(signUpInfo));
        }
    };

    const gotoLogin = () => {
        navigation.navigate('Login');
    };

    return (
        <SafeAreaView style={styles.background}>
            <ScrollView
                style={styles.container}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}>
                {/* <LoadingSpinner /> */}
                <View style={styles.signUpContainer}>
                    <View style={styles.signUpTitleContainer}>
                        <Text style={styles.signUpTitle}>Sign up</Text>
                    </View>
                    <View style={styles.signUpTextContainer}>
                        <Text style={styles.signUpText}>
                            Sign up and join the challenge community
                        </Text>
                    </View>
                </View>
                <View style={styles.form}>
                    <Controller
                        control={control}
                        render={({ field: { onChange, onBlur, value } }) => (
                            <TextInput
                                style={styles.input}
                                onBlur={onBlur}
                                placeholder="User Email"
                                placeholderTextColor="#ccff6683"
                                onChangeText={value => onChange(value)}
                                value={value}
                            />
                        )}
                        name="email"
                        rules={{
                            required: 'Email is required',
                            pattern: {
                                value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                message: 'Please enter a valid email',
                            }
                        }}
                        defaultValue=""
                    />
                    <ErrorBlock name="email" />
                    <Controller
                        control={control}
                        render={({ field: { onChange, onBlur, value } }) => (
                            <TextInput
                                secureTextEntry={true}
                                style={styles.input}
                                onBlur={onBlur}
                                placeholder="Password"
                                placeholderTextColor="#ccff6683"
                                onChangeText={value => onChange(value)}
                                value={value}
                            />
                        )}
                        name="password"
                        rules={{ required: 'Password is required' }}
                        defaultValue=""
                    />
                    <ErrorBlock name="password" />
                    <Controller
                        control={control}
                        render={({ field: { onChange, onBlur, value } }) => (
                            <TextInput
                                style={styles.input}
                                onBlur={onBlur}
                                placeholder="Nickname"
                                placeholderTextColor="#ccff6683"
                                onChangeText={value => onChange(value)}
                                value={value}
                            />
                        )}
                        name="nickname"
                        rules={{
                            required: {
                                value: true,
                                message: 'Nickname is required',
                            },
                        }}
                        defaultValue=""
                    />
                    <ErrorBlock name="nickname" />
                    {photo && (
                        <View style={styles.photoContainer}>
                            <Text style={styles.previewPhotoText}>Preview</Text>
                            <Image
                                source={{ url: photo.uri }}
                                style={styles.previewPhoto}
                            />
                        </View>
                    )}
                    <TouchableOpacity
                        style={styles.takePhoto}
                        onPress={handleTakePhoto}>
                        <Text style={styles.takePhotoText}>Take Photo</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.upload}
                        onPress={handleChoosePhoto}>
                        <Text style={styles.uploadText}>Upload Profile</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.signUpSubmit}
                        onPress={handleSubmit(submitSignUp)}>
                        <Text style={styles.signUpSubmitText}>Sign Up</Text>
                    </TouchableOpacity>
                    <View style={styles.loginContainer}>
                        <Text style={styles.loginText}>
                            Already have account?
                        </Text>
                        <TouchableOpacity
                            style={styles.loginLinkContainer}
                            onPress={gotoLogin}>
                            <Text style={styles.loginLink}>Login</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    background: {
        backgroundColor: '#313131',
    },
    container: {
        backgroundColor: 'transparent',
        height: '100%',
        marginRight: 10,
        marginLeft: 10,
    },
    signUpContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '30%',
        marginBottom: 10,
    },
    signUpTitleContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: '100%',
    },
    signUpTitle: {
        color: '#FFFFFF',
        fontSize: 35,
        fontWeight: 'bold',
    },
    signUpTextContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: '100%',
    },
    signUpText: {
        color: '#FFFFFF',
        fontSize: 20,
    },
    form: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#313131',
    },
    input: {
        height: 60,
        borderColor: '#ccff66',
        borderWidth: 1,
        borderRadius: 20,
        marginTop: 10,
        paddingLeft: 10,
        color: '#FFFFFF',
        fontSize: 20,
    },
    //error message styles
    errorContainer: {
        paddingTop: 5,
    },
    errorMessage: {
        color: '#FFFFFF',
    },
    //photo related styles
    photoContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 10,
    },
    previewPhotoText: {
        color: '#FFFFFF',
        fontSize: 25,
        fontWeight: '500',
    },
    previewPhoto: {
        width: 150,
        height: 150,
        borderRadius: 75,
        borderColor: '#ccff66',
        borderWidth: 1,
    },
    takePhoto: {
        backgroundColor: '#FF59A5',
        height: 50,
        width: '100%',
        borderRadius: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
    },
    takePhotoText: {
        fontSize: 20,
        fontWeight: '500',
    },
    upload: {
        backgroundColor: '#9073FF',
        height: 50,
        width: '100%',
        borderRadius: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
    },
    uploadText: {
        fontSize: 20,
        fontWeight: '500',
    },
    signUpSubmit: {
        backgroundColor: '#ccff66',
        height: 50,
        width: '100%',
        borderRadius: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
    },
    signUpSubmitText: {
        fontSize: 20,
        fontWeight: '500',
    },

    loginContainer: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
    },
    loginText: {
        fontSize: 20,
        color: '#FFFFFF',
    },
    loginLinkContainer: {
        marginLeft: 5,
    },
    loginLink: {
        fontSize: 20,
        color: '#89B336',
    },
});

export default SignUpScreen;
