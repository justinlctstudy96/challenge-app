import React, {useEffect, useState, useRef} from 'react';
// import React from 'react-native';
import {
    ImageBackground,
    View,
    Text,
    StyleSheet,
    FlatList,
    SafeAreaView,
    ListRenderItem,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    Picker,
    ViewPropTypes,
    Platform,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    Image,
} from 'react-native';
import {RootState} from '../store';
import {useSelector, useDispatch} from 'react-redux';
import {
    fetchRoomMessages,
    getNewMessageSuccess,
    sendMessage,
} from '../redux/chatRoom/actions';
import {Message} from '../redux/chatRoom/reducers';
import ChallengeTimesDaysCyclesBlock from './components/ChallengeTimesDaysCyclesBlock';
import {config} from '../redux/config';

import SocketIOClient from 'socket.io-client';
import io from 'socket.io-client';
import {State} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import {fetchRoomUser} from '../redux/roomStory/actions';
import {ChallengeEventSummary} from '../redux/challengeLobby/reducers';
import {fetchEventDetails} from '../redux/challengeRoom/actions';


// const io = require('socket.io-client/socket.io')
// import io from 'socket.io-client/socket.io';
// const io = require('socket.io-client/socket.io');
// const socket = io()

// const socket = io('http://chat.feathersjs.com', {
//     transports: ['websocket'], // you need to explicitly tell it to use websockets
// });

interface ChatRoomDetails {
    eventId: number;
}

export const getTimeString = (timestamp: string) => {
    // Check correct time format and split into components
    let hours = parseInt(timestamp.split(':')[0]) + 8;
    timestamp = `${hours > 24 ? hours - 24 : hours}:${timestamp.split(':')[1]}`;
    let time = timestamp
        .toString()
        .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [timestamp];

    if (time.length > 1) {
        // If time format correct
        time = time.slice(1); // Remove full string match value
        time[5] = (+time[0] < 12 ? 'AM' : 'PM').toString(); // Set AM/PM
        time[0] = (+time[0] % 12 || 12).toString(); // Adjust hours
    }
    return time.join(''); // return adjusted time or original string
};

export interface IChatRoomScreenProps{
    route:{
        params:{
            eventId:number
        }
    }
}


export default function ChatRoomScreen({route}:IChatRoomScreenProps) {
    const userDetails = useSelector((state: RootState) => state.user);
    const messages = useSelector((state: RootState) => state.chatRoom.messages);
    const allUser = useSelector((state: RootState) => state.userList.allUser);
    const dispatch = useDispatch();
    const [input, setInput] = useState('');
    const eventDetails: ChallengeEventSummary = useSelector(
        (state: RootState) => state.challengeRoomScreen.eventDetails,
    );
    useEffect(() => {
        dispatch(fetchRoomMessages(route.params?.eventId));
        dispatch(fetchRoomUser(route.params?.eventId));
        dispatch(fetchEventDetails(route.params.eventId));
        // const socket = SocketIOClient(`${config.REACT_APP_BACKEND_URL}`);
        // const socket = io(`${config.REACT_APP_BACKEND_URL}`);
        // socket.emit('joinRoom', route.params?.eventId);
        // socket.on('chatMessage', (message: Message) => {
        //     if (message.user_id !== userDetails.user?.id) {
        //         dispatch(getNewMessageSuccess(message));
        //     }
        // });
        const intervalId = setInterval(() => {
            dispatch(fetchRoomMessages(route.params?.eventId));
        }, 1000);
        return () => {
            clearInterval(intervalId);
        };
    }, []);

    const renderMessages: ListRenderItem<Message> = ({item}) => {
        let userMessage = false;
        if (item.user_id == userDetails.user?.id) {
            userMessage = true;
        }

        return (
            <SafeAreaView>
                <ScrollView>
                    <View
                        style={
                            userMessage
                                ? styles.userMessage
                                : styles.otherMessage
                        }>
                        <View
                            style={
                                userMessage
                                    ? styles.userMessageBlock
                                    : styles.otherMessageBlock
                            }>
                            {userMessage ? null : (
                                <Text
                                    style={{
                                        color: 'white',
                                    }}>
                                    {item.nickname}:
                                </Text>
                            )}
                            <Text
                                style={
                                    userMessage
                                        ? {
                                              alignSelf: 'flex-end',
                                          }
                                        : {color: 'white', fontWeight: 'bold'}
                                }>
                                {item.content}
                            </Text>
                            <Text
                                style={
                                    userMessage
                                        ? {
                                              alignSelf: 'flex-end',
                                              fontSize: 10,
                                          }
                                        : {fontSize: 10, color: 'white'}
                                }>
                                {getTimeString(
                                    item.created_at.split('T')[1].split('.')[0],
                                )}
                            </Text>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    };

    const onSend = () => {
        dispatch(sendMessage(input, route.params.eventId));
        setInput('');
    };

    const scrollViewRef = useRef<ScrollView | null>(null);

    const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : 0;
    const navigation = useNavigation();

    const renderAllUser = ({item}) => {
        return (
            <View style={styles.eachUser}>
                <Image
                    style={styles.userPropic}
                    source={{
                        // uri: `${config.S3_BUCKET_URL}/${item.pro_pic}.jpeg`,
                        uri: `${config.S3_BUCKET_URL}/${item.pro_pic}`,
                    }}
                />
                <Text style={styles.whiteSmallText}>{item.nickname}</Text>
            </View>
        );
    };

    return (
        <SafeAreaView style={styles.background}>
            <LinearGradient
                colors={['rgba(204, 255, 102, 1)', 'rgba(49,49,49,0)']}
                style={styles.linear}
            />
            <KeyboardAvoidingView
                behavior="position"
                keyboardVerticalOffset={keyboardVerticalOffset}>
                <View style={styles.headingV}>
                    <TouchableOpacity
                        onPress={() => {
                            navigation.navigate('ChallengeRoom');
                        }}>
                        <Icon
                            name="chevron-back-outline"
                            size={30}
                            color="#FFFFFF"
                        />
                    </TouchableOpacity>
                    <Text style={styles.headingText}>{eventDetails.title}</Text>
                    <View>
                        <FlatList
                            data={allUser}
                            renderItem={renderAllUser}
                            horizontal={true}
                        />
                    </View>
                </View>
                <ScrollView
                    style={styles.scrollV}
                    ref={scrollViewRef}
                    onContentSizeChange={() => {
                        scrollViewRef?.current?.scrollToEnd({
                            animated: false,
                        });
                    }}>
                    <FlatList data={messages} renderItem={renderMessages} />
                </ScrollView>
                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.inputBlock}
                        value={input}
                        placeholder={'type...'}
                        placeholderTextColor="grey"
                        onChangeText={setInput}
                    />
                    <View style={styles.sendButton}>
                        <TouchableOpacity onPress={onSend}>
                            <Text style={{fontWeight: 'bold'}}>Send</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    whiteSmallText: {
        color: 'white',
        fontSize: 12,
        width: 50,
    },
    linear: {
        width: '100%',
        height: '20%',
        position: 'absolute',
        top: 0,
    },
    background: {
        backgroundColor: '#313131',
        height: '100%',
    },
    headingText: {
        marginBottom: 10,
        fontStyle: 'normal',
        fontWeight: '500',
        color: 'white',
        fontSize: 23,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },
    headingV: {
        height: '25%',
        borderBottomWidth: 1,
        borderColor: '#ccff66',
    },
    scrollV: {
        padding: 10,
        height: '68%',
        backgroundColor: '#313131',
    },
    userPropic: {
        marginBottom: 5,
        width: 50,
        height: 50,
        borderRadius: 50,
    },
    eachUser: {
        padding: 3,

        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    userMessage: {
        width: '100%',
        alignItems: 'flex-end',
    },
    otherMessage: {
        width: '100%',
        alignItems: 'flex-start',
    },
    userMessageBlock: {
        backgroundColor: '#ccff66',
        margin: 5,
        padding: 5,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 15,
        marginBottom: 10,
    },
    otherMessageBlock: {
        backgroundColor: 'grey',

        margin: 5,
        padding: 5,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 15,
        marginBottom: 10,
    },
    inputContainer: {
        paddingTop: 10,
        height: '7%',
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-around',
        backgroundColor: '#313131',
    },
    inputBlock: {
        backgroundColor: 'white',
        width: '75%',
        margin: 4,
        marginRight: 0,
        borderRadius: 10,
        padding: 4,
    },
    sendButton: {
        backgroundColor: '#ccff66',
        width: '20%',
        margin: 4,
        marginLeft: 1,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
