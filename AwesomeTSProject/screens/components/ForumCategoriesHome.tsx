import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    ListRenderItem,
    FlatList,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import {config} from '../../redux/config';
import {useDispatch} from 'react-redux';
import {switchCategory} from '../../redux/forum/actions';

const styles = StyleSheet.create({
    categoryButtonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    },
    item: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        padding: 5,
    },
    imageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        opacity: 0.6,
        borderRadius: 10,
        width: 120,
        height: 80,
        borderWidth: 2,
    },
    itemText: {
        color: '#FFFFFF',
        textAlign: 'center',
        fontSize: 20,
    },
    touchtouch: {
        height: 110,
        // paddingBottom: 10,
    },
});

const items = [
    {key: 1, name: 'Health', borderColor: '#F0CE1D'},
    {key: 2, name: 'Study', borderColor: '#597EDE'},
    {key: 3, name: 'Lifestyle', borderColor: '#1F9D3B'},
    {key: 4, name: 'Sport', borderColor: '#FFA800'},
    {key: 5, name: 'Music', borderColor: '#9D1F1F'},
    {key: 6, name: 'Others', borderColor: '#CB59DE'},
];

interface IItem {
    key: number;
    name: string;
    borderColor: string;
}

const ForumCategories = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const renderItem: ListRenderItem<IItem> = ({item}) => (
        <TouchableOpacity
            style={styles.touchtouch}
            onPress={() => {
                handleClickToSwitch(item.key);
                navigation.navigate('Forum');
            }}>
            <View style={styles.item}>
                <View style={styles.imageContainer}>
                    <Image
                        source={{
                            uri: `${config.S3_BUCKET_URL}/background/${item.key}.jpg`,
                        }}
                        style={[
                            styles.image,
                            {borderColor: `${item.borderColor}`},
                        ]}
                    />
                    <Text style={styles.itemText}>{item.name}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );

    const handleClickToSwitch = (categoryId: number) => {
        dispatch(switchCategory(categoryId));
    };
    return (
        <View style={styles.categoryButtonsContainer}>
            <FlatList
                data={items}
                renderItem={renderItem}
                horizontal={true}
                showsHorizontalScrollIndicator={false}></FlatList>
        </View>
    );
};

export default ForumCategories;
