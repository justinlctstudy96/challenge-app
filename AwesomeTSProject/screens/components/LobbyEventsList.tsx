import React, {useState, useEffect} from 'react';
import {
    ImageBackground,
    View,
    Text,
    FlatList,
    ListRenderItem,
    StyleSheet,
    ScrollView,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {fetchEvents} from '../../redux/challengeLobby/actions';
import {
    fetchCategories,
    fetchVerifications,
} from '../../redux/challengeForm/actions';
import {Category, Verification} from '../../redux/challengeForm/reducers';
import {ChallengeEventSummary} from '../../redux/challengeLobby/reducers';
import {RootState} from '../../store';
import {FilterOrder} from '../../redux/challengeLobby/reducers';
import SelectedCategoryBlocks from './SelectedCategoryBlocks';
import SelectedVerification from './SelectedVerificationIcon';
import SelectedBooleanAttributes from './SelectedBooleanAttributes';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import {
    NavigationHelpersContext,
    useNavigation,
} from '@react-navigation/native';
import {config} from '../../redux/config';

export default function LobbyEventsList(props: FilterOrder) {
    const [eventList, setEventList] = useState<ChallengeEventSummary[]>([]);
    const navigation = useNavigation();
    const [allCategories, setAllCategories] = useState<Category[]>([]);
    const [allVerifications, setAllVerifications] = useState<Verification[]>(
        [],
    );

    const userDetails = useSelector((state: RootState) => state.user.user);


    // const dispatch = useDispatch()

    const getEvents = async (
        filter: number[],
        savedFilter: boolean,
        orderedBy: string,
    ) => {
        let filterOrder = {} as FilterOrder;
        filterOrder.filter = filter;
        filterOrder.savedFilter = savedFilter;
        filterOrder.orderedBy = orderedBy;
        let eventsDetails = await fetchEvents(filterOrder);
        // setEventList(eventsDetails);
        setEventList(
            eventsDetails.filter(event =>
                event.title
                    .toLowerCase()
                    .includes(props.searchText.toLowerCase()),
            ),
        );
    };

    const getAllCategoriesVerifications = async () => {
        setAllCategories((await fetchCategories()).rows);
        setAllVerifications((await fetchVerifications()).rows);
    };

    useEffect(() => {
        getEvents(props.filter, props.savedFilter, props.orderedBy);
        getAllCategoriesVerifications();
        // setEventsDetailCategoryName();
    }, [props.filter, props.savedFilter, props.orderedBy]);

    useEffect(() => {
        getEvents(props.filter, props.savedFilter, props.orderedBy);
    }, [props.searchText]);

    const renderEvents: ListRenderItem<ChallengeEventSummary> = ({item}) => {
        try {
            let verificationMethod = '';
            allVerifications.map(verification => {
                if (verification.id === item.verifications[0]) {
                    verificationMethod = verification.method;
                }
            });
            const totalChallengeDays = item.persistence
                ? item.days * item.cycles
                : item.days;
            let startYear, startMonth, startDay, endYear, endMonth, endDay;
            [startYear, startMonth, startDay] = item.start_date.split('-');
            [endYear, endMonth, endDay] = item.end_date.split('-');

            const eventRoomNavigate = (eventId: number) => {
                navigation.navigate('ChallengeRoom', {eventId: eventId});
            };

            return (
                <View style={item.host_id===userDetails?.id? styles.hostEventSummaryContainer : styles.eventSummaryContainer}>
                    <TouchableOpacity
                        onPress={() => {
                            eventRoomNavigate(item.event_id);
                        }}>
                        <ImageBackground
                            source={{
                                uri: `${config.S3_BUCKET_URL}/background/${item.categories[0]}.jpg`,
                            }}
                            style={styles.image}>
                            <View style={styles.overlay}>
                                <View style={styles.titleRowContainer}>
                                    <View style={{width: '79%'}}>
                                        <ScrollView
                                            style={{
                                                width: '100%',
                                                marginRight: 4,
                                            }}
                                            horizontal={true}>
                                            <View>
                                                <Text style={styles.titleText}>
                                                    {item.title}
                                                </Text>
                                            </View>
                                        </ScrollView>
                                        <ScrollView
                                            style={{
                                                width: '100%',
                                                marginRight: 4,
                                            }}
                                            horizontal={true}>
                                            <View>
                                                <Text style={styles.ruleText}>
                                                    {item.rule}
                                                </Text>
                                            </View>
                                        </ScrollView>
                                    </View>
                                    <View
                                        style={{
                                            width: '20%',
                                            justifyContent: 'center',
                                        }}>
                                        <View style={styles.totalDays}>
                                            <Text style={{color: 'white'}}>
                                                {totalChallengeDays}{' '}
                                                {totalChallengeDays == 1
                                                    ? 'Day'
                                                    : 'Days'}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.rowContainer}>
                                    <View style={{width: '50%'}}>
                                        <SelectedCategoryBlocks
                                            selectedCategories={item.categories}
                                            allCategories={allCategories}
                                        />
                                        <View style={styles.rowContainer}>
                                            <View style={styles.eventTime}>
                                                <Text style={styles.timeText}>
                                                    Start
                                                </Text>
                                                <Text style={styles.timeText}>
                                                    {startDay +
                                                        '/' +
                                                        startMonth +
                                                        '/' +
                                                        startYear.slice(-2)}
                                                </Text>
                                            </View>
                                            <View style={styles.eventTime}>
                                                <Text style={styles.timeText}>
                                                    End
                                                </Text>
                                                <Text style={styles.timeText}>
                                                    {endDay +
                                                        '/' +
                                                        endMonth +
                                                        '/' +
                                                        endYear.slice(-2)}
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{width: '50%'}}>
                                        <View style={{marginBottom: 10}}>
                                            <Text
                                                style={{
                                                    color: 'white',
                                                    textAlign: 'left',
                                                    marginBottom: 13,
                                                }}>
                                                Host: {item.host_name}
                                            </Text>
                                        </View>
                                        <View style={{flexDirection: 'row'}}>
                                            <View style={styles.verification}>
                                                <SelectedVerification
                                                    selectedVerification={
                                                        item.verifications
                                                    }
                                                    allVerifications={
                                                        allVerifications
                                                    }
                                                />
                                            </View>
                                            <View style={styles.participants}>
                                                <Icon
                                                    name={'people'}
                                                    color={
                                                        'rgba(255,255,255,0.8)'
                                                    }
                                                    size={30}
                                                />
                                                <Text
                                                    style={
                                                        styles.participantsText
                                                    }>
                                                    {item.participants}/
                                                    {item.accommodation}
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                                <SelectedBooleanAttributes
                                    persistence={item.persistence}
                                    consecutive={item.consecutive}
                                    withSection={item.with_section}
                                    bold={false}
                                />
                            </View>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
            );
        } catch (e) {
            console.log(e);
            return <View></View>;
        }
    };

    return (
        <View style={styles.eventListContainer}>
            <FlatList
                data={eventList}
                showsVerticalScrollIndicator={false}
                renderItem={renderEvents}
                // keyExtractor={item => item.event_id.toString()}
                contentContainerStyle={{justifyContent: 'center'}}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    eventListContainer: {
        flex: 1,
        height: '100%',
        borderWidth: 2,
        borderRadius: 20,
        borderColor: '#ccff66',
        padding: '3%',
        marginTop: 15,
    },
    image: {
        flex: 1,
        borderRadius: 100,
    },
    overlay: {
        backgroundColor: 'rgba(49,49,49,0.8)',
        flex: 1,
        padding: 10,
    },
    eventSummaryContainer: {
        margin: 5,
        borderRadius: 30,
        // borderWidth: 3,
        overflow: 'hidden',
        borderColor: 'white',
    },
    hostEventSummaryContainer: {
        margin: 5,
        borderRadius: 30,
        borderWidth: 2,
        overflow: 'hidden',
        borderColor: '#ccff66',
    },
    titleRowContainer: {
        flexDirection: 'row',
    },
    titleText: {
        width: '100%',
        fontSize: 20,
        alignSelf: 'center',
        color: 'white',
        fontWeight: 'bold',
    },
    ruleText: {
        color: 'orange',
        width: '100%',
        fontSize: 15,
        alignSelf: 'center',
    },
    totalDays: {
        // width: '20%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    rowContainer: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    eventTime: {
        width: '50%',
    },
    timeText: {
        color: 'white',
    },
    verification: {
        width: '50%',
    },
    participants: {
        width: '50%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    participantsText: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center',
    },
});
