import React from 'react';
import {
    ImageBackground,
    View,
    Text,
    StyleSheet,
    FlatList,
    SafeAreaView,
    ListRenderItem,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    Picker,
    ViewPropTypes,
} from 'react-native';

interface TimesDaysCycles {
    persistence: boolean;
    times: number;
    days: number;
    cycles: number;
}

export default function ChallengeTimesDaysCyclesBlock(props: TimesDaysCycles) {
    return (
        <View
            style={[
                styles.timesContainer,
                props.persistence ? {} : {height: 130, alignItems: 'center'},
            ]}>
            <View style={styles.timesBlock}>
                <Text style={styles.timesNumber}>{props.times}</Text>
                <Text style={styles.timesText}>
                    {(props.times) > 1 ? 'times' : 'time'}
                </Text>
            </View>
            <View style={styles.timesBlock}>
                <Text style={styles.timesText}>
                    {props.persistence ? 'every' : 'within'}
                </Text>
            </View>
            <View style={styles.timesBlock}>
                <Text style={styles.timesNumber}>{props.days}</Text>
                <Text style={styles.timesText}>
                    {(props.days) > 1 ? 'days' : 'day'}
                </Text>
            </View>
            {props.persistence ? (
                <>
                    <View style={styles.timesBlock}>
                        <Text style={styles.timesText}>for</Text>
                    </View>
                    <View style={styles.timesBlock}>
                        <Text style={styles.timesNumber}>{props.cycles}</Text>
                        <Text style={styles.timesText}>
                            {(props.days) > 1 ? 'cycle' : 'day'}
                        </Text>
                    </View>
                </>
            ) : null}
        </View>
    );
}

const styles = StyleSheet.create({
    timesContainer: {
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    timesBlock: {
        height: 80,
        width: '20%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 10,
    },
    timesText: {
        width: '100%',
        textAlign: 'center',
        height: 30,
        borderBottomRightRadius: 100,
        fontSize: 20,
        color: 'white',
    },
    timesNumber: {
        height: 40,
        fontSize: 30,
        color: 'white',
    },
})