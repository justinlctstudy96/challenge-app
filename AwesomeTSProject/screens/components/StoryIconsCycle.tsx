import React, {useEffect, useState} from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ListRenderItem,
    ScrollView,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';
import {
    VerificationRecord,
    ChallengerVerificationRecords,
    CycleVerificationRecords,
    CycleDate,
    ChallengerInfo,
    ChallengeTimeDetails,
} from '../../redux/roomStory/reducer';
import {useDispatch, useSelector} from 'react-redux';
import {config} from '../../redux/config';
import LinearGradient from 'react-native-linear-gradient';
import CircularProgress from '../CircularProgress';
import Icon from 'react-native-vector-icons/Ionicons';
interface CycleStories {
    cycleRecords: ChallengerVerificationRecords[];
    challengeTimeDetails: ChallengeTimeDetails;
}
interface User {
    nickname: string;
    pro_pic: string;
}
// Break down into two - three level of components
export default function StoryIconsCycle(props: CycleStories) {
    const [expand, setExpand] = useState(false);
    const navigation = useNavigation();
    const renderChallengers: ListRenderItem<ChallengerVerificationRecords> = ({
        item,
    }) => {
        const noOfRecords = item.verificationRecords.length;
        const renderVerifications: ListRenderItem<VerificationRecord> = ({
            item,
            index,
        }) => {
            return (
                // story circle
                <>
                    <TouchableOpacity
                        onPress={() => {
                            // dispatch(storyNum(item.id));

                            navigation.navigate('Stories', {
                                flatListDataId: item.storyFlatListIndex,
                                nth_cycle: item.nth_cycle,
                            });
                        }}>
                        <View style={styles.storyBlock}>
                            <CircularProgress
                                percent={
                                    (noOfRecords /
                                        props?.challengeTimeDetails?.times) *
                                    100
                                }
                                textFontSize={0}
                                radius={45}
                                bgRingWidth={5}
                                progressRingWidth={4}
                                showPercent={false}
                                ringColor={'#ccff66'}
                                // ringColor={'#CCFF66'}
                            />
                            <Image
                                style={[styles.pendingStory]}
                                source={{
                                    // uri: `${config.S3_BUCKET_URL}/${item.pro_pic}.jpeg`,
                                    uri: `${config.S3_BUCKET_URL}/${item.pro_pic}`,
                                }}
                            />
                        </View>
                    </TouchableOpacity>
                    {!expand ? (
                        index === 0 ? (
                            <Text style={styles.name}>{item.nickname}</Text>
                        ) : null
                    ) : null}
                </>
            );
        };

        let nonSubmittedUserData = [] as User[];
        for (
            let i = 0;
            i <
            props.challengeTimeDetails?.times - item.verificationRecords?.length;
            i++
        ) {
            let user = {} as User;
            user.nickname = item.challenger.nickname;
            user.pro_pic = item.challenger.pro_pic;
            nonSubmittedUserData.push(user);
        }
        const renderNonSubmittedIcons: ListRenderItem<User> = ({
            item,
            index,
        }) => {
            return (
                <View>
                    <View style={styles.storyBlock}>
                        <Image
                            style={[styles.noStory]}
                            source={{
                                uri: `${config.S3_BUCKET_URL}/${item.pro_pic}`,
                            }}
                        />
                    </View>
                    {!expand ? (
                        index === 0 ? (
                            <View>
                                <Text style={styles.name}>{item.nickname}</Text>
                            </View>
                        ) : null
                    ) : null}
                </View>
            );
        };

        return (
            // list of story circles of each challenger
            <View>
                {item.verificationRecords.length === 0 ? null : (
                    <FlatList
                        data={item.verificationRecords}
                        renderItem={renderVerifications}
                        keyExtractor={index => index.id.toString()}
                        scrollEnabled={false}
                    />
                )}
                <FlatList
                    data={nonSubmittedUserData}
                    renderItem={renderNonSubmittedIcons}
                    scrollEnabled={false}
                />
                {expand ? (
                    <Text style={styles.name}>{item.challenger.nickname}</Text>
                ) : null}
            </View>
        );
    };
    return (
        // list of challengers
        <View style={expand ? styles.expand : styles.notExpand}>
            <ScrollView scrollEnabled={false}>
                <FlatList
                    data={props?.cycleRecords}
                    renderItem={renderChallengers}
                    horizontal={true}
                />
            </ScrollView>
            {props?.challengeTimeDetails?.times>1?
            <LinearGradient
                colors={['rgba(196, 196, 196, 0)', '#ccff66']}
                style={styles.expand}>
                <TouchableOpacity
                    onPress={() => {
                        setExpand(!expand);
                    }}>
                    {expand ? (
                        <Icon
                            name="caret-up-outline"
                            size={20}
                            color="#ccff66"
                        />
                    ) : (
                        <Icon
                            name="caret-down-outline"
                            size={20}
                            color="#ccff66"
                        />
                    )}
                </TouchableOpacity>
            </LinearGradient>
            :null}
        </View>
    );
}

const styles = StyleSheet.create({
    text: {
        color: 'white',
        fontSize: 16,
    },
    storyBlock: {
        // position:'absolute',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: 5,
    },
    pendingStory: {
        width: 80,
        height: 80,
        borderRadius: 50,
        marginBottom: 7,
        marginRight: 10,
        // borderWidth: 5,
        // borderColor: 'cyan',
        marginTop: 5,
        // backgroundColor: 'green',
        position: 'absolute',
    },
    verifiedStory: {
        width: 80,
        height: 80,
        borderRadius: 50,
        marginBottom: 7,
        marginRight: 10,
        borderWidth: 5,
        borderColor: 'green',
    },

    noStory: {
        width: 90,
        height: 90,
        borderRadius: 50,
        // marginBottom: 7,
        // marginRight: 10,
        borderWidth: 5,
        borderColor: 'gray',
        // backgroundColor: 'red',
    },

    uploadPending: {
        borderWidth: 3,
        borderRadius: 50,
        borderColor: '#C4C4C4',
    },
    hostStory: {
        borderWidth: 3,
        borderRadius: 50,
        borderColor: '#FFA800',
    },
    pending_for_verify: {
        borderWidth: 3,
        borderRadius: 50,
        borderColor: '#37D0E4',
    },
    verifyDone: {
        borderWidth: 3,
        borderRadius: 50,
        borderColor: '#CCFF66',
    },
    missionFail: {
        borderWidth: 3,
        borderRadius: 50,
        borderColor: '#FE0303',
    },
    expand: {
        width: '100%',
        // borderWidth: 2,
        // borderColor: 'transparent',
        // borderBottomColor: '#CCFF66',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    notExpand: {
        width: '100%',
        height: 145,
        // borderWidth: 2,
        // borderColor: 'transparent',
        // borderBottomColor: '#CCFF66',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    name: {
        fontSize: 15,
        color: 'white',
        marginBottom: 3,
        textAlign: 'center',
    },
    // expand: {
    //     borderWidth: 2,
    //     borderColor: 'transparent',
    //     borderBottomColor: '#CCFF66',
    //     borderRadius: 30,
    //     justifyContent: 'center',
    //     alignItems: 'center',
    // },
});
