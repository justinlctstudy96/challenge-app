'use strict';
import React, {useRef, useState} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {RNCamera} from 'react-native-camera';
import GestureRecognizer from 'react-native-swipe-gestures';
import Icon from 'react-native-vector-icons/Ionicons';
import RNFS from 'react-native-fs';

export default function LiveCamera() {
    const [isPreview, setIsPreview] = useState(false);
    const navigation = useNavigation();
    const cameraRef = useRef(null);
    const takePicture = async () => {
        if (cameraRef && cameraRef.current) {
            const imagePath = `../../uploads/img.jpg`;
            const options = {quality: 0.5, base64: true, skipProcessing: true};
            //@ts-ignore
            const data = await cameraRef.current.takePictureAsync(options);
            const source = data.base64;
            RNFS.writeFile(imagePath, source, 'base64').then(() =>
                console.log('Image converted to jpg and saved at ' + imagePath),
            );
            if (source) {
                //@ts-ignore
                await cameraRef.current.pausePreview();
                setIsPreview(true);
            }
        }
    };
    const renderTakePictureBtn = () => {
        return (
            <View
                style={{
                    flex: 0,
                    flexDirection: 'row',
                    justifyContent: 'center',
                }}>
                <TouchableOpacity onPress={takePicture} style={styles.capture}>
                    <Icon name="camera-outline" size={40} color="#ccff66" />
                </TouchableOpacity>
            </View>
        );
    };
    const renderCancelPreviewBtn = () => {
        return (
            <View
                style={{
                    flex: 0,
                    flexDirection: 'row',
                    justifyContent: 'center',
                }}>
                <TouchableOpacity
                    onPress={async () => {
                        setIsPreview(false);
                        //@ts-ignore
                        await cameraRef.current.resumePreview();
                    }}
                    style={styles.capture}>
                    <Icon name="close-outline" size={40} color="#e9213b" />
                </TouchableOpacity>
            </View>
        );
    };
    return (
        <GestureRecognizer
            style={styles.container}
            onSwipeDown={() => navigation.navigate('ChallengeRoom')}>
            <RNCamera
                ref={cameraRef}
                style={styles.preview}
                type={RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.off}
            />

            {isPreview ? renderCancelPreviewBtn() : renderTakePictureBtn()}
        </GestureRecognizer>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#313131',
        borderRadius: 30,
        padding: 10,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
});
