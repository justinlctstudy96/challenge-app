import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    ListRenderItem,
    FlatList,
    Dimensions,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import {config} from '../../redux/config';
import {useDispatch, useSelector} from 'react-redux';
import {switchCategory, switchSelectedPost} from '../../redux/forum/actions';
import {IPostsDetail} from '../../redux/forum/state';
import {RootState} from '../../store';
import {
    fetchDeleteLike,
    fetchLikes,
    fetchPostLike,
} from '../../redux/forum/thunks';
import {getTimeString} from '../ChatRoomScreen';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    postsContainer: {
        width: '92%',
        marginLeft: 15,
        marginRight: 15,
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    },
    postContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        padding: 5,
        marginBottom: 15,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ccff66',
    },
    postMainBody: {
        width: '100%',
    },
    //styles for profile
    profileContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: '100%',
        padding: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#888888',
    },
    profilePicture: {
        width: 50,
        height: 50,
        borderRadius: 50,
    },
    profileInfoContainer: {
        justifyContent: 'flex-start',
        padding: 10,
    },
    profileNickname: {
        fontSize: 15,
        color: '#FFFFFF',
        fontWeight: '500',
    },
    createAt: {
        fontSize: 12,
        color: '#FFFFFF',
    },

    //post contents
    postContentContainer: {
        width: '100%',
    },
    postContent: {
        padding: 10,
        color: '#FFFFFF',
        fontSize: 20,
    },

    postImageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 5,

        width: '100%',
    },
    postImage: {
        width: windowWidth - 50,
        height: windowWidth - 50,
        borderRadius: 20,
        marginBottom: 10,
        resizeMode: 'cover',
    },

    //count related
    countContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderColor: '#888888',
        borderTopWidth: 1,
    },
    countItemContainer: {
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    countItem: {
        color: '#FFFFFF',
        fontSize: 15,
    },
    //like and comment button
    buttonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderTopWidth: 1,
        borderTopColor: '#888888',
    },
    buttonContainer: {
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        width: '50%',
    },
    button: {
        color: '#FFFFFF',
    },
    buttonText: {
        color: '#FFFFFF',
        fontSize: 15,
    },
    likedButton: {
        color: '#FE0303',
    },
    likedButtonText: {
        color: '#FE0303',
        fontSize: 15,
    },
});

const Posts = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const allPosts = useSelector((state: RootState) => state.forum.posts);
    const allLikes = useSelector((state: RootState) => state.forum.likes);
    const selectedCategory = useSelector(
        (state: RootState) => state.forum.category,
    );
    const selectedPosts = allPosts.filter(
        post => post.category_id === selectedCategory,
    );

    const renderItem: ListRenderItem<IPostsDetail> = ({item}) => (
        <View style={styles.postContainer}>
            <View style={styles.postMainBody}>
                <View style={styles.profileContainer}>
                    <Image
                        style={styles.profilePicture}
                        source={{
                            uri: `${config.S3_BUCKET_URL}/${item.pro_pic}`,
                        }}
                    />
                    <View style={styles.profileInfoContainer}>
                        <Text style={styles.profileNickname}>
                            {item.nickname}
                        </Text>
                        <Text style={styles.createAt}>
                            {item.created_at.split('T')[0]}{' '}
                            {getTimeString(
                                item.created_at.split('T')[1].split('.')[0],
                            )}
                        </Text>
                    </View>
                </View>
                <View style={styles.postContentContainer}>
                    <Text style={styles.postContent}>{item.content}</Text>
                    {item.photo && (
                        <Image
                            style={styles.postImage}
                            source={{
                                uri: `${config.S3_BUCKET_URL}/${item.photo}`,
                            }}
                        />
                    )}
                    <View style={styles.countContainer}>
                        <View style={styles.countItemContainer}>
                            <Text style={styles.countItem}>
                                {item.likes} Likes
                            </Text>
                        </View>
                        <View style={styles.countItemContainer}>
                            <Text style={styles.countItem}>
                                {item.comments} Comments
                            </Text>
                        </View>
                    </View>
                </View>
            </View>
            <View style={styles.buttonsContainer}>
                {allLikes.includes(item.id) ? (
                    <TouchableOpacity
                        style={styles.buttonContainer}
                        onPress={() => clickToUnlike(item.id)}>
                        <Icon
                            name="heart"
                            size={20}
                            style={styles.likedButton}
                        />
                        <Text style={styles.likedButtonText}> Liked</Text>
                    </TouchableOpacity>
                ) : (
                    <TouchableOpacity
                        style={styles.buttonContainer}
                        onPress={() => clickToLike(item.id)}>
                        <Icon name="heart" size={20} style={styles.button} />
                        <Text style={styles.buttonText}> Like</Text>
                    </TouchableOpacity>
                )}

                <TouchableOpacity
                    style={styles.buttonContainer}
                    onPress={() => clickToComment(item.id)}>
                    <Icon name="arrow-redo" size={20} style={styles.button} />
                    <Text style={styles.buttonText}> Comment</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
    const clickToLike = (postId: number) => {
        dispatch(fetchPostLike(postId));
    };
    const clickToUnlike = (postId: number) => {
        dispatch(fetchDeleteLike(postId));
    };
    const clickToComment = (postId: number) => {
        const selectedPost = selectedPosts.filter(
            selectedPosts => selectedPosts.id === postId,
        )[0];
        dispatch(switchSelectedPost(selectedPost));
        navigation.navigate('PostDetail');
    };
    return (
        <View style={styles.postsContainer}>
            <FlatList
                data={selectedPosts}
                renderItem={renderItem}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}></FlatList>
        </View>
    );
};

export default Posts;
