import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    FlatList,
    useColorScheme,
    View,
    Image,
    TextInput,
    Platform,
    Alert,
    Dimensions,
} from 'react-native';
import {
    launchCamera,
    launchImageLibrary,
    Asset,
    ImageLibraryOptions,
} from 'react-native-image-picker';
import { useForm, Controller } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { RootState } from '../../store';
import { removeForumImage, uploadForumImage } from '../../redux/forum/actions';
import { config } from '../../redux/config';
import { fetchCreatePost } from '../../redux/forum/thunks';
import Icon from 'react-native-vector-icons/Ionicons';
import LoadingSpinner from './LoadingSpinner';


const items = [
    { key: 1, name: 'Health', borderColor: '#F0CE1D' },
    { key: 2, name: 'Study', borderColor: '#597EDE' },
    { key: 3, name: 'Lifestyle', borderColor: '#1F9D3B' },
    { key: 4, name: 'Sport', borderColor: '#FFA800' },
    { key: 5, name: 'Music', borderColor: '#9D1F1F' },
    { key: 6, name: 'Others', borderColor: '#CB59DE' },
];

interface IPost {
    content: string;
}

interface IErrorProps {
    name: keyof IPost;
}

const CreatePost = () => {

    const {
        control,
        handleSubmit,
        formState: { errors },
    } = useForm<IPost>();
    const dispatch = useDispatch();
    const navigation = useNavigation();

    const isAuthenticated = useSelector(
        (state: RootState) => state.user.isAuthenticated,
    );
    const userDetails = useSelector((state: RootState) => state.user.user);

    useEffect(() => {
        if (!isAuthenticated) {
            navigation.navigate('Login');
        }
    }, [isAuthenticated, navigation]);


    const ErrorBlock = (props: IErrorProps) => {
        return (
            <ErrorMessage
                errors={errors}
                name={props.name}
                render={({ message }) => (
                    <View style={styles.errorContainer}>
                        <Text style={styles.errorMessage}>{message}</Text>
                    </View>
                )}
            />
        );
    };

    //setting image upload logic
    const postPhoto = useSelector((state: RootState) => state.forum.forumPhoto);
    const postCategory = useSelector(
        (state: RootState) => state.forum.category,
    );
    const options: ImageLibraryOptions = {
        mediaType: 'photo',
        quality: 0.2,
    };

    const handleChoosePhoto = () => {
        launchImageLibrary(options, response => {
            if (response.assets) {
                dispatch(uploadForumImage(response.assets[0]));
            } else {
                console.log(response);
            }
        });
    };

    const handleTakePhoto = () => {
        launchCamera(options, response => {
            if (response.assets) {
                dispatch(uploadForumImage(response.assets[0]));
            } else {
                console.log(response);
            }
        });
    };

    const clearImage = () => {
        dispatch(removeForumImage());
    }

    const submitPost = (data: IPost) => {
        const postInfo = new FormData();
        postInfo.append('categoryId', postCategory);
        postInfo.append('content', data.content);
        if (postPhoto) {
            postInfo.append('postPhoto', {
                name: postPhoto.fileName,
                type: postPhoto.type,
                uri:
                    Platform.OS === 'ios'
                        ? postPhoto.uri.replace('file://', '')
                        : postPhoto.uri,
            });
        }
        dispatch(fetchCreatePost(postInfo));
        navigation.navigate('Forum')
    };

    return (
        <SafeAreaView style={styles.background}>
            <ScrollView
                style={styles.container}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}>
                <LoadingSpinner />
                <View style={styles.postContainer}>
                    <View style={styles.postTitleContainer}>
                        <Text style={styles.postTitle}>Post</Text>
                    </View>
                    <View style={styles.postTitleDetailContainer}>
                        <View style={styles.profileContainer}>
                            <Image
                                style={styles.profilePicture}
                                source={{
                                    uri: `${config.S3_BUCKET_URL}/${userDetails?.pro_pic}`,
                                }}
                            />
                            <View style={styles.profileNicknameContainer}>
                                <Text style={styles.profileNickname}>
                                    {userDetails?.nickname
                                        ? userDetails?.nickname
                                        : 'null'}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.category}>
                            <View style={styles.categoryImageContainer}>
                                <Image
                                    source={{
                                        uri: `${config.S3_BUCKET_URL}/background/${postCategory}.jpg`,
                                    }}
                                    style={[
                                        styles.categoryImage,
                                        {
                                            borderColor: `${items[postCategory - 1]
                                                .borderColor
                                                }`,
                                        },
                                    ]}
                                />
                                <Text style={styles.categoryText}>
                                    {items[postCategory - 1].name}
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.form}>
                    <View style={styles.inputContainer}>
                        <Controller
                            control={control}
                            render={({ field: { onChange, onBlur, value } }) => (
                                <TextInput
                                    style={styles.input}
                                    onBlur={onBlur}
                                    placeholder="Share your thoughts"
                                    placeholderTextColor="#ccff6683"
                                    onChangeText={value => onChange(value)}
                                    value={value}
                                    multiline={true}
                                />
                            )}
                            name="content"
                            rules={{
                                required: {
                                    value: true,
                                    message: 'Content cannot be empty',
                                },
                            }}
                            defaultValue=""
                        />
                        <ErrorBlock name="content" />
                        {postPhoto && (
                            <View style={styles.postImageContainer}>
                                <Image
                                    source={{ url: postPhoto.uri }}
                                    style={styles.postImage}
                                />
                            </View>
                        )}
                    </View>

                    <View style={styles.photoButtonContainer}>
                        {postPhoto && (
                            <TouchableOpacity style={styles.removePhoto} onPress={clearImage} >
                                <Icon name="trash" size={50} color='#FFFFFF'/>
                            </TouchableOpacity>
                        )}
                        <TouchableOpacity
                            style={styles.takePhoto}
                            onPress={handleTakePhoto}>
                            <Icon name="camera" size={50} color='#FFFFFF'/>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.uploadPhoto}
                            onPress={handleChoosePhoto}>
                            <Icon name="arrow-up" size={50} color='#FFFFFF'/>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                        style={styles.signUpSubmit}
                        onPress={handleSubmit(submitPost)}>
                        <Text style={styles.signUpSubmitText}>Post</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
    background: {
        backgroundColor: '#313131',
    },
    container: {
        backgroundColor: 'transparent',
        height: '100%',
        marginRight: 10,
        marginLeft: 10,
    },
    postContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
    },
    postTitleContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: '100%',
    },
    postTitle: {
        color: '#FFFFFF',
        fontSize: 35,
        fontWeight: 'bold',
    },

    postTitleDetailContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
    },

    profileContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        flexWrap: 'wrap',
    },
    profilePicture: {
        width: 75,
        height: 75,
        borderRadius: 50,
    },
    profileNicknameContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    profileNickname: {
        fontSize: 20,
        color: '#FFFFFF',
        fontWeight: '500',
    },
    category: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    categoryImageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    categoryImage: {
        borderRadius: 25,
        width: 50,
        height: 50,
        borderWidth: 2,
    },
    categoryText: {
        color: '#FFFFFF',
        textAlign: 'center',
        fontSize: 15,
    },
    form: {
        justifyContent: 'center',
        flex: 1,
        backgroundColor: '#313131',
    },
    inputContainer: {
        borderColor: '#ccff66',
        borderWidth: 1,
        borderRadius: 20,
        height:windowHeight-400
    },
    input: {
        marginTop: 10,
        paddingLeft: 10,
        color: '#FFFFFF',
        fontSize: 20,
    },
    //error message styles
    errorContainer: {
        padding: 5,
    },
    errorMessage: {
        color: '#FFFFFF',
    },
    //photo related styles
    postImageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        marginBottom: 10,
    },
    postImage: {
        width: windowWidth - 50,
        height: windowHeight- 500,
        borderRadius: 20,
        resizeMode: 'contain',
    },

    photoButtonContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
    },
    removePhoto: {
        backgroundColor: '#FFEE59',
        height: 75,
        width:75,
        borderRadius: 50,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
    },
    takePhoto: {
        backgroundColor: '#FF59A5',
        height: 75,
        width:75,
        borderRadius: 50,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
    },
    uploadPhoto: {
        backgroundColor: '#9073FF',
        height: 75,
        width:75,
        borderRadius: 50,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
    },
    signUpSubmit: {
        backgroundColor: '#ccff66',
        height: 50,
        width: '100%',
        borderRadius: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
    },
    signUpSubmitText: {
        fontSize: 20,
        fontWeight: '500',
    },
});

export default CreatePost;
