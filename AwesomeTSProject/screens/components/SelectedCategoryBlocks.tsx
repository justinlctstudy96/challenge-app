import React from 'react';
import {Category} from '../../redux/challengeForm/reducers';
import {View, Text, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

interface Selection {
    selectedCategories: number[];
    allCategories: Category[];
}

export default function SelectedCategoryBlocks(props: Selection) {
    let selectedCategoriesBlocks: any = [];
    props.selectedCategories.map(categoryId => {
        let categoryName = '';
        props.allCategories.map(category => {
            if (category.id === categoryId) {
                categoryName = category.category;
            }
        });
        selectedCategoriesBlocks.push(
            <View key={categoryName} style={[styles.category, styles[`${categoryName}`]]}>
                <TouchableOpacity onPress={() => {}}>
                    <Text>{categoryName}</Text>
                </TouchableOpacity>
            </View>,
        );
    });
    return (
        <View style={{flexDirection: 'row'}}>{selectedCategoriesBlocks}</View>
    );
}

const styles = StyleSheet.create({
    category: {
        paddingLeft: 10,
        paddingRight: 10,
        borderColor: '#D1EF0F',
        borderRadius: 30,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
    },
    "Health": {
        backgroundColor:'#F0CE1D'
    },
    "Study": {
        backgroundColor:'#597EDE'
    },
    "Lifestyle": {
        backgroundColor:'#1F9D3B'
    },
    "Sport": {
        backgroundColor:'#FFA800'
    },
    "Music": {
        backgroundColor:'#9D1F1F'
    },
    "Others": {
        backgroundColor:'#CB59DE'
    },
});

