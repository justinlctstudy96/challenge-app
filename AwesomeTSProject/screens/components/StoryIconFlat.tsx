import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ListRenderItem,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import Story from './Story';
import {
    fetchAllRecordsAccordingToDate,
    fetchRoomUser,
    storyNum,
} from '../../redux/roomStory/actions';
import {RootState} from '../../store';
import {config} from '../../redux/config';
import {fetchAllDate} from '../../redux/challengeRoom/actions';
import {fetchAllVerificationRecords} from '../../redux/roomStory/actions';
import {
    VerificationRecord,
    ChallengerVerificationRecords,
    CycleVerificationRecords,
    CycleDate,
    ChallengerInfo,
} from '../../redux/roomStory/reducer';
import StoryIconsCycle from './StoryIconsCycle';

interface Iitem {
    nickname: string;
    pro_pic: string;
    id: number;
}

interface storyProps {
    eventId: number;
    methodId: number;
}

export default function StoryIconFlat(props: storyProps) {
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const allUsers = useSelector((state: RootState) => state.userList.allUser);
    // const allRecords = useSelector(
    //     (state: RootState) => state.allRecords.allRecords,
    // );
    const allVerificationRecords: CycleVerificationRecords[] = useSelector(
        (state: RootState) =>
            state.roomVerificationRecordsState.allVerificationRecords,
    );
    const cycleDates = useSelector(
        (state: RootState) => state.roomVerificationRecordsState.cycleDates,
    );
    const challengeTimeDetails = useSelector(
        (state: RootState) =>
            state.roomVerificationRecordsState.challengeTimeDetails,
    );

    const renderCycles: ListRenderItem<CycleVerificationRecords> = ({item}) => {
        if(cycleDates){
            return (
                // list of challengers
                <View style={{width: '100%'}}>
                    <Text style={styles.text}>Cycle {item.nth_cycle}</Text>
                    <Text style={styles.greenText}>
                        {cycleDates[item.nth_cycle - 1]?.start_date} ~{' '}
                        {cycleDates[item.nth_cycle - 1]?.end_date}
                    </Text>
    
                    <StoryIconsCycle
                        cycleRecords={item.challengersVerificationRecords}
                        challengeTimeDetails={challengeTimeDetails}
                    />
                </View>
            );
        }else{
            return (<View></View>)
        }
    };

    return (
        // list of cycles
        <SafeAreaView style={{width: '100%', justifyContent: 'space-around'}}>
            <FlatList
                data={allVerificationRecords}
                renderItem={renderCycles}
                scrollEnabled={false}
            />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    greenText: {
        fontWeight: 'bold',
        color: '#ccff66',
    },
    text: {
        color: 'white',
        fontSize: 16,
    },
    storyBlock: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginRight: 10,
    },
    pendingStory: {
        width: 80,
        height: 80,
        borderRadius: 50,
        marginBottom: 7,
        marginRight: 10,
        borderWidth: 5,
        borderColor: 'cyan',
    },
    verifiedStory: {
        width: 80,
        height: 80,
        borderRadius: 50,
        marginBottom: 7,
        marginRight: 10,
        borderWidth: 5,
        borderColor: 'green',
    },

    noStory: {
        width: 80,
        height: 80,
        borderRadius: 50,
        marginBottom: 7,
        marginRight: 10,
        borderWidth: 5,
        borderColor: 'gray',
    },

    uploadPending: {
        borderWidth: 3,
        borderRadius: 50,
        borderColor: '#C4C4C4',
    },
    hostStory: {
        borderWidth: 3,
        borderRadius: 50,
        borderColor: '#FFA800',
    },
    pending_for_verify: {
        borderWidth: 3,
        borderRadius: 50,
        borderColor: '#37D0E4',
    },
    verifyDone: {
        borderWidth: 3,
        borderRadius: 50,
        borderColor: '#CCFF66',
    },
    missionFail: {
        borderWidth: 3,
        borderRadius: 50,
        borderColor: '#FE0303',
    },
});
