import { useNavigation } from '@react-navigation/native';
import React, {useState} from 'react';
import {useEffect} from 'react';
import {
    StyleSheet,
    View,
    Text,
    FlatList,
    ListRenderItem,
    ImageBackground,
    TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useDispatch } from 'react-redux';
import { createChallengeSuccess } from '../../redux/challengeForm/actions';
import {fetchChallenges} from '../../redux/challengeLobby/actions';
import {FilterOrder} from '../../redux/challengeLobby/reducers';
import {ChallengeSummary} from '../../redux/challengeLobby/reducers';
import {config} from '../../redux/config';

const styles = StyleSheet.create({
    popularChallengesContainer: {
        flexDirection: 'row',
        width: '100%',
    },
    itemContainer: {
        borderRadius: 15,
        marginLeft: 5,
        marginRight: 5,
        backgroundColor: 'rgba(196,196,196,0.2)',
        height: 120,
        width: 200,
        /* add drop shadow */
        // shadowColor: "#CCFF66",
        // shadowOffset: {
        //     width: 1,
        //     height: 1,
        // },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
    },
    image: {
        flex: 1,
        resizeMode: 'cover',
        borderRadius: 15,
    },
    overlay: {
        backgroundColor: 'rgba(49,49,49,0.8)',
        flex: 1,
        padding: 10,
    },
    itemText: {
        fontSize: 15,
        color: '#FFFFFF',
    },
    challengeInfoContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    likesChallengers: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
});

interface IItem {
    key: String;
}

const PopularChallenges = () => {
    const [popularChallenges, setPopularChallenges] = useState<
        ChallengeSummary[]
    >([]);
    const [secret, setSecret] = useState(false);
    const navigation = useNavigation();
    const dispatch = useDispatch();

    const renderItem: ListRenderItem<ChallengeSummary> = ({item}) => (
            <TouchableOpacity onPress={()=>{
                navigation.navigate('CreateChallenge')
                dispatch(createChallengeSuccess(item.challenge_id))
            }}>
        <View style={styles.itemContainer}>
            <ImageBackground
                source={{
                    uri: `${config.S3_BUCKET_URL}/background/${item.categories[0]}.jpg`,
                }}
                style={styles.image}
                imageStyle={styles.image}>
                <View style={styles.overlay}>
                    <Text style={styles.itemText}>{item.title}</Text>
                    <View style={styles.challengeInfoContainer}>
                        <View style={styles.likesChallengers}>
                            <Icon name={'heart'} color={'red'} size={20} />
                            <Text
                                style={{
                                    color: 'white',
                                    textAlign: 'center',
                                    marginLeft: 10,
                                }}>
                                {item.likes}
                            </Text>
                        </View>
                        <View style={styles.likesChallengers}>
                            <Icon name={'people'} color={'white'} size={20} />
                            <Text
                                style={{
                                    color: 'white',
                                    textAlign: 'center',
                                    marginLeft: 10,
                                }}>
                                {item.past_participants}
                            </Text>
                        </View>
                    </View>
                    <View>
                        <Text style={{color:'orange'}}>{item.rule}</Text>
                    </View>
                </View>

            </ImageBackground>
        </View>
            </TouchableOpacity>
    );

    const getPopularChallenges = async () => {
        let filterOrder = {} as FilterOrder;
        filterOrder.searchText = '';
        filterOrder.filter = [];
        filterOrder.orderedBy = 'Challengers';
        setPopularChallenges(await fetchChallenges(filterOrder));
    };

    useEffect(() => {
        getPopularChallenges();
    }, [secret]);

    return (
        <View>
            <FlatList
                data={popularChallenges}
                renderItem={renderItem}
                horizontal={true}
                showsHorizontalScrollIndicator={false}></FlatList>
            <TouchableOpacity
                onPress={() => {
                    setSecret(!secret);
                }}>
                <Text>.</Text>
            </TouchableOpacity>
        </View>
    );
};

export default PopularChallenges;
