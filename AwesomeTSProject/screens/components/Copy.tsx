


import React, {useEffect, useState} from 'react';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
const {vw, vh, vmin, vmax} = require('react-native-expo-viewport-units');
import {
    ImageBackground,
    View,
    Text,
    StyleSheet,
    FlatList,
    SafeAreaView,
    ListRenderItem,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    Picker,
} from 'react-native';
import {Challenge} from '../../redux/challengeForm/reducers'
import {createChallenge} from '../../redux/challengeForm/actions'
import ChallengeTitle from './ChallengeTitle';

// import ChallengeTitle from './ChallengeTitle';

interface TextWithDefaultProps extends Text {
    defaultProps?: {allowFontScaling?: boolean};
}

interface TextInputWithDefaultProps extends TextInput {
    defaultProps?: {allowFontScaling?: boolean};
}

(Text as unknown as TextWithDefaultProps).defaultProps =
    (Text as unknown as TextWithDefaultProps).defaultProps || {};
(Text as unknown as TextWithDefaultProps).defaultProps!.allowFontScaling =
    false;
(TextInput as unknown as TextInputWithDefaultProps).defaultProps =
    (TextInput as unknown as TextInputWithDefaultProps).defaultProps || {};
(
    TextInput as unknown as TextInputWithDefaultProps
).defaultProps!.allowFontScaling = false;

const image = {uri: 'https://reactjs.org/logo-og.png'};

export interface challengeFormDetailsStates { // information of challenge needed to be fetch (get/post)
    title: string;
    rule: string
    categories: number[];
    task: string;
    times: string;
    section: boolean;
    sectionHours: number[]
    renewHour: string
    renewMin: string
    consecutive: boolean
    persistence: boolean
    days: string
    cycles: string
    description: string
    verifications: number[]
}


function CreateChallengeForm(props:challengeFormDetailsStates) {
    const [title, setTitle] = useState('Challenge Title');
    const [defaultTitle, setDefaultTitle] = useState(true);
    const [categories, setCategories] = useState([]);
    const [task, setTask] = useState('');
    const [times, setTimes] = useState('1');
    const [section, setSection] = useState(false);
    const [sectionTime, setSectionTime] = useState<section[]>([]);
    const [sectionHoursList, setSectionHoursList] = useState<sectionHours[]>([]);
    const [renewHour, setRenewHour] = useState('00');
    const [renewHourSelect, setRenewHourSelect] = useState(false);
    const [renewMin, setRenewMin] = useState('00');
    const [renewMinSelect, setRenewMinSelect] = useState(false);
    const [consecutive, setConsecutive] = useState(false);
    const [limitedTime, setLimitedTime] = useState('1');
    const [cycles, setCycles] = useState('1');
    const [persistence, setPersistence] = useState(true);
    const [description, setDescription] = useState('');
    const [verifyMethod, setVerifyMethod] = useState<number[]>([]);

    const dispatch = useDispatch();

    // const categories = useSelector((state:))

    // useEffect(() => {
    //     dispatchEvent(fetchCategories())
    // })


    const onCreateChallenge = (start:boolean) => {
        let sectionArray: Array<string> = []
        sectionTime.map((section)=>{sectionArray.push(`${section.totalDeadHour}:${renewMin}`)})
        let challenge = {} as Challenge;
        challenge.title = title
        challenge.rule = title
        challenge.task = task
        challenge.description = description
        challenge.persistence = persistence
        challenge.times = parseInt(times)
        challenge.days = parseInt(limitedTime)
        challenge.cycles = parseInt(cycles)
        challenge.renewTime = renewHour+':'+renewMin
        challenge.withSection = section
        challenge.sections = sectionArray
        challenge.consecutive = consecutive
        challenge.verifyMethods = verifyMethod
        challenge.categories = categories

        dispatch(createChallenge(start, challenge));
    };

    function autoTitle() {
        if (task == '') {
            return '';
        }
        let timeNum = parseInt(times);
        let timesText = '';
        if (timeNum > 1) {
            timesText = ` ${times} times`;
        }
        let timeLimitNum = parseInt(limitedTime);
        let timeLimitText = persistence ? 'every' : 'within';
        let cyclesText = '';
        if (timeLimitNum == 1) {
            timeLimitText += persistence ? 'day' : ' one day';
            cyclesText = 'days';
        } else if (timeLimitNum > 1) {
            timeLimitText += ` ${limitedTime} days`;
            cyclesText = 'cycles';
        }
        return `${task}${timesText} ${timeLimitText} ${
            persistence
                ? 'for ' +
                  cycles.toString() +
                  ` ${cyclesText} ` +
                  (consecutive ? 'consecutively' : '')
                : ''
        }`;
    }

    

    //   const onSelectMethod = () => {

    //   }

    const HourSelection = () => {
        let hourButtons = [];
        for (let i = 0; i <= 24; i++) {
            let numberString = `${i.toString().length == 1 ? '0' + i : i}`;
            hourButtons.push(
                <TouchableOpacity
                    onPress={() => {
                        setRenewHour(numberString),
                            setRenewHourSelect(false),
                            updateSectionsDeadTime();
                        setSection(false);
                    }}>
                    <Text style={styles.renewNumberSelectButton}>
                        {numberString}
                    </Text>
                </TouchableOpacity>,
            );
        }
        return (
            <View style={styles.hourSelection}>
                <ScrollView>
                    <View onStartShouldSetResponder={() => true}>
                        {hourButtons}
                    </View>
                </ScrollView>
            </View>
        );
    };

    const MinSelection = () => {
        let minButtons = [];
        for (let i = 0; i <= 59; i++) {
            let numberString = `${i.toString().length == 1 ? '0' + i : i}`;
            minButtons.push(
                <TouchableOpacity
                    onPress={() => {
                        setRenewMin(numberString), setRenewMinSelect(false);
                        setSection(false);
                    }}>
                    <Text style={styles.renewNumberSelectButton}>
                        {numberString}
                    </Text>
                </TouchableOpacity>,
            );
        }
        return (
            <View style={styles.minSelection}>
                <ScrollView>
                    <View onStartShouldSetResponder={() => true}>
                        {minButtons}
                    </View>
                </ScrollView>
            </View>
        );
    };

    interface method {
        id: number;
        method: string;
    }
    interface section {
        time: number;
        afterHour: number;
        totalDeadHour: number;
    }

    const methodData = [
        {id: 1, method: 'Text'},
        {id: 2, method: 'Dialog'},
        {id: 3, method: 'Button'},
        {id: 4, method: 'Photo'},
        {id: 5, method: 'Video'},
        {id: 6, method: 'Live Photo'},
        {id: 7, method: 'Streaming'},
        {id: 8, method: 'Location'},
    ];

    const renderMethod: ListRenderItem<method> = ({item}) => {
        return (
            <View
                onStartShouldSetResponder={() => true} // making flatlist scrollable
                style={[
                    styles.verificationBlock,
                    styles[`${item.method}`],
                    {backgroundColor: 'rgba(209,239,15,0.8)'},
                ]}>
                <View
                    style={
                        verifyMethod.includes(item.id)
                            ? {}
                            : styles.unselectedOverlay
                    }>
                    <TouchableOpacity
                        style={styles.verifyMethod}
                        onPress={() => {
                            verifyMethod.includes(item.id)
                                ? setVerifyMethod(
                                      verifyMethod.filter(
                                          methodId => methodId !== item.id,
                                      ),
                                  )
                                : setVerifyMethod([...verifyMethod, item.id]);
                        }}>
                        <Text>{item.method}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    // [2,3,1,4,5]

    interface sectionHours {
        time:number
        hours:number
    }

    const renderTime: ListRenderItem<sectionHours> = ({item}) => {
        let timeString = item.hours.toString();
        let lastDigit = timeString.slice(-1);
        if (timeString == '11' || timeString == '12' || timeString == '13') {
            timeString += 'th';
        } else if (lastDigit == '1') {
            timeString += 'st';
        } else if (lastDigit == '2') {
            timeString += 'nd';
        } else if (lastDigit == '3') {
            timeString += 'rd';
        } else {
            timeString += 'th';
        }

        return (
            <View
                style={styles.sectionBlock}
                onStartShouldSetResponder={() => true} // making flatlist scrollable
            >
                <TouchableOpacity
                    onPress={() => {
                        let limitedHour = parseInt(limitedTime) * 24;
                        if (
                            sectionTime[sectionTime.length - 1].totalDeadHour <
                            limitedHour
                        ) {
                            setSectionHoursList(
                                sectionHoursList.map((sectionHours) => {
                                    let setSectionHours = sectionHours.hours
                                    if (sectionHours.time === item.time){
                                        setSectionHours +=1
                                    }
                                    // if (sectionHours.time >= item.time){

                                    // }

                                    return {
                                        ...sectionHours,
                                        hours: 
                                            // sectionHours.time === item.time
                                    }
                                })
                            )

                            //original
                            setSectionTime(
                                sectionTime.map(section => {
                                    let setAfterHour = section.afterHour;
                                    if (section.time === item.time) {
                                        setAfterHour = section.afterHour + 1;
                                    }
                                    let afterButtonTotalDeadHour = 0;
                                    if (section.time >= item.time) {
                                        afterButtonTotalDeadHour = 1;
                                    }
                                    return {
                                        ...section,
                                        afterHour: setAfterHour,
                                        totalDeadHour:
                                            section.time === 1
                                                ? parseInt(renewHour) +
                                                  setAfterHour
                                                : sectionTime[section.time - 2]
                                                      .totalDeadHour +
                                                  section.afterHour +
                                                  afterButtonTotalDeadHour,
                                    };
                                }),
                            );
                        } else {
                            
                        }
                    }}>
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                        }}>
                        <View>
                            <Text style={{color: 'black'}}>{timeString}</Text>
                        </View>
                        <Text style={{color: 'black'}}>
                            {item.hours > 1 ? `+${item.hours}hrs` : `+${item.hours}hr`}
                        </Text>
                    </View>
                    <Text style={{color: 'black', textAlign: 'center'}}>
                        before
                    </Text>
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                        }}>
                        <Text>
                            Day{calculateDayByHour(item.totalDeadHour)[0]}
                        </Text>
                        <Text style={{color: 'black', textAlign: 'center'}}>
                            {calculateDayByHour(item.totalDeadHour)[1]}:
                            {renewMin}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    };

    // const renderTime: ListRenderItem<section> = ({item}) => {
    //     let timeString = item.time.toString();
    //     let lastDigit = timeString.slice(-1);
    //     if (timeString == '11' || timeString == '12' || timeString == '13') {
    //         timeString += 'th';
    //     } else if (lastDigit == '1') {
    //         timeString += 'st';
    //     } else if (lastDigit == '2') {
    //         timeString += 'nd';
    //     } else if (lastDigit == '3') {
    //         timeString += 'rd';
    //     } else {
    //         timeString += 'th';
    //     }
    //     let hours =
    //         sectionTime[
    //             sectionTime.findIndex(section => section.time == item.time)
    //         ].afterHour;

    //     return (
    //         <View
    //             style={styles.sectionBlock}
    //             onStartShouldSetResponder={() => true} // making flatlist scrollable
    //         >
    //             <TouchableOpacity
    //                 onPress={() => {
    //                     let limitedHour = parseInt(limitedTime) * 24;
    //                     if (
    //                         sectionTime[sectionTime.length - 1].totalDeadHour <
    //                         limitedHour
    //                     ) {
    //                         setSectionTime(
    //                             sectionTime.map(section => {
    //                                 let setAfterHour = section.afterHour;
    //                                 if (section.time === item.time) {
    //                                     setAfterHour = section.afterHour + 1;
    //                                 }
    //                                 let afterButtonTotalDeadHour = 0;
    //                                 if (section.time >= item.time) {
    //                                     afterButtonTotalDeadHour = 1;
    //                                 }
    //                                 return {
    //                                     ...section,
    //                                     afterHour: setAfterHour,
    //                                     totalDeadHour:
    //                                         section.time === 1
    //                                             ? parseInt(renewHour) +
    //                                               setAfterHour
    //                                             : sectionTime[section.time - 2]
    //                                                   .totalDeadHour +
    //                                               section.afterHour +
    //                                               afterButtonTotalDeadHour,
    //                                 };
    //                             }),
    //                         );
    //                     } else {
    //                         setLimitedTime('5');
    //                     }
    //                 }}>
    //                 <View
    //                     style={{
    //                         flexDirection: 'row',
    //                         justifyContent: 'space-around',
    //                     }}>
    //                     <View>
    //                         <Text style={{color: 'black'}}>{timeString}</Text>
    //                     </View>
    //                     <Text style={{color: 'black'}}>
    //                         {hours > 1 ? `+${hours}hrs` : `+${hours}hr`}
    //                     </Text>
    //                 </View>
    //                 <Text style={{color: 'black', textAlign: 'center'}}>
    //                     before
    //                 </Text>
    //                 <View
    //                     style={{
    //                         flexDirection: 'row',
    //                         justifyContent: 'space-around',
    //                     }}>
    //                     <Text>
    //                         Day{calculateDayByHour(item.totalDeadHour)[0]}
    //                     </Text>
    //                     <Text style={{color: 'black', textAlign: 'center'}}>
    //                         {calculateDayByHour(item.totalDeadHour)[1]}:
    //                         {renewMin}
    //                     </Text>
    //                 </View>
    //             </TouchableOpacity>
    //         </View>
    //     );
    // };

    const calculateDayByHour = (hours: number) => {
        const hour = hours % 24;
        const day = Math.floor(hours / 24) + 1;
        return [day, hour];
    };

    const updateSections = () => {
        let sectionArray = [];
        for (let i = 1; i <= parseInt(times); i++) {
            let sectionInfo = {} as section;
            sectionInfo.time = i;
            sectionInfo.afterHour = 0;
            sectionInfo.totalDeadHour = parseInt(renewHour);
            sectionArray.push(sectionInfo);
        }
        setSectionTime(sectionArray);

        return sectionArray;
    };
    // const updateSections = (hourList:number[]) => {
    //     let tempSectionHours = [];
    //     for (let i = 1; i <= parseInt(times); i++) {
    //         if(hourList){
    //             tempSectionHours.push({hours:hourList[i]})
    //         }else{
    //             tempSectionHours.push({hours:1})
    //         }
    //     }
    //     setSectionHoursList(tempSectionHours);

    //     return tempSectionHours;
    // };

    const updateSectionsDeadTime = () => {
        setSectionTime(
            sectionTime.map(section => {
                return {
                    ...section,
                    totalDeadHour:
                        section.time === 1
                            ? parseInt(renewHour) + section.afterHour
                            : sectionTime[section.time - 2].totalDeadHour +
                              section.afterHour,
                };
            }),
        );
    };

    return (
        <ScrollView style={styles.formContainer}>
            <View style={styles.titleContainer}>
                <TouchableOpacity
                    style={
                        defaultTitle
                            ? styles.defaultTitleButton
                            : styles.typeTitleButton
                    }
                    onPress={() => {
                        defaultTitle
                            ? setDefaultTitle(false)
                            : setDefaultTitle(true);
                        setTitle(autoTitle());
                    }}>
                    <Text>{defaultTitle ? 'Default' : 'Self-defined'}</Text>
                </TouchableOpacity>
                <TextInput
                    style={styles.title}
                    multiline={true}
                    value={defaultTitle ? autoTitle() : title}
                    placeholder="Title"
                    onChangeText={setTitle}
                />
            </View>
            <ImageBackground source={image} style={styles.image}>
                <View style={styles.overlay}>
                    <View style={styles.categoryContainer}>
                        <View style={styles.category}>
                            <Text>Music</Text>
                        </View>
                        <TouchableOpacity onPress={() => {}}>
                            <View style={styles.addCategory}>
                                <Text
                                    style={{
                                        color: '#D1EF0F',
                                        fontSize: 15,
                                        fontWeight: 'bold',
                                    }}>
                                    +
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <Text style={styles.categoryText}>Categories</Text>
                    </View>
                    <View style={styles.taskContainer}>
                        <TextInput
                            placeholderTextColor={'rgba(255,255,255,0.2)'}
                            style={styles.taskInput}
                            value={task}
                            placeholder="Task"
                            onChangeText={setTask}
                        />
                        <View
                            style={[
                                styles.timesContainer,
                                persistence
                                    ? {}
                                    : {height: 130, alignItems: 'center'},
                            ]}>
                            <View style={styles.timesBlock}>
                                <TextInput
                                    style={styles.timesNumberInput}
                                    keyboardType="numeric"
                                    value={times}
                                    placeholder=""
                                    onChangeText={(text: string) => {
                                        setTimes(text);
                                        updateSections();
                                        setSection(false);
                                    }}
                                    // onChangeText={setTimes}
                                />
                                <Text style={styles.timesText}>
                                    {parseInt(times) > 1 ? 'times' : 'time'}
                                </Text>
                            </View>
                            <View style={styles.timesBlock}>
                                <Text style={styles.timesText}>
                                    {persistence ? 'every' : 'within'}
                                </Text>
                            </View>
                            <View style={styles.timesBlock}>
                                <TextInput
                                    style={styles.timesNumberInput}
                                    keyboardType="numeric"
                                    value={limitedTime}
                                    placeholder=""
                                    onChangeText={setLimitedTime}
                                />
                                <Text style={styles.timesText}>
                                    {parseInt(limitedTime) > 1 ? 'days' : 'day'}
                                </Text>
                            </View>
                            {persistence ? (
                                <>
                                    <View style={styles.timesBlock}>
                                        <Text style={styles.timesText}>
                                            for
                                        </Text>
                                    </View>
                                    <View style={styles.timesBlock}>
                                        <TextInput
                                            style={styles.timesNumberInput}
                                            keyboardType="numeric"
                                            value={cycles}
                                            placeholder=""
                                            onChangeText={setCycles}
                                        />
                                        <Text style={styles.timesText}>
                                            {parseInt(limitedTime) > 1
                                                ? 'cycle'
                                                : 'day'}
                                        </Text>
                                    </View>
                                </>
                            ) : null}
                        </View>

                        {persistence ? (
                            <View style={styles.taskSelectionContainer}>
                                <View style={styles.renewTimeBlock}>
                                    <Text style={styles.renewText}>Renew</Text>
                                    <View style={styles.renewNumberBlock}>
                                        <TouchableOpacity
                                            onPress={() =>
                                                setRenewHourSelect(true)
                                            }>
                                            <Text style={styles.renewNumber}>
                                                {renewHour}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                    <Text
                                        style={{
                                            fontSize: 19,
                                            color: 'white',
                                        }}>
                                        {' '}
                                        :{' '}
                                    </Text>
                                    <View style={styles.renewNumberBlock}>
                                        <TouchableOpacity
                                            onPress={() =>
                                                setRenewMinSelect(true)
                                            }>
                                            <Text style={styles.renewNumber}>
                                                {renewMin}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                    {renewHourSelect ? <HourSelection /> : null}
                                    {renewMinSelect ? <MinSelection /> : null}
                                </View>
                                <View
                                    style={
                                        consecutive
                                            ? styles.consecutive
                                            : styles.inconsecutive
                                    }>
                                    <TouchableOpacity
                                        onPress={() => {
                                            consecutive
                                                ? setConsecutive(false)
                                                : setConsecutive(true);
                                        }}>
                                        <Text
                                            style={{
                                                fontSize: 18,
                                                color: consecutive
                                                    ? 'white'
                                                    : 'gray',
                                            }}>
                                            Consecutive
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        ) : null}
                    </View>
                    <View style={styles.modeContainer}>
                        <View
                            style={[
                                styles.mode,
                                persistence
                                    ? styles.unselectedMode
                                    : styles.selectedMode,
                            ]}>
                            <TouchableOpacity
                                onPress={() => {
                                    persistence
                                        ? (setPersistence(false),
                                          setSection(false))
                                        : (setPersistence(true),
                                          setSection(true));
                                }}>
                                <Text style={{fontSize: 18}}>Achievement</Text>
                            </TouchableOpacity>
                        </View>
                        <View
                            style={[
                                styles.mode,
                                persistence
                                    ? styles.selectedMode
                                    : styles.unselectedMode,
                            ]}>
                            <TouchableOpacity
                                onPress={() => {
                                    persistence
                                        ? setPersistence(false)
                                        : setPersistence(true);
                                }}>
                                <Text style={{fontSize: 18}}>Persistence</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.descriptionContainer}>
                        <TextInput
                            placeholderTextColor={'rgba(255,255,255,0.2)'}
                            style={styles.descriptionInput}
                            value={description}
                            placeholder="Description"
                            onChangeText={setDescription}
                        />
                    </View>
                    <View style={styles.verificationContainer}>
                        <Text
                            style={{
                                fontSize: 30,
                                color: 'rgba(255,255,255,1)',
                                textAlign: 'center',
                            }}>
                            Verification
                        </Text>
                        <View style={styles.methodContainer}>
                            <ScrollView
                                keyboardDismissMode="on-drag"
                                style={{flex: 1}}>
                                <FlatList
                                    style={{flex: 1}}
                                    data={methodData}
                                    renderItem={renderMethod}
                                    keyExtractor={item => item.id.toString()}
                                    horizontal={true}
                                />
                            </ScrollView>
                        </View>
                        <View style={styles.advancedSettingContainer}>
                            {persistence ? (
                                <>
                                    <View
                                        style={
                                            section
                                                ? styles.withSections
                                                : styles.withoutSections
                                        }>
                                        <TouchableOpacity
                                            onPress={() => {
                                                section
                                                    ? setSection(false)
                                                    : setSection(true);
                                                updateSections();
                                            }}>
                                            <Text
                                                style={{
                                                    fontSize: 20,
                                                    color: section
                                                        ? 'white'
                                                        : 'gray',
                                                    textAlign: 'center',
                                                }}>
                                                Sections
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                    {section ? (
                                        <>
                                            <FlatList
                                                data={sectionTime}
                                                renderItem={renderTime}
                                                keyExtractor={item =>
                                                    item.time.toString()
                                                }
                                                horizontal={true}
                                                contentContainerStyle={{
                                                    // flex: 1,
                                                    justifyContent: 'center',
                                                }}></FlatList>
                                        </>
                                    ) : null}
                                </>
                            ) : null}
                        </View>
                    </View>
                    <View style={styles.submitContainer}>
                        <View style={styles.submitButton}>
                            <TouchableOpacity
                                style={styles.save}
                                onPress={()=>onCreateChallenge(false)}>
                                <Text>Save</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.submitButton}>
                            <TouchableOpacity
                                style={styles.start}
                                onPress={()=>onCreateChallenge(true)}>
                                <Text>Start</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ImageBackground>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    formContainer: {
        height: '100%',
    },
    image: {
        flex: 1,
        borderRadius: 100,
    },
    overlay: {
        backgroundColor: 'rgba(49,49,49,0.95)',
        flex: 1,
    },
    titleContainer: {
        margin: 0,
        height: 80,
        position: 'relative',
    },
    title: {
        backgroundColor: 'rgba(209,239,14,0.1)',
        height: 50,
        fontSize: 20,
        flex: 1,
        flexWrap: 'wrap',
        textAlign: 'center',
        color: 'white',
        borderTopWidth: 3,
        borderBottomWidth: 3,
        borderColor: '#D1EF0F',
    },
    defaultTitleButton: {
        alignSelf: 'flex-end',
        backgroundColor: '#D1EF0F',
        width: 100,
        height: 19,
        fontSize: 13,
        alignItems: 'center',
        paddingBottom: 4,
        // borderTopRightRadius:60,
        // borderTopLeftRadius:60,
    },
    typeTitleButton: {
        alignSelf: 'flex-end',
        backgroundColor: '#EFB300',
        width: 100,
        height: 19,
        fontSize: 13,
        alignItems: 'center',
        paddingBottom: 4,
        // borderTopRightRadius:60,
        // borderTopLeftRadius:60,
    },
    categoryContainer: {
        marginTop: 5,
        paddingRight: 5,
        height: 40,
        flexDirection: 'row',
    },
    categoryText: {
        position: 'absolute',
        right: 0,
        fontSize: 30,
        color: 'rgba(255,255,255,0.2)',
    },
    category: {
        width: 100,
        // borderWidth: 4,
        borderColor: '#D1EF0F',
        borderRadius: 30,
        height: 30,
        backgroundColor: 'cyan',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
    },
    addCategory: {
        width: 30,
        height: 30,
        borderRadius: 30,
        backgroundColor: 'black',
        borderWidth: 4,
        borderColor: '#D1EF0F',
        alignItems: 'center',
        margin: 5,
    },
    taskContainer: {
        marginLeft: 10,
        marginRight: 10,
        borderTopWidth: 4,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 4,
        borderColor: '#D1EF0F',
        borderRadius: 20,
        marginTop: 20,
        height: 170,
    },
    taskInput: {
        marginTop: 10,
        fontSize: 30,
        textAlign: 'center',
        color: 'white',
    },
    timesContainer: {
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    timesBlock: {
        height: 80,
        width: '20%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 10,
    },
    timesBlockCycles: {
        // borderTopWidth: 0.5,
        // borderColor: '#D1EF0F',
        height: 80,
        // width: '20%',
        alignItems: 'center',
        justifyContent: 'center',
        color: 'black',
    },
    timesWords: {
        color: 'white',
        width: '20%',
        justifyContent: 'center',
        height: 70,
    },
    timesText: {
        width: '100%',
        textAlign: 'center',
        height: 30,
        borderBottomRightRadius: 100,
        fontSize: 20,
        color: 'white',
    },
    timesNumberInput: {
        height: 40,
        fontSize: 30,
        color: 'white',
    },
    taskSelectionContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    renewNumberSelectButton: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        margin: 3,
    },
    renewTimeBlock: {
        position: 'relative',
        width: '50%',
        flexDirection: 'row',
    },
    renewText: {
        fontSize: 18,
        textAlign: 'center',
        color: 'white',
        width: '40%',
    },
    renewNumberBlock: {
        width: '20%',
        height: 20,
    },
    renewNumber: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center',
    },
    hourSelection: {
        position: 'absolute',
        left: '40%',
        bottom: 0,
        width: '20%',
        height: '600%',
        backgroundColor: 'black',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        zIndex: 100,
        justifyContent: 'center',
    },
    minSelection: {
        position: 'absolute',
        left: '70%',
        bottom: 0,
        width: '20%',
        height: '600%',
        backgroundColor: 'black',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        zIndex: 100,
        justifyContent: 'center',
    },
    consecutive: {
        width: '30%',
        alignItems: 'center',
        backgroundColor: 'rgba(239,179,0,0.5)',
    },
    inconsecutive: {
        width: '30%',
        alignItems: 'center',
        backgroundColor: 'rgba(159,157,157,0.2)',
    },
    modeContainer: {
        marginLeft: 30,
        marginRight: 30,
        height: 25,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    mode: {
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        width: '50%',
        alignItems: 'center',
    },
    selectedMode: {
        backgroundColor: '#D1EF0F',
    },
    unselectedMode: {
        backgroundColor: 'rgba(159,157,157,0.2)',
    },
    descriptionContainer: {
        marginTop: 10,
        height: 100,
        borderBottomWidth: 5,
        borderColor: '#D1EF0F',
    },
    descriptionInput: {
        fontSize: 20,
        textAlign: 'center',
        color: 'white',
        flexWrap: 'wrap',
        flex: 1,
        justifyContent: 'center',
        // backgroundColor:'yellow'
    },
    verificationContainer: {
        marginTop: 10,
    },
    methodContainer: {
        // flexDirection: 'row',
        height: 80,
    },
    verificationBlock: {
        width: 80,
        height: 80,
        backgroundColor: 'rgba(209,239,15,0.7)',
        flex: 1,
        justifyContent: 'center',
        borderWidth: 3,
    },
    Text: {
        backgroundColor: 'rgba(106,61,255,0.8)',
    },
    Dialog: {
        backgroundColor: 'rgba(203,61,255,0.8)',
    },
    Button: {
        backgroundColor: 'rgba(255,61,112,0.8)',
    },
    Photo: {
        backgroundColor: 'rgba(209,255,61,0.8)',
    },
    Video: {
        backgroundColor: 'rgba(109,255,56,0.8)',
    },
    'Live Photo': {
        backgroundColor: 'rgba(50,255,98,0.8)',
    },
    Streaming: {
        backgroundColor: 'rgba(56,208,255,0.8)',
    },
    Location: {
        backgroundColor: 'rgba(56,109,255,0.8)',
    },
    unselectedOverlay: {
        backgroundColor: 'rgba(49,49,49,0.7)',
        flex: 1,
        justifyContent: 'center',
    },
    verifyMethod: {
        alignItems: 'center',
    },
    advancedSettingContainer: {
        // marginTop: 20,
    },
    withSections: {
        backgroundColor: 'rgba(239,179,0,0.5)',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    withoutSections: {
        backgroundColor: 'rgba(159,157,157,0.2)',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    sectionBlock: {
        width: 100,
        height: 70,
        // alignItems: 'left',
        padding: 5,
        justifyContent: 'center',
        // backgroundColor: 'rgba(202,252,2,0.8)',
        backgroundColor: 'rgba(239,179,0,0.5)',
        color: 'white',
        borderWidth: 3,
        margin: 5,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
    },
    submitContainer: {
        marginTop: 20,
        flexDirection: 'row',
    },
    submitButton: {
        width: '50%',
        backgroundColor: 'grey',
        alignItems: 'center',
    },
    save: {},
    start: {},
});

export default CreateChallengeForm;
