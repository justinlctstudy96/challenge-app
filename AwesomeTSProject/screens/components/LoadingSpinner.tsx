
import React from 'react';
import Spinner from 'react-native-loading-spinner-overlay';
import {
    StyleSheet,
} from 'react-native';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';

function LoadingSpinner() {
    const spinning = useSelector((state: RootState) => state.loading.spinning)
    return (
        <Spinner
            visible={spinning}
            overlayColor={'rgba(0,0,0,0.8)'}
            color={'#CCFF66'}
            textContent={'Loading...'}
            textStyle={styles.spinnerTextStyle}
        />
    );
};


const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: '#CCFF66',
        fontSize:20,
    },
});

export default LoadingSpinner;