import React, {useEffect, useState} from 'react';
import {
    ImageBackground,
    View,
    Text,
    StyleSheet,
    FlatList,
    SafeAreaView,
    ListRenderItem,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    Picker,
    ViewPropTypes,
    TouchableOpacity,
} from 'react-native';
import {
    fetchCategories,
    fetchTotalLikes,
    fetchUserLike,
    fetchVerifications,
    postLike,
    deleteLike,
} from '../../redux/challengeForm/actions';
import {
    Category,
    Challenge,
    Verification,
} from '../../redux/challengeForm/reducers';
import SelectedCategoryBlocks from './SelectedCategoryBlocks';
import ChallengeTimesDaysCyclesBlock from './ChallengeTimesDaysCyclesBlock';
import Icon from 'react-native-vector-icons/Ionicons';
import SelectedBooleanAttributes from './SelectedBooleanAttributes';
import SelectedVerificationIcon from './SelectedVerificationIcon';
import {config} from '../../redux/config';
const {vw, vh, vmin, vmax} = require('react-native-expo-viewport-units');

interface ChallengeDetails {
    challengeDetails: Challenge;
}

export default function ChallengeSummaryBlock(props: ChallengeDetails) {
    const [allCategories, setAllCategories] = useState<Category[]>([]);
    const [allVerifications, setAllVerifications] = useState<Verification[]>(
        [],
    );
    // Should be redux
    const [totalLikes, setTotalLikes] = useState(0);
    const [liked, setLiked] = useState(false);

    const getAllCategoriesVerifications = async () => {
        setAllCategories((await fetchCategories()).rows);
        setAllVerifications((await fetchVerifications()).rows);
    };

    const getTotalLikes = async () => {
        setTotalLikes(await fetchTotalLikes(props.challengeDetails.id));
    };

    const getUserLikeStatus = async () => {
        setLiked(await fetchUserLike(props.challengeDetails.id));
    };

    const onLike = async () => {
        setLiked(await postLike(props.challengeDetails.id));
    };

    const disLike = async () => {
        setLiked(await deleteLike(props.challengeDetails.id));
    };

    let createdAtArray = props.challengeDetails.created_at.split('T');
    let createdAtString;
    if(createdAtArray.length > 0 ){
        createdAtString = props.challengeDetails.created_at.split('T')[0];
    }

    useEffect(() => {
        getAllCategoriesVerifications();
        getUserLikeStatus();
        getTotalLikes();
    }, [props.challengeDetails.id, liked]);

    return (
        <View style={styles.challengeSummaryBlock}>
            <ImageBackground
                source={{
                    uri: `${config.S3_BUCKET_URL}/background/${props.challengeDetails.categories[0]}.jpg`,
                }}
                // borderRadius={100}
                style={{flex: 1, backgroundColor: 'rgba(0,0,0,0)'}}>
                <View style={styles.overlay}>
                    <View>
                        <View style={styles.title}>
                            <Text
                                style={{
                                    color: 'white',
                                    fontSize: 20,
                                    textAlign: 'center',
                                    fontWeight: 'bold',
                                }}>
                                {props.challengeDetails.title}
                            </Text>
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                            }}>
                            <View>
                                {allCategories.length > 1 ? (
                                    <SelectedCategoryBlocks
                                        selectedCategories={
                                            props.challengeDetails.categories
                                        }
                                        allCategories={allCategories}
                                    />
                                ) : null}
                            </View>
                            <View
                                style={{
                                    paddingTop: 8,
                                    justifyContent: 'center',
                                    paddingRight: 10,
                                }}>
                                <Text
                                    style={{
                                        textAlign: 'left',
                                        color: 'white',
                                    }}>
                                    Create Date: {createdAtString}
                                    {'\n'}
                                    Creator:{' '}
                                    {props.challengeDetails.creator_name}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.detailsRowContainer}>
                            <View style={styles.descriptionContainer}>
                                <ScrollView>
                                    <View
                                        onStartShouldSetResponder={() => true}>
                                        <Text
                                            style={{
                                                margin: 5,
                                                fontSize: 15,
                                                // textAlign: 'center',
                                                color: 'white',
                                            }}>
                                            Description: {'\n'}
                                            {props.challengeDetails.description}
                                        </Text>
                                    </View>
                                </ScrollView>
                            </View>
                            {/* <View style={{width: '30%'}}> */}
                            <SelectedVerificationIcon
                                selectedVerification={
                                    props.challengeDetails.verifyMethods
                                }
                                allVerifications={allVerifications}
                            />
                            {/* </View> */}
                        </View>
                        {/* <View style={styles.textDetailsContainer}>
                    <Text>Rule: {props.challengeDetails.rule}</Text>
                </View> */}
                        <ChallengeTimesDaysCyclesBlock
                            persistence={props.challengeDetails.persistence}
                            times={props.challengeDetails.times}
                            days={props.challengeDetails.days}
                            cycles={props.challengeDetails.cycles}
                        />
                        <View style={{alignItems: 'center', margin: 5}}>
                            <Text style={{color: 'orange'}}>
                                {props.challengeDetails.rule}
                            </Text>
                        </View>
                        <SelectedBooleanAttributes
                            persistence={props.challengeDetails.persistence}
                            consecutive={props.challengeDetails.consecutive}
                            withSection={props.challengeDetails.with_section}
                            bold={true}
                        />
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-around',
                                margin: 10,
                            }}>
                            <View
                                style={{
                                    // flexDirection: 'row',
                                    justifyContent: 'center',
                                    width: '50%',
                                    alignItems: 'center',
                                }}>
                                <TouchableOpacity
                                    onPress={liked ? disLike : onLike}>
                                    {liked ? (
                                        <Icon
                                            name={'ios-heart'}
                                            color={'white'}
                                            size={40}
                                        />
                                    ) : (
                                        <Icon
                                            name={'ios-heart-outline'}
                                            color={'white'}
                                            size={40}
                                        />
                                    )}
                                </TouchableOpacity>
                                <Text style={{color: 'white'}}>Save</Text>
                            </View>
                            <View
                                style={{
                                    width: '50%',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                <View style={styles.likesChallengers}>
                                    <Icon
                                        name={'heart'}
                                        color={'red'}
                                        size={20}
                                    />
                                    <Text
                                        style={{
                                            color: 'white',
                                            textAlign: 'center',
                                            marginLeft: 10,
                                        }}>
                                        {totalLikes}
                                    </Text>
                                </View>
                                <View style={styles.likesChallengers}>
                                    <Icon
                                        name={'people'}
                                        color={'white'}
                                        size={20}
                                    />
                                    <Text
                                        style={{
                                            color: 'white',
                                            textAlign: 'center',
                                            marginLeft: 10,
                                        }}>
                                        {
                                            props.challengeDetails
                                                .past_participants
                                        }
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    challengeSummaryBlock: {
        width: '100%',
        height: 470,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        backgroundColor: 'gray',
        alignSelf: 'center',
    },
    title: {
        backgroundColor: 'rgba(209,239,14,0.1)',
        height: '20%',
        fontSize: 20,
        // flex: 1,
        // flexWrap: 'wrap',
        textAlign: 'center',
        color: 'white',

        borderBottomWidth: 1,
        borderColor: '#D1EF0F',
        justifyContent: 'center',
        alignItems: 'center',
    },
    detailsRowContainer: {
        // height:'27%',
        height: 100,
        flexDirection: 'row',
        alignItems: 'center',
    },
    descriptionContainer: {
        width: '70%',
    },
    textDetailsContainer: {
        padding: vw(3),
    },
    overlay: {
        backgroundColor: 'rgba(49,49,49,0.8)',
        flex: 1,
    },
    challengeAttributes: {
        margin: 10,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    persistence: {
        backgroundColor: 'orange',
        padding: 3,
        paddingLeft: 5,
        paddingRight: 5,
    },
    consecutive: {
        backgroundColor: 'red',
        padding: 3,
        paddingLeft: 5,
        paddingRight: 5,
    },
    sections: {
        backgroundColor: 'yellow',
        padding: 3,
        paddingLeft: 5,
        paddingRight: 5,
    },
    likesChallengers: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
});
