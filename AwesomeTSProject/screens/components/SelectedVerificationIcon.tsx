import React from 'react';
import {Verification} from '../../redux/challengeForm/reducers';
import {View, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import verificationIonicons from './ioniconIndex';

interface Selection {
    selectedVerification: number[];
    allVerifications: Verification[];
}

export default function SelectedVerificationIcon(props: Selection) {
    let verificationName = '';
    props.allVerifications.map(verification => {
        if (verification.id === props.selectedVerification[0]) {
            verificationName = verification.method;
        }
    });

    return (
        <View
            style={{
                flexDirection: 'column',
                marginRight: 8,
                marginLeft: 8,
                height:60,
                width:60,
                alignItems:'center',
            }}>
            <View
                onStartShouldSetResponder={() => true} // making flatlist scrollable
                style={[
                    styles.verificationBlock,
                    {
                        backgroundColor:
                            verificationIonicons.BackgroundColor[
                                `${verificationName}`
                            ],
                    },
                ]}>
                <View style={styles.verifyMethod}>
                    <Icon
                        name={verificationIonicons.Ionicons[`${verificationName}`]}
                        color={'rgba(255,255,255,0.5)'}
                        size={30}
                    />
                </View>
            </View>
            <Text style={{color: 'white', textAlign: 'center'}}>
                {verificationName}
            </Text>
        </View>
    );
}

const styles = StyleSheet.create({
    verificationBlock: {
        width: 40,
        height: 40,
        borderRadius: 40,
        backgroundColor: 'rgba(209,239,15,0.7)',
        flex: 1,
        justifyContent: 'center',
        alignItems:'center'
        // borderWidth: 3,
    },
    verifyMethod: {
        alignItems: 'center',
        justifyContent:'center'
    },
});
