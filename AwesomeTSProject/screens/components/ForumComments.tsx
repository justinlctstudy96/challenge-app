import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    ListRenderItem,
    FlatList,
} from 'react-native';
import {useSelector} from 'react-redux';
import {config} from '../../redux/config';
import {ICommentsDetail} from '../../redux/forum/state';
import {RootState} from '../../store';
import {getTimeString} from '../ChatRoomScreen';

const Comments = () => {
    const comments = useSelector((state: RootState) => state.forum.comments);

    const renderItem: ListRenderItem<ICommentsDetail> = ({item}) => (
        <View style={styles.commentContainer}>
            <View style={styles.commentProPicContainer}>
                <Image
                    style={styles.commentProPic}
                    source={{
                        uri: `${config.S3_BUCKET_URL}/${item.pro_pic}`,
                    }}
                />
            </View>
            <View style={styles.commentOuterBodyContainer}>
                <View style={styles.commentBodyContainer}>
                    <View style={styles.commentNicknameContainer}>
                        <Text style={styles.commentNickname}>
                            {item.nickname}
                        </Text>
                    </View>
                    <Text style={styles.commentContentContainer}>
                        <Text style={styles.commentContent}>
                            {item.comment}
                        </Text>
                    </Text>
                </View>
                <View style={styles.commentTimeContainer}>
                    <Text style={styles.commentCreatedAt}>
                        {/* {item.comment_created_at} */}
                        {item.comment_created_at.split('T')[0]}{' '}
                        {getTimeString(
                            item.comment_created_at.split('T')[1].split('.')[0],
                        )}
                    </Text>
                </View>
            </View>
        </View>
    );
    return (
        <FlatList
            data={comments}
            renderItem={renderItem}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
        />
    );
};

const styles = StyleSheet.create({
    commentContainer: {
        flexDirection: 'row',
        width: '100%',
        padding: 5,
        marginBottom: 5,
        borderRadius: 10,
    },
    commentProPicContainer: {
        justifyContent: 'flex-start',
        paddingHorizontal: 5,
    },
    commentProPic: {
        width: 30,
        height: 30,
        borderRadius: 50,
        padding: 5,
    },
    commentOuterBodyContainer: {
        flex: 1,
    },
    commentBodyContainer: {
        borderWidth: 1,
        borderColor: '#888888',
        flex: 1,
        borderRadius: 10,
        padding: 5,
    },

    commentNicknameContainer: {
        justifyContent: 'flex-start',
        width: '100%',
    },
    commentNickname: {
        fontSize: 15,
        color: '#FFFFFF',
        fontWeight: '500',
    },
    commentContentContainer: {},
    commentContent: {
        fontSize: 15,
        color: '#FFFFFF',
    },
    commentTimeContainer: {},
    commentCreatedAt: {
        color: '#888888',
    },
});

export default Comments;
