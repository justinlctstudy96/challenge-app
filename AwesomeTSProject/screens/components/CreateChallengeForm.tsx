import React, {useEffect, useState} from 'react';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
const {vw, vh, vmin, vmax} = require('react-native-expo-viewport-units');
import {
    ImageBackground,
    View,
    Text,
    StyleSheet,
    FlatList,
    SafeAreaView,
    ListRenderItem,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    // Picker,
} from 'react-native';
import {
    Category,
    Verification,
    Challenge,
} from '../../redux/challengeForm/reducers';
import {
    fetchCategories,
    fetchVerifications,
    createChallenge,
} from '../../redux/challengeForm/actions';
import {Picker} from 'react-native-wheel-pick';
import Icon from 'react-native-vector-icons/Ionicons';
import {config} from '../../redux/config';
import verificationIonicons from './ioniconIndex';

interface TextWithDefaultProps extends Text {
    defaultProps?: {allowFontScaling?: boolean};
}

interface TextInputWithDefaultProps extends TextInput {
    defaultProps?: {allowFontScaling?: boolean};
}

(Text as unknown as TextWithDefaultProps).defaultProps =
    (Text as unknown as TextWithDefaultProps).defaultProps || {};
(Text as unknown as TextWithDefaultProps).defaultProps!.allowFontScaling =
    false;
(TextInput as unknown as TextInputWithDefaultProps).defaultProps =
    (TextInput as unknown as TextInputWithDefaultProps).defaultProps || {};
(
    TextInput as unknown as TextInputWithDefaultProps
).defaultProps!.allowFontScaling = false;

// const image = {uri: 'https://reactjs.org/logo-og.png'};

interface OpenChallengeFormState {
    challengeId: number; // if number is 0 then wont useeffect dispatch to fetch existing challenge details
    // setChallengeId: (value: number | ((prevVar: number) => number)) => void;
    // setOpenChallengeForm: (value: boolean | ((prevVar: boolean) => boolean)) => void;
}

function CreateChallengeForm(props: OpenChallengeFormState) {
    const [title, setTitle] = useState('');
    const [defaultTitle, setDefaultTitle] = useState(false);
    const [categories, setCategories] = useState<number[]>([]);
    const [categorySelect, setCategorySelect] = useState(false);
    const [allCategories, setAllCategories] = useState<Category[]>([]);
    const [officialCategories, setOfficialCategories] = useState<Category[]>(
        [],
    );
    const [task, setTask] = useState('');
    const [times, setTimes] = useState('1');
    const [section, setSection] = useState(false);
    const [sectionTime, setSectionTime] = useState<section[]>([]);
    const [renewHour, setRenewHour] = useState('00');
    const [renewHourSelect, setRenewHourSelect] = useState(false);
    const [renewMin, setRenewMin] = useState('00');
    const [renewMinSelect, setRenewMinSelect] = useState(false);
    const [consecutive, setConsecutive] = useState(false);
    const [limitedTime, setLimitedTime] = useState('1');
    const [cycles, setCycles] = useState('2');
    const [persistence, setPersistence] = useState(true);
    const [description, setDescription] = useState('');
    const [verifyMethod, setVerifyMethod] = useState<number[]>([]);
    const [publicity, setPublicity] = useState(false);
    const [allVerifications, setAllVerifications] = useState<Verification[]>(
        [],
    );
    const [selectTimes, setSelectTimes] = useState(false);
    const [selectDays, setSelectDays] = useState(false);
    const [selectCycles, setSelectCycles] = useState(false);
    // let officialCategoriesArray: Category[] = [];

    const dispatch = useDispatch();

    // Write map to do this
    // Can use useMemo to avoid generating every time
    const selectableTimes = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const selectableDays = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const selectableCycles = [
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        '11',
        '12',
        '13',
        '14',
        '15',
        '16',
        '17',
        '18',
        '19',
        '20',
        '21',
        '22',
        '23',
        '24',
        '25',
        '26',
        '27',
        '28',
        '29',
        '30',
        '31',
        '32',
        '33',
        '34',
        '35',
        '36',
        '37',
        '38',
        '39',
        '40',
        '41',
        '42',
        '43',
        '44',
        '45',
        '46',
        '47',
        '48',
        '49',
        '50',
    ];


    // Can write custom hooks
    const getCategories = async () => {
        setAllCategories((await fetchCategories()).rows);
        let officialCategoriesArray: Category[] = [];
        allCategories.map(category => {
            if (category.official === true) {
                officialCategoriesArray.push(category);
            }
        });
        setOfficialCategories(officialCategoriesArray);
    };

    const getVerifications = async () => {
        setAllVerifications((await fetchVerifications()).rows);
    };

    useEffect(() => {
        getCategories();
        getVerifications();
    }, []);

    const onCreateChallenge = (start: boolean) => {
        let sectionArray: Array<string> = [];
        sectionTime.map(section => {
            sectionArray.push(`${section.totalDeadHour}:${renewMin}`);
        });
        let challenge = {} as Challenge;
        challenge.title = defaultTitle ? autoTitle() : title;
        challenge.rule = autoTitle();
        challenge.task = task;
        challenge.description = description;
        challenge.persistence = persistence;
        challenge.times = parseInt(times);
        challenge.days = parseInt(limitedTime);
        challenge.cycles = parseInt(cycles);
        challenge.renewTime = renewHour + ':' + renewMin;
        challenge.withSection = section;
        challenge.publicity = publicity;
        challenge.sections = sectionArray;
        challenge.consecutive = consecutive;
        challenge.verifyMethods = verifyMethod;
        challenge.categories = categories;

        // Use React Hook form
        let emptyList = '';
        if (challenge.title === '') {
            emptyList += 'Title ';
        }
        if (challenge.task === '') {
            emptyList += 'Task ';
        }
        if (challenge.description === '') {
            emptyList += 'Description ';
        }
        if (challenge.categories.length === 0) {
            emptyList += 'Category ';
        }
        if (challenge.verifyMethods.length === 0) {
            emptyList += 'Verification';
        }

        if (emptyList == '') {
            dispatch(createChallenge(start, challenge));
        } else {
            alert(emptyList + '\nis empty');
        }
    };

    function autoTitle() {
        if (task == '') {
            return '';
        }
        let timeNum = parseInt(times);
        let timesText = '';
        if (timeNum > 1) {
            timesText = ` ${times} times`;
        }
        let timeLimitNum = parseInt(limitedTime);
        let timeLimitText = persistence ? 'every' : 'within';
        let cyclesText = '';
        if (timeLimitNum == 1) {
            timeLimitText += persistence ? 'day' : ' one day';
            cyclesText = 'days';
        } else if (timeLimitNum > 1) {
            timeLimitText += ` ${limitedTime} days`;
            cyclesText = 'cycles';
        }
        const autoTitleString = `${task}${timesText} ${timeLimitText} ${
            persistence
                ? 'for ' +
                  cycles.toString() +
                  ` ${cyclesText} ` +
                  (consecutive ? 'consecutively' : '')
                : ''
        }`;
        // setTitle(autoTitleString)
        return autoTitleString;
    }

    const CategorySelection = () => {
        let categoryButtons = [];
        officialCategories.map(category => {
            if (!categories.includes(category.id)) {
                categoryButtons.push(
                    <View style={styles.optionalCategory}>
                        <TouchableOpacity
                            onPress={() => {
                                setCategories([...categories, category.id]);
                                setCategorySelect(false);
                            }}>
                            <Text>{category.category}</Text>
                        </TouchableOpacity>
                    </View>,
                );
            }
        });
        return (
            <View>
                <ScrollView horizontal={true}>
                    <View
                        style={styles.categorySelection}
                        onStartShouldSetResponder={() => true}>
                        {categoryButtons}
                    </View>
                </ScrollView>
            </View>
        );
    };

    const SelectedCategories = () => {
        let selectedCategoriesBlocks: any = [];
        categories.map(categoryId => {
            let categoryName = '';
            allCategories.map(category => {
                if (category.id === categoryId) {
                    categoryName = category.category;
                }
            });
            selectedCategoriesBlocks.push(
                <View>
                    <TouchableOpacity
                        style={styles.category}
                        onPress={() => {
                            setCategories(
                                categories.filter(id => id !== categoryId),
                            );
                        }}>
                        <Text>{categoryName}</Text>
                    </TouchableOpacity>
                </View>,
            );
        });
        return (
            <View style={{flexDirection: 'row'}}>
                {selectedCategoriesBlocks}
            </View>
        );
    };

    const HourSelection = () => {
        let hourButtons = [];
        for (let i = 0; i <= 24; i++) {
            let numberString = `${i.toString().length == 1 ? '0' + i : i}`;
            hourButtons.push(
                <TouchableOpacity
                    onPress={() => {
                        setRenewHour(numberString),
                            setRenewHourSelect(false),
                            updateSectionsDeadTime();
                        setSection(false);
                    }}>
                    <Text style={styles.renewNumberSelectButton}>
                        {numberString}
                    </Text>
                </TouchableOpacity>,
            );
        }
        return (
            <View style={styles.hourSelection}>
                <ScrollView>
                    <View onStartShouldSetResponder={() => true}>
                        {hourButtons}
                    </View>
                </ScrollView>
            </View>
        );
    };

    const MinSelection = () => {
        let minButtons = [];
        for (let i = 0; i <= 59; i++) {
            let numberString = `${i.toString().length == 1 ? '0' + i : i}`;
            minButtons.push(
                <TouchableOpacity
                    onPress={() => {
                        setRenewMin(numberString), setRenewMinSelect(false);
                        setSection(false);
                    }}>
                    <Text style={styles.renewNumberSelectButton}>
                        {numberString}
                    </Text>
                </TouchableOpacity>,
            );
        }
        return (
            <View style={styles.minSelection}>
                <ScrollView>
                    <View onStartShouldSetResponder={() => true}>
                        {minButtons}
                    </View>
                </ScrollView>
            </View>
        );
    };

    interface method {
        id: number;
        method: string;
    }
    interface section {
        time: number;
        afterHour: number;
        afterMin: number;
        totalDeadHour: number;
    }

    const renderMethod: ListRenderItem<method> = ({item}) => {
        return (
            <View
                style={{
                    flexDirection: 'column',
                    marginRight: 8,
                    marginLeft: 8,
                }}>
                <View
                    onStartShouldSetResponder={() => true} // making flatlist scrollable
                    style={[
                        styles.verificationBlock,
                        // styles[`${item.method}`],
                        // {backgroundColor: 'rgba(209,239,15,0.8)'},
                        {
                            backgroundColor:
                                verificationIonicons.BackgroundColor[
                                    `${item.method}`
                                ],
                        },
                    ]}>
                    <View
                        style={
                            verifyMethod.includes(item.id)
                                ? {}
                                : styles.unselectedOverlay
                        }>
                        <TouchableOpacity
                            style={styles.verifyMethod}
                            onPress={() => {
                                if(item.method==='Camera'){
                                    verifyMethod.includes(item.id)
                                        ? setVerifyMethod(
                                              verifyMethod.filter(
                                                  methodId => methodId !== item.id,
                                              ),
                                          )
                                        : setVerifyMethod([
                                              ...verifyMethod,
                                              item.id,
                                          ]);
                                }
                            }}>
                            <Icon
                                name={
                                    verificationIonicons.Ionicons[
                                        `${item.method}`
                                    ]
                                }
                                color={'rgba(255,255,255,0.5)'}
                                size={40}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <Text style={{color: 'white', textAlign: 'center'}}>
                    {item.method}
                </Text>
            </View>
        );
    };

    const renderTime: ListRenderItem<section> = ({item}) => {
        let timeString = item.time.toString();
        let lastDigit = timeString.slice(-1);
        if (timeString == '11' || timeString == '12' || timeString == '13') {
            timeString += 'th';
        } else if (lastDigit == '1') {
            timeString += 'st';
        } else if (lastDigit == '2') {
            timeString += 'nd';
        } else if (lastDigit == '3') {
            timeString += 'rd';
        } else {
            timeString += 'th';
        }
        let hours =
            sectionTime[
                sectionTime.findIndex(section => section.time == item.time)
            ].afterHour;

        return (
            <View
                style={styles.sectionBlock}
                onStartShouldSetResponder={() => true} // making flatlist scrollable
            >
                <TouchableOpacity
                    onPress={() => {
                        let limitedHour = parseInt(limitedTime) * 24;
                        if (
                            sectionTime[sectionTime.length - 1].totalDeadHour <
                            limitedHour
                        ) {
                            setSectionTime(
                                sectionTime.map(section => {
                                    let setAfterHour = section.afterHour;
                                    if (section.time === item.time) {
                                        setAfterHour = section.afterHour + 1;
                                    }
                                    let afterButtonTotalDeadHour = 0;
                                    if (section.time >= item.time) {
                                        afterButtonTotalDeadHour = 1;
                                    }
                                    return {
                                        ...section,
                                        afterHour: setAfterHour,
                                        totalDeadHour:
                                            section.time === 1
                                                ? parseInt(renewHour) +
                                                  setAfterHour
                                                : sectionTime[section.time - 2]
                                                      .totalDeadHour +
                                                  section.afterHour +
                                                  afterButtonTotalDeadHour,
                                    };
                                }),
                            );
                        } else {
                            // setLimitedTime('5');
                        }
                    }}>
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                        }}>
                        <View>
                            <Text style={{color: 'white'}}>{timeString}</Text>
                        </View>
                        <Text style={{color: 'white'}}>
                            {hours > 1 ? `+${hours}hrs` : `+${hours}hr`}
                        </Text>
                    </View>
                    <Text style={{color: 'white', textAlign: 'center'}}>
                        before
                    </Text>
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                        }}>
                        <Text style={{color: 'white'}}>
                            Day{calculateDayByHour(item.totalDeadHour)[0]}
                        </Text>
                        <Text style={{color: 'white', textAlign: 'center'}}>
                            {calculateDayByHour(item.totalDeadHour)[1]}:
                            {renewMin}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    };

    const calculateDayByHour = (hours: number) => {
        const hour = hours % 24;
        const day = Math.floor(hours / 24) + 1;
        return [day, hour];
    };

    const updateSections = () => {
        let sectionArray = [];
        for (let i = 1; i <= parseInt(times); i++) {
            let sectionInfo = {} as section;
            sectionInfo.time = i;
            sectionInfo.afterHour = 0;
            sectionInfo.totalDeadHour = parseInt(renewHour);
            sectionArray.push(sectionInfo);
        }
        setSectionTime(sectionArray);

        return sectionArray;
    };

    const updateSectionsDeadTime = () => {
        setSectionTime(
            sectionTime.map(section => {
                return {
                    ...section,
                    totalDeadHour:
                        section.time === 1
                            ? parseInt(renewHour) + section.afterHour
                            : sectionTime[section.time - 2].totalDeadHour +
                              section.afterHour,
                };
            }),
        );
    };

    return (
        <View style={styles.formContainer}>
            <View style={styles.titleContainer}>
                <Text style={styles.heading}>Step 1: Set room title</Text>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <TouchableOpacity
                        style={
                            defaultTitle
                                ? styles.greyButton
                                : styles.lightButton
                        }
                        onPress={() => {
                            defaultTitle && setDefaultTitle(false);
                        }}>
                        <Text>Self Defined</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={
                            defaultTitle
                                ? styles.lightButton
                                : styles.greyButton
                        }
                        onPress={() => {
                            !defaultTitle && setDefaultTitle(true);
                            setTitle(autoTitle());
                        }}>
                        <Text>Automatic generated</Text>
                    </TouchableOpacity>
                </View>
                <TextInput
                    style={styles.title}
                    multiline={true}
                    value={defaultTitle ? autoTitle() : title}
                    placeholder="Room title"
                    placeholderTextColor="#ccff663d"
                    onChangeText={setTitle}
                />
            </View>

            {/* <ImageBackground
                source={{
                    uri: `${config.S3_BUCKET_URL}/background/${categories[0]}.jpg`,
                }}
                style={styles.image}> */}
            <View style={styles.overlay}>
                <Text style={styles.heading}>
                    Step 2: Choose the categories (max. 2)
                </Text>
                <View style={styles.categoryContainer}>
                    <SelectedCategories />
                    {categories.length < 2 ? (
                        <TouchableOpacity
                            onPress={async () => {
                                setCategorySelect(!categorySelect);
                                await getCategories();
                            }}>
                            {/* <View style={styles.addCategory}>
                                <Text
                                    style={{
                                        color: '#D1EF0F',
                                        fontSize: 15,
                                        fontWeight: 'bold',
                                        textAlign: 'center',
                                    }}>
                                    <Icon
                                        name="add"
                                        color={'D1EF0F'}
                                        size={20}
                                    />
                                </Text>
                            </View> */}
                            <Icon
                                    name="add-circle"
                                    color={'#D1EF0F'}
                                    size={35}
                                />
                        </TouchableOpacity>
                    ) : null}
                    {categorySelect ? <CategorySelection /> : null}
                </View>
                <View style={styles.descriptionContainer}>
                    <Text style={styles.heading}>
                        Step 3: Add a description for the challenge
                    </Text>
                    <TextInput
                        placeholderTextColor="#ccff663d"
                        style={styles.descriptionInput}
                        value={description}
                        placeholder="Description"
                        onChangeText={setDescription}
                    />
                </View>
                <View style={styles.taskContainerContainer}>
                    <Text style={styles.heading}>
                        Step 4: Type in task name, set challenge rules
                    </Text>
                    <View style={styles.taskContainer}>
                        <TextInput
                            placeholderTextColor="#ccff663d"
                            style={styles.taskInput}
                            value={task}
                            placeholder="Task name (e.g. Eat an apple)"
                            onChangeText={setTask}
                        />
                        <View
                            style={[
                                styles.timesContainer,
                                persistence
                                    ? {}
                                    : {height: 130, alignItems: 'center'},
                            ]}>
                            <View style={styles.timesBlock}>
                                <TouchableOpacity
                                    onPress={() => {
                                        setSelectTimes(true);
                                    }}>
                                    <Text style={styles.timesNumberInput}>
                                        {times}
                                    </Text>
                                    <Text style={styles.timesText}>
                                        {parseInt(times) > 1 ? 'times' : 'time'}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.timesBlock}>
                                <Text style={styles.timesText}>
                                    {persistence ? 'every' : 'within'}
                                </Text>
                            </View>
                            <View style={styles.timesBlock}>
                                <TouchableOpacity
                                    onPress={() => {
                                        setSelectDays(true);
                                    }}>
                                    <Text style={styles.timesNumberInput}>
                                        {limitedTime}
                                    </Text>
                                    <Text style={styles.timesText}>
                                        {parseInt(limitedTime) > 1
                                            ? 'days'
                                            : 'day'}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            {persistence ? (
                                <>
                                    <View style={styles.timesBlock}>
                                        <Text style={styles.timesText}>
                                            for
                                        </Text>
                                    </View>
                                    <View style={styles.timesBlock}>
                                        {/* <TextInput
                                            style={styles.timesNumberInput}
                                            keyboardType="numeric"
                                            value={cycles}
                                            placeholder=""
                                            onChangeText={setCycles}
                                        /> */}
                                        <TouchableOpacity
                                            onPress={() => {
                                                setSelectCycles(true);
                                            }}>
                                            <Text
                                                style={styles.timesNumberInput}>
                                                {cycles}
                                            </Text>

                                            <Text style={styles.timesText}>
                                                {parseInt(limitedTime) > 1
                                                    ? parseInt(cycles) > 1
                                                        ? 'cycles'
                                                        : 'cycle'
                                                    : parseInt(cycles) > 1
                                                    ? 'days'
                                                    : 'day'}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </>
                            ) : null}
                        </View>

                        {persistence ? (
                            <View style={styles.taskSelectionContainer}>
                                <View style={styles.renewTimeBlock}>
                                    <Text style={styles.renewText}>
                                        Total{' '}
                                        {parseInt(limitedTime) *
                                            parseInt(cycles)}{' '}
                                        days
                                    </Text>
                                    {/* <Text style={styles.renewText}>challenge</Text> */}
                                    {/* <Text style={styles.renewText}>Renew</Text>
                                    <View style={styles.renewNumberBlock}>
                                        <TouchableOpacity
                                            onPress={() =>
                                                setRenewHourSelect(true)
                                            }>
                                            <Text style={styles.renewNumber}>
                                                {renewHour}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                    <Text
                                        style={{
                                            fontSize: 19,
                                            color: 'white',
                                        }}>
                                        {' '}
                                        :{' '}
                                    </Text>
                                    <View style={styles.renewNumberBlock}>
                                        <TouchableOpacity
                                            onPress={() =>
                                                setRenewMinSelect(true)
                                            }>
                                            <Text style={styles.renewNumber}>
                                                {renewMin}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                    {renewHourSelect ? <HourSelection /> : null}
                                    {renewMinSelect ? <MinSelection /> : null} */}
                                </View>
                                {/* <View
                                    style={
                                        consecutive
                                            ? styles.consecutive
                                            : styles.inconsecutive
                                    }>
                                    <TouchableOpacity
                                        onPress={() => {
                                            consecutive
                                                ? setConsecutive(false)
                                                : setConsecutive(true);
                                        }}>
                                        <Text
                                            style={{
                                                fontSize: 18,
                                                color: consecutive
                                                    ? 'white'
                                                    : 'black',
                                            }}>
                                            Consecutive
                                        </Text>
                                    </TouchableOpacity>
                                </View> */}
                            </View>
                        ) : null}
                    </View>

                    <View style={styles.modeContainer}>
                        {/* <View
                            style={[
                                styles.mode,
                                persistence
                                    ? styles.unselectedMode
                                    : styles.selectedMode,
                            ]}>
                            <TouchableOpacity
                                onPress={() => {
                                    persistence
                                        ? (setPersistence(false),
                                          setSection(false))
                                        : (setPersistence(true),
                                          setSection(true));
                                }}>
                                <Text style={{fontSize: 18}}>Achievement</Text>
                            </TouchableOpacity>
                        </View> */}
                        {/* <View
                            style={[
                                styles.mode,
                                persistence
                                    ? styles.selectedMode
                                    : styles.unselectedMode,
                            ]}>
                            <TouchableOpacity
                                onPress={() => {
                                    persistence
                                        ? setPersistence(false)
                                        : setPersistence(true);
                                }}>
                                <Text style={{fontSize: 18}}>Persistence</Text>
                            </TouchableOpacity>
                        </View> */}
                    </View>
                </View>

                {/* Sections part */}
                {/* <View style={styles.advancedSettingContainer}>
                    <Text style={styles.heading}>
                        Step 5: Add sections (Optional)
                    </Text>
                    {persistence ? (
                        <>
                            <View
                                style={
                                    section
                                        ? styles.withSections
                                        : styles.withoutSections
                                }>
                                <TouchableOpacity
                                    onPress={() => {
                                        section
                                            ? setSection(false)
                                            : setSection(true);
                                        updateSections();
                                    }}>
                                    <Text
                                        style={{
                                            fontSize: 20,
                                            color: section ? 'white' : 'black',
                                            textAlign: 'center',
                                        }}>
                                        Sections
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            {section ? (
                                <>
                                    <FlatList
                                        data={sectionTime}
                                        renderItem={renderTime}
                                        keyExtractor={item =>
                                            item.time.toString()
                                        }
                                        horizontal={true}
                                        contentContainerStyle={{
                                            // flex: 1,
                                            justifyContent: 'center',
                                        }}></FlatList>
                                </>
                            ) : null}
                        </>
                    ) : null}
                </View> */}

                <View style={styles.verificationContainer}>
                    <Text style={styles.heading}>
                        Step 5: Choose verification method (Only Choose One)
                    </Text>

                    <View style={styles.methodContainer}>
                        <ScrollView
                            keyboardDismissMode="on-drag"
                            style={{flex: 1}}>
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                showsHorizontalScrollIndicator={false}
                                style={{flex: 1}}
                                data={allVerifications}
                                renderItem={renderMethod}
                                keyExtractor={item => item.id.toString()}
                                horizontal={true}
                            />
                        </ScrollView>
                    </View>
                </View>
                <View style={styles.publicRoom}>
                    <Text style={styles.heading}>
                        Step 6: Want your template room to be seen by the
                        public?
                    </Text>
                    <View style={styles.publicBtnContainer}>
                        <TouchableOpacity
                            style={[
                                styles.publicBtn,
                                publicity
                                    ? styles.publicLight
                                    : styles.publicGrey,
                            ]}
                            onPress={() => {
                                setPublicity(!publicity);
                            }}>
                            <Text style={{fontSize: 20, color: 'white'}}>
                                Public
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[
                                styles.publicBtn,
                                publicity
                                    ? styles.publicGrey
                                    : styles.publicLight,
                            ]}
                            onPress={() => {
                                setPublicity(!publicity);
                            }}>
                            <Text style={{fontSize: 20, color: 'white'}}>
                                Private
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.submitContainer}>
                    {/* <View style={styles.submitButton}>
                            <TouchableOpacity
                                style={styles.save}
                                onPress={() => onCreateChallenge(false)}>
                                <Text>Save</Text>
                            </TouchableOpacity>
                        </View> */}
                    <View style={styles.submitButton}>
                        <TouchableOpacity
                            style={styles.start}
                            onPress={() => onCreateChallenge(true)}>
                            <Text style={{fontSize: 20}}>Submit</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            {/* </ImageBackground> */}
            {selectTimes ? (
                <View style={styles.pickerContainer}>
                    <Text style={{textAlign: 'center', fontSize: 20}}>
                        Times
                    </Text>
                    <Picker
                        style={{
                            height: '80%',
                            backgroundColor: 'rgba(255,255,255,0)',
                        }}
                        selectedValue={times}
                        pickerData={selectableTimes}
                        onValueChange={value => {
                            setTimes(value);
                        }}
                    />
                    <TouchableOpacity
                        onPress={() => {
                            setSelectTimes(false);
                        }}>
                        <Text style={{textAlign: 'center', fontSize: 20}}>
                            Select
                        </Text>
                    </TouchableOpacity>
                </View>
            ) : selectDays ? (
                <View style={styles.pickerContainer}>
                    <Text style={{textAlign: 'center', fontSize: 20}}>
                        Days
                    </Text>
                    <Picker
                        style={{
                            height: '80%',
                            backgroundColor: 'rgba(255,255,255,0)',
                        }}
                        selectedValue={limitedTime}
                        pickerData={selectableDays}
                        onValueChange={value => {
                            setLimitedTime(value);
                        }}
                    />
                    <TouchableOpacity
                        onPress={() => {
                            setSelectDays(false);
                        }}>
                        <Text style={{textAlign: 'center', fontSize: 20}}>
                            Select
                        </Text>
                    </TouchableOpacity>
                </View>
            ) : selectCycles ? (
                <View style={styles.pickerContainer}>
                    <Text style={{textAlign: 'center', fontSize: 20}}>
                        {parseInt(limitedTime) > 1
                            ? parseInt(cycles) > 1
                                ? 'cycles'
                                : 'cycle'
                            : parseInt(cycles) > 1
                            ? 'days'
                            : 'day'}
                    </Text>
                    <Picker
                        style={{
                            height: '80%',
                            backgroundColor: 'rgba(255,255,255,0)',
                        }}
                        selectedValue={cycles}
                        pickerData={selectableCycles}
                        onValueChange={value => {
                            setCycles(value);
                        }}
                    />
                    <TouchableOpacity
                        onPress={() => {
                            setSelectCycles(false);
                        }}>
                        <Text style={{textAlign: 'center', fontSize: 20}}>
                            Select
                        </Text>
                    </TouchableOpacity>
                </View>
            ) : null}
        </View>
    );
}

const styles = StyleSheet.create({
    heading: {
        color: 'white',
        fontSize: 20,
        marginBottom: 10,
    },
    formContainer: {
        height: '100%',
        width: '94%',
        marginLeft: '3%',
        marginRight: '3%',
    },
    image: {
        flex: 1,
        borderRadius: 100,
    },
    overlay: {
        // backgroundColor: '#313131f2',
        flex: 1,
        paddingTop: 10,
    },

    titleContainer: {
        borderColor: 'transparent',
        borderBottomColor: '#ccff66',
        borderWidth: 1,
        paddingBottom: 10,
    },
    title: {
        color: 'white',
        marginTop: 10,
        borderRadius: 20,
        borderColor: '#ccff66',
        borderWidth: 1,
        fontSize: 20,
        padding: 10,
        paddingLeft: 15,
        paddingTop: 11,
    },
    greyButton: {
        backgroundColor: 'grey',
        padding: 5,
        borderRadius: 20,
        marginLeft: 2,
        marginRight: 2,
    },
    lightButton: {
        backgroundColor: '#D1EF0F',
        padding: 5,
        borderRadius: 20,
        marginLeft: 2,
        marginRight: 2,
    },

    categoryContainer: {
        paddingRight: 5,

        flexDirection: 'row',
        borderColor: 'transparent',
        borderBottomColor: '#ccff66',
        borderWidth: 1,
        paddingBottom: 10,
    },
    categoryText: {
        right: 0,
        fontSize: 20,
        color: '#ccff66',
        zIndex: -1,
    },
    category: {
        padding: 5,
        borderColor: '#D1EF0F',
        borderRadius: 20,

        backgroundColor: '#D966FF',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        marginRight: 0,
        marginLeft: 2,
    },
    // 'category:nth-child(3n+1)': {
    //     backgroundColor: 'black',
    // },
    optionalCategory: {
        padding: 5,
        borderColor: '#D1EF0F',
        borderRadius: 20,

        backgroundColor: 'grey',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        marginRight: 0,
        marginLeft: 2,
    },
    addCategory: {
        width: 25,
        height: 25,
        borderRadius: 30,
        backgroundColor: 'black',
        borderWidth: 4,
        borderColor: '#D1EF0F',
        alignItems: 'center',
        margin: 5,
    },
    categorySelection: {
        flexDirection: 'row',
    },
    taskContainerContainer: {
        paddingTop: 10,
        borderColor: 'transparent',
        borderBottomColor: '#ccff66',
        borderWidth: 1,
        paddingBottom: 10,
    },
    taskContainer: {
        borderWidth: 1,
        borderColor: '#ccff66',
        borderRadius: 20,
        marginTop: 10,
        paddingBottom: 10,
    },
    taskInput: {
        marginTop: 10,
        fontSize: 20,
        paddingLeft: 15,
        color: 'white',
    },
    timesContainer: {
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    timesBlock: {
        width: '20%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 10,
    },
    timesBlockCycles: {
        alignItems: 'center',
        justifyContent: 'center',
        color: 'black',
    },
    timesWords: {
        color: 'white',
        width: '20%',
        justifyContent: 'center',
    },
    timesText: {
        width: '100%',
        textAlign: 'center',

        borderBottomRightRadius: 100,
        fontSize: 20,
        color: 'white',
    },
    timesNumberInput: {
        fontSize: 30,
        color: 'white',
        textAlign: 'center',
    },
    taskSelectionContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    renewNumberSelectButton: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        margin: 3,
    },
    renewTimeBlock: {
        position: 'relative',
        width: '60%',
        flexDirection: 'row',
    },
    renewText: {
        fontSize: 18,
        textAlign: 'center',
        color: 'orange',
        width: '100%',
    },
    renewNumberBlock: {
        width: '20%',
    },
    renewNumber: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center',
    },
    hourSelection: {
        position: 'absolute',
        left: '40%',
        bottom: 0,
        width: '20%',
        height: '600%',
        backgroundColor: 'black',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        zIndex: 100,
        justifyContent: 'center',
    },
    minSelection: {
        position: 'absolute',
        left: '70%',
        bottom: 0,
        width: '20%',
        height: '600%',
        backgroundColor: 'black',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        zIndex: 100,
        justifyContent: 'center',
    },
    consecutive: {
        width: '35%',
        alignItems: 'center',
        backgroundColor: '#9073FF',
        padding: 5,
        borderRadius: 20,
    },
    inconsecutive: {
        width: '35%',
        alignItems: 'center',
        backgroundColor: 'grey',
        padding: 5,
        borderRadius: 20,
    },
    modeContainer: {
        marginLeft: 30,
        marginRight: 30,
        height: 25,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    mode: {
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        width: '50%',
        alignItems: 'center',
    },
    selectedMode: {
        backgroundColor: '#CCFF66',
    },
    unselectedMode: {
        backgroundColor: 'grey',
    },
    descriptionContainer: {
        paddingTop: 10,
        height: 120,
        borderBottomWidth: 1,
        borderColor: '#CCFF66',
        paddingBottom: 10,
    },
    descriptionInput: {
        borderWidth: 1,
        borderColor: '#ccff66',
        borderRadius: 20,
        fontSize: 20,
        paddingLeft: 15,
        color: 'white',
        flexWrap: 'wrap',
        flex: 1,
        justifyContent: 'center',
    },
    verificationContainer: {
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderColor: '#ccff66',
    },
    methodContainer: {
        height: 80,
        marginTop: 10,
        marginBottom: 10,
    },
    // verificationBlock: {
    //     width: 80,
    //     height: 80,
    //     backgroundColor: 'rgba(209,239,15,0.7)',
    //     flex: 1,
    //     justifyContent: 'center',
    //     borderWidth: 3,
    // },
    verificationBlock: {
        width: 60,
        height: 60,
        borderRadius: 60,
        backgroundColor: 'rgba(209,239,15,0.7)',
        flex: 1,
        justifyContent: 'center',
        // borderWidth: 3,
    },
    Text: {
        backgroundColor: 'rgba(106,61,255,0.8)',
    },
    Dialog: {
        backgroundColor: 'rgba(203,61,255,0.8)',
    },
    Button: {
        backgroundColor: 'rgba(255,61,112,0.8)',
    },
    Photo: {
        backgroundColor: 'rgba(209,255,61,0.8)',
    },
    Video: {
        backgroundColor: 'rgba(109,255,56,0.8)',
    },
    'Live Photo': {
        backgroundColor: 'rgba(50,255,98,0.8)',
    },
    Streaming: {
        backgroundColor: 'rgba(56,208,255,0.8)',
    },
    Location: {
        backgroundColor: 'rgba(56,109,255,0.8)',
    },
    unselectedOverlay: {
        backgroundColor: 'rgba(49,49,49,0.7)',
        flex: 1,
        justifyContent: 'center',
    },
    verifyMethod: {
        alignItems: 'center',
    },
    advancedSettingContainer: {
        paddingTop: 10,
        borderBottomWidth: 1,
        borderColor: '#ccff66',
        paddingBottom: 10,
    },
    withSections: {
        backgroundColor: '#9073FF',
        padding: 5,
        borderRadius: 20,
    },
    withoutSections: {
        backgroundColor: 'grey',
        padding: 5,
        borderRadius: 20,
    },
    publicRoom: {
        paddingTop: 10,
        paddingBottom: 10,
        borderColor: '#ccff66',
        borderBottomWidth: 1,
    },
    publicBtnContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    publicBtn: {
        width: 150,
        padding: 5,
        borderRadius: 20,
        alignItems: 'center',
    },
    publicGrey: {
        backgroundColor: 'grey',
    },
    publicLight: {
        backgroundColor: '#D966FF',
    },
    sectionBlock: {
        width: 100,
        height: 70,
        // alignItems: 'left',
        padding: 5,
        justifyContent: 'center',
        // backgroundColor: 'rgba(202,252,2,0.8)',
        backgroundColor: '#9073FF',
        color: 'white',
        borderWidth: 1,
        borderColor: 'white',
        margin: 5,
        borderRadius: 20,
    },
    submitContainer: {
        paddingTop: 20,
        paddingBottom: 10,
    },
    submitButton: {
        backgroundColor: '#ccff66',
        alignItems: 'center',
        padding: 10,
        borderRadius: 20,
    },
    pickerContainer: {
        backgroundColor: 'rgba(255,255,255,0.95)',
        width: '100%',
        height: '40%',
        position: 'absolute',
        bottom: '20%',
        padding: '5%',
        paddingBottom: 40,
    },
    save: {},
    start: {},
});

export default CreateChallengeForm;
