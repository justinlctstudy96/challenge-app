import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native'
import Timeline from 'react-native-timeline-flatlist'
import { useDispatch, useSelector, } from 'react-redux';
import { RootState } from '../../store';


const ChallengeTimeline = () => {
    const selectedDate = useSelector((state: RootState) => state.journalScreen.selectedDate);
    const selectedDateEvent = useSelector((state: RootState) => state.journalScreen.selectedDateEvent);
    const currentDate = new Date();
    const currentDateSliced = currentDate.toISOString().slice(0, 10);
    return (
        <View style={styles.timelineContainer}>
            <View style={styles.timelineTitle}>
                <Text style={styles.timelineDate}>
                    {currentDateSliced === selectedDate ? "Today" : selectedDate}
                </Text>
            </View>

            <Timeline
                timeContainerStyle={{ minWidth: 40 }}
                style={styles.timeline}
                data={selectedDateEvent}
                //container styles
                circleSize={20}
                innerCircle='dot'
                circleColor='#CCFF66'
                lineColor='#CCFF66'
                timeStyle={styles.time}
                titleStyle={styles.eventTitle}
                descriptionStyle={styles.description}
            />
        </View>
    );
}

export default ChallengeTimeline

const styles = StyleSheet.create({
    timelineContainer: {
        marginTop:10,
        width: '100%',
    },
    timelineTitle: {
        marginHorizontal: 10,
        justifyContent: "center",
        alignItems: "center",
        borderBottomColor:'#89B336',
        borderBottomWidth:2,
        paddingBottom:5
    },
    timelineDate: {
        fontSize: 20,
        fontWeight: '600',
        color: '#FFFFFF',
    },
    timeline: {
        width: "100%",
        paddingTop: 10,
        marginHorizontal: 10,
    },
    eventTitle: {
        fontSize: 15,
        fontWeight:'500',
        color: '#FFFFFF',

    },
    time: {
        textAlign: 'center',
        color: 'white',
        padding: 5,
        borderRadius: 13,
        borderColor: '#668033',
        borderWidth: 1
    },
    description: {
        color: 'gray'
    }
});
