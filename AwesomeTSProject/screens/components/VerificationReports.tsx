import React, {useEffect, useRef, useState} from 'react';
import {
    Pressable,
    Modal,
    Alert,
    Dimensions,
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ListRenderItem,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import {RootState} from '../../store';
import {useDispatch, useSelector} from 'react-redux';
import {VerificationReport} from '../../redux/roomStory/reducer';
import {useNavigation} from '@react-navigation/native';
import {UserEventRelation} from '../../redux/challengeRoom/reducers';
export default function VerificationReports() {
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const [openReports, setOpenReports] = useState(false);
    const allReports: VerificationReport[] = useSelector(
        (state: RootState) =>
            state.roomVerificationRecordsState.allVerificationReports,
    );
    const userDetails: UserEventRelation = useSelector(
        (state: RootState) => state.challengeRoomScreen.userRole,
    );

    const renderReport: ListRenderItem<VerificationReport> = ({
        item,
        index,
    }) => {
        return (
            <View>
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate('StoryReported', {
                            index: index,
                            verification_id: item.verification_id,
                            verification_content: item.verification_content,
                            verification_submitted_at:
                                item.verification_submitted_at,
                            challenger_name: item.challenger_name,
                            reported_at: item.report_at,
                        });
                    }}>
                    <View style={styles.renderList}>
                        <Text style={styles.text}>{item.challenger_name}</Text>
                        <Text style={styles.text}>{item.content}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    };

    return (
        <>
            <TouchableOpacity
                style={styles.reportBtn}
                onPress={() => {
                    setOpenReports(!openReports);
                }}>
                <Text style={styles.boldText}>Reported List</Text>
            </TouchableOpacity>
            <View style={styles.reportsContainer}>
                {openReports ? (
                    <ScrollView>
                        <FlatList data={allReports} renderItem={renderReport} />
                    </ScrollView>
                ) : null}
            </View>
        </>
    );
}

const styles = StyleSheet.create({
    text: {
        color: 'orange',
        fontSize: 16,
    },
    boldText: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
    },
    reportsContainer: {
        width: '80%',
    },
    reportBtn: {
        width: '100%',
        backgroundColor: 'red',
        padding: 5,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 1,
        marginRight: 1,
    },
    renderList: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: '#ccff66',
        paddingTop: 5,
        paddingBottom: 5,
    },
});
