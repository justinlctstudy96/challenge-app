 import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';

interface showEvents {
    showEvents: boolean
}

const styles = StyleSheet.create({
    routeContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    },
    item: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 80,
        padding: 5,
    },
    iconContainer: {
        borderRadius: 50,
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemText: {
        color: '#FFFFFF',
        textAlign: 'center',
    },
});

const items = [
    {
        key: 'bookmarks',
        name: 'All rooms',
        route: 'Challenges',
        params: { showChallenges: false },
        backgroundColor: '#1F9D3B',
    },
    {
        key: 'bookmark',
        name: 'Template',
        route: 'Challenges',
        params: { showChallenges: true },
        backgroundColor: '#FFA800',
    },
    {
        key: 'people',
        name: 'Forum',
        route: 'Forum',
        params: { showChallenges: false },
        backgroundColor: '#9D1F1F',
    },
    {
        key: 'create',
        name: 'Create challenge',
        route: 'CreateChallenge',
        params: { showChallenges: false },
        backgroundColor: '#597EDE',
    },
];

const RoutingButtons = () => {
    const navigation = useNavigation();
    const routing = (navigationName: string, params: showEvents = { showEvents: true }) => {
        // {
        //     params === {show}
        //         ? navigation.navigate(navigationName)
        //         : navigation.navigate(navigationName, params);
        // }
        navigation.navigate(navigationName, params)
    }
    return (
        <View style={styles.routeContainer}>
            {items.map(item => (
                <TouchableOpacity
                    onPress={() => {
                        routing(item.route, item.params);
                    }}
                    key={item.key}>
                    <View style={styles.item} >
                        <View
                            style={[
                                styles.iconContainer,
                                {
                                    backgroundColor: `${item.backgroundColor}`,
                                },
                            ]}>
                            <Icon
                                name={item.key}
                                size={30}
                                color="#FFFFFF"
                            />
                        </View>
                        <Text style={styles.itemText}>{item.name}</Text>
                    </View>
                </TouchableOpacity>
            ))}

        </View>
    );
};

export default RoutingButtons;
