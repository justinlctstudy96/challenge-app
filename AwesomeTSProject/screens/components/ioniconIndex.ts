const verificationIonicons = {
    Ionicons:{
        Button: "radio-button-on",
        Camera: "camera",
        Photo: "image",
        Video: "videocam",
        Location: "compass"},
    BackgroundColor:{
        Button: '#1F9D3B',
        Camera: '#FFA800',
        Photo: '#9D1F1F',
        Video: '#597EDE',
        Location: '#1F9D3B'
    }
}

export default verificationIonicons