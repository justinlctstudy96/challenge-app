import React from 'react';
import { StyleSheet, View, Text, FlatList, ListRenderItem } from 'react-native';

const styles = StyleSheet.create({
    popularChallengesContainer: {
        flexDirection: "row",
        width: "100%",
    },
    item: {
        padding: 10,
        borderRadius: 15,
        marginLeft: 5,
        marginRight: 5,
        backgroundColor: "rgba(196,196,196,0.2)",
        height: 120,
        width: 200,
        /* add drop shadow */
        shadowColor: "#CCFF66",
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    },
    itemText: {
        fontSize: 20,
        color: "#FFFFFF"
    }
});

interface IItem {
    key: String,
}

const CategoryChallenges = () => {
    const renderItem: ListRenderItem<IItem> = ({ item }) => (
        <View style={styles.item}>
            <Text style={styles.itemText}>{item.key}</Text>
        </View>
    )

    return (
        <View>
            <FlatList data={items}
                renderItem={renderItem}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            >
            </FlatList>
        </View>
    );
}

export default CategoryChallenges
const items = [
    { "key": "Donal" },
    { "key": "Goddard" },
    { "key": "Jard" },
    { "key": "Jerald" },
    { "key": "Brigitta" },
    { "key": "Henrie" },
    { "key": "Sibbie" },
    { "key": "Lavinie" },
    { "key": "Marc" },
    { "key": "Inesita" },
    { "key": "Emma" },
    { "key": "Archy" },
    { "key": "Hartley" },
    { "key": "Brigida" },
    { "key": "Eugene" },
    { "key": "Kerr" },
    { "key": "Aeriela" },
    { "key": "Symon" },
    { "key": "Reinald" },
    { "key": "Izabel" },
    { "key": "Sukey" },
    { "key": "Dolli" },
    { "key": "Zechariah" },
    { "key": "Tucker" },
    { "key": "Loreen" },
    { "key": "Ellynn" },
    { "key": "Maurise" },
    { "key": "Caesar" },
    { "key": "Kizzie" },
    { "key": "Jarrett" },
    { "key": "Brendis" },
    { "key": "Normand" },
    { "key": "Arabelle" },
    { "key": "Orton" },
    { "key": "Emmanuel" },
    { "key": "Mick" },
    { "key": "Amy" },
    { "key": "Farrell" },
    { "key": "Lorraine" },
    { "key": "Ryun" },
    { "key": "Tamma" },
    { "key": "Wallis" },
    { "key": "Ned" },
    { "key": "Nelie" },
    { "key": "Sherie" },
    { "key": "Ethelind" },
    { "key": "Hollie" },
    { "key": "Alethea" },
    { "key": "Jethro" },
    { "key": "Jeniffer" },
    { "key": "Blinnie" },
    { "key": "Cullan" },
    { "key": "Noni" },
    { "key": "Kacey" },
    { "key": "Cordie" },
    { "key": "Arty" },
    { "key": "Berkly" },
    { "key": "Lem" },
    { "key": "Jared" },
    { "key": "Holden" },
    { "key": "Andras" },
    { "key": "Hadleigh" },
    { "key": "Clerissa" },
    { "key": "Morton" },
    { "key": "Garrek" },
    { "key": "Berthe" },
    { "key": "Chev" }
]