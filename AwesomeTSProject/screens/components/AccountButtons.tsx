import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    Alert,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import {fetchDeleteAccount, logout} from '../../redux/user/thunks';
import {useDispatch} from 'react-redux';

const items = [
    // {
    //     key: 'book',
    //     name: 'History',
    //     route: 'Challenges',
    //     backgroundColor: '#1F9D3B',
    // },
    // { key: "trophy", name: "Trophies", route: "Challenges",  backgroundColor:"#FFA800"},
    // {
    //     key: 'star',
    //     name: 'Favorite',
    //     route: 'Setting',
    //     backgroundColor: '#9D1F1F',
    // },
    // { key: "cog", name: "Settings", route: "CreateChallenge", backgroundColor:"#597EDE" },
    {
        key: 'log-out',
        name: 'Logout',
        route: 'CreateChallenge',
        backgroundColor: '#CB59DE',
    },
    // { key: "trash", name: "Delete Account", route: "CreateChallenge", backgroundColor:"#FF0B0B" },
];

const AccountButtons = () => {
    const navigation = useNavigation();
    const routing = (navigationName: string) => {
        navigation.navigate(navigationName);
    };
    const dispatch = useDispatch();

    const deleteAccountAlert = () => {
        Alert.alert(
            'Confirm to delete you account?',
            'This action cannot be restored.',
            [
                {
                    text: 'Cancel',
                    style: 'cancel',
                },
                {
                    text: 'Confirm',
                    onPress: () => dispatch(fetchDeleteAccount()),
                },
            ],
        );
    };

    return (
        <View style={styles.buttonsContainer}>
            {items.map(item => (
                <TouchableOpacity
                    onPress={() => {
                        switch (item.name) {
                            case 'Logout':
                                dispatch(logout());
                                break;
                            case 'Delete Account':
                                deleteAccountAlert();
                                break;
                            default:
                                routing(item.route);
                        }
                    }}>
                    <View style={styles.item}>
                        <View
                            style={[
                                styles.iconContainer /*  {backgroundColor:`${item.backgroundColor}`} */,
                            ]}>
                            <Icon name={item.key} size={30} color="#FFFFFF" />
                        </View>
                        <View style={styles.itemTextContainer}>
                            <Text style={styles.itemText}>{item.name}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            ))}
        </View>
    );
};

export default AccountButtons;

const styles = StyleSheet.create({
    buttonsContainer: {
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        padding: 5,
        width: '100%',
    },
    item: {
        padding: 10,
        borderRadius: 10,
        backgroundColor: 'rgba(196,196,196,0.2)',
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginBottom: 5,
        /* add drop shadow */
        shadowColor: '#CCFF66',
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    },
    iconContainer: {
        borderRadius: 50,
        width: '20%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemTextContainer: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: 10,
        width: '80%',
    },
    itemText: {
        color: '#FFFFFF',
        textAlign: 'left',
        fontSize: 20,
    },
});
