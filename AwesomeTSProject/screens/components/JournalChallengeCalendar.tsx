import React, {useEffect, useState} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import {Calendar, DateObject} from 'react-native-calendars';
import {useDispatch, useSelector} from 'react-redux';
import {
    fetchEventByDate,
    fetchEventDots,
} from '../../redux/challengeJournal/thunks';
import {RootState} from '../../store';

const ChallengeCalendar = () => {
    const selectedDate = useSelector(
        (state: RootState) => state.journalScreen.selectedDate,
    );
    const userId = useSelector((state: RootState) => state.user.user?.id);
    const eventDots = useSelector(
        (state: RootState) => state.journalScreen.eventDots,
    );
    const dispatch = useDispatch();

    const updateSelectedDate = (day: DateObject) => {
        const newDate = day.dateString;

        dispatch(fetchEventByDate(userId, newDate));
    };

    return (
        <Calendar
            onDayPress={updateSelectedDate}
            style={styles.calendar}
            markedDates={{
                ...eventDots,
                [selectedDate]: {
                    selected: true,
                    disableTouchEvent: true,
                    ...(eventDots.hasOwnProperty(selectedDate)
                        ? eventDots[selectedDate]
                        : {}),
                },
            }}
            markingType={'multi-dot'}
            theme={calendarTheme}
        />
    );
};

export default ChallengeCalendar;

const styles = StyleSheet.create({
    calendar: {
        width: '100%',
        borderWidth: 10,
        borderRadius: 20,
    },
});

const calendarTheme = {
    calendarBackground: '#000000',
    //month title
    textMonthFontWeight: 'bold',
    textMonthFontSize: 20,
    monthTextColor: '#FFFFFF',
    //arrow
    arrowColor: '#81E851',
    //day text
    todayTextColor: '#CCFF66',
    dayTextColor: '#FFFFFF',
    textDisabledColor: '#313131',
    //selected day
    selectedDayBackgroundColor: '#CCFF66',
    selectedDayTextColor: '#33401A',
    //week title
    textSectionTitleColor: '#FFFFFF',
    textDayHeaderFontWeight: '500',
    textDayHeaderFontSize: 15,
};
