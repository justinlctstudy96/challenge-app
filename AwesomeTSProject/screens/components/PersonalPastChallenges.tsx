import React, {useEffect, useState} from 'react';
import {
    StyleSheet,
    View,
    Text,
    FlatList,
    ListRenderItem,
    TouchableOpacity,
    ImageBackground,
    ScrollView,
} from 'react-native';
import {fetchPastUserChallenges} from '../../redux/challengeJournal/thunks';
import {useSelector, useDispatch} from 'react-redux';
import {RootState} from '../../store';
import {PersonalChallengeEventSummary} from '../../redux/challengeJournal/state';
import {useNavigation} from '@react-navigation/native';
import {config} from '../../redux/config';
import CircularProgress from '../CircularProgress';
import {Category} from '../../redux/challengeForm/reducers';
import {fetchAllCategories} from '../../redux/challengeForm/actions';
import SelectedCategoryBlocks from './SelectedCategoryBlocks';
import {ProgressBar, Colors} from 'react-native-paper';

const PersonalPastChallenges = () => {
    const dispatch = useDispatch();
    const navigation = useNavigation();
    // const [allCategories, setAllCategories] = useState<Category[]>([]);

    const allCategories = useSelector(
        (state: RootState) => state.createChallengeScreen.allCategories,
    );
    const pastChallengeEvents = useSelector(
        (state: RootState) => state.journalScreen.userPastChallenges,
    );

    useEffect(() => {
        dispatch(fetchAllCategories());
        dispatch(fetchPastUserChallenges());
    }, []);

    const renderPastEvents: ListRenderItem<PersonalChallengeEventSummary> = ({
        item,
    }) => (
        <View style={styles.pastEventBlock}>
            <TouchableOpacity>
                <ImageBackground
                    source={item.categories?{
                        uri: `${config.S3_BUCKET_URL}/background/${item?.categories[0]}.jpg`,
                    }:{uri:`${config.S3_BUCKET_URL}/background/1.jpg`}}
                    style={styles.image}>
                    <View style={styles.overlay}>
                        <View style={{width: '100%'}}>
                            <Text style={styles.titleText}>{item.title}</Text>
                            <View style={{flexDirection: 'row'}}>
                                <View style={{width: '50%'}}>
                                    <SelectedCategoryBlocks
                                        selectedCategories={item.categories}
                                        allCategories={allCategories}
                                    />
                                </View>
                                <View
                                    style={{
                                        width: '50%',
                                        justifyContent: 'space-around',
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                    }}>
                                    <Text style={styles.text}>
                                        {item.days * item.cycles} Days
                                    </Text>
                                    <Text style={styles.text}>
                                        {item.end_date}
                                    </Text>
                                </View>
                            </View>
                            <ProgressBar
                                progress={
                                    item.verificationSubmittedTimes /
                                    (item.times * item.cycles)
                                }
                                color={'#D1EF0F'}
                            />
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'flex-end',
                                    margin: 5,
                                }}>
                                {/* <Text style={styles.text}>
                                    {item.verificationSubmittedTimes /
                                        (item.times * item.cycles)}
                                    /{item.times * item.cycles} Times
                                </Text> */}
                                <Text style={styles.percentText}>
                                    {Math.round(
                                        (item.verificationSubmittedTimes /
                                            (item.times * item.cycles)) *
                                            100 *
                                            10,
                                    ) / 10}{' '}
                                    %
                                </Text>
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        </View>
    );

    return (
        <View style={styles.outerContainer}>
            <ScrollView>
                <FlatList
                    data={pastChallengeEvents}
                    renderItem={renderPastEvents}
                    showsVerticalScrollIndicator={false}
                />
            </ScrollView>
        </View>
    );
};

export default PersonalPastChallenges;

const styles = StyleSheet.create({
    outerContainer: {
        padding: 10,
        maxHeight: 400,
    },
    pastEventBlock: {
        display: 'flex',
        overflow: 'hidden',
        alignItems: 'center',
        // padding: 10,
        borderRadius: 15,
        marginLeft: 5,
        marginRight: 5,
        backgroundColor: 'rgba(196,196,196,0.2)',
        height: 115,
        // width: 250,
        /* add drop shadow */
        shadowColor: '#CCFF66',
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    },
    image: {
        // padding:10,
        // flex: 1,
        borderRadius: 100,
        height: 200,
        // width: '100%',
    },
    overlay: {
        backgroundColor: 'rgba(49,49,49,0.8)',
        flex: 1,
        padding: 10,
    },
    titleText: {
        // width: '100%',
        fontSize: 20,
        alignSelf: 'flex-start',
        color: 'white',
        fontWeight: 'bold',
    },
    text: {
        color: 'white',
        textAlign: 'center',
        justifyContent: 'center',
    },
    percentText: {
        color: '#D1EF0F',
        fontSize: 25,
        marginRight: 10,
    },
});
