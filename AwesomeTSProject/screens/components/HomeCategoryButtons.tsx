import React from 'react';
import { StyleSheet, View, Text, FlatList, ListRenderItem } from 'react-native';

const styles = StyleSheet.create({
    categoryButtonsContainer: {
        flexDirection: "row",
        width: "100%",
      },
    item: {
        justifyContent: "center",
        height: 30,
        borderRadius:15,
        paddingLeft:10,
        paddingRight:10,
        marginHorizontal:5,
        marginBottom:10
    },

    itemText: {
        fontSize: 15,
        textAlign:"center",
        color:"#FFFFFF"
    }
});

interface IItem {
    key: String,
    backgroundColor:String
}

const items = [
    { key: "Official", backgroundColor:"#A0D03E"},
    { key: "Lifestyle", backgroundColor:"#1F9D3B",},
    { key: "Sports",  backgroundColor:"#FFA800"}, 
    { key: "Music", backgroundColor:"#9D1F1F" },
    { key: "Study", backgroundColor:"#597EDE" },
    { key: "Others", backgroundColor:"#CB59DE" },

]

const CategoryButtons = () => {
    const renderItem:ListRenderItem<IItem> = ( { item }) => (
        <View style={[styles.item, { backgroundColor: `${item.backgroundColor}`}]}>
            <Text style={styles.itemText}>{item.key}</Text>
        </View>
    )

    return (
        <View style={styles.categoryButtonsContainer}>
            <FlatList data={items}
                renderItem={renderItem}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            >
            </FlatList>
        </View>
    );
}

export default CategoryButtons

