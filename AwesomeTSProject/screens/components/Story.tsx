import React, {useEffect, useRef, useState} from 'react';
import GestureRecognizer from 'react-native-swipe-gestures';
import {
    Pressable,
    Modal,
    Alert,
    Dimensions,
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ListRenderItem,
    TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {RootState} from '../../store';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import RNPickerSelect from 'react-native-picker-select';
import {fetchAllStories} from '../../redux/roomStory/actions';
import {config} from '../../redux/config';
import {fetchAllVerificationRecords} from '../../redux/roomStory/actions';
import {
    CycleStoriesFlatList,
    VerificationRecord,
    CycleVerificationRecords,
    ChallengerVerificationRecords,
} from '../../redux/roomStory/reducer';
import {postReport} from '../../redux/roomStory/actions';
import {getTimeString} from '../ChatRoomScreen';
const width = '100%';

interface Iitem {
    nickname: string;
    pro_pic: string;
    status: string;
    content: string;
    role: string;
    updated_at: string;
    id: number;
}

// Add interface for route
export default function Story({route}) {
    const dispatch = useDispatch();
    const placeholder = {
        label: 'Tap to select...',
        value: null,
    };
    const [selectedValue, setSelectedValue] = useState('');
    const flatList = useRef<FlatList>(null);
    const [modalVisible, setModalVisible] = useState(false);
    const dimensions = Dimensions.get('window');
    const imageWidth = dimensions.width;
    const storyId = useSelector((state: RootState) => state.storyId);
    const navigation = useNavigation();
    const onSwipeDown = () => {
        navigation.navigate('ChallengeRoom');
    };

    const currentCycleVerificationRecords = useSelector(
        (state: RootState) =>
            state.roomVerificationRecordsState.multipleCyclesVerifications,
    );


    const renderStory: ListRenderItem<VerificationRecord> = ({item}) => {
        return (
            <View
                style={{
                    width: imageWidth,
                }}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                        setModalVisible(!modalVisible);
                    }}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Pressable
                                style={{
                                    position: 'absolute',
                                    top: 0,
                                    right: 10,
                                }}
                                onPress={() => setModalVisible(!modalVisible)}>
                                <Icon
                                    name="close-outline"
                                    size={40}
                                    color="#ccff66"
                                />
                            </Pressable>
                            <Icon
                                name="warning-outline"
                                size={50}
                                color="#e40d0d"
                            />
                            <Text style={styles.text}>Report</Text>
                            <View style={styles.picker}>
                                <RNPickerSelect
                                    style={pickerStyle}
                                    onValueChange={value =>
                                        setSelectedValue(value)
                                    }
                                    placeholder={placeholder}
                                    items={[
                                        {
                                            label: 'Untrustable Verification',
                                            value: 'untrustable',
                                        },
                                        {
                                            label: 'Inappropriate Content',
                                            value: 'inappropriate',
                                        },
                                    ]}
                                />
                            </View>
                            <Pressable
                                onPress={() => {
                                    if (selectedValue !== null) {
                                        setModalVisible(!modalVisible);
                                        dispatch(
                                            postReport(item.id, selectedValue),
                                        );
                                    } else {
                                        console.log('report is blank');
                                    }
                                }}
                                style={{
                                    backgroundColor: '#CCFF66',
                                    borderRadius: 20,
                                    padding: 7,
                                    marginBottom: 15,
                                }}>
                                <Text style={{fontSize: 16}}>Submit</Text>
                            </Pressable>

                            <Text style={{color: '#ccff66', fontSize: 12}}>
                                *report will be sent to host to judge
                            </Text>
                        </View>
                    </View>
                </Modal>
                <View
                    style={[{flex: 1}, modalVisible ? styles.blurView : null]}>
                    <Pressable
                        style={{
                            position: 'absolute',
                            top: 0,
                            right: 10,
                            zIndex: 100,
                        }}
                        onPress={onSwipeDown}>
                        <Icon name="close-outline" size={40} color="#ccff66" />
                    </Pressable>

                    <Image
                        style={{
                            width: imageWidth,
                            resizeMode: 'cover',
                            height: '70%',
                            borderWidth: 2,
                            borderColor: '#CCFF66',
                        }}
                        source={{
                            uri: `${config.S3_BUCKET_URL}/${item.content}`,
                        }}
                    />
                    <View
                        style={{
                            width: '92%',
                            marginLeft: '4%',
                            marginRight: '4%',
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            marginTop: 7,
                            borderWidth: 1,
                            borderColor: 'transparent',
                            borderBottomColor: '#ccff66',
                        }}>
                        <Text
                            style={{
                                fontSize: 16,
                                marginBottom: 10,
                                color: '#ccff66',
                            }}>
                            {item.submitted_at.split('T')[0]}{' '}
                            {getTimeString(
                                item.submitted_at.split('T')[1].split('.')[0],
                            )}
                            {/* {item.submitted_at.toLocaleString()} */}
                        </Text>
                    </View>
                    <View
                        style={{
                            width: '92%',
                            marginLeft: '4%',
                            marginRight: '4%',
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                            alignItems: 'center',
                            marginTop: 7,
                            borderWidth: 1,
                            borderColor: 'transparent',
                            borderBottomColor: '#ccff66',
                        }}>
                        <Image
                            style={styles.userImg}
                            source={{
                                uri: `${config.S3_BUCKET_URL}/${item.pro_pic}`,
                            }}
                        />
                        <View>
                            <Text style={styles.text}>{item.nickname}</Text>
                            <Text style={[styles.capitalize]}>{item.role}</Text>
                        </View>
                        {/* <View
                            style={{
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                            <Text style={styles.text}>Success Rate: </Text>
                            <Text style={styles.text}>
                                <Text style={styles.greenText}>5</Text>/30
                            </Text>
                        </View> */}
                    </View>
                    <View
                        style={{
                            width: '92%',
                            marginLeft: '4%',
                            marginRight: '4%',
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                            alignItems: 'center',
                            marginTop: 7,
                        }}>
                        <TouchableOpacity
                            style={styles.buttonBelow}
                            onPress={() => setModalVisible(true)}>
                            <Icon
                                name="warning-outline"
                                size={30}
                                color="#e40d0d"
                            />
                            <Text style={styles.text}>Report</Text>
                        </TouchableOpacity>
                        {/* {item.status === 'pending_for_verify' ? (
                            <TouchableOpacity style={styles.buttonBelow}>
                                <Icon
                                    name="hourglass-outline"
                                    size={30}
                                    color="#37D0E4"
                                />
                                <Text style={styles.text}>
                                    Pending for verify
                                </Text>
                            </TouchableOpacity>
                        ) : null} */}
                        {item.status === 'verified' ? (
                            <TouchableOpacity style={styles.buttonBelow}>
                                <Icon
                                    name="checkmark-circle-outline"
                                    size={30}
                                    color="#ccff66"
                                />
                                <Text style={styles.text}>Verified</Text>
                            </TouchableOpacity>
                        ) : null}
                    </View>
                </View>
            </View>
        );
    };
    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                style={{flex: 1}}
                data={
                    currentCycleVerificationRecords[route.params.nth_cycle - 1]
                        ?.stories
                }
                renderItem={renderStory}
                horizontal={true}
                initialScrollIndex={route.params.flatListDataId}
                decelerationRate={0}
                snapToInterval={imageWidth} //your element width: ;
                snapToAlignment={'center'}
                initialNumToRender={60}
                ref={flatList}
                getItemLayout={(data, index) => ({
                    length: Dimensions.get('window').width,
                    offset: Dimensions.get('window').width * index,
                    index,
                })}
                // getItemLayout={(data,index) => ({length: 200, offset: 200 * index, index})}
                onScrollToIndexFailed={info => {
                    const wait = new Promise(resolve =>
                        setTimeout(resolve, 100),
                    );
                    wait.then(() => {
                        flatList.current?.scrollToIndex({
                            index: info.index,
                            animated: true,
                        });
                    });
                }}
            />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    capitalize: {
        textTransform: 'uppercase',
        color: '#ccff33',
        fontSize: 13,
    },
    blurView: {
        opacity: 0.3,
    },
    container: {
        flex: 1,
        backgroundColor: '#313131',
    },
    storyImg: {
        resizeMode: 'cover',
        height: '70%',
    },
    text: {
        color: 'white',
        fontSize: 16,
    },
    greenText: {
        color: '#CCFF66',
        fontSize: 16,
    },
    userImg: {
        width: 60,
        height: 60,
        borderRadius: 50,
        marginBottom: 7,
    },
    storyView: {
        // flex: 1,
    },
    buttonBelow: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalView: {
        margin: 20,
        backgroundColor: '#313131',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        borderWidth: 1,
        borderColor: '#ccff66',
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
    },
    picker: {
        borderWidth: 1,
        borderColor: '#ccff66',
        borderRadius: 20,
        padding: 10,
        marginBottom: 10,
        marginTop: 10,
    },
});

const pickerStyle = {
    inputIOS: {
        color: '#ccff66',
        fontSize: 16,
    },
};
