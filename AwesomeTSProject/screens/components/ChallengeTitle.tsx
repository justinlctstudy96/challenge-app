import React from 'react'
import { TextInput } from 'react-native-gesture-handler'
import { View, Text } from 'react-native'

interface title{
    title: string,
    setTitle: ,
    defaultTitle: boolean,
    task: string,
    times: string,
    limitedTime: string,
    cycles: string
}

function ChallengeTitle(props: title){


    if (props.defaultTitle){
        const auto =`${props.task} ${props.times} per ${props.limitedTime} with ${props.cycles}`
    }

    return(
        <TextInput value={props.title} />
    )
}

export default ChallengeTitle