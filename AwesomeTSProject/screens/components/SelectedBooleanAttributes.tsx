import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

interface BooleanAttributes {
    persistence: boolean;
    consecutive: boolean;
    withSection: boolean;
    bold: boolean
}

export default function SelectedBooleanAttributes(props: BooleanAttributes) {
    return (
        <View style={styles.challengeAttributes}>
            <View style={props.bold?styles.persistence:{}}>
                <Text style={props.bold?{fontWeight: 'bold'}:{color:'yellow'}}>
                    {props.persistence
                        ? 'Persistence'
                        : 'Achievement'}
                </Text>
            </View>
            {props.consecutive ? (
                <View style={props.bold?styles.consecutive:{}}>
                    <Text style={props.bold?{fontWeight: 'bold'}:{color:'red'}}>Sudden-death</Text>
                </View>
            ) : null}
            {props.withSection?(
            <View style={props.bold?styles.sections:{}}>
                <Text style={props.bold?{fontWeight: 'bold'}:{color:'orange'}}>
                    With-section
                </Text>
            </View>   
            ) : null}
        </View>
    );
}

const styles = StyleSheet.create({
    challengeAttributes: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%'
    },
    persistence: {
        backgroundColor: 'orange',
        padding: 3,
        paddingLeft: 5,
        paddingRight: 5,
    },
    consecutive: {
        backgroundColor: 'red',
        padding: 3,
        paddingLeft: 5,
        paddingRight: 5,
    },
    sections: {
        backgroundColor: 'yellow',
        padding: 3,
        paddingLeft: 5,
        paddingRight: 5,
    },
})