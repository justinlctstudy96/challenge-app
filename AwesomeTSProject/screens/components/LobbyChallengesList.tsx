import React, {useState, useEffect} from 'react';
import {
    ImageBackground,
    View,
    Text,
    FlatList,
    ListRenderItem,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import {fetchChallenges} from '../../redux/challengeLobby/actions';
import {
    fetchCategories,
    fetchVerifications,
} from '../../redux/challengeForm/actions';
import {
    FilterOrder,
    ChallengeSummary,
} from '../../redux/challengeLobby/reducers';
import {Challenge} from '../../redux/challengeForm/reducers';
import {useNavigation} from '@react-navigation/native';
import {Category, Verification} from '../../redux/challengeForm/reducers';
import {useDispatch, useSelector} from 'react-redux';
import {createChallengeSuccess} from '../../redux/challengeForm/actions';
import SelectedCategoryBlocks from './SelectedCategoryBlocks';
import SelectedVerification from './SelectedVerificationIcon';
import SelectedBooleanAttributes from './SelectedBooleanAttributes';
import Icon from 'react-native-vector-icons/Ionicons';
import {config} from '../../redux/config';
import { RootState } from '../../store';

export default function LobbyChallengesList(props: FilterOrder) {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const [challengeList, setChallengeList] = useState<ChallengeSummary[]>([]);
    const [allCategories, setAllCategories] = useState<Category[]>([]);
    const [allVerifications, setAllVerifications] = useState<Verification[]>(
        [],
    );

    const userDetails = useSelector((state: RootState) => state.user.user);


    const getChallenges = async (
        filter: number[],
        savedFilter: boolean,
        orderedBy: string,
    ) => {
        let filterOrder = {} as FilterOrder;
        filterOrder.filter = filter;
        filterOrder.savedFilter = savedFilter; // filtered by user saved challenges
        filterOrder.orderedBy = orderedBy;
        let challengesDetails = await fetchChallenges(filterOrder);
        // setChallengeList(challengesDetails);
        setChallengeList(
            challengesDetails.filter(challenge =>
                challenge.title
                    .toLowerCase()
                    .includes(props.searchText.toLowerCase()),
            ),
        );
    };

    const getAllCategoriesVerifications = async () => {
        setAllCategories((await fetchCategories()).rows);
        setAllVerifications((await fetchVerifications()).rows);
    };

    useEffect(() => {
        getChallenges(props.filter, props.savedFilter, props.orderedBy);
        getAllCategoriesVerifications();
    }, [props.filter, props.savedFilter, props.orderedBy]);

    useEffect(() => {
        getChallenges(props.filter, props.savedFilter, props.orderedBy);
    }, [props.searchText]);

    const renderChallenges: ListRenderItem<ChallengeSummary> = ({item}) => {
        try {
            let verificationMethod = '';
            allVerifications.map(verification => {
                if (verification.id === item.verifications[0]) {
                    verificationMethod = verification.method;
                }
            });
            const totalChallengeDays = item.persistence
                ? item.days * item.cycles
                : item.days;

            const challengeDetailsNavigate = (challengeId: number) => {
                navigation.navigate('CreateChallenge');
                dispatch(createChallengeSuccess(challengeId));
            };

            return (
                <View style={item.creator_id===userDetails?.id?styles.userChallengeSummaryContainer:styles.challengeSummaryContainer}>
                    <TouchableOpacity
                        onPress={() => {
                            challengeDetailsNavigate(item.challenge_id);
                        }}>
                        <ImageBackground
                            source={{
                                uri: `${config.S3_BUCKET_URL}/background/${item.categories[0]}.jpg`,
                            }}
                            style={styles.image}>
                            <View style={styles.overlay}>
                                <View style={styles.titleRowContainer}>
                                    <View style={{width: '79%'}}>
                                        <ScrollView
                                            style={{
                                                width: '100%',
                                                marginRight: 4,
                                            }}
                                            horizontal={true}>
                                            <View>
                                                <Text style={styles.titleText}>
                                                    {item.title}
                                                </Text>
                                            </View>
                                        </ScrollView>
                                        <ScrollView
                                            style={{
                                                width: '100%',
                                                marginRight: 4,
                                            }}
                                            horizontal={true}>
                                            <View>
                                                <Text style={styles.ruleText}>
                                                    {item.rule}
                                                </Text>
                                            </View>
                                        </ScrollView>
                                    </View>
                                    <View
                                        style={{
                                            width: '20%',
                                            justifyContent: 'center',
                                        }}>
                                        <View style={styles.totalDays}>
                                            <Text style={{color: 'white'}}>
                                                {totalChallengeDays}{' '}
                                                {totalChallengeDays == 1
                                                    ? 'Day'
                                                    : 'Days'}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.rowContainer}>
                                    <View style={{width: '50%'}}>
                                        <SelectedCategoryBlocks
                                            selectedCategories={item.categories}
                                            allCategories={allCategories}
                                        />
                                    </View>
                                    <View style={styles.verification}>
                                        <SelectedVerification
                                            selectedVerification={
                                                item.verifications
                                            }
                                            allVerifications={allVerifications}
                                        />
                                    </View>
                                    <View
                                        style={{
                                            width: '20%',
                                            marginBottom: 10,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}>
                                        <View style={styles.likesChallengers}>
                                            <Icon
                                                name={'heart'}
                                                color={'red'}
                                                size={20}
                                            />
                                            <Text
                                                style={{
                                                    color: 'white',
                                                    textAlign: 'center',
                                                    marginLeft: 10,
                                                }}>
                                                {item.likes}
                                            </Text>
                                        </View>
                                        <View style={styles.likesChallengers}>
                                            <Icon
                                                name={'people'}
                                                color={'white'}
                                                size={20}
                                            />
                                            <Text
                                                style={{
                                                    color: 'white',
                                                    textAlign: 'center',
                                                    marginLeft: 10,
                                                }}>
                                                {item.past_participants}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                                <SelectedBooleanAttributes
                                    persistence={item.persistence}
                                    consecutive={item.consecutive}
                                    withSection={item.with_section}
                                    bold={false}
                                />
                            </View>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
            );
        } catch (e) {
            console.log(e);
        }
    };

    return (
        <View style={styles.challengeListContainer}>
            <ScrollView>
                <FlatList
                    data={challengeList}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderChallenges}
                    // keyExtractor={item => item.challenge_id.toString()}
                    contentContainerStyle={{justifyContent: 'center'}}
                />
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    challengeListContainer: {
        flex: 1,
        height: '100%',
        borderWidth: 2,
        borderRadius: 20,
        borderColor: '#D1EF0F',
        padding: '3%',
        marginTop: 15,
    },
    image: {
        flex: 1,
        borderRadius: 100,
    },
    overlay: {
        backgroundColor: 'rgba(49,49,49,0.8)',
        flex: 1,
        padding: 10,
    },
    challengeSummaryContainer: {
        margin: 5,
        borderRadius: 30,
        overflow: 'hidden',
        borderColor: 'white',
    },
    userChallengeSummaryContainer: {
        margin: 5,
        borderRadius: 30,
        overflow: 'hidden',
        borderWidth:2,
        borderColor:'#ccff66'
    },
    titleRowContainer: {
        flexDirection: 'row',
    },
    titleText: {
        width: '100%',
        fontSize: 20,
        alignSelf: 'center',
        color: 'white',
        fontWeight: 'bold',
    },
    ruleText: {
        color: 'orange',
        width: '100%',
        fontSize: 15,
        alignSelf: 'center',
    },
    totalDays: {
        // width: '20%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    rowContainer: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    verification: {
        width: '30%',
    },
    likesChallengers: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
});
