import Icon from 'react-native-vector-icons/Ionicons';
import React, {useEffect, useState} from 'react';
import {
    StyleSheet,
    View,
    Text,
    FlatList,
    ListRenderItem,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';
import {fetchOngoingUserChallenges} from '../../redux/challengeJournal/thunks';
import {useSelector, useDispatch} from 'react-redux';
import {RootState} from '../../store';
import {PersonalChallengeEventSummary} from '../../redux/challengeJournal/state';
import {useNavigation} from '@react-navigation/native';
import {config} from '../../redux/config';
import CircularProgress from '../CircularProgress';

const styles = StyleSheet.create({
    popularChallengesContainer: {
        flexDirection: 'row',
        width: '100%',
    },
    ongoingEventBlock: {
        display: 'flex',
        overflow: 'hidden',
        alignItems: 'center',
        // padding: 10,
        borderRadius: 15,
        marginLeft: 5,
        marginRight: 5,
        backgroundColor: 'rgba(196,196,196,0.2)',
        height: 200,
        // width: 250,
        /* add drop shadow */
        shadowColor: '#CCFF66',
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    },
    title: {
        fontSize: 20,
        color: '#FFFFFF',
        fontWeight: 'bold',
    },
    image: {
        flex: 1,
        borderRadius: 100,
        height: 200,
        width: 360,
    },
    overlay: {
        backgroundColor: 'rgba(49,49,49,0.8)',
        flex: 1,
        padding: 10,
    },
    timeTitle: {
        color: 'white',
        fontSize: 15,
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 5,
    },
    timeText: {
        color: 'white',
        letterSpacing: 1,
        fontWeight: 'bold',
    },
    percentContainer: {
        margin: 5,
    },
});

interface IItem {
    key: String;
}

export const dateString = (date: string) => {
    try {
        const dateArray = date?.split('-');
        return `${dateArray[2]}/${dateArray[1]}/${dateArray[0].slice(-2)}`;
    } catch (e) {
        return '';
    }
};

export const PersonalChallenges = () => {
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const [secret, setSecret] = useState(false);

    const ongoingChallengeEvents = useSelector(
        (state: RootState) => state.journalScreen.userOngoingChallenges,
    );

    useEffect(() => {
        dispatch(fetchOngoingUserChallenges());
    }, []);

    useEffect(() => {
        dispatch(fetchOngoingUserChallenges());
    }, [secret]);

    const eventRoomNavigate = (eventId: number) => {
        navigation.navigate('ChallengeRoom', {eventId: eventId});
    };

    const renderOngoingEvents: ListRenderItem<PersonalChallengeEventSummary> =
        ({item}) =>
            item.categories ? (
                <View style={styles.ongoingEventBlock}>
                    <TouchableOpacity
                        onPress={() => {
                            eventRoomNavigate(item.event_id);
                        }}>
                        <ImageBackground
                            source={{
                                uri: `${config.S3_BUCKET_URL}/background/${item.categories[0]}.jpg`,
                            }}
                            style={styles.image}>
                            <View style={styles.overlay}>
                                <Text style={styles.title}>{item.title}</Text>
                                <Text style={{color: 'orange', height: 20}}>
                                    {item.rule}
                                </Text>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-around',
                                    }}>
                                    <View style={styles.percentContainer}>
                                        <Text style={styles.timeTitle}>
                                            Cycle{' '}
                                            {
                                                item.cycleDates[
                                                    item.cycleDates.length - 1
                                                ].current_cycle
                                            }
                                        </Text>
                                        <View style={{flexDirection: 'row'}}>
                                            <CircularProgress
                                                percent={
                                                    (item.currentCycleVerificationTimes /
                                                        item.times) *
                                                    100
                                                }
                                                radius={40}
                                                bgRingWidth={5}
                                                progressRingWidth={5}
                                                showPercent={true}
                                                textFontSize={15}
                                                textFontColor={
                                                    'rgba(204, 255, 102, 1)'
                                                }
                                                ringColor={
                                                    'rgba(204, 255, 102, 1)'
                                                }
                                            />
                                            <View
                                                style={{
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    margin: 5,
                                                }}>
                                                <Text style={styles.timeText}>
                                                    {dateString(
                                                        item.cycleDates[
                                                            item.cycleDates
                                                                .length - 1
                                                        ].start_date,
                                                    )}
                                                </Text>
                                                {/* <Icon
                                name="arrow-down"
                                color={'white'}
                                size={20}
                            /> */}
                                                <Text style={styles.timeText}>
                                                    to
                                                </Text>
                                                <Text style={styles.timeText}>
                                                    {dateString(
                                                        item.cycleDates[
                                                            item.cycleDates
                                                                .length - 1
                                                        ].end_date,
                                                    )}
                                                </Text>
                                            </View>
                                        </View>
                                        <Text
                                            style={[
                                                styles.timeText,
                                                {
                                                    margin: 5,
                                                    alignSelf: 'center',
                                                    fontWeight: 'normal',
                                                },
                                            ]}>
                                            Times:{' '}
                                            {item.currentCycleVerificationTimes}
                                            /{item.times}
                                        </Text>
                                    </View>
                                    <View style={styles.percentContainer}>
                                        <Text style={styles.timeTitle}>
                                            Total {item.cycles} Cycles
                                        </Text>
                                        <View style={{flexDirection: 'row'}}>
                                            <CircularProgress
                                                percent={
                                                    (item.verificationSubmittedTimes /
                                                        (item.times *
                                                            item.cycles)) *
                                                    100
                                                }
                                                radius={40}
                                                bgRingWidth={5}
                                                progressRingWidth={5}
                                                showPercent={true}
                                                textFontSize={15}
                                                textFontColor={
                                                    'rgba(204, 255, 102, 1)'
                                                }
                                                ringColor={
                                                    'rgba(204, 255, 102, 1)'
                                                }
                                            />
                                            <View
                                                style={{
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    margin: 5,
                                                }}>
                                                <Text style={styles.timeText}>
                                                    {dateString(
                                                        item.start_date,
                                                    )}
                                                </Text>
                                                {/* <Icon
                                name="arrow-down"
                                color={'white'}
                                size={20}
                            /> */}
                                                <Text style={styles.timeText}>
                                                    to
                                                </Text>
                                                <Text style={styles.timeText}>
                                                    {dateString(item.end_date)}
                                                </Text>
                                            </View>
                                        </View>
                                        <Text
                                            style={[
                                                styles.timeText,
                                                {
                                                    margin: 5,
                                                    alignSelf: 'center',
                                                    fontWeight: 'normal',
                                                },
                                            ]}>
                                            Times:{' '}
                                            {item.verificationSubmittedTimes}/
                                            {item.times * item.cycles}
                                        </Text>
                                    </View>
                                </View>
                                {/* <Text style={styles.itemText}>{item.verificationSubmittedTimes}</Text> */}
                            </View>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
            ) : null;

    return (
        <View>
            {/* <TouchableOpacity
                onPress={() => {
                    dispatch(fetchOngoingUserChallenges());
                }}>
                <Text>dispatch</Text>
            </TouchableOpacity> */}
            <FlatList
                data={ongoingChallengeEvents}
                renderItem={renderOngoingEvents}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            />
            <TouchableOpacity
                onPress={() => {
                    setSecret(!secret);
                }}>
                <Text>.</Text>
            </TouchableOpacity>
        </View>
    );
};

export default PersonalChallenges;
