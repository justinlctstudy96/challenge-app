import React, {useEffect, useState} from 'react';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {
    ImageBackground,
    View,
    Text,
    StyleSheet,
    FlatList,
    SafeAreaView,
    ListRenderItem,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    Picker,
    DatePickerIOSBase,
} from 'react-native';
import DatePicker from 'react-native-date-picker';
import {ChallengeEvent} from '../../redux/challengeForm/reducers';
import {createChallengeEvent} from '../../redux/challengeForm/actions';
import {useDispatch, useSelector} from 'react-redux';
import RNPickerSelect from 'react-native-picker-select';
import {RootState} from '../../store';
import { useNavigation } from '@react-navigation/native';

interface DaysCycles {
    challengeId: number;
    persistence: boolean;
    days: number;
    cycles: number;
}

export default function CreateChallengeEventForm(props: DaysCycles) {
    const totalChallengeDays = props.persistence
        ? props.days * props.cycles
        : props.days;

    const [accommodation, setAccommodation] = useState('');
    const [publicity, setPublicity] = useState(true);
    const [selectStartDate, setSelectStartDate] = useState(false);
    const [selectEndDate, setSelectEndDate] = useState(false);
    const [hostComment, setHostComment] = useState('');
    const [official, setOfficial] = useState(false);
    const [officialReward, setOfficialReward] = useState('');
    const userId = useSelector((state: RootState) => state.user.user?.id);
    const eventId = useSelector((state: RootState) => state.createChallengeScreen.eventId)

    const dispatch = useDispatch();

    let minStartDay = new Date();
    minStartDay.setDate(minStartDay.getDate() + 1);
    let maxStartDay = new Date();
    maxStartDay.setDate(minStartDay.getDate() + 10);

    const [startDate, setStartDate] = useState(minStartDay);
    const getMinEndDate = () => {
        let date = new Date();
        date.setDate(startDate.getDate() + totalChallengeDays);
        return date;
    };
    let minEndDay = new Date();
    minEndDay.setDate(startDate.getDate() + totalChallengeDays);
    let maxEndDay = new Date();
    maxEndDay.setDate(minEndDay.getDate() + 10);
    const [endDate, setEndDate] = useState(new Date());

    useEffect(() => {
        setEndDate(getMinEndDate());
    }, [props, props.days, startDate]);

    // useEffect(()=>{
    //     navigation.navigate('ChallengeRoom', {eventId: eventId})
    // },[eventId])

    const navigation = useNavigation();

    const onCreateChallengeEvent = () => {
        let event = {} as ChallengeEvent;
        event.challengeId = props.challengeId;
        event.accommodation = parseInt(accommodation);
        event.official = official;
        event.officialReward = officialReward;
        event.start = startDate;
        event.end = endDate;
        event.comment = hostComment;

        dispatch(createChallengeEvent(event, userId));
        // Need to check , can put inside createChallengeEvent Thunk to do the navigation to guarantee orders
        navigation.navigate('ChallengeRoom', {eventId: eventId});
    };

    return (
        <View style={styles.eventFormContainer}>
            {/* <Text style={{textAlign: 'center', fontSize: 20}}>
                Challenge Event
            </Text> */}
            <View style={styles.totalChallengeDays}>
                <Text style={{fontSize: 25}}>
                    {totalChallengeDays}
                    {props.days > 1 ? ' Days ' : ' Day '}Challenge
                </Text>
            </View>
            {selectStartDate ? (
                <View style={styles.datePickerContainer}>
                    <View style={{backgroundColor: 'white'}}>
                        <Text
                            style={{
                                marginTop: '3%',
                                alignSelf: 'center',
                                fontSize: 15,
                            }}>
                            Start Date
                        </Text>
                        <DatePicker
                            date={startDate}
                            onDateChange={startDate => {
                                setStartDate(startDate);
                                if (minEndDay > endDate) {
                                    let tempDay = minEndDay;
                                    tempDay.setDate(minEndDay.getDate() + 1);
                                    setEndDate(tempDay);
                                }
                            }}
                            mode={'date'}
                            minimumDate={minStartDay}
                            maximumDate={maxStartDay}
                        />
                    </View>
                    <TouchableOpacity
                        style={{alignSelf: 'center'}}
                        onPress={() => {
                            setSelectStartDate(!selectStartDate);
                        }}>
                        <Text style={{fontSize: 15}}>Select</Text>
                    </TouchableOpacity>
                </View>
            ) : selectEndDate ? (
                <View style={styles.datePickerContainer}>
                    <View style={{backgroundColor: 'white'}}>
                        <Text
                            style={{
                                marginTop: '3%',
                                alignSelf: 'center',
                                fontSize: 15,
                            }}>
                            End Date
                        </Text>
                        <DatePicker
                            date={endDate}
                            onDateChange={setEndDate}
                            mode={'date'}
                            minimumDate={getMinEndDate()}
                            maximumDate={maxEndDay}
                        />
                    </View>
                    <TouchableOpacity
                        style={{alignSelf: 'center'}}
                        onPress={() => {
                            setSelectEndDate(!selectEndDate);
                        }}>
                        <Text style={{fontSize: 15}}>Select</Text>
                    </TouchableOpacity>
                </View>
            ) : (
                <>
                    <View style={styles.rowContainer}>
                        <View style={styles.dateSelection}>
                            <TouchableOpacity
                                style={styles.dateSelection}
                                onPress={() => {
                                    setSelectStartDate(!selectStartDate);
                                }}>
                                <Text style={styles.dateText}>
                                    {startDate.toISOString().split('T')[0]}
                                </Text>
                                <Text style={styles.dateText}>Start Date</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.dateSelection}>
                            <TouchableOpacity
                                style={styles.dateSelection}
                                onPress={() => {
                                    // setSelectEndDate(!selectEndDate);
                                }}>
                                <Text style={styles.dateText}>
                                    {endDate.toISOString().split('T')[0]}
                                </Text>
                                <Text style={styles.dateText}>End Date</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.rowContainer}>
                        <View style={styles.accommodationInput}>
                            <Text style={styles.greenText}>Accommodation</Text>
                            <TextInput
                                style={styles.inputs}
                                placeholderTextColor={'#ccff6684'}
                                value={accommodation}
                                placeholder="Input Number"
                                onChangeText={setAccommodation}
                                keyboardType={'numeric'}
                            />
                        </View>
                        <View style={styles.publicityButton}>
                            <View style={styles.wordLimit}>
                                <Text style={styles.greenText}>
                                    Toggle button
                                </Text>
                            </View>
                            <TouchableOpacity
                                style={styles.toggleBtn}
                                onPress={() => {
                                    setPublicity(!publicity);
                                }}>
                                <Text>{publicity ? 'Public' : 'Private'}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.officialOptions}>
                        <TouchableOpacity></TouchableOpacity>
                    </View>

                    <View style={styles.rowContainer}>
                        <View style={styles.submitButton}>
                            <TouchableOpacity
                                style={styles.back}
                                onPress={() => {}}>
                                <Text>Back</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.submitButton}>
                            <TouchableOpacity
                                style={styles.start}
                                onPress={() => onCreateChallengeEvent()}>
                                <Text>Start</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    wordLimit: {
        width: 120,
    },
    greenText: {
        color: '#ccff66',
        fontSize: 18,
    },
    eventFormContainer: {
        width: '100%',
        height: 400,

        alignSelf: 'center',
    },
    inputs: {
        marginTop: 10,
        borderWidth: 1,
        color: 'white',
        borderColor: '#ccff66',
        paddingLeft: 10,
        padding: 10,
        borderRadius: 20,
    },

    rowContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        height: '20%',
        margin: '2%',
    },
    accommodationInput: {
        // backgroundColor:'black',
        width: '50%',
    },
    publicityButton: {},
    totalChallengeDays: {
        height: '10%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9073FF',
    },
    dateSelection: {
        alignItems: 'center',
    },
    dateText: {
        color: '#ccff66',
        fontSize: 17,
        margin: '3%',
    },
    officialOptions: {},
    datePickerContainer: {
        height: '20%',
        backgroundColor: 'white',
        alignSelf: 'center',
    },
    back: {
        padding: 10,
        backgroundColor: '#D966FF',
        borderRadius: 20,
        width: 120,
        alignItems: 'center',
    },
    start: {
        padding: 10,
        width: 120,
        alignItems: 'center',
        backgroundColor: '#ccff66',
        borderRadius: 20,
    },
    toggleBtn: {
        marginTop: 10,
        padding: 10,
        backgroundColor: 'orange',
        borderRadius: 20,
        alignItems: 'center',
    },
});
