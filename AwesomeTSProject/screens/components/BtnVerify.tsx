import CheckBox from '@react-native-community/checkbox';
import React, {useEffect, useState} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import {useTime} from 'react-timer-hook';
import {useSelector, useDispatch} from 'react-redux';
import {pressBtn} from '../../redux/roomVerification/actions';
import {RootState} from '../../store';

export default function BtnVerify() {
    const {seconds, minutes, hours, ampm} = useTime({format: '12-hour'});
    const dispatch = useDispatch();

    const btnStatus = useSelector(
        (state: RootState) => state.btnStatus.btnStatus,
    );

    return (
        <View style={styles.verifyContainer}>
            <Text style={[styles.text, styles.verifyHeading]}>Verify:</Text>
            {hours >= 0 && hours < 100 && ampm === 'pm' ? (
                <View style={styles.verify}>
                    <Text style={[styles.text, styles.verifyText]}>
                        Click the check box to complete verification
                    </Text>
                    <CheckBox
                        disabled={false}
                        value={btnStatus}
                        onValueChange={() => dispatch(pressBtn(btnStatus))}
                        onCheckColor="#CCFF66"
                        onTintColor="#CCFF66"
                    />
                </View>
            ) : (
                <View style={styles.verify}>
                    <Text style={[styles.text, styles.verifyText]}>
                        The checkbox will be avaliable during 4:00 am to 4:59am
                    </Text>
                    <CheckBox disabled={true} value={false} />
                </View>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    text: {
        color: 'white',
        fontSize: 16,
    },
    verifyContainer: {
        marginTop: 20,
        padding: 15,
        borderWidth: 2,
        width: '100%',
        borderColor: '#CCFF66',
        borderRadius: 10,
        paddingTop: 0,
        marginBottom: 10,
    },
    verifyHeading: {
        marginTop: 15,
    },
    verify: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    verifyText: {
        width: '80%',
    },
});
