import React, {useEffect, useState} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    Platform,
} from 'react-native';
import {ImageLibraryOptions, launchCamera} from 'react-native-image-picker';
import {
    camImg,
    fetchHaveUpload,
    fetchLivePhoto,
} from '../../redux/roomVerification/actions';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import {doPreventUpload, fetchAllStories, fetchAllVerificationRecords} from '../../redux/roomStory/actions';
import {RootState} from '../../store';

interface ICameraVerifyProps {
    challengerID: number;
    methodID: number;
    eventId: number;
    cycleStartDate?: string
}

export default function CameraVerify(props: ICameraVerifyProps) {
    const preventUploadUserId = useSelector(
        (state: RootState) => state.preventUpload.userId,
    );
    // let userArr: number[] = preventUploadUserId;
    const dispatch = useDispatch();

    const options: ImageLibraryOptions = {
        mediaType: 'photo',
        quality: 0.1
    }
    const handleTakePhoto = () => {
        launchCamera(options, response => {
            if (response.assets) {
                const cameraImage = response.assets[0];

                //@ts-ignore

                const sentLivePhotoToDB = new FormData();
                sentLivePhotoToDB.append('challengerId', props.challengerID +"");
                sentLivePhotoToDB.append('methodId', props.methodID +"");
                sentLivePhotoToDB.append('livePhoto', {
                    name: cameraImage.fileName,
                    type: cameraImage.type,
                    uri:
                        Platform.OS === 'ios'
                            ? cameraImage.uri.replace('file://', '')
                            : cameraImage.uri,
                });
                (props.cycleStartDate?sentLivePhotoToDB.append('cycleStartDate',props.cycleStartDate):null)
                sentLivePhotoToDB.append('eventId',props.eventId)
                dispatch(fetchLivePhoto(sentLivePhotoToDB, props.eventId));
                // dispatch(fetchAllStories(props.eventId));
                dispatch(fetchAllVerificationRecords(props.eventId));

                // userArr.push(props.challengerID);

                // dispatch(doPreventUpload(true, userArr));
                // dispatch(camImg(cameraImage));
            } else {
                console.log(response);
            }
        });
    };

    useEffect(() => {
        dispatch(fetchHaveUpload(props.eventId, props.challengerID));
    }, []);
    return (
        <View style={styles.verifyContainer}>
            <View
                style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>
                <View>
                    <Text style={[styles.text, styles.verifyHeading]}>
                        Verify:
                    </Text>
                    <Text style={[styles.text, {marginLeft: 10}]}>
                        Take live photo to verify
                    </Text>
                </View>

                {preventUploadUserId.challenger_id === props.challengerID ? (
                    <Icon
                        name="cloud-done-outline"
                        size={40}
                        color={'#ccff66'}
                        style={{
                            marginTop: 15,
                            padding: 10,
                            borderWidth: 1,
                            borderColor: '#ccff66',
                            borderRadius: 30,
                        }}
                    />
                ) : (
                    <TouchableOpacity onPress={handleTakePhoto}>
                        <Icon
                            name="camera-outline"
                            size={40}
                            color={'white'}
                            style={{
                                marginTop: 15,
                                padding: 10,
                                borderWidth: 1,
                                borderColor: '#ccff66',
                                borderRadius: 30,
                            }}
                        />
                    </TouchableOpacity>
                )}

                {/* <TouchableOpacity onPress={handleTakePhoto}>
                    <Icon
                        name="cloud-upload-outline"
                        size={40}
                        color={'white'}
                        style={{
                            marginTop: 15,
                            padding: 10,
                            borderWidth: 1,
                            borderColor: '#ccff66',
                            borderRadius: 30,
                        }}
                    />
                </TouchableOpacity> */}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    text: {
        color: 'white',
        fontSize: 16,
    },
    verifyContainer: {
        marginTop: 20,
        padding: 15,
        borderWidth: 2,
        width: '100%',
        borderColor: '#CCFF66',
        borderRadius: 10,
        paddingTop: 0,
        marginBottom: 10,
    },
    verifyHeading: {
        marginTop: 15,
    },
});
