
import React from 'react';
import { TextInput } from 'react-native-gesture-handler';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

function SearchBar() {
  return (
    <View style={styles.searchContainer}>
      <Text style={styles.searchTitle}>Quick Search</Text>
      <TextInput style={styles.searchInput}></TextInput>
    </View>
  );
};


const styles = StyleSheet.create({

  searchContainer: {
    marginLeft: 10,
    marginRight:10

  },
  searchTitle: {
    color: 'white',
    marginBottom:5,
    fontSize:15
  },
  searchInput: {
    backgroundColor: '#6D6D6D',
    height: 30,
    borderRadius: 30,
  }
});

export default SearchBar;