import React, {useEffect} from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    FlatList,
    useColorScheme,
    View,
    Image,
    TextInput,
    Platform,
    Alert,
    Dimensions,
    KeyboardAvoidingView,
} from 'react-native';
import {useForm, Controller} from 'react-hook-form';
import {ErrorMessage} from '@hookform/error-message';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {RootState} from '../../store';
import {config} from '../../redux/config';
import LoadingSpinner from './LoadingSpinner';
import Icon from 'react-native-vector-icons/Ionicons';
import {IComment} from '../../redux/forum/state';
import {fetchComments, fetchCreateComment} from '../../redux/forum/thunks';
import Comments from './ForumComments';
import {getTimeString} from '../ChatRoomScreen';

const items = [
    {key: 1, name: 'Health', borderColor: '#F0CE1D'},
    {key: 2, name: 'Study', borderColor: '#597EDE'},
    {key: 3, name: 'Lifestyle', borderColor: '#1F9D3B'},
    {key: 4, name: 'Sport', borderColor: '#FFA800'},
    {key: 5, name: 'Music', borderColor: '#9D1F1F'},
    {key: 6, name: 'Others', borderColor: '#CB59DE'},
];

const PostDetails = () => {
    const {
        control,
        handleSubmit,
        formState: {errors},
        reset,
    } = useForm<IComment>();

    const dispatch = useDispatch();
    const navigation = useNavigation();

    const isAuthenticated = useSelector(
        (state: RootState) => state.user.isAuthenticated,
    );
    const userDetails = useSelector((state: RootState) => state.user.user);

    useEffect(() => {
        if (!isAuthenticated) {
            navigation.navigate('Login');
        }
        dispatch(fetchComments(selectedPostDetail?.id));
    }, [isAuthenticated, navigation]);

    interface IErrorProps {
        name: keyof IComment;
    }

    const ErrorBlock = (props: IErrorProps) => {
        return (
            <ErrorMessage
                errors={errors}
                name={props.name}
                render={({message}) => (
                    <View style={styles.errorContainer}>
                        <Text style={styles.errorMessage}>{message}</Text>
                    </View>
                )}
            />
        );
    };

    const selectedPostDetail = useSelector(
        (state: RootState) => state.forum.selectedPost,
    );
    const comments = useSelector((state: RootState) => state.forum.comments);
    const postCategory = useSelector(
        (state: RootState) => state.forum.category,
    );

    const submitComment = (data: IComment) => {
        const formObject = {} as IComment;
        formObject['postId'] = selectedPostDetail?.id;
        formObject['comment'] = data.comment;
        dispatch(fetchCreateComment(formObject));
        reset();
    };

    return (
        <SafeAreaView style={styles.background}>
            <KeyboardAvoidingView behavior="position">
                <ScrollView
                    style={styles.container}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}>
                    <LoadingSpinner />
                    <View style={styles.postTitleContainer}>
                        <Text style={styles.postTitle}>Post</Text>
                    </View>
                    <View style={styles.postContainer}>
                        <View style={styles.postMainBody}>
                            <View style={styles.profileContainer}>
                                <Image
                                    style={styles.profilePicture}
                                    source={{
                                        uri: `${config.S3_BUCKET_URL}/${selectedPostDetail?.pro_pic}`,
                                    }}
                                />
                                <View style={styles.profileInfoContainer}>
                                    <Text style={styles.profileNickname}>
                                        {selectedPostDetail?.nickname}
                                    </Text>
                                    <Text style={styles.createAt}>
                                        {
                                            selectedPostDetail?.created_at.split(
                                                'T',
                                            )[0]
                                        }
                                        {'  '}
                                        {getTimeString(
                                            selectedPostDetail?.created_at
                                                .split('T')[1]
                                                .split('.')[0],
                                        )}
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.postContentContainer}>
                                <Text style={styles.postContent}>
                                    {selectedPostDetail?.content}
                                </Text>
                                {selectedPostDetail?.photo && (
                                    <Image
                                        style={styles.postImage}
                                        source={{
                                            uri: `${config.S3_BUCKET_URL}/${selectedPostDetail?.photo}`,
                                        }}
                                    />
                                )}
                            </View>
                        </View>
                    </View>

                    {comments[0] && <Comments />}
                </ScrollView>
                <View style={styles.form}>
                    <View style={styles.inputContainer}>
                        <Controller
                            control={control}
                            render={({field: {onChange, onBlur, value}}) => (
                                <TextInput
                                    style={styles.input}
                                    onBlur={onBlur}
                                    placeholder="Leave you comment"
                                    placeholderTextColor="#888888"
                                    onChangeText={value => onChange(value)}
                                    value={value}
                                />
                            )}
                            name="comment"
                            rules={{
                                required: {
                                    value: true,
                                    message: 'Comment cannot be empty',
                                },
                            }}
                            defaultValue=""
                        />
                    </View>
                    <TouchableOpacity
                        style={styles.commentSubmit}
                        onPress={handleSubmit(submitComment)}>
                        <Icon name="send" size={25} style={styles.button} />
                    </TouchableOpacity>
                </View>
                <ErrorBlock name="comment" />
            </KeyboardAvoidingView>
        </SafeAreaView>
    );
};
const windowWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
    background: {
        backgroundColor: '#313131',
    },
    container: {
        height: '90%',
        backgroundColor: 'transparent',

        marginRight: 10,
        marginLeft: 10,
    },
    postContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        padding: 5,
        marginBottom: 5,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#888888',
    },
    postMainBody: {
        width: '100%',
    },
    //post title
    postTitleContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: '100%',
    },
    postTitle: {
        color: '#FFFFFF',
        fontSize: 35,
        fontWeight: 'bold',
    },
    //styles for profile
    profileContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: '100%',
        padding: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#888888',
    },
    profilePicture: {
        width: 50,
        height: 50,
        borderRadius: 50,
    },
    profileInfoContainer: {
        justifyContent: 'flex-start',
        padding: 10,
    },
    profileNickname: {
        fontSize: 15,
        color: '#FFFFFF',
        fontWeight: '500',
    },
    createAt: {
        fontSize: 12,
        color: '#FFFFFF',
    },

    //post contents
    postContentContainer: {
        width: '100%',
    },
    postContent: {
        padding: 10,
        color: '#FFFFFF',
        fontSize: 20,
        width: '100%',
    },

    postImageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
        width: '100%',
    },
    postImage: {
        width: windowWidth - 50,
        height: windowWidth - 50,
        borderRadius: 20,
        marginBottom: 10,
        resizeMode: 'contain',
    },
    form: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: '10%',
    },
    inputContainer: {
        borderColor: '#888888',
        borderWidth: 1,
        borderRadius: 20,
        flex: 1,
    },
    input: {
        padding: 10,
        color: '#FFFFFF',
        fontSize: 15,
    },
    //error message styles
    errorContainer: {
        paddingTop: 5,
    },
    errorMessage: {
        color: '#888888',
    },
    commentSubmit: {
        backgroundColor: '#888888',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        width: 40,
        borderRadius: 50,
    },
    button: {
        color: '#FFFFFF',
    },

    //style for comments
    commentContainer: {
        flexDirection: 'row',
        width: '100%',
        padding: 5,
        marginBottom: 5,
        borderRadius: 10,
    },
    commentProPicContainer: {
        justifyContent: 'flex-start',
        paddingHorizontal: 5,
    },
    commentProPic: {
        width: 30,
        height: 30,
        borderRadius: 50,
        padding: 5,
    },
    commentOuterBodyContainer: {
        flex: 1,
    },
    commentBodyContainer: {
        borderWidth: 1,
        borderColor: '#888888',
        flex: 1,
        borderRadius: 10,
        padding: 5,
    },

    commentNicknameContainer: {
        justifyContent: 'flex-start',
        width: '100%',
    },
    commentNickname: {
        fontSize: 15,
        color: '#FFFFFF',
        fontWeight: '500',
    },
    commentContentContainer: {},
    commentContent: {
        fontSize: 15,

        color: '#FFFFFF',
    },
    commentTimeContainer: {},
    commentCreatedAt: {
        color: '#888888',
    },
});

export default PostDetails;
