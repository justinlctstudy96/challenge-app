import React, { useEffect } from 'react';
import { View, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { restoreLogin } from '../redux/user/thunks'
import { RootState } from '../store'
import { useNavigation } from '@react-navigation/native'

const LoadingScreen = () => {
    const dispatch = useDispatch()
    const navigation = useNavigation()
    const isAuthenticated = useSelector((state: RootState) => state.user.isAuthenticated)

    // useEffect(()=>{
    //     // Retain login status
    //     async function loadToken(){
    //         const token = await AsyncStorage.getItem('token');
    //         if(token){
    //             dispatch(loginSuccess(token, null as any as User));
    //             // fetch user
    //         }else{
    //             // do nothing
    //         }
    //     }
    //     loadToken();
    // },[]);
    useEffect(() => {
        if (isAuthenticated == false) {
            dispatch(restoreLogin())
            return
        }
        if (isAuthenticated) {
            navigation.navigate('Main')
        } else {
            navigation.navigate('Login')
        }
    }, [isAuthenticated])

    return (
        <View style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1
        }}>
            <Text>Loading...</Text>
        </View>
    )
}

export default LoadingScreen;