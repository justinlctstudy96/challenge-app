import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';

import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';

import Story from './screens/components/Story';
import StoryReported from './screens/components/StoryReported';
import LoadingScreen from './screens/LoadingScreen';
import LoginScreen from './screens/LoginScreen';
import CreateChallengeScreen from './screens/CreateChallengeScreen';
import ChallengeRoomScreen from './screens/ChallengeRoomScreen';
import HomeScreen from './screens/HomeScreen';
import JournalScreen from './screens/JournalScreen';
import AccountScreen from './screens/AccountScreen';
import ChallengeLobbyScreen from './screens/ChallengeLobbyScreen';
import ChatRoomScreen from './screens/ChatRoomScreen';

import LiveCamera from './screens/components/LiveCamera';
import SignUpScreen from './screens/SignUpScreen';

import ForumScreen from './screens/ForumScreen';
import CreatePost from './screens/components/ForumCreatePost';
import PostDetails from './screens/components/ForumPostDetails';

import { LogBox } from 'react-native';
LogBox.ignoreLogs(['Warning: ...']); // Ignore log notification by message
LogBox.ignoreAllLogs();//Ignore all log notifications
//for guard
import {connect, useDispatch, useSelector} from 'react-redux';
import {RootState} from './store';
import { useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { loginSuccess, User } from './redux/user/actions';

const Tab = createBottomTabNavigator();
// Slide to left go to loading page
const Root = createStackNavigator();
const Stack = createStackNavigator();

const Tabs = () => {
    return (
        <Tab.Navigator
            initialRouteName="Home"
            tabBarOptions={{
                style: {
                    backgroundColor: '#313131',
                    borderTopWidth: 2,
                    borderTopColor: 'rgba(196,196,196,0.3)',
                    paddingTop: 5,
                },
                activeTintColor: '#CCFF66',
                labelStyle: {
                    fontSize: 12,
                },
            }}>
            {/* <LinearGradient colors={['#2974FA', '#38ABFD', '#43D4FF']} style={styles.gradient}></LinearGradient> */}
            <Tab.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    tabBarIcon: ({color, size}) => (
                        <Icon name="home" color={color} size={size} />
                    ),
                }}
            />
            <Tab.Screen
                name="Challenges"
                component={ChallengeLobbyScreen}
                options={{
                    tabBarIcon: ({color, size}) => (
                        <Icon name="map" color={color} size={size} />
                    ),
                }}
            />
            <Tab.Screen
                name="Forum"
                component={ForumScreen}
                options={{
                    tabBarIcon: ({color, size}) => (
                        <Icon name="people-circle" color={color} size={size} />
                    ),
                }}
            />
            <Tab.Screen
                name="Personal"
                component={AccountScreen}
                options={{
                    tabBarIcon: ({color, size}) => (
                        <Icon name="person-circle" color={color} size={size} />
                    ),
                }}
            />
        </Tab.Navigator>
    );
};

function MainStackScreen() {
    return (
        <Stack.Navigator
            initialRouteName="Home"
            headerMode="none"
            screenOptions={{
                gestureEnabled: true,
            }}>
            <Stack.Screen name="Home" component={Tabs} />
            <Stack.Screen
                name="CreateChallenge"
                component={CreateChallengeScreen}
                initialParams={{showChallenges: false}}
            />
            <Stack.Screen
                name="ChallengeRoom"
                component={ChallengeRoomScreen}
            />

            <Stack.Screen name="ChatRoom" component={ChatRoomScreen} />
            {/* <Stack.Screen name="Loading" component={LoadingScreen} /> */}
            {/* <Stack.Screen name="Login" component={LoginScreen} /> */}
            <Stack.Screen name="SignUp" component={SignUpScreen} />
            <Stack.Screen name="CreatePost" component={CreatePost} />
            <Stack.Screen name="PostDetail" component={PostDetails} />
        </Stack.Navigator>
    );
}

function App() {
    return (
        <NavigationContainer>
            <Root.Navigator 
                mode="modal"
                initialRouteName="Loading">
                <Root.Screen
                    name="Main"
                    component={MainStackScreen}
                    options={{headerShown: false}}
                />
                <Root.Screen
                    name="Login"
                    component={LoginScreen}
                    options={{headerShown:false}}
                />
                <Root.Screen
                    name="Loading"
                    component={LoadingScreen}
                    options={{headerShown:false}}
                />
                <Root.Screen
                    name="Stories"
                    component={Story}
                    options={{headerShown: false}}
                />
                <Root.Screen
                    name="StoryReported"
                    component={StoryReported}
                    options={{headerShown: false}}
                />
            </Root.Navigator>
        </NavigationContainer>
    );
}

export default App;
