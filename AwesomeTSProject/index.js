/**
 * @format
 */

import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { Provider } from 'react-redux'
import { store } from './store'
import React from 'react';


AppRegistry.registerComponent("AwesomeTSProject", ()=>AppWrapper);

export function AppWrapper(){
    return (    
        <Provider store={store}>
            <App />
        </Provider>
    )
}

// ReactDOM.render(
//     <React.StrictMode>

//     </React.StrictMode>
// )
