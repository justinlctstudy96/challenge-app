export function loading() {
    return {
        type: '@@loading/LOADING' as '@@loading/LOADING'
    }
}

export function finishLoading(){
    return {
        type: '@@loading/FINISH_LOADING' as '@@loading/FINISH_LOADING'
    }
}

export function loadingSpinnerStart(){
    return {
        type:'LOADING_SPINNER_START' as const 
    }
}

export function loadingSpinnerStop(){
    return {
        type:'LOADING_SPINNER_STOP' as const 
    }
}

export type LoadingActions = ReturnType<typeof loading 
| typeof finishLoading
| typeof loadingSpinnerStart
| typeof loadingSpinnerStop>;