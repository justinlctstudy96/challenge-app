import { LoadingActions } from "./actions"

export interface LoadingState {
    isLoading: number;
    spinning: boolean
}
const initialState = {
    isLoading: 0,
    spinning: false
}


export function loadingReducer(state: LoadingState = initialState, action: LoadingActions): LoadingState {
    switch (action.type) {
        case '@@loading/FINISH_LOADING':
            return {
                ...state,
                isLoading: state.isLoading - 1
            }
        case '@@loading/LOADING':
            return {
                ...state,
                isLoading: state.isLoading + 1
            }
        case 'LOADING_SPINNER_START':
            return{
                ...state,
                spinning: false // Update it back to true later


            }
        case 'LOADING_SPINNER_STOP':
            return{
                ...state,
                spinning: false

            }
        default:
            return state
    }
}