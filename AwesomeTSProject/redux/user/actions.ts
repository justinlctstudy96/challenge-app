import {Asset} from 'react-native-image-picker';

//User related actions
export function uploadProfileImage(photo: Asset) {
    return {
        type: 'UPLOAD_PROFILE_IMAGE' as const,
        photo,
    };
}

export function removePreviewImage() {
    return {
        type: 'REMOVE_PREVIEW_IMAGE' as const,
    };
}

export interface User {
    id: number;
    email: string;
    nickname: string;
    pro_pic: string;
}

//login related actions
export function loginSuccess(token: string, user: User) {
    return {
        type: '@@auth/LOGIN_SUCCESS' as '@@auth/LOGIN_SUCCESS',
        token,
        user,
    };
}

export function loginFailed(error: string) {
    return {
        type: '@@auth/LOGIN_FAILED' as '@@auth/LOGIN_FAILED',
        error,
    };
}

export function logoutSuccess() {
    return {
        type: '@@auth/LOGOUT' as '@@auth/LOGOUT',
    };
}

export function clearError(error: string) {
    return {
        type: '@@auth/CLEAR_ERROR' as '@@auth/CLEAR_ERROR',
    };
}

export function accountDeleteSuccess (){
    return {
        type: 'ACCOUNT_DELETE_SUCCESS' as const
    }
}
 
type FAILED_INTENT =
    | 'UPLOAD_PROFILE_IMAGE_FAILED'
    | 'CHECK_SIGN_UP_INFO_FAILED';

export function failed(type: FAILED_INTENT, msg: string) {
    return {
        type,
        msg,
    };
}

type UserActionCreators =
    | typeof uploadProfileImage
    | typeof removePreviewImage
    | typeof failed
    | typeof loginSuccess
    | typeof loginFailed
    | typeof logoutSuccess
    | typeof clearError
    | typeof accountDeleteSuccess

export type IUserActions = ReturnType<UserActionCreators>;
