import { Asset } from 'react-native-image-picker';
import { User } from "./actions"

export interface IUserState {
    uploadPhoto: boolean | Asset,
    isAuthenticated: boolean;
    token: string | null;
    user: User | null;
    error: string | null;

    // messages: IMessage[]
}

export interface IMessage{
    msg:string
    level: "WARNING" | "INFO" |"ERROR"
}

// Possible npm packages
// https://www.npmjs.com/package/react-native-toast-message

