import {Alert} from 'react-native';
import {Dispatch} from 'redux';
import {config} from '../config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    IUserActions,
    removePreviewImage,
    loginSuccess,
    loginFailed,
    logoutSuccess,
    accountDeleteSuccess,
} from './actions';
import {ThunkDispatch, RootState} from '../../store';

export function fetchAccountSignUp(data: FormData) {
    return async (dispatch: ThunkDispatch) => {
        dispatch({type: 'LOADING_SPINNER_START'});
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/user/register`,
            {
                method: 'POST',
                mode: 'cors',
                headers: {
                    'content-type': 'multipart/form-data',
                },
                body: data,
            },
        );
        const result = await res.json();
        if (res.status === 200 && result.token) {
            Alert.alert(result.message);
            await AsyncStorage.setItem('token', result.token);
            dispatch(loginSuccess(result.token, result.user));
            dispatch(removePreviewImage());
            dispatch({type: 'LOADING_SPINNER_STOP'});
        } else {
            dispatch({type: 'LOADING_SPINNER_STOP'});
            Alert.alert(result.message);
        }
    };
}

export function fetchDeleteAccount() {
    return async (dispatch: ThunkDispatch) => {
        dispatch({type: 'LOADING_SPINNER_START'});
        const token = await AsyncStorage.getItem('token');
        if (token == null) {
            dispatch(logout());
            return;
        }
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/user/deleteAccount`,
            {
                method: 'DELETE',
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            },
        );
        const result = await res.json();
        dispatch({type: 'LOADING_SPINNER_STOP'});
        Alert.alert(result.message);
        dispatch(accountDeleteSuccess());
    };
}

export function logout() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        dispatch({type: 'LOADING_SPINNER_START'});
        await AsyncStorage.removeItem('token');
        dispatch(logoutSuccess());
        dispatch({type: 'LOADING_SPINNER_STOP'});
        Alert.alert('Logout successfully!');
    };
}

export function login(email: string, password: string) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            // const res = await fetch(`http://192.168.168.50:8080/login`, {
            dispatch({type: 'LOADING_SPINNER_START'});
            const res = await fetch(`${config.REACT_APP_BACKEND_URL}/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email,
                    password,
                }),
            });
            const json = await res.json();
            if (res.status != 200) {
                dispatch({type: 'LOADING_SPINNER_STOP'});
                Alert.alert(json.message);
                return dispatch(loginFailed(json.error));
            }

            if (!json.token) {
                dispatch({type: 'LOADING_SPINNER_STOP'});
                return dispatch(loginFailed('Network error'));
            }
            await AsyncStorage.setItem('token', json.token);
            dispatch(loginSuccess(json.token, json.user));
            dispatch({type: 'LOADING_SPINNER_STOP'});
            // Alert.alert('Login successfully');
        } catch (e) {
            console.error(e);
            dispatch({type: 'LOADING_SPINNER_STOP'});
            dispatch(loginFailed('Unknown error'));
        }
    };
}

export function restoreLogin() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            dispatch({type: 'LOADING_SPINNER_START'});
            const token = await AsyncStorage.getItem('token');
            if (token) {
                const res = await fetch(
                    `${config.REACT_APP_BACKEND_URL}/user`,
                    {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    },
                );
                const json = await res.json();
                if (json.id) {
                    dispatch(loginSuccess(token, json));
                    return dispatch({type: 'LOADING_SPINNER_STOP'});
                }
            }
            dispatch({type: 'LOADING_SPINNER_STOP'});
            return;
        } catch (e) {
            console.error(e);
            dispatch({type: 'LOADING_SPINNER_STOP'});
        }
    };
}
