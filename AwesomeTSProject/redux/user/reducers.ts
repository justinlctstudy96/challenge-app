import {IUserActions} from './actions';
import {IUserState} from './state';

const initialState = {
    uploadPhoto: false,
    isAuthenticated: false,
    token: null,
    user: null,
    error: null
};

export const userReducers = (
    state: IUserState = initialState,
    action: IUserActions,
) => {
    switch (action.type) {
        case 'UPLOAD_PROFILE_IMAGE':
            const {photo} = action;
            return {
                ...state,
                uploadPhoto: photo,
            };
        case 'REMOVE_PREVIEW_IMAGE':
            return {
                ...state,
                uploadPhoto: false,
            };
        case '@@auth/LOGIN_SUCCESS':
            return {
                ...state,
                isAuthenticated: true,
                token: action.token,
                user: action.user,
            };
        case '@@auth/CLEAR_ERROR':
            return {
                ...state,
                error: null,
            };
        case '@@auth/LOGIN_FAILED':
            return {
                ...state,
                isAuthenticated: false,
                token: null,
                user: null,
                error: action.error,
            };
        case '@@auth/LOGOUT':
            return {
                ...state,
                isAuthenticated: false,
                token: null,
                user: null,
            };
        case 'ACCOUNT_DELETE_SUCCESS':
            return{
                ...initialState
            };
        default:
            return state;
    }
};
