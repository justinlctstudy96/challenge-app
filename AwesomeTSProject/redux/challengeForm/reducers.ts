
import {ThunkDispatch, RootState} from '../../store';
import {CreateChallengeScreenActions} from './actions';

export interface Category {
    id: number;
    category: string;
    official: boolean
}

export interface Verification {
    id: number;
    method: string;
}

export interface Challenge {
    id: number;
    title: string;
    rule: string;
    task: string;
    description: string;
    persistence: boolean;
    consecutive: boolean;
    times: number;
    days: number;
    cycles: number;
    renewTime: string;
    with_section: boolean;
    publicity: boolean;
    sections: string[];
    verifyMethods: number[]; // other table
    categories: number[]; // other table
    creator_id: number;
    creator_name: string;
    created_at: string
}

const emptyChallengeDetails: Challenge = {
    id: 0,
    title: '',
    rule: '',
    task: '',
    description: '',
    persistence: false,
    consecutive: false,
    times: 0,
    days: 0,
    cycles: 0,
    renewTime: '',
    with_section: false,
    publicity: false,
    sections: [],
    verifyMethods: [],// other table
    categories: [], // other table
    creator_id: 0,
    creator_name: '',
    created_at:''
}

export interface CreateChallengeScreenState {
    openChallengeForm: boolean;
    openChallengeEventForm: boolean;
    challengeId: number;
    challengeDetails: Challenge;
    allCategories: Category[];
    eventId: number;
    // start: boolean | null; //not use since choosing start in create challenge form will auto back to lobby
}

const initialState: CreateChallengeScreenState = {
    openChallengeForm: true,
    openChallengeEventForm: false,
    challengeId: 1,
    challengeDetails: emptyChallengeDetails,
    allCategories: [],
    eventId: 0
    // start: false,
};

export interface ChallengeEvent {
    challengeId: number;
    accommodation: number;
    official: boolean;
    officialReward: string;
    start: Date;
    end: Date;
    comment: string;
}



export const createChallengeReducer = (
    state: CreateChallengeScreenState = initialState,
    action: CreateChallengeScreenActions,
): CreateChallengeScreenState => {
    switch (action.type) {
        case '@@challenge/CREATE_SUCCESS':
            return {
                ...state,
                challengeId: action.challengeId,
                openChallengeForm: false,
                openChallengeEventForm: true
            };
        case '@@challenge/CREATE_FAILED':
            return {
                ...state,
                openChallengeForm: true,
                openChallengeEventForm: false
            };
        case '@@challenge/GET_SUCCESS':
            return {
                ...state,
                challengeDetails: action.challengeDetails
            }
        case '@@challenge_screen/RESET_CREATE_SCREEN':
            return initialState
        case '@@challenge_event/CREATE_SUCCESS':
            // navigation.navigate('ChallengeRoom', {eventId: action.})
            return {
                ...state,
                openChallengeEventForm: false,
                eventId: action.eventId
            }
        case '@@challenge_event/CREATE_FAILED':
            return {
                ...state,
            }
        case '@@all_categories/GET_SUCCESS':
            return {
                ...state,
                allCategories: action.categories
            }
        default: {
            return state;
        }
    }
};

