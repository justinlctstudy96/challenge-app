import {ThunkDispatch, RootState} from '../../store';
import {Dispatch} from 'redux';
// import { AsyncStorage } from "react-native"
import AsyncStorage from '@react-native-async-storage/async-storage';
import {config} from '../config';
import {Category, Challenge, ChallengeEvent} from './reducers';
import { fetchEventDots } from '../challengeJournal/thunks';
import { fetchAllChallengeRelatedData } from '../combinedChallengesThunk';

export function createChallengeSuccess(challengeId: number) {
    return {
        type: '@@challenge/CREATE_SUCCESS' as '@@challenge/CREATE_SUCCESS',
        challengeId,
    };
}

export function createChallengeFailed(error: string) {
    return {
        type: '@@challenge/CREATE_FAILED' as '@@challenge/CREATE_FAILED',
        error,
    };
}

export function getChallengeDetailsSuccess(challengeDetails: Challenge) {
    return {
        type: '@@challenge/GET_SUCCESS' as '@@challenge/GET_SUCCESS',
        challengeDetails,
    };
}

export function getChallengeDetailsFailed(error: string) {
    return {
        type: '@@challenge/GET_FAILED' as '@@challenge/GET_FAILED',
        error,
    };
}

export function resetCreateChallengeScreen() {
    return {
        type: '@@challenge_screen/RESET_CREATE_SCREEN' as '@@challenge_screen/RESET_CREATE_SCREEN',
    };
}

export function createChallengeEventSuccess(eventId: number) {
    // alert('eventIdsuccess  '+eventId)
    return {
        type: '@@challenge_event/CREATE_SUCCESS' as '@@challenge_event/CREATE_SUCCESS',
        eventId,
    };
}

export function createChallengeEventFailed(error: string) {
    return {
        type: '@@challenge_event/CREATE_FAILED' as '@@challenge_event/CREATE_FAILED',
        error,
    };
}

export function getAllCategoriesSuccess(categories: Category[]) {
    return {
        type: '@@all_categories/GET_SUCCESS' as const,
        categories
    }
}

export type CreateChallengeScreenActions = ReturnType<
    | typeof createChallengeSuccess
    | typeof createChallengeFailed
    | typeof getChallengeDetailsSuccess
    | typeof getChallengeDetailsFailed
    | typeof resetCreateChallengeScreen
    | typeof createChallengeEventSuccess
    | typeof createChallengeEventFailed
    | typeof getAllCategoriesSuccess
>;

export async function fetchCategories() {
    try {
        const res = await fetch(`${config.REACT_APP_BACKEND_URL}/categories`);
        const categories = await res.json();
        return categories;
    } catch (e) {
    }
}

export async function fetchVerifications() {
    try {
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/verifications`,
        );
        const verifications = await res.json();
        return verifications;
    } catch (e) {
    }
}

export async function fetchUserLike(challengeId: number) {
    try {
        const token = await AsyncStorage.getItem('token');
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/like/${challengeId.toString()}`,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            },
        );
        return (await res.json()).userLiked;
    } catch (e) {
    }
}

export async function fetchTotalLikes(challengeId: number) {
    try {
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/likes/${challengeId.toString()}`,
        );
        return (await res.json()).totalLikes;
    } catch (e) {
    }
}

export async function postLike(challengeId: number) {
    try {
        const token = await AsyncStorage.getItem('token');
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/like/${challengeId.toString()}`,
            {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            },
        );
        return (await res.json()).userLiked;
    } catch (e) {
    }
}

export async function deleteLike(challengeId: number) {
    try {
        const token = await AsyncStorage.getItem('token');
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/like/${challengeId.toString()}`,
            {
                method: 'DELETE',
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            },
        );
        return (await res.json()).userLiked;
    } catch (e) {
    }
}

//thunk

export function fetchAllCategories() {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${config.REACT_APP_BACKEND_URL}/categories`);
            const categories = (await res.json()).rows;
            if (res.status != 200) {
                return 'failed'
            }
            dispatch(getAllCategoriesSuccess(categories))
        } catch (e) {
        }
    }
}

export function createChallenge(start: boolean, challenge: Challenge) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        try {
            const token = await AsyncStorage.getItem('token');
            const res = await fetch(
                `${config.REACT_APP_BACKEND_URL}/challenge`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify(challenge),
                },
            );
            const json = await res.json();
            if (res.status != 200) {
                return dispatch(createChallengeFailed(json.error));
            }
            dispatch(createChallengeSuccess(json.challengeId));
        } catch (e) {
            console.error(e);
            dispatch(createChallengeFailed('Unknown error'));
        }
    };
}

export function fetchChallengeDetails(challengeId: number) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        try {
            const res = await fetch(
                `${config.REACT_APP_BACKEND_URL}/challenge/${challengeId}`,
            );
            const json = await res.json();
            if (res.status != 200) {
                return dispatch(getChallengeDetailsFailed(json.error));
            }
            const challengeDetails: Challenge = json;
            dispatch(getChallengeDetailsSuccess(challengeDetails));
        } catch (e) {
            dispatch(getChallengeDetailsFailed('Unknown error'));
        }
    };
}

export function createChallengeEvent(challengeEvent: ChallengeEvent, userId: number|undefined) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        try {
            dispatch({type: 'LOADING_SPINNER_START'});
            const token = await AsyncStorage.getItem('token');
            const res = await fetch(`${config.REACT_APP_BACKEND_URL}/event`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify(challengeEvent),
            });
            const json = await res.json();
            if (res.status != 200) {
                return dispatch({type: 'LOADING_SPINNER_STOP'});
            }
            dispatch(createChallengeEventSuccess(json.eventId));
            await fetchAllChallengeRelatedData(challengeEvent.challengeId, userId)
            return dispatch({type: 'LOADING_SPINNER_STOP'});
        } catch (e) {
            dispatch(createChallengeEventFailed('Unknown error'));
            dispatch({type: 'LOADING_SPINNER_STOP'});
        }
    };
}
