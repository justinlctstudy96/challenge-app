import { ChallengeLobbyScreenActions } from './actions'

export interface ChallengeEventSummary {
    challenge_id: number;
    title: string;
    rule: string;
    task: string;
    description: string;
    times: number;
    days: number;
    cycles: number;
    renewTime: string;
    persistence: boolean;
    consecutive: boolean;
    with_section: boolean;
    sections: string[]
    verifications: number[]
    categories: number[]

    creator_id: number;
    creator_name: string

    event_id: number
    accommodation: number
    official: boolean
    officialReward: string
    comment: string
    start_date: string;
    end_date: string;

    host_id: number;
    host_name: string;

    participants: number;

    verificationSubmittedTimes: number;
    currentCycleVerificationTimes: number;
    cycleDates: CycleDate[]
}

export interface CycleDate{
    current_cycle: number
    start_date: string;
    end_date: string;
}

export interface ChallengeSummary {
    challenge_id: number;
    title: string;
    rule: string;
    task: string;
    description: string;
    times: number;
    days: number;
    cycles: number;
    renewTime: string;
    persistence: boolean;
    consecutive: boolean;
    with_section: boolean;
    sections: string[]
    verifications: number[]
    categories: number[]
    likes: number
    past_participants: number

    creator_id: number;
    creator_name: string
}

export interface FilterOrder {
    searchText: string;
    filter: number[];
    savedFilter: boolean
    orderedBy: string;
}


export const emptyEvent: ChallengeEventSummary =  {
    challenge_id: 0,
    title: '',
    rule: '',
    task: '',
    description: '',
    times: 0,
    days: 0,
    cycles: 0,
    renewTime: '',
    persistence: false,
    consecutive: false,
    with_section: false,
    sections: [],
    verifications: [],
    categories: [],

    creator_id: 0,
    creator_name: '',

    event_id: 0,
    accommodation: 0,
    official: false,
    officialReward: '',
    comment: '',
    start_date: '',
    end_date: '',

    host_id: 0,
    host_name: '',

    participants: 0,
    verificationSubmittedTimes: 0,
    currentCycleVerificationTimes: 0,
    cycleDates: []
}

// export const challengeLobbyReducer = (
//     state:
// )

// export const eventListReducer = (
//     state: 
// )