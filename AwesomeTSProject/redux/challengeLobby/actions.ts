import {RootState} from '../../store';
import {Dispatch} from 'redux';
import {config} from '../config';
import {FilterOrder} from './reducers';
// import { AsyncStorage } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

export function getChallengesSuccess() {
    return {
        type: '@@challenges/GET_SUCCESS' as '@@challenges/GET_SUCCESS',
    };
}

export function getChallengesFailed() {
    return {
        type: '@@challenges/GET_FAILED' as '@@challenges/GET_FAILED',
    };
}

export function getEventsSuccess() {
    return {
        type: '@@events/GET_SUCCESS' as 'events/GET_SUCCESS',
    };
}

export function getEventsFailed() {
    return {
        type: '@@events/GET_FAILED' as '@@events/GET_FAILED',
    };
}

export type ChallengeLobbyScreenActions = ReturnType<
    | typeof getChallengesSuccess
    | typeof getChallengesFailed
    | typeof getEventsSuccess
    | typeof getEventsFailed
>;

export async function fetchChallenges(filterOrder: FilterOrder) {
    try {
        const token = await AsyncStorage.getItem('token')
        const res = await fetch(`${config.REACT_APP_BACKEND_URL}/challenges`,{
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(filterOrder)
        });
        const challenges = await res.json();
        return challenges;
    } catch (e) {
    }
}

export async function fetchEvents(filterOrder: FilterOrder) {
    try {
        const token = await AsyncStorage.getItem('token')
        const res = await fetch(`${config.REACT_APP_BACKEND_URL}/events`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(filterOrder),
        });
        const events = await res.json();
        return events;
    } catch (e) {
        return []
    }
}
