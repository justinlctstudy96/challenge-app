import {IForumActions} from './actions';
import {IForumState} from './state';

const initialState = {
    category: 1,
    forumPhoto: false,
    posts: [],
    comments: [],
    error: null,
    selectedPost: null,
    likes:[]
};

export const forumReducers = (
    state: IForumState = initialState,
    action: IForumActions,
) => {
    switch (action.type) {
        case 'SWITCH_CATEGORY':
            return {
                ...state,
                category: action.categoryId,
            };
        case 'SWITCH_SELECTED_POST':
            const { selectedPost } = action
            return {
                ...state,
                selectedPost: selectedPost
            }
        case 'UPLOAD_FORUM_IMAGE':
            const {photo} = action
            return {
                ...state,
                forumPhoto: photo,
            };
        case 'REMOVE_FORUM_IMAGE':
            return {
                ...state,
                forumPhoto: false,
            };
        case 'CREATE_POST_SUCCESS':
            return {
                ...state,
            };
        case 'CREATE_POST_FAILED':
            return {
                ...state,
                error: action.msg,
            };
        case 'CREATE_COMMENT_SUCCESS':
            return {
                ...state,
            };
        case 'CREATE_COMMENT_FAILED':
            return {
                ...state,
                error: action.msg,
            };
        case 'GET_ALL_POSTS_SUCCESS':
            const { postsDetails } = action
            return {
                ...state,
                posts: postsDetails
            };
        case 'GET_ALL_POSTS_FAILED':
            return {
                ...state,
                error: action.msg,
            };
        case 'GET_ALL_COMMENTS_SUCCESS':
            const { commentsDetail } = action
            return {
                ...state,
                comments: commentsDetail

            };
        case 'GET_LIKES_SUCCESS':
            const { likesList } = action
            return {
                ...state,
                likes: likesList
            }
        case 'GET_ALL_COMMENTS_FAILED':
            return {
                ...state,
                error: action.msg,
            };
        default:
            return state;
    }
};
