import {Asset} from 'react-native-image-picker';
import { ICommentsDetail, IPostsDetail } from './state';

export function switchCategory(categoryId: number) {
    return {
        type: 'SWITCH_CATEGORY' as const,
        categoryId,
    };
}


export function switchSelectedPost(selectedPost: IPostsDetail) {
    return {
        type: 'SWITCH_SELECTED_POST' as const,
        selectedPost,
    };
}

export function uploadForumImage(photo: Asset) {
    return {
        type: 'UPLOAD_FORUM_IMAGE' as const,
        photo,
    };
}

export function removeForumImage() {
    return {
        type: 'REMOVE_FORUM_IMAGE' as const,
    };
}

export function createPostSuccess(token: string) {
    return {
        type: 'CREATE_POST_SUCCESS' as const,
    };
}


export function createCommentSuccess(token: string, postId: number) {
    return {
        type: 'CREATE_COMMENT_SUCCESS' as const,
    };
}


export function getAllPostsSuccess(postsDetails: IPostsDetail[]) {
    return {
        type: 'GET_ALL_POSTS_SUCCESS' as const,
        postsDetails
    };
}


export function getAllCommentsSuccess(commentsDetail: ICommentsDetail[]) {
    return {
        type: 'GET_ALL_COMMENTS_SUCCESS' as const,
        commentsDetail
    };
}

export function getLikeSuccess(likesList:string[]) {
    return {
        type: 'GET_LIKES_SUCCESS' as const,
        likesList
    };
}


type FAILED_INTENT =
    | 'CREATE_POST_FAILED'
    | 'CREATE_COMMENT_FAILED'
    | 'GET_ALL_POSTS_FAILED'
    | 'GET_ALL_COMMENTS_FAILED';

export function failed(type: FAILED_INTENT, msg: string) {
    return {
        type,
        msg,
    };
}

type ForumActionCreators =
    | typeof switchCategory
    | typeof switchSelectedPost
    | typeof uploadForumImage
    | typeof removeForumImage
    | typeof createPostSuccess
    | typeof createCommentSuccess
    | typeof getAllPostsSuccess
    | typeof getAllCommentsSuccess
    | typeof getLikeSuccess
    | typeof failed;

export type IForumActions = ReturnType<ForumActionCreators>;
