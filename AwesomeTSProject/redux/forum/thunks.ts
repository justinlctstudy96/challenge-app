import {Alert} from 'react-native';
import {config} from '../config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {getAllCommentsSuccess, getAllPostsSuccess, getLikeSuccess, removeForumImage} from './actions';
import {ThunkDispatch, RootState} from '../../store';
import { IComment } from './state';

export function fetchCreatePost(data: FormData) {
    return async (dispatch: ThunkDispatch) => {
        dispatch({type: 'LOADING_SPINNER_START'});
        const token = await AsyncStorage.getItem('token');
        const res = await fetch(`${config.REACT_APP_BACKEND_URL}/forum/posts`, {
            method: 'POST',
            mode: 'cors',
            headers: {
                Authorization: `Bearer ${token}`,
                'content-type': 'multipart/form-data',
            },
            body: data,
        });
        const result = await res.json();
        if (res.status === 200) {
            dispatch(removeForumImage());
            dispatch(fetchLikes())
            dispatch(fetchAllPosts());
            dispatch({type: 'LOADING_SPINNER_STOP'});
            Alert.alert(result.message);
        } else {
            dispatch({type: 'LOADING_SPINNER_STOP'});
            Alert.alert(result.message);
        }
    };
}

export function fetchAllPosts() {
    return async (dispatch: ThunkDispatch) => {
        dispatch({type: 'LOADING_SPINNER_START'});
        const token = await AsyncStorage.getItem('token');
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/forum/posts`,
            {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            },
        );
        const result = await res.json();
        if(res.status === 200){
            dispatch(getAllPostsSuccess(result))
            dispatch({type: 'LOADING_SPINNER_STOP'});
        } else {
            dispatch({type: 'LOADING_SPINNER_STOP'});
            Alert.alert(result.message);
        }
    };
}

export function fetchCreateComment(data: IComment) {
    return async (dispatch: ThunkDispatch) => {
        dispatch({type: 'LOADING_SPINNER_START'});
        const token = await AsyncStorage.getItem('token');
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/forum/comments`,
            {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            },
        );
        const result = await res.json();
        if(res.status === 200){
            dispatch(fetchComments(data.postId))
            dispatch(fetchAllPosts());
            dispatch({type: 'LOADING_SPINNER_STOP'});
        } else {
            dispatch({type: 'LOADING_SPINNER_STOP'});
            Alert.alert(result.message);
        }

    };
}

export function fetchComments(postId:number|null|undefined) {
    return async (dispatch: ThunkDispatch) => {
        dispatch({type: 'LOADING_SPINNER_START'});
        const token = await AsyncStorage.getItem('token');
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/forum/comments/${postId}`,
            {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            },
        );
        const result = await res.json();
        if(res.status === 200){
            dispatch(getAllCommentsSuccess(result))
            dispatch({type: 'LOADING_SPINNER_STOP'});
        } else {
            Alert.alert(result.message);
            dispatch({type: 'LOADING_SPINNER_STOP'});
        }
    };
}

export function fetchPostLike(postId:number|undefined) {
    return async (dispatch: ThunkDispatch) => {
        dispatch({type: 'LOADING_SPINNER_START'});
        const token = await AsyncStorage.getItem('token');
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/forum/posts/like/${postId}`,
            {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            },
        );
        const result = await res.json();
        if(res.status === 200){
            dispatch(fetchLikes())
            dispatch(fetchAllPosts());
            dispatch({type: 'LOADING_SPINNER_STOP'});
        } else {
            Alert.alert(result.message);
            dispatch({type: 'LOADING_SPINNER_STOP'});
        }
    };
}

export function fetchDeleteLike(postId:number|undefined) {
    return async (dispatch: ThunkDispatch) => {
        dispatch({type: 'LOADING_SPINNER_START'});
        const token = await AsyncStorage.getItem('token');
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/forum/posts/like/${postId}`,
            {
                method: 'DELETE',
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            },
        );
        const result = await res.json();
        if(res.status === 200){
            dispatch(fetchLikes())
            dispatch(fetchAllPosts());
            dispatch({type: 'LOADING_SPINNER_STOP'});
        } else {
            Alert.alert(result.message);
            dispatch({type: 'LOADING_SPINNER_STOP'});
        }
    };
}

export function fetchLikes() {
    return async (dispatch: ThunkDispatch) => {
        dispatch({type: 'LOADING_SPINNER_START'});
        const token = await AsyncStorage.getItem('token');
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/forum/posts/like`,
            {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            },
        );
        const result = await res.json();
        if(res.status === 200){
            dispatch(getLikeSuccess(result));
            dispatch({type: 'LOADING_SPINNER_STOP'});
        } else {
            Alert.alert(result.message);
            dispatch({type: 'LOADING_SPINNER_STOP'});
        }
    };
}