import { Asset } from 'react-native-image-picker';

export interface IPostsDetail{
    category_id: number,
    comments: string
    content: string,
    created_at: string,
    id: number,
    likes: string,
    nickname: string,
    photo: string
    pro_pic: string
    user_id: number,
}

export interface IComment {
    comment: string,
    postId?: number|null|undefined
}

export interface ICommentsDetail{
    id: number,
    post_id: number,
    user_id :number,
    comment: string,
    comment_created_at: Date,
    nickname:string,
    pro_pic:string
}


export interface IForumState {
    category: number,
    forumPhoto: boolean | Asset,
    posts: IPostsDetail[],
    comments: ICommentsDetail[]
    error: string | null,
    selectedPost: IPostsDetail | null,
    likes: number[]
}