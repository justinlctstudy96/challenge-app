import {config} from '../config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    getUserOngoingChallengesFailed,
    getUserOngoingChallengesSuccess,
    getUserPastChallengesSuccess,
    getUserPastChallengesFailed,
    updateEventDots,
    updateEventDotsFailed,
    updateSelectedDateEvent,
    updateSelectedDateEventFailed,
} from './actions';
import {ThunkDispatch, RootState} from '../../store';
import {Alert} from 'react-native';
import { fetchCategories } from '../challengeForm/actions';
// import { authFetch } from '../../common/fetchs';

// fetchXXXYYY
// getXXXXThunk

export function fetchOngoingUserChallenges() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const token = await AsyncStorage.getItem('token');
            const res = await fetch(
                `${config.REACT_APP_BACKEND_URL}/challenges/ongoing`,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                },
            );
            const ongoingChallenges = await res.json();
            if (res.status != 200) {
                dispatch(getUserOngoingChallengesFailed('unknown error'));
            }
            dispatch(getUserOngoingChallengesSuccess(ongoingChallenges));
            return ongoingChallenges;
        } catch (e) {
            dispatch(getUserOngoingChallengesFailed('unknown error'));
        }
    };
}

export function fetchPastUserChallenges() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const token = await AsyncStorage.getItem('token');

            // authFetch('/challenges/past',dispatch);
            const res = await fetch(
                `${config.REACT_APP_BACKEND_URL}/challenges/past`,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                },
            );
            const pastChallenges = await res.json();
            if (res.status != 200) {
                dispatch(getUserPastChallengesFailed('unknown error'));
            }
            dispatch(getUserPastChallengesSuccess(pastChallenges));
        } catch (e) {
            dispatch(getUserPastChallengesFailed('unknown error'))
        }
    }
}

export function fetchEventDots(userId: number | undefined) {
    return async (dispatch: ThunkDispatch) => {
        dispatch({type: 'LOADING_SPINNER_START'});

        if (userId) {
            const token = await AsyncStorage.getItem('token');
            const res = await fetch(
                `${config.REACT_APP_BACKEND_URL}/challenges/ongoing/dots/${userId}`,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                },
            );
            const result = await res.json();
            if (res.status == 200) {
                dispatch(updateEventDots(result));
                dispatch({type: 'LOADING_SPINNER_STOP'});
                return 
            }
            dispatch(updateEventDotsFailed());
            dispatch({type: 'LOADING_SPINNER_STOP'});
            return 
        }
    };
}

export function fetchEventByDate(userId: number | undefined, newDate: string) {
    return async (dispatch: ThunkDispatch) => {
        dispatch({type: 'LOADING_SPINNER_START'});
        if (userId) {
            const token = await AsyncStorage.getItem('token');
            const res = await fetch(
                `${config.REACT_APP_BACKEND_URL}/challenges/ongoing/dots/${userId}/${newDate}`,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                },
            );
            const result = await res.json();
            if (res.status == 200) {
                dispatch(updateSelectedDateEvent(newDate, result));
                dispatch({type: 'SWITCH_DISPLAY_MODE'});
                dispatch({type: 'LOADING_SPINNER_STOP'});
                return 
            }
            dispatch(updateSelectedDateEventFailed(newDate));
            dispatch({type: 'LOADING_SPINNER_STOP'});
            return Alert.alert(result.message);
        }
    };
}
