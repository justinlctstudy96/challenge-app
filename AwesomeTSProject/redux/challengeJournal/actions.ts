import {PersonalChallengeEventSummary} from './state';

export function getUserOngoingChallengesSuccess(
    ongoingUserChallenges: PersonalChallengeEventSummary[],
) {
    return {
        type: '@@ongoingUserChallenges/GET_SUCCESS' as const,
        ongoingUserChallenges,
    };
}

export function getUserOngoingChallengesFailed(error: string) {
    return {
        type: '@@ongoingUserChallenges/GET_FAILED' as const,
        error,
    };
}
export function getUserPastChallengesSuccess(
    pastUserChallenges: PersonalChallengeEventSummary[],
){
    return {
        type: '@@pastUserChallenges/GET_SUCCESS' as const,
        pastUserChallenges
    }
}
export function getUserPastChallengesFailed(error: string) {
    return {
        type: '@@pastUserChallenges/GET_FAILED' as const,
        error,
    };
}
export function switchDisplayMode() {
    return {
        type: 'SWITCH_DISPLAY_MODE' as const,
    };
}

export function updateSelectedDateEvent(newDate:string, newDateEvent:{[key: string]: any}[]) {
    return {
        type: 'UPDATE_SELECTED_DATE_EVENT_SUCCESS' as const,
        newDate,
        newDateEvent,

    };
}

export function updateSelectedDateEventFailed(updatedDate:string) {
    return {
        type: 'UPDATE_SELECTED_DATE_EVENT_FAILED' as const,
        updatedDate

    };
}


export function updateEventDots(newEventDots: {[key: string]: any}) {
    return {
        type: 'UPDATE_EVENT_DOTS_SUCCESS' as const,
        newEventDots,
    };
}


export function updateEventDotsFailed() {
    return {
        type: 'UPDATE_EVENT_DOTS_FAILED' as const
    };
}

type JournalChallengeActionsCreators =
    | typeof getUserOngoingChallengesSuccess
    | typeof getUserOngoingChallengesFailed
    | typeof getUserPastChallengesSuccess
    | typeof getUserPastChallengesFailed
    | typeof switchDisplayMode
    | typeof updateSelectedDateEvent
    | typeof updateSelectedDateEventFailed
    | typeof updateEventDots
    | typeof updateEventDotsFailed

export type IJournalScreenActions = ReturnType<JournalChallengeActionsCreators>;
