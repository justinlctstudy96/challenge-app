import {IJournalScreenActions} from './actions';
import {IJournalScreenState} from './state';

const currentDate = new Date();
const currentDateSliced = currentDate.toISOString().slice(0, 10);

const initialState: IJournalScreenState = {
    calendarMode: true,
    selectedDate: currentDateSliced,
    selectedDateEvent: [],
    userOngoingChallenges: [],
    userPastChallenges: [],
    eventDots: {},
};

export const journalReducer = (
    state: IJournalScreenState = initialState,
    action: IJournalScreenActions,
): IJournalScreenState => {
    switch (action.type) {
        case '@@ongoingUserChallenges/GET_SUCCESS':
            return {
                ...state,
                userOngoingChallenges: action.ongoingUserChallenges,
            };
        case '@@ongoingUserChallenges/GET_FAILED':
            return {
                ...state,
            };
        case '@@pastUserChallenges/GET_SUCCESS':
            return {
                ...state,
                userPastChallenges: action.pastUserChallenges
            }
        case 'UPDATE_EVENT_DOTS_SUCCESS':
            const {newEventDots} = action;
            return {
                ...state,
                eventDots: newEventDots,
            };
        case 'UPDATE_EVENT_DOTS_FAILED':
            return {
                ...state,
                eventDots: {},
            };
        case 'SWITCH_DISPLAY_MODE':
            return {
                ...state,
                calendarMode: !state.calendarMode,
            };
        case 'UPDATE_SELECTED_DATE_EVENT_SUCCESS':
            const {newDate, newDateEvent} = action;
            return {
                ...state,
                selectedDate: newDate,
                selectedDateEvent: newDateEvent,
            };
        case 'UPDATE_SELECTED_DATE_EVENT_FAILED':
            const {updatedDate} = action;
            return {
                ...state,
                selectedDate: updatedDate,
            };
        default: {
            return state;
        }
    }
};
