import { ChallengeEventSummary } from "../challengeLobby/reducers";

export interface PersonalChallengeEventSummary extends ChallengeEventSummary{
    verification_times: number
}


export interface IJournalScreenState {
    userOngoingChallenges: PersonalChallengeEventSummary[],
    userPastChallenges: PersonalChallengeEventSummary[],
    eventDots: { [key: string]: any },
    calendarMode: boolean,
    selectedDate: string,
    selectedDateEvent: {[key: string]: any}[]
}


