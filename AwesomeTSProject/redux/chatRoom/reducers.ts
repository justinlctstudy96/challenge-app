import { ChatRoomActions, sendMessageSuccess } from './actions'

export interface Message {
    user_id: number;
    nickname: string
    pro_pic: string;
    content: string
    likes: number;
    created_at: string;
}

export interface ChatRoomState {
    eventId: number;
    messages: Message[]
}

const initialState: ChatRoomState = {
    eventId: 0,
    messages: [] as Message[]
}

export const chatRoomReducer = (
    state: ChatRoomState = initialState,
    action: ChatRoomActions,
): ChatRoomState => {
    switch (action.type){
        case '@@messages/GET_SUCCESS':
            return {
                ...state,
                messages: action.messages
            }
        case '@@newMessage/GET_SUCCESS':
            return {
                ...state,
                messages: state.messages.concat(action.message)
            }
        case '@@newMessage/POST_SUCCESS':
            return {
                ...state,
                messages: state.messages.concat(action.message)
            }
        default: {
            return state
        }
    }
}
