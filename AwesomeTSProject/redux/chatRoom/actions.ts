import {RootState} from '../../store';
import {Dispatch} from 'redux';
import {config} from '../config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Message} from './reducers';

// const io = require('socket.io-client/socket.io');
// const socket = io();
import SocketIOClient from 'socket.io-client';
// const socket = SocketIOClient(`${config.REACT_APP_BACKEND_URL}`)

import io from 'socket.io-client';
const socket = io(`${config.REACT_APP_BACKEND_URL}`, {
    transports: ['websocket'], // you need to explicitly tell it to use websockets
});

// socket.on('connection', () => {});

if (!window.location) {
    // App is running in simulator
    (window.navigator as any).userAgent = 'ReactNative';
} //////

export function getRoomMessagesSuccess(messages: Message[]) {
    return {
        type: '@@messages/GET_SUCCESS' as const,
        messages,
    };
}

export function getNewMessageSuccess(message: Message) {
    return {
        type: '@@newMessage/GET_SUCCESS' as const,
        message,
    };
}

export function sendMessageSuccess(message: Message) {
    return {
        type: '@@newMessage/POST_SUCCESS' as const,
        message,
    };
}

export type ChatRoomActions = ReturnType<
    | typeof getRoomMessagesSuccess
    | typeof getNewMessageSuccess
    | typeof sendMessageSuccess
>;

export function fetchRoomMessages(eventId: number) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        try {
            const res = await fetch(
                `${config.REACT_APP_BACKEND_URL}/messages/${eventId}`,
            );
            const messages = await res.json();
            if (res.status != 200) {
                console.log('failed');
            }
            dispatch(getRoomMessagesSuccess(messages));
        } catch (e) {}
    };
}

export function sendMessage(content: string, eventId: number) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        try {
            const token = await AsyncStorage.getItem('token');
            const res = await fetch(
                `${config.REACT_APP_BACKEND_URL}/message/${eventId}`,
                {
                    method: 'POST',
                    headers: {
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({content: content}),
                },
            );
            const newMessage = (await res.json()).messageDetails;
            if (res.status != 200) {
                return ''
            }
            dispatch(sendMessageSuccess(newMessage));
            socket.emit('joinRoom', eventId);
            socket.emit('chatMessage', newMessage);
        } catch (e) {
            console.log('error');
        }
    };
}
