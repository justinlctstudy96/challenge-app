import {RootState} from '../../store';
import {Dispatch} from 'redux';
import {config} from '../config';
import {ChallengeEventSummary} from '../challengeLobby/reducers';
import {UserEventRelation} from './reducers';
// import {AsyncStorage} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { fetchAllVerificationRecords } from '../roomStory/actions';

export function getEventDetailsSuccess(eventDetails: ChallengeEventSummary) {
    return {
        type: '@@event/GET_SUCCESS' as const,
        eventDetails,
    };
}

export function getEventDetailsFailed(error: string) {
    return {
        type: '@@event/GET_FAILED' as const,
        error,
    };
}

export function getUserEventRelationSuccess(relation: UserEventRelation) {
    return {
        type: '@@relation/GET_SUCCESS' as const,
        relation,
    };
}

export function getUserEventRelationFailed(error: string) {
    return {
        type: '@@relation/GET_FAILED' as const,
        error,
    };
}

export function joinEventSuccess(challengerId: number) {
    return {
        type: '@@challenger/POST_SUCCESS' as const,
        challengerId,
    };
}

export function joinEventFailed(error: string) {
    return {
        type: '@@challenger/POST_FAILED' as const,
        error,
    };
}

export function allDate(dateArr: []) {
    return {
        type: '@@allDate' as const,
        date: dateArr,
    };
}


export type ChallengeRoomScreenActions = ReturnType<
    | typeof getEventDetailsSuccess
    | typeof getEventDetailsFailed
    | typeof getUserEventRelationSuccess
    | typeof getUserEventRelationFailed
    | typeof joinEventSuccess
    | typeof joinEventFailed
>;

// thunk
export function fetchEventDetails(eventId: number) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        try {
            const token = await AsyncStorage.getItem('token')
            const res = await fetch(
                `${config.REACT_APP_BACKEND_URL}/event/${eventId}`,
                {
                    method: 'GET',
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            );
            const json = await res.json();
            if (res.status != 200) {
                return dispatch(getEventDetailsFailed(json.error));
            }
            const eventDetails: ChallengeEventSummary = json;
            dispatch(getEventDetailsSuccess(eventDetails));
        } catch (e) {
            dispatch(getEventDetailsFailed('Unknown error'));
        }
    };
}

export function fetchUserEventRelation(eventId: number) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        try {
            const token = await AsyncStorage.getItem('token');
            const res = await fetch(
                `${config.REACT_APP_BACKEND_URL}/relation/${eventId}`,
                {
                    method: 'GET',
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                },
            );
            const json = await res.json();
            if (res.status != 200) {
                return dispatch(getUserEventRelationFailed(json.error));
            }
            dispatch(
                getUserEventRelationSuccess({
                    challengerId: json.id,
                    role: json.role,
                    status: json.status,
                }),
            );
        } catch (e) {
        }
    };
}

export function joinEvent(eventId: number) {
    return async (dispatch: Dispatch<any>, getState: () => RootState) => {
        try {
            const token = await AsyncStorage.getItem('token');
            const res = await fetch(
                `${config.REACT_APP_BACKEND_URL}/event/${eventId}`,
                {
                    method: 'POST',
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                },
            );
            const json = await res.json();
            if (res.status != 200) {
                return dispatch(joinEventFailed(json.error));
            }
            dispatch(joinEventSuccess(json.challengerId));
            dispatch(fetchAllVerificationRecords(eventId));
        } catch (e) {}
    };
}

export function participantDetails(challengerId: number) {}

export function fetchChallengersDetails(eventId: number) {
    return async (dispatch: Dispatch, getState: () => RootState) => {
        try {
            const res = await fetch(
                `${config.REACT_APP_BACKEND_URL}/event/${eventId}`,
            );
            const challengers = await res.json;
        } catch (e) {}
    };
}

export function fetchAllDate(eventId: number) {
    return async (dispatch: Dispatch<any>) => {
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/details/startEnd/${eventId}`,
        );

        const result = await res.json();
        dispatch(allDate(result));
    };
}


