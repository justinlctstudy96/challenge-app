import {ChallengeRoomScreenActions} from './actions';
import {ChallengeEventSummary, emptyEvent} from '../challengeLobby/reducers';
import { VerificationReport } from '../roomStory/reducer';

export interface UserEventRelation {
    challengerId: number;
    role: string;
    status: string;
}

const emptyRelation = {
    challengerId: 0,
    role: '',
    status: '',
};

export interface ChallengeRoomScreenState {
    eventId: number;
    eventDetails: ChallengeEventSummary;
    userRole: UserEventRelation;
    reports: VerificationReport[]
}

const initialState: ChallengeRoomScreenState = {
    eventId: 0,
    eventDetails: emptyEvent,
    userRole: emptyRelation, // /'participant'/'host'
    reports: []
};

export const challengeRoomReducer = (
    state: ChallengeRoomScreenState = initialState,
    action: ChallengeRoomScreenActions,
): ChallengeRoomScreenState => {
    switch (action.type) {
        case '@@event/GET_SUCCESS':
            return {
                ...state,
                eventDetails: action.eventDetails,
            };
        case '@@event/GET_FAILED':
            return {
                ...state,
            };
        case '@@relation/GET_SUCCESS':
            return {
                ...state,
                userRole: action.relation,
            };
        case '@@challenger/POST_SUCCESS':
            return {
                ...state,
                userRole: {
                    challengerId: action.challengerId,
                    role: 'participant',
                    status: 'waiting',
                },
            };
        default: {
            return state;
        }
    }
};

export const allDateReducer = (state: [] = [], action: any) => {
    if (action.type === '@@allDate') {
        return {
            ...state,
            date: action.date,
        };
    }
    return state;
};
