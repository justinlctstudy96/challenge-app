import {ThunkDispatch } from '../store';
import { fetchCategories, fetchChallengeDetails, fetchTotalLikes, fetchUserLike, fetchVerifications } from "./challengeForm/actions";
import { fetchEventDots, fetchOngoingUserChallenges } from './challengeJournal/thunks';

export function fetchAllChallengeRelatedData(challengeId:number, userId:number|undefined) {
    return async(dispatch: ThunkDispatch) => {
        dispatch({type: 'LOADING_SPINNER_START'});
        await fetchCategories();
        await fetchVerifications();
        await fetchUserLike(challengeId);
        await fetchTotalLikes(challengeId)
        await fetchChallengeDetails(challengeId);
        await fetchOngoingUserChallenges();
        await fetchEventDots(userId);

        dispatch({type: 'LOADING_SPINNER_STOP'});
    }
}