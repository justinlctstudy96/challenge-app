import {camImgAction, pressBtnAction} from './actions';

import {Asset} from 'react-native-image-picker';

export interface ICamImg {
    camImg: string | Asset | undefined;
}

export interface IbtnStatus {
    btnStatus: boolean;
}

const initalState = {
    camImg: '',
};

const initialBtn = {
    btnStatus: false,
};

export function camImgReducer(
    state: ICamImg = initalState,
    action: camImgAction,
) {
    if (action.type === '@@camImg/getImg') {
        return {
            camImg: action.image,
        };
    }
    return state;
}

export function btnStatusReducer(
    state: IbtnStatus = initialBtn,
    action: pressBtnAction,
) {
    if (action.type === '@@pressBtn/changeStatus') {
        return {
            btnStatus: !action.status,
        };
    }
    return state;
}
