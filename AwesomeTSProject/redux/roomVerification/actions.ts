import {Dispatch} from 'react';
import {Alert} from 'react-native';
import {Asset} from 'react-native-image-picker';
import { fetchEventDetails } from '../challengeRoom/actions';
import {config} from '../config';
import {doPreventUpload, fetchAllVerificationRecords} from '../roomStory/actions';

export function camImg(image: Asset) {
    return {
        type: '@@camImg/getImg' as const,
        image: image.uri,
    };
}

export function pressBtn(status: boolean) {
    return {
        type: '@@pressBtn/changeStatus' as const,
        status: status,
    };
}

export function fetchLivePhoto(data: FormData, eventId: number) {
    return async (dispatch: Dispatch<any>) => {
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/verify/livePhoto`,
            {
                method: 'POST',
                mode: 'cors',
                headers: {
                    'content-type': 'multipart/form-data',
                },
                body: data,
            },
        );
        const result = await res.json();
        if (res.status === 200) {
            Alert.alert(result.message);
            dispatch(fetchAllVerificationRecords(eventId));
            dispatch(fetchEventDetails(eventId))
        } else {
            Alert.alert(result.message);
        }
    };
}

export function fetchHaveUpload(eventId: number, userId: number) {
    return async (dispatch: Dispatch<any>) => {
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/verify/check/${eventId}/${userId}`,
        );

        const result = await res.json();
        dispatch(doPreventUpload(result));
    };
}

export type camImgAction = ReturnType<typeof camImg>;
export type pressBtnAction = ReturnType<typeof pressBtn>;
