import {Dispatch} from 'react';
import {ReturnKeyType} from 'react-native';
import {config} from '../config';
import {
    CycleStoriesFlatList,
    IUploadStatus,
    VerificationRecord,
    ChallengerVerificationRecords,
    CycleVerificationRecords,
    CycleDate,
    ChallengeTimeDetails,
    VerificationReport
} from './reducer';
import AsyncStorage from '@react-native-async-storage/async-storage';


export function storyNum(id: number) {
    return {
        type: '@@StoryNum/getId' as const,
        id: id,
    };
}

export function doPreventUpload(userId: any) {
    return {
        type: '@@preventUpload' as const,

        userId: userId,
    };
}

export function getRoomUser(allUser: any) {
    return {
        type: '@@getRoomUser' as const,
        allUser,
    };
}

export function getAllStories(allStories: any) {
    return {
        type: '@@getAllStories' as const,
        allStories,
    };
}

export function getAllRecords(allRecords: any) {
    return {
        type: '@@getAllRecords' as const,
        allRecords,
    };
}

export function getInfo(detailInfo: any) {
    return {
        type: '@@getDetailInfo' as const,
        detailInfo,
    };
}

export type StoryNumAction = ReturnType<typeof storyNum>;
export type GetRoomUserAction = ReturnType<typeof getRoomUser>;
export type GetAllStoriesAction = ReturnType<typeof getAllStories>;
export type GetAllRecordsAction = ReturnType<typeof getAllRecords>;
export type GetAllInfoAction = ReturnType<typeof getInfo>;
export type PreventUploadAction = ReturnType<typeof doPreventUpload>;

export function fetchRoomUser(eventID: number) {
    return async (dispatch: Dispatch<any>) => {
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/story/users/${eventID}`,
        );
        const result = await res.json();
        dispatch(getRoomUser(result));
    };
}

export function fetchAllStories(eventID: number) {
    return async (dispatch: Dispatch<any>) => {
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/story/allContent/${eventID}`,
        );
        const result = await res.json();
        dispatch(getAllStories(result));
    };
}

export function fetchAllRecordsAccordingToDate(eventID: number) {
    return async (dispatch: Dispatch<any>) => {
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/details/allRecords/${eventID}`,
        );
        const result = await res.json();
        dispatch(getAllRecords(result));
    };
}

export function fetchAllInfo(eventID: number) {
    return async (dispatch: Dispatch<any>) => {
        const res = await fetch(
            `${config.REACT_APP_BACKEND_URL}/details/${eventID}`,
        );

        const result = await res.json();
        dispatch(getInfo(result));
    };
}

// Justin

export function getAllVerificationRecordsSuccess(
    allVerificationRecords: CycleVerificationRecords[],
) {
    // justin
    return {
        type: '@@verificationRecords/GET_SUCCESS' as const,
        allVerificationRecords,
    };
}

export function getCycleDatesSuccess(cycleDates: CycleDate[]) {
    return {
        type: '@@cycleDates/GET_SUCCESS' as const,
        cycleDates,
    };
}

export function getChallengeTimeDetailsSuccess(
    challengeTimeDetails: ChallengeTimeDetails,
) {
    return {
        type: '@@challengeTimeDetails/GET_SUCCESS' as const,
        challengeTimeDetails,
    };
}

export function getMultipleCyclesFlatListDataSuccess(
    multipleCyclesFlatListData: CycleStoriesFlatList[],
) {
    return {
        type: '@@multipleCyclesVerifications/GET_SUCCESS' as const,
        multipleCyclesFlatListData,
    };
}

export function getReportsSuccess(verificationReports: VerificationReport[]) {
    return {
        type: '@@verificationReports/GET_SUCCESS' as const,
        verificationReports
    }
}
export function postReportSuccess(verificationReport: VerificationReport) {
    return {
        type: '@@verificationReport/POST_SUCCESS' as const,
        verificationReport
    }
}

export type RoomVerificationRecordsActions = ReturnType<
    | typeof getAllVerificationRecordsSuccess
    | typeof getCycleDatesSuccess
    | typeof getChallengeTimeDetailsSuccess
    | typeof getMultipleCyclesFlatListDataSuccess
    | typeof getReportsSuccess
    | typeof postReportSuccess
>;

export function fetchAllVerificationRecords(eventId: number) {
    return async (dispatch: Dispatch<any>) => {
        try {
            const res = await fetch(
                `${config.REACT_APP_BACKEND_URL}/verification/allRecords/${eventId}`,
            );
            const allVerificationDetails = await res.json();
            const allVerificationRecords =
                allVerificationDetails.pastVerifications;
            const cycleDates = allVerificationDetails.cycleDates;
            const challengeTimeDetails =
                allVerificationDetails.challengeTimeDetails;
            dispatch(getAllVerificationRecordsSuccess(allVerificationRecords));
            dispatch(getCycleDatesSuccess(cycleDates));
            dispatch(getChallengeTimeDetailsSuccess(challengeTimeDetails));
            let multipleCyclesFlatListData = [] as CycleStoriesFlatList[];
            allVerificationRecords.map(
                (cycleVerificationDetails: CycleVerificationRecords) => {
                    let oneCycleFlatListData = {} as CycleStoriesFlatList;
                    oneCycleFlatListData.nth_cycle =
                        cycleVerificationDetails.nth_cycle;
                    oneCycleFlatListData.stories = [];
                    cycleVerificationDetails.challengersVerificationRecords.map(
                        (challenger: ChallengerVerificationRecords) => {
                            challenger.verificationRecords.map(
                                (verification: VerificationRecord) => {
                                    oneCycleFlatListData.stories.push(
                                        verification,
                                    );
                                },
                            );
                        },
                    );
                    multipleCyclesFlatListData.push(oneCycleFlatListData);
                },
            );
            dispatch(
                getMultipleCyclesFlatListDataSuccess(
                    multipleCyclesFlatListData.reverse(),
                ),
            );
        } catch (e) {
            console.log('error getting records');
        }
    };
}

export function fetchReports(eventId: number) {
    return async (dispatch: Dispatch<any>) => {
        try {
            const res = await fetch(`${config.REACT_APP_BACKEND_URL}/verification/reports/${eventId}`)
            const reports = await res.json()
            dispatch(getReportsSuccess(reports))
        } catch (e) {
            console.log('get reports failed')
        }
    }
}

export function postReport(verificationId: number, content: string) {
    return async (dispatch : Dispatch<any>) => {
        try {
            const token = await AsyncStorage.getItem('token');
            const res = await fetch(
                `${config.REACT_APP_BACKEND_URL}/verification/report/${verificationId}`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`
                    },
                    body: JSON.stringify({content:content})
                }
            )
            const reportDetails = await res.json();
            if (res.status != 200) {
                return 'failed'
            }
            dispatch(postReportSuccess(reportDetails));
        } catch (e) {
            console.log('failed')
        }
    }
}

export function acceptReport(eventId: number, verificationReportId: number) {
    return async (dispatch: Dispatch<any>) => {
        try {
            const res = await fetch(`${config.REACT_APP_BACKEND_URL}/verification/report/accept/${verificationReportId}`,{method:'DELETE'})
            if (res.status != 200) {
                return 'failed'
            }
            dispatch(fetchReports(eventId))
            dispatch(fetchAllVerificationRecords(eventId));
        } catch (e) {
            console.log('failed')
        }
    }
}

export function rejectReport(eventId: number, verificationReportId: number) {
    return async (dispatch: Dispatch<any>) => {
        try {
            const res = await fetch(`${config.REACT_APP_BACKEND_URL}/verification/report/reject/${verificationReportId}`,{method: 'DELETE'})
            if (res.status != 200) {
                return 'failed'
            }
            dispatch(fetchReports(eventId))
        } catch (e) {
            console.log('failed')
        }
    }
}