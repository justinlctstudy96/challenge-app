import { ActionSheetIOS } from 'react-native';
import {
    GetAllInfoAction,
    GetAllRecordsAction,
    GetAllStoriesAction,
    GetRoomUserAction,
    PreventUploadAction,
    StoryNumAction,
    RoomVerificationRecordsActions,
    getMultipleCyclesFlatListDataSuccess
} from './actions';

///////////justin

export interface VerificationRecord {
    nth_cycle: number;
    storyFlatListIndex: number;
    id: number;
    challenger_id: number;
    method_id: number;
    content: string;
    role: string;
    status: string;
    created_at: string;
    updated_at: string;
    submitted_at: string;
    nickname: string;
    pro_pic: string;
}

export interface ChallengerVerificationRecords { // records of one challenger
    challenger: ChallengerInfo;
    verificationRecords: VerificationRecord[]
}

export interface CycleVerificationRecords { // records of may challengers within a cycle
    nth_cycle: number;
    challengersVerificationRecords: ChallengerVerificationRecords[]
}

export interface ChallengerInfo {
    challenger_id: number;
    user_id: number;
    nickname: string;
    pro_pic: string;
}

export interface ChallengeTimeDetails {
    times: number;
    days: number;
    cycles: number;
    start_date: string
}

export interface CycleDate{
    start_date: string;
    end_date: string;
}

export interface VerificationReport {
    id: number
    verification_id: number;
    verification_content: string;
    verification_submitted_at: string
    challenger_id: number;
    challenger_name: number;
    challenger_pro_pic: string;
    reporter_id: number;
    // reporter_name: number;
    content: string;
    report_at: string
}

// largest summary object of the above 
export interface RoomVerificationRecordsState {
    allVerificationRecords: CycleVerificationRecords[]
    challengeTimeDetails: ChallengeTimeDetails
    cycleDates: CycleDate[]
    multipleCyclesVerifications: CycleStoriesFlatList[]
    allVerificationReports: VerificationReport[]
    // cyclesStories: 
}

const initialVerificationRecordsState: RoomVerificationRecordsState ={
    allVerificationRecords: [] as CycleVerificationRecords[],
    challengeTimeDetails: {} as ChallengeTimeDetails,
    cycleDates: [] as CycleDate[],
    multipleCyclesVerifications: [] as CycleStoriesFlatList[],
    allVerificationReports: [] as VerificationReport[]
}

// verification story scrolling flatlist data
export interface CycleStoriesFlatList {
    nth_cycle: number
    stories: VerificationRecord[]
}


// reducer

export const verificationRecordsReducer = (
    state: RoomVerificationRecordsState = initialVerificationRecordsState,
    action: RoomVerificationRecordsActions,
): RoomVerificationRecordsState => {
    switch (action.type) {
        case '@@verificationRecords/GET_SUCCESS':
            return {
                ...state,
                allVerificationRecords: action.allVerificationRecords
            }
        case '@@cycleDates/GET_SUCCESS':
            return {
                ...state,
                cycleDates: action.cycleDates
            }
        case '@@challengeTimeDetails/GET_SUCCESS':
            return {
                ...state,
                challengeTimeDetails: action.challengeTimeDetails
            }
        case '@@multipleCyclesVerifications/GET_SUCCESS':
            return {
                ...state,
                multipleCyclesVerifications: action.multipleCyclesFlatListData
            }
        case '@@verificationReports/GET_SUCCESS':
            return {
                ...state,
                allVerificationReports: action.verificationReports
            }
        case '@@verificationReport/POST_SUCCESS':
            return {
                ...state,
                allVerificationReports: state.allVerificationReports.concat(action.verificationReport)
                
            }
        default: {
            return state
        }
    }
}





/////////////
export interface IStoryNum {
    pressId: number;
}

export interface IdetailInfo {
    total: string;
    failed: string;
    verified: string;
    pendingForVerify: string;
    pendingUpload: number;
}

export interface IUploadStatus {
    userId: {};
}

const initialState = {
    pressId: 0,
};

const initialStateInfo = {
    total: '0',
    failed: '0',
    verified: '0',
    pendingForVerify: '0',
    pendingUpload: 0,
};

export function storyNumReducer(
    state: IStoryNum = initialState,
    action: StoryNumAction,
) {
    if (action.type === '@@StoryNum/getId') {
        return {
            pressId: action.id,
        };
    }
    return state;
}

const initialStateUpload = {
    already: false,
    userId: [],
};

export function preventUploadReducer(
    state: IUploadStatus = initialStateUpload,
    action: PreventUploadAction,
) {
    if (action.type === '@@preventUpload') {
        return {
            ...state,

            userId: action.userId,
        };
    }
    return state;
}

export function allUserReducer(state = [], action: GetRoomUserAction) {
    if (action.type === '@@getRoomUser') {
        return {
            ...state,
            allUser: action.allUser,
        };
    }
    return state;
}

export function allStoriesReducer(state = [], action: GetAllStoriesAction) {
    if (action.type === '@@getAllStories') {
        return {
            ...state,
            allStories: action.allStories,
        };
    }
    return state;
}

export function allRecordsReducer(state = [], action: GetAllRecordsAction) {
    if (action.type === '@@getAllRecords') {
        return {
            ...state,
            allRecords: action.allRecords,
        };
    }
    return state;
}

export function allInfoReducer(
    state: IdetailInfo = initialStateInfo,
    action: GetAllInfoAction,
) {
    if (action.type === '@@getDetailInfo') {
        return {
            ...state,
            detailInfo: action.detailInfo,
        };
    }
    return state;
}



//////////

