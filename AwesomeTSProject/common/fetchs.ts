
// import AsyncStorage from '@react-native-async-storage/async-storage';
// import {config} from '../redux/config';
// import { ThunkDispatch } from '../store';

// export async function authFetch(url:string,dispatch:ThunkDispatch){
//     const token = await AsyncStorage.getItem('token');
//     const res = await fetch(
//         `${config.REACT_APP_BACKEND_URL}${url}`,
//         {
//             headers: {
//                 Authorization: `Bearer ${token}`,
//             },
//         },
//     );
//     const result = await res.json();
//     if(res.status !== 200){
//         dispatch(showError(result.msg)); // <-- showError action creator
//     }

//     return [res,result];
// }