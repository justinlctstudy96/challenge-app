# Challenge app
Habit building community iOS app created with React Native, Node.js and PostgreSQL

## Technology
The Project is created with:
* React Native: 0.64.2
* Typescript: 3.8.3
* Node.js: v16.1.0
* Docker: 20.10.7

## Setup
To run this project, clone and install it locally using  yarn:

```
$ cd ../challenge-app/AwesomeTSProject
$ yarn install
$ cd ios
# pod install
```

After installing the required packages for frontend, you can build it on Xcode. 
For the to Backend server, run the docker file inside the folder Backend.

```
$ cd ../Backend
$ docker-compose up -d
```
## Created by
Justin Lo, Edward Wong and Peter Chan